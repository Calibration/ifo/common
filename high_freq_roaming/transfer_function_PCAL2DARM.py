#!/usr/bin/env python3

from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.timeseries import TimeSeries as ts
from gwpy.segments import DataQualityFlag
from gwpy.time import tconvert
from gwpy.time import to_gps
from gwpy.plot import BodePlot

import numpy as np
import os
import configparser
import argparse


def plot_crop(fs, f1, f2, title):
    fs2 = fs.crop(f1, f2)
    bp = BodePlot(fs2, dB=False, title=title)
    return bp

def generate_result_file_stub_from_freq(freq):
    return f"DARM_OVER_PCAL_{int(freq)}"

def generate_file_stub_from_gps_time(gps_start, gps_end, tag='notag'):
    fname = f"{tag}_{int(gps_start):d}_{int(gps_end):d}"
    return fname

def split_timeseries_segments_by_change(ts):
    '''split timeseries at each change.

    returns tuple of ts objects. each one will be cropped so that there are
    no changes contained within the timeseries.

    This should only be used on timeseries that are not expected to change
    often.
    '''

    ts_out = []
    if np.all(ts.value == ts[0].value):
        ts_out.append(ts)
    else:
        idx = np.where(ts.value != ts[0].value)[0][0]
        t_end = ts.span[1]
        t_change = ts.t0 + idx*ts.dt
        print(f'found ts change at {t_change}')
        ts_segment = ts.crop(ts.t0, t_change)
        ts_out.append(ts_segment)
        ts_out.extend(split_timeseries_segments_by_change(ts.crop(t_change, t_end)))

    return tuple(ts_out)

def add_record_to_file(fout, freq, tf_val, coh, navg, kappa_c, fcc,
                       gps_start, gps_end):
    dt_fmt = "%Y%m%dT%H%M%SZ"
    t_s = tconvert(gps_start).strftime(dt_fmt)
    t_e = tconvert(gps_end).strftime(dt_fmt)
    tf_mag = np.abs(tf_val)
    tf_pha = np.angle(tf_val)

    result = (f'{freq:0.4f}, {tf_mag:0.4f}, {tf_pha:0.4f}, '
              f'{coh:0.4f}, {navg:d}, '
              f'{kappa_c:0.4f}, {fcc:0.4f}, '
              f'{int(seg_start)}, {int(seg_end)}, '
              f'{t_s}, {t_e}')
    print(result)

    if not os.path.exists(fout):
        # create file and write header
        hdr = ("# Frequency (Hz), tf mag, tf phase (rad), coherence, "
               "# of averages, "
               "Kappa_C, Fcc, gps start, gps end, datetime start, datetime end")

        print(f'Creating output file: {fout}')

        with open(fout, 'w') as f:
            f.write(hdr + '\n')

    with open(fout, 'a+') as f:
        f.write(result + '\n')


if __name__ == "__main__":
    #############################################################################
    ##################### Program Command Line Options ##########################
    #############################################################################

    now = int(to_gps("now"))
    default_duration = 86400
    parser = argparse.ArgumentParser(description = __doc__)

    # Append program specific options
    parser.add_argument("--gps-start-time", metavar="seconds",
                        default=int(now - default_duration - 1800),
                        type=int,
                        help = "Set the start time of the segment to analyze in GPS "
                        "seconds. Defaults to now-24.5hrs.")
    parser.add_argument("--gps-end-time", metavar = "seconds",
                        default = int(now-1800),
                        type=int,
                        help = "Set the end time of the segment to analyze in GPS "
                        "seconds. Defaults to now-30min.")
    parser.add_argument("--config-file", metavar = "name",
                        help = "Full path to configuration file for running.")
    parser.add_argument('--output', '-o',
                        default='',
                        type=str,
                        help = "path to output directory")

    # Parse options
    args = parser.parse_args()

    gpsstarttime = args.gps_start_time
    gpsendtime = args.gps_end_time

    print(f"Processing GPS span: {gpsstarttime}-{gpsendtime}")
    # duration = float(gpsendtime) - float(gpsstarttime)

    config = configparser.ConfigParser()
    config.read(args.config_file)

    hfconfig = config["high-frequency-roaming-lines"]

    endstation = hfconfig.getboolean("Endstation")
    fftlength = hfconfig.getint("FFTLength")
    overlap = hfconfig.getint("Overlap")
    window = hfconfig.get("Window")
    cadence = hfconfig.getfloat("Cadence")
    min_coh = hfconfig.getfloat("MinCoh")
    max_coh = hfconfig.getfloat("MaxCoh")
    freq_array = list(map(float, hfconfig.get("FreqArray").split(',')))
    min_time_array = list(map(int, hfconfig.get("MinTimeArray").split(',')))
    tag = hfconfig.get("tag")
    dqflag = hfconfig.get("DQFlag")
    segs_save_path = hfconfig.get("SaveSegsPath")
    data_save_path = hfconfig.get("SaveDataPath")
    save_data_bool = hfconfig.getboolean("SaveData")
    save_segs_bool = hfconfig.getboolean("SaveSegs")
    verbose = hfconfig.getboolean("Verbose")

    if args.output:
        outputdir = args.output
    else:
        outputdir = hfconfig.get("OutputDir")

    IFO = config["interferometer"].get('name')

    # channel names
    darm_in1_ch = f'{IFO}:{hfconfig["Strain"]}'
    pcal_rxpd_ch = f'{IFO}:{hfconfig["Pcal"]}'
    osc_freq_ch = f"{IFO}:{hfconfig['OscillatorFrequencies']}"
    kappa_c_ch = f"{IFO}:{hfconfig['kappac']}"
    fcc_ch = f"{IFO}:{hfconfig['fcc']}"
    data_chan_list = [darm_in1_ch, pcal_rxpd_ch]
    kappa_chan_list = [kappa_c_ch, fcc_ch]

    def get_data(chan, gps_start, gps_end):

        if type(chan) is str:
            data_module = ts
            stub_tag = chan.replace(':', '_')
        else:
            data_module = tsd
            stub_tag = 'dict'

        stub = generate_file_stub_from_gps_time(gps_start, gps_end,
                                                tag=stub_tag)
        fname = os.path.join(data_save_path, f'data_{stub}.h5')
        if os.path.exists(fname):
            print(f"reading saved ts data: {fname}")
            try:
                data = data_module.read(fname, start=gps_start, end=gps_end)
            except:
                print(f'ERROR: unable to read {fname}.')
        else:
            print(f'fetching data {chan} {gps_start}-{gps_end}',
                  flush=True)
            a = to_gps(tconvert())
            data = data_module.find(chan, gps_start, gps_end,
                                   nproc=8,
                                   verbose=True,
                                   allow_tape=True)
            b = to_gps(tconvert())
            print(f'..finished fetching in {b-a}s.')
            if save_data_bool:
                print(f'writing to disk: {fname}', end='', flush=True)
                a = to_gps(tconvert())
                data.write(fname)
                b = to_gps(tconvert())
                print(f'..finished writing in {b-a}s.')
        return data

    def rescale_pcal2darm_kappas(tf_raw, freq, kappa_c_t, fcc_t,
                                 kappa_c_ref, fcc_ref):
        """Rescale PCAL2DARM transfer function evaluated at time t to
        reference time t0 by
        applying the appropriate corrections based on kappa_c(t), fcc(t),
        kappa_c(t0), and fcc(t0). The reference values, assumed to
        represent the 'kappa' values at t0, are kappa_c_ref and fcc_ref.

        Parameters
        ==========
        tf_raw : complex
            Pcal2DARM transfer function value at frequency `freq`.
        freq : float
            Frequency at which tf_raw was evaluated.
        kappa_c_t : float

        fcc_t : float

        kappa_c_ref : float

        fcc_ref : float

        Returns
        =======
        tf_scaled :
        """
        corr_factor = (kappa_c_ref / kappa_c_t) \
            * (1+1J*freq/fcc_t) / (1+1J*freq/fcc_ref)

        tf_scaled = tf_raw * corr_factor
        return tf_scaled

    def get_min_time_from_freq(freq):
        default_min_time = 3600  # seconds
        # min times are stored in hours in ini file
        min_times = np.array(min_time_array)[np.isclose(freq, freq_array)]
        if not np.any(min_times):
            print(f"Warning: OSC frequency {freq:0.4f} is not in the list of "
                  f"recognized OSC frequencies: {freq_array}. Defaulting to "
                  f"a minimum integration time of {default_min_time}s.")
            min_time = default_min_time
        else:
            min_time = min_times[0] * 3600

        return min_time


    if verbose:
        print("Gathering the data segments")
    seg_fname_stub = generate_file_stub_from_gps_time(gpsstarttime, gpsendtime)
    seg_fname = f"dqsegs_{seg_fname_stub}.h5"
    seg_path = os.path.join(segs_save_path, seg_fname)
    if os.path.exists(seg_path):
        if verbose:
            print(f"Reading saved data segs file {seg_path}")
        segs = DataQualityFlag.read(seg_path, dqflag)
    else:
        segs = DataQualityFlag.query(dqflag,
                                     float(gpsstarttime),
                                     float(gpsendtime),
                                     verbose=True)
        if save_segs_bool:
            segs.write(seg_path)
    if verbose:
        print("Finished reading data quality segments.")

    # Loop over times where the specified data quality flag is active
    # and identify time segments for which to calculate pcal/darm tf
    #
    # Here we will build the segment list.
    osc_freq_segments = []
    for seg in segs.active:
        # Check whether Pcal Oscillator frequency changes during this segment
        print(f"Getting osc_freq data for active segment, {seg[0]}-{seg[1]} "
              f"dur: {int(seg[1]-seg[0]):d}s")
        osc_freq = get_data(osc_freq_ch, int(seg[0]), int(seg[1]))
        # import pdb;pdb.set_trace()
        freq_segments = split_timeseries_segments_by_change(osc_freq)
        for freq_seg in freq_segments:
            # add segment to list to be processed if duration is larger
            # than one fft length
            seg_start = freq_seg.span[0]
            seg_end = freq_seg.span[1]
            seg_duration = seg_end - seg_start
            if seg_duration < fftlength:
                f = float(freq_seg[0].value)
                print(f"Segment {f:0.2f} Hz, {seg_start}-{seg_end} "
                      f"{seg_duration}s is shorter than "
                      f"required fft length of {fftlength}s. Skipping...\n\n\n")
                continue
            osc_freq_segments.append(freq_seg)

    n_segs = len(osc_freq_segments)
    print('Processing the following indivdual segments:')
    print(f'Number of segments: {n_segs}')
    for s in osc_freq_segments:
        print(f"GPS Segment: {s.span[0]}-{s.span[1]}, "
              f"dur: {int(s.span[1]-s.span[0]):d}s")
        print(s)
    for iseg, fseg in enumerate(osc_freq_segments):
        seg_start = fseg.span[0]
        seg_end = fseg.span[1]
        seg_dur = int(seg_end - seg_start)

        # Since segment has been built, we can safely assume the first sample
        # is representative of this segment's osc frequency
        freq = fseg[0].value

        min_time = get_min_time_from_freq(freq)
        print(f"Minimum integration time for {freq:0.4f} Hz is "
              f"{min_time} seconds.")

        # check that minimum integration time is met by this segment
        if seg_dur < min_time:
            print(f"Current segment {int(seg_start)}-{int(seg_end)} "
                  f"({seg_dur}s) is less than the minimum "
                  f"integration time of {min_time}s at {freq:0.4f} Hz. "
                  "Skipping segment.")
            continue

        # check that we have at least fftlength data
        if seg_dur < fftlength:
            print(f"Current segment duration ({seg_dur}s) is less than "
                  f"the requested fftlength ({fftlength}s). Skipping.")
            continue

        kappa_data = get_data(kappa_chan_list, int(seg_start), int(seg_end))

        kappa_c_full = kappa_data[kappa_c_ch]
        kappa_fcc_full = kappa_data[fcc_ch]

        n_subsegs = int(seg_dur / cadence)
        print("####################")
        print(f"Processing tf at {freq:0.2f}Hz for segment {iseg+1} of {n_segs}.")
        print(f"The current segment is {seg_dur}s long.")
        print(f"With cadence={cadence}s, we expect {n_subsegs} sub-segments.")

        print("This is also the number of 'averages' for the current segment.")
        # calculate mean kappas for this entire segment
        # We will scale each sub-integration's tf value to the full segment's
        # kappaC and fcc values.
        kappa_c_ref = kappa_c_full.mean()
        kappa_fcc_ref = kappa_fcc_full.mean()
        print(f'Segment mean Kappa C: {kappa_c_ref}')
        print(f'Segment mean Fcc: {kappa_fcc_ref}')

        # define the initial window over which to compute and average a batch of
        # FFTs for the CSDs and PSDs.
        fft_start = float(seg_start)
        fft_end = float(seg_start + cadence)

        # loop over current segment, creating fft windows of length `cadence`
        cohs = []
        tfs = []

        # we keep track of the number of 'averages' or 'samples'.
        # this should be the same as the total number of sub-segments.
        navg = 0
        while fft_end <= float(seg_end):
            navg += 1
            print(f"\nWorking on sub-segment {navg} of {n_subsegs}.")

            data = get_data(data_chan_list, fft_start, fft_end)
            darm_in1 = data[darm_in1_ch].astype(np.float64)
            pcal_rxpd = data[pcal_rxpd_ch].astype(np.float64)
            kappa_c_t = kappa_c_full.crop(fft_start, fft_end, copy=False).mean()
            kappa_fcc_t = kappa_fcc_full.crop(fft_start, fft_end, copy=False).mean()

            print(f"sub-segment KappaC: {kappa_c_t}")
            print(f"sub-segment Fcc: {kappa_fcc_t}")

            print("Performing calculations: ")
            print(f"data span: {fft_start}-{fft_end}, {data.span[1]-data.span[0]}s"
                  f" fftlength: {fftlength}")

            tf = pcal_rxpd.transfer_function(darm_in1, fftlength=fftlength,
                                             overlap=overlap, window=window)
            tf_f = np.interp(freq, tf.frequencies.value, tf.value)
            tf_f_scaled = rescale_pcal2darm_kappas(tf_f, freq, kappa_c_t,
                                                   kappa_fcc_t, kappa_c_ref,
                                                   kappa_fcc_ref)

            coh = pcal_rxpd.coherence(darm_in1, fftlength=fftlength,
                                      overlap=overlap, window=window)

            print(f'tf (raw) {navg}/{n_subsegs}: {tf_f}')
            print(f'tf (scaled) {navg}/{n_subsegs}: {tf_f_scaled}')
            df = 0.05
            coh_crop = coh.crop(freq-df, freq+df).value
            coh_crop[coh_crop> 1.0] = 1.0 # this renders max_coh useless when >1
            coh_high = coh_crop[coh_crop > min_coh]

            # take the mean of coh values near freq of interest
            print(f"Coh near f ({freq-df}Hz - {freq+df}Hz):")
            print(f"{coh_high}")
            coh_f = np.mean(coh_high)

            tfs.append(tf_f_scaled)
            cohs.append(coh_f)

            fft_start = fft_end
            fft_end = fft_start + cadence


        del data
        # average coh values calculated over entire segment
        cohs = np.array(cohs)
        print(f'coherence array: {cohs}')
        coh = cohs[~np.isnan(cohs)].mean()

        # get average tf value for entire segment
        tfs = np.array(tfs)

        tf_f = tfs[~np.isnan(tfs)].mean()

        # ofname = generate_result_file_stub_from_freq(freq) + '.txt'
        ofname = generate_result_file_stub_from_freq(freq) + f'_{int(seg_start)}' \
            + f'_{int(seg_end)}'+ '.txt'
        add_record_to_file(os.path.join(outputdir, ofname), freq, tf_f,
                           coh, navg, kappa_c_ref, kappa_fcc_ref,
                           seg_start, seg_end
                           )
