#!/usr/bin/env sh

# make segments for O4a chunk2

PROG=$CAL_IFO_COMMON/high_freq_roaming/make_segments_file.py

$PYTHON_EXEC $PROG -o o4a_chunk2_segments.txt "2023-10-01 00:00:00 UTC" "2024-01-16 16:00:00 UTC"
