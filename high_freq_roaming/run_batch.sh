#!/usr/bin/env sh

source ~cal/opt/pydarm-dev/bin/activate
# this "set -a" specifies that the variables sourced from the config
# are marked for export
set -a
source ~cal/config
set +a

config="/home/cal/archive/$IFO/ifo/transfer_function_HF_$IFO.ini"
script="/home/cal/archive/common/high_freq_roaming/transfer_function_PCAL2DARM.py"


# argument is path to segments file
num=$(cat $1 | wc -l)
ii=0
while IFS=' ' read -r start end
do
    ((ii++))
    echo "line $ii of $num: $start - $end"
    python $script --config-file $config --gps-start-time $start --gps-end-time $end
done <$1
