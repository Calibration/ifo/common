import subprocess
from gpstime import gpstime
from datetime import timedelta

if __name__ == '__main__':
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('startdate', type=str, help='start date time (UTC)')
    p.add_argument('enddate', type=str, help='end date time (UTC)')
    p.add_argument('-o', '--output', type=str,
                   default='batch_segments.txt',
                   help='path to output file')
    args = p.parse_args()

    dt_start = gpstime.parse(args.startdate)
    dt_end = gpstime.parse(args.enddate)
    outfile = args.output

    td = dt_end - dt_start

    run_segments = []
    for i in range(td.days):
        run_gps_start = dt_start + timedelta(days=i)
        run_gps_stop = dt_start + timedelta(days=i+1)
        run_segments.append((run_gps_start, run_gps_stop))

    with open(outfile, 'w') as f:
        for i, rs in enumerate(run_segments):
            start = int(rs[0].gps())
            end = int(rs[1].gps())

            f.write(f"{start} {end}\n")
