#!/usr/bin/env python3

import os
from glob import glob

header = "# Frequency (Hz), tf mag, tf phase (rad), coherence, # of averages, Kappa_C, Fcc, gps start, gps end, datetime start, datetime end"

def freq_from_filename(fname):
    fbase = os.path.basename(fname)
    fstub, _ = os.path.splitext(fbase)
    ffreq = fstub.split('_')[3]
    return ffreq

def fdat_from_file(fname):
    '''get first data row from file

    ignore commented header and subsequent entries
    '''
    with open(fname, 'r') as f:
        d = f.readlines()

    if len(d) > 1:
        o = d[1]
    else:
        o = None

    return o


if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('-i', '--indir',
                   type=str,
                   help='path to input batch directory')
    p.add_argument('-o', '--outdir',
                   type=str,
                   help='path to output directory')

    args = p.parse_args()
    indir = args.indir
    outdir = args.outdir

    flist = sorted(glob(os.path.join(indir, '*.txt')))

    dout = {}

    for f in flist:
        freq = freq_from_filename(f)
        if freq not in dout.keys():
            dout[freq] = []

        dnew = fdat_from_file(f)
        if dnew not in dout[freq]:
            dout[freq].append(dnew)

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    for freq, dlines in dout.items():
        fnout = os.path.join(args.outdir, f'DARM_OVER_PCAL_{freq}.txt')
        if os.path.exists(fnout):
            write_hdr = False
            print(f'Appending data: {fnout}')
        else:
            write_hdr = True
            print(f'Writing data: {fnout}')

        with open(fnout, 'a') as fout:
            if write_hdr:
                fout.write(header + '\n')
            for dl in dlines:
                fout.write(dl)
            print(f'Wrote {len(dlines)} records for {freq} Hz.')
