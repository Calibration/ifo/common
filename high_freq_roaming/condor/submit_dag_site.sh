#!/usr/bin/env sh

ROBOT_ROLE=cal_O4_H1_hfrl-scitoken
CREDKEY=cal_O4_H1_hfrl-scitoken/robot/cdsvmscript1.cds.ligo-wa.caltech.edu

kinit  cal_O4_H1_hfrl-scitoken/robot/cdsvmscript1.cds.ligo-wa.caltech.edu@LIGO.ORG -k -t /home/louis.dartez/robot/cal_O4_H1_hfrl-scitoken_robot_cdsvmscript1.cds.ligo-wa.caltech.edu.keytab
export HTGETTOKENOPTS="--role $ROBOT_ROLE --credkey $CREDKEY"
condor_vault_storer igwn

condor_submit_dag -force -include_env HTGETTOKENOPTS $1
