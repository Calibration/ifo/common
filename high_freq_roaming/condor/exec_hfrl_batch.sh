#!/usr/bin/env sh

set -a
source /home/cal/config
set +a

CAL_HOME=/home/cal
PROG=/home/louis.dartez/ifo/common/high_freq_roaming/transfer_function_PCAL2DARM.py
OUTDIR=/home/louis.dartez/O4/O4a/part2/condor/hfrl_batch_condor_results
CONFIGFILE=$CAL_ROOT/ifo/pydarm_$IFO.ini


exec $CAL_HOME/conda/pydarm/bin/python $PROG -o $OUTDIR --config-file $CONFIGFILE --gps-start-time $1 --gps-end-time $2
