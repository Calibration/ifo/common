#!/usr/bin/env sh

IFO_REPO_PATH=/home/louis.dartez/repos/ifo

set -a
source $IFO_REPO_PATH/common/high_freq_roaming/condor/$1_config
set +a

ENV_PATH=/home/louis.dartez/conda/pydarm
PROG=$IFO_REPO_PATH/common/high_freq_roaming/transfer_function_PCAL2DARM.py
OUT_DIR=/home/louis.dartez/calibration_unc/O4/a/chunk2/condor/$IFO/hfrl_batch_condor_results
CONFIG_FILE=$IFO_REPO_PATH/$IFO/pydarm_$IFO.ini

export LIGO_DATAFIND_SERVER=datafind.ldas.cit:80
export GWDATAFIND_SERVER=datafind.ldas.cit:80

exec $ENV_PATH/bin/python $PROG -o $OUT_DIR --config-file $CONFIG_FILE --gps-start-time $2 --gps-end-time $3
