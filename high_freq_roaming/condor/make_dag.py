#!/usr/bin/env python

import os

IFO = os.getenv('IFO')
assert IFO is not None

CAL_CONDOR_DIR = os.getenv('CAL_CONDOR_DIR')
assert CAL_CONDOR_DIR is not None

DEFAULT_SUBMIT_FILE = os.path.join(CAL_CONDOR_DIR, 'hfrl_dag.sub')
DEFAULT_DAG_FILE = f"jobs_{IFO}.dag"


if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('-s', '--segfile',
                   type=str,
                   help='path to segments file to process')
    p.add_argument('-b', '--submit',
                   type=str,
                   default=DEFAULT_SUBMIT_FILE,
                   help='path to condor submit file. '
                   f'default is {DEFAULT_SUBMIT_FILE}')
    p.add_argument('-o', '--output',
                   type=str,
                   default=DEFAULT_DAG_FILE,
                   help='path to output dag file. default is '
                   f"{DEFAULT_DAG_FILE}")
    cli_args = p.parse_args()
    segfile = cli_args.segfile
    subfile = cli_args.submit
    outfile = cli_args.output


    arg_list = []
    with open(segfile, 'r') as f:
        for l in f.readlines():
            l = list(int(x) for x in l.strip('\n').split())
            arg_list.append(l)

    childline = "PARENT 0 CHILD "
    with open(outfile, 'w') as f:
        f.write('\n')
        for i, args in enumerate(arg_list):
            jline = f"JOB\t{i} {subfile}\n"
            vline = f"VARS\t{i} jobid=\"{i}\" ifo=\"{IFO}\" start=\"{args[0]}\" end=\"{args[1]}\"\n"
            rline = f"RETRY\t{i} 1\n"

            f.write(jline)
            f.write(vline)
            f.write(rline)
            f.write('\n')

            if i > 0:
                childline += f"{i} "

        f.write(childline+'\n')
