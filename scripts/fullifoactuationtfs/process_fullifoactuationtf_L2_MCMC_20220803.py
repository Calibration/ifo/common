from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
"""
Copied originally from ipython notebook that coming soon via merge request:
[2] https://git.ligo.org/evan-goetz/pydarm/-/blob/update-doc/examples/pydarm_example.ipynb
Took that, and successfull produced an answer, then started coping over all the information extraction stuff from
actuation_MCMC method in
[3] https://svn.ligo.caltech.edu/svn/aligocalibration/trunk/Common/pyDARM/src/actuation.py
    """

cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

run = 'O3'
IFO = 'H1'
optic = 'ETMX'
actuatorStage = 'L2'
measDate = '2022-08-03'
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')

verbose = True
printFigs = True

COHTHRESH_SUSEXCTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_SUSEXCTF # for now

# Default is 1e3, 9e3. That takes ~20 minutes on Jeff's laptop. No thanks.
MCMC_PARAMS_BURNINSTEPS = 100;
MCMC_PARAMS_STEPS = 900;
MCMC_PARAMS_FITREGION_HZ = [20,400]

# These are the xml files we want to get our data from.
susexctf_filename = \
   '{}/Runs/{}/{}/Measurements/FullIFOActuationTFs/{}_{}SUS{}_{}_iEXC2DARM_12min.xml'\
   .format(cal_data_root,\
        run,\
        IFO,\
        measDate,\
        IFO,\
        optic,\
        actuatorStage)
pcaltf_filename = \
    '{}/Runs/{}/{}/Measurements/FullIFOActuationTFs/{}_{}SUS{}_{}_PCAL2DARM_6min.xml'\
    .format(cal_data_root,\
        run,\
        IFO,\
        measDate,\
        IFO,\
        optic,\
        actuatorStage)
  
# New model file for this measurement set,
# Updated optical gain to gt80Hz fit value for optical gain from LHO aLOG 63405; no other change to C.
# Also, in A, switched DARM actuator back to All EX.
# Verified and validated all EX digital settings.
# model_parameters_file = \
#     cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20220527.ini'
model_parameters_file = \
    cal_ifo_root + 'pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'

modelDate = model_parameters_file.split('.')[0][-8:]

resultsDir = \
    cal_data_root+'/Runs/'+run+'/'+IFO+'/Results/FullIFOActuationTFs/'

anythingExtraInTheFigTag = ''

figTag = \
    'actuationFunction_{}_{}_{}_proc{}_model{}_meas{}_MCMCfitfreqrange{}-{}Hz{}'\
    .format(IFO,\
        optic,\
        actuatorStage,\
        todayDate,\
        modelDate,\
        measDate.replace('-',''),\
        '{:.1f}'.format(MCMC_PARAMS_FITREGION_HZ[0]).replace('.','p'),\
        '{:.1f}'.format(MCMC_PARAMS_FITREGION_HZ[1]).replace('.','p'),\
        anythingExtraInTheFigTag)

# Create measurement object and extract raw tfs for verification
susexctf_obj = pydarm.measurement.Measurement(susexctf_filename)
pcaltf_obj  = pydarm.measurement.Measurement(pcaltf_filename)

darmolgtf_freq, darmolgtf_tf,  darmolgtf_coh, darmolgtf_unc = \
    susexctf_obj.get_raw_tf(\
        '{}:SUS-{}_{}_CAL_EXC'.format(IFO,optic,actuatorStage),\
        '{}:LSC-DARM_IN1_DQ'.format(IFO),\
        COHTHRESH_SUSEXCTF)
pcaltf_freq, pcaltf_tf,  pcaltf_coh, pcaltf_unc = \
    pcaltf_obj.get_raw_tf(\
        '{}:CAL-{}_PD_OUT_DQ'.format(IFO,refPCAL),\
        '{}:LSC-DARM_IN1_DQ'.format(IFO),\
        COHTHRESH_PCALTF)

# Process measurement suite, extract A_actuatorStage, and fit for actuation strength and delay
processed_actuator_obj = pydarm.measurement.ProcessActuationMeasurement(\
    model_parameters_file,\
    'actuation_{}_arm'.format(optic[-1].lower()),\
    susexctf_obj,\
    pcaltf_obj,\
    ('{}:SUS-{}_{}_CAL_EXC'.format(IFO,optic,actuatorStage), '{}:LSC-DARM_IN1_DQ'.format(IFO)),\
    ('{}:CAL-{}_PD_OUT_DQ'.format(IFO,refPCAL), '{}:LSC-DARM_IN1_DQ'.format(IFO)),\
    COHTHRESH_SUSEXCTF,\
    COHTHRESH_PCALTF)
    
frequencies, processed_actuator_tf, processed_actuator_unc = \
    processed_actuator_obj.get_processed_measurement_response()
    
mcmc_posterior_chain_obj = processed_actuator_obj.run_mcmc(fmin=MCMC_PARAMS_FITREGION_HZ[0],\
                                                           fmax=MCMC_PARAMS_FITREGION_HZ[1],\
                                                           burn_in_steps=MCMC_PARAMS_BURNINSTEPS,\
                                                           steps=MCMC_PARAMS_STEPS)
                                                   
# Unlike O3 pyDARM, MAP returns N/A or N/V^2 rather than N/ct!!
MAP = np.median(mcmc_posterior_chain_obj, axis=0)

#### END PRIMARY COMPUTATION

#### START "INFORMATION EXTRACTION AND VALIDATION FROM COMPUTATION"

# Build the model:
C = pydarm.sensing.SensingModel(model_parameters_file)
A = pydarm.actuation.DARMActuationModel(model_parameters_file)
darm = pydarm.darm.DARMModel(model_parameters_file)

# In order to be generic and allow for both arms as actuators,
# A has methods for xarm and yarm. But, if I'm only using one arm's
# actuator, then I can reduce the complexitity (and simplify later
# code) by extract the layer of method I need and naming it generically
if optic == 'ETMX':
    modelA_obj = A.xarm
else:
    modelA_obj = A.yarm

if actuatorStage=='L1':
    model_ActuatorStrengthParam = abs(modelA_obj.uim_npa)
    model_Npct = abs(modelA_obj.uim_dc_gain_Npct())
    model_ActuatorStrengthParam_Scale_to_Npct = abs(modelA_obj.uim_dc_gain_Apct())
    actuatorStrengthParam_units = '[N/A]'
elif actuatorStage=='L2':
    model_ActuatorStrengthParam = abs(modelA_obj.pum_npa)
    model_Npct = abs(modelA_obj.pum_dc_gain_Npct())
    model_ActuatorStrengthParam_Scale_to_Npct = abs(modelA_obj.pum_dc_gain_Apct())
    actuatorStrengthParam_units = '[N/A]'
else:
    model_ActuatorStrengthParam = abs(modelA_obj.tst_npv2)
    model_Npct = abs(modelA_obj.tst_dc_gain_Npct())
    model_ActuatorStrengthParam_Scale_to_Npct = abs(modelA_obj.tst_dc_gain_V2pct())
    actuatorStrengthParam_units = '[N/V**2]'
    
model_actuatorstrength_tf = model_ActuatorStrengthParam * np.ones(frequencies.shape)

# Quantify the MCMC output in terms of 68% ci parameter values
ciQuantiles = np.array([0.16, 0.5, 0.84])

quantileValues = np.zeros((len(MAP),3))
for nMAP in range(0,len(MAP)):
    quantileValues[nMAP,:] = corner.quantile(mcmc_posterior_chain_obj[:,nMAP], ciQuantiles)
    quantileValues[nMAP,2] = quantileValues[nMAP,2]-quantileValues[nMAP,1];
    quantileValues[nMAP,0] = quantileValues[nMAP,1]-quantileValues[nMAP,0];
    
# Quantile values, including the MAP take the output of the MCMC.
# The output of the MCMC is un paramGain_units, *not* N/ct.
LB_actuatorStrength = quantileValues[0,0]
MAP_actuatorStrength = quantileValues[0,1]
UB_actuatorStrength = quantileValues[0,2]

LB_residualTimeDelay_usec = quantileValues[1,0] * 1e6
MAP_residualTimeDelay_usec = quantileValues[1,1] * 1e6
UB_residualTimeDelay_usec = quantileValues[1,2] * 1e6

LB_actuatorStrength_Npct = LB_actuatorStrength * model_ActuatorStrengthParam_Scale_to_Npct
MAP_actuatorStrength_Npct = MAP_actuatorStrength * model_ActuatorStrengthParam_Scale_to_Npct
UB_actuatorStrength_Npct = UB_actuatorStrength * model_ActuatorStrengthParam_Scale_to_Npct

# The MAP output transfer function of the MCMC is un paramGain_units, *not* N/ct.
MAP_actuatorstrength_tf = MAP_actuatorStrength * np.exp(-2.0*np.pi*1j*MAP_residualTimeDelay_usec*1e-6*frequencies)

# Change from previous model
MAP_kappa_A = MAP_actuatorStrength_Npct/model_Npct

#### SPIT ANSWER OUT TO COMMAND LINE FOR COPY AND PASTE TO ALOG

print(' ')
print('The Force Coefficient "answer" in terms of ... ')
print('    Force per count of longitudinal DAC request (just down stream of DRIVEALIGN bank):')
print('    Gain = {:4.4g} [N/ct]'.format(MAP_actuatorStrength_Npct))
print('    Force per amount of actuator signal in its physical units (current or voltage):')


print('    Gain = {:4.4g} {}'.format(MAP_actuatorStrength,actuatorStrengthParam_units))
    
print('------------------------------- OR ----------------------------------')
print('Actuation Strength, H_A {}, {}              | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'\
    .format(\
    actuatorStrengthParam_units,
    actuatorStage,\
    MAP_actuatorStrength,\
    UB_actuatorStrength,\
    LB_actuatorStrength,\
    UB_actuatorStrength/MAP_actuatorStrength*100,\
    LB_actuatorStrength/MAP_actuatorStrength*100))
print('Actuation Strength, H_A {}, [N/ct]                | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'\
    .format(\
        actuatorStage,\
        MAP_actuatorStrength_Npct,\
        UB_actuatorStrength_Npct,\
        LB_actuatorStrength_Npct,\
        UB_actuatorStrength_Npct/MAP_actuatorStrength_Npct*100,\
        LB_actuatorStrength_Npct/MAP_actuatorStrength_Npct*100))

print(r'Residual Time Delay, \Delta \tau_A [usec], {}     | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'\
    .format(\
        actuatorStage,\
        MAP_residualTimeDelay_usec,\
        UB_residualTimeDelay_usec,\
        LB_residualTimeDelay_usec,\
        abs(UB_residualTimeDelay_usec/MAP_residualTimeDelay_usec)*100,\
        abs(LB_residualTimeDelay_usec/MAP_residualTimeDelay_usec)*100))

print('')
print('TDCF Parameter | Value')
print('---------------------------')
print(\
    'kappa_A_{} = MCMC_MAP_{}/Model_{} = {:1.6g}/{:1.6g} = {:1.6g}'\
        .format(\
            actuatorStage,\
            actuatorStrengthParam_units,\
            actuatorStrengthParam_units, \
            MAP_actuatorStrength, \
            model_ActuatorStrengthParam, \
            MAP_kappa_A))
print('')


#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

expscale = int(np.floor(np.log10(MAP_actuatorStrength)))

plotFreqRange = [4, 2e3]

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)
s1.plot(frequencies, abs(MAP_actuatorstrength_tf)/10**expscale, label='MCMC Fit')
s1.plot(frequencies, abs(model_actuatorstrength_tf)/10**expscale,label='2021 Model (H_A = {:.3f}e{} {}) '.format(model_ActuatorStrengthParam/10**expscale,expscale,actuatorStrengthParam_units))
s1.errorbar(frequencies, abs(processed_actuator_tf)/10**expscale, yerr=processed_actuator_unc*abs(processed_actuator_tf)/10**expscale, fmt='.', label='Measurement / (A / H_A_model)')
s1.axvline(MCMC_PARAMS_FITREGION_HZ[0], color='k',ls='--',label='Fit Range = [{:.1f}, {:.1f}] Hz'.format(MCMC_PARAMS_FITREGION_HZ[0],MCMC_PARAMS_FITREGION_HZ[1]))
s1.axvline(MCMC_PARAMS_FITREGION_HZ[1], color='k',ls='--')
s1.set_ylabel('Magnitude {} x 10$^{{{}}}$'.format(actuatorStrengthParam_units,expscale),usetex=True)
s1.set_xscale('log')
s1.set_yscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(1,1e3)
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend(ncol=1)
s1.set_title(\
'$H_A = {{{:.4g}}}_{{-{:.3g}}}^{{+{:.3g}}}$ {}'\
    .format(\
        MAP_actuatorStrength,\
        LB_actuatorStrength,\
        UB_actuatorStrength,\
        actuatorStrengthParam_units),
    usetex=True)

s2.errorbar(frequencies, np.angle(MAP_actuatorstrength_tf, deg=True))
s2.plot(frequencies, np.angle(model_actuatorstrength_tf,deg=True))
s2.errorbar(frequencies, np.angle(processed_actuator_tf, deg=True), yerr=processed_actuator_unc*180.0/np.pi, fmt='.',  label='meas.')
s2.axvline(MCMC_PARAMS_FITREGION_HZ[0], color='k',ls='--',label='Fit Range = [{:.1f}, {:.1f}] Hz'.format(MCMC_PARAMS_FITREGION_HZ[0],MCMC_PARAMS_FITREGION_HZ[1]))
s2.axvline(MCMC_PARAMS_FITREGION_HZ[1], color='k',ls='--')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-180,180)
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
s2.set_title(\
    '$H_A = {{{:.4g}}}_{{-{:.3g}}}^{{+{:.3g}}}$ [N/ct]'\
        .format(\
            MAP_actuatorStrength_Npct,\
            LB_actuatorStrength_Npct,\
            UB_actuatorStrength_Npct),
        usetex=True)

s3.errorbar(frequencies, abs(processed_actuator_tf/MAP_actuatorstrength_tf), yerr=processed_actuator_unc, fmt='.',label='meas. / MCMC')
s3.errorbar(frequencies, abs(processed_actuator_tf/model_actuatorstrength_tf), yerr=processed_actuator_unc, fmt='.', label='2021 Model')
s3.set_ylabel('|meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.85,1.15])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.01))
s3.set_title(\
'$\kappa_A$ = (MCMC / model) = ({{{:.4g}}} / {{{:.4g}}}) = {{{:1.6g}}} [ ]'\
    .format(\
        MAP_actuatorStrength,\
        model_ActuatorStrengthParam,\
        MAP_kappa_A),
    usetex=True)

s4.errorbar(frequencies, np.angle(processed_actuator_tf/MAP_actuatorstrength_tf, deg=True), yerr=processed_actuator_unc*180.0/np.pi, fmt='.')
s4.errorbar(frequencies, np.angle(processed_actuator_tf/model_actuatorstrength_tf, deg=True), yerr=processed_actuator_unc*180.0/np.pi, fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase diff. (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,10])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
s4.set_title(\
'$\Delta \\tau_A = {{{:.4g}}}_{{-{:.3g}}}^{{+{:.3g}}}$ [usec]'\
    .format(\
        MAP_residualTimeDelay_usec,\
        LB_residualTimeDelay_usec,\
        UB_residualTimeDelay_usec),
    usetex=True)

plt.suptitle('{}SUS{} {} Actuation Strength Measurement: {}'.format(IFO,optic,actuatorStage, measDate),fontsize='x-large')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_mcmcModel_vs_measurement.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Meas. Plot stored at {resultsDir+figTag}_mcmcModel_vs_measurement.pdf")


## Corner plot of MCMC results
histBins = 100 # Set delibrately to have each bins contain 1% of the PDF #FIXME -- this doesn't really work any more. Ethan?
tickFontSize = 10
titleFontSize = 15
shadingColor = 'C3'
theLegend = ['$1\\sigma$','$2\\sigma$','$3\\sigma$', 'MAP', '1D PDF bins: {}'.format(histBins)]
MAP_variableNames = [r'$H_A$ {}'.format(actuatorStrengthParam_units), r'$\Delta \tau_A$ [usec]']

# See discussion in https://corner.readthedocs.io/en/latest/pages/sigmas.html
               # 1-sigma,              2-sigma,               3-sigma
ci_levels = (1 - np.exp(-1. / 2.), 1 - np.exp(- 4. / 2.), 1 - np.exp(-9 / 2.))

cp = corner.corner(mcmc_posterior_chain_obj,
                   bins = histBins,
                   quantiles = [0.16, 0.84],
                   smooth = 2.0,
                   labels = MAP_variableNames,
                   legend_labels = theLegend,
                   verbose = False,
                   label_kwargs = {'fontsize': tickFontSize},
                   show_titles = True,
                   title_fmt = '.4g',
                   title_kwargs = {'fontsize': tickFontSize},
                   plot_datapoints = False,
                   plot_density = False,
                   fill_contours = True,
                   color = shadingColor,
                   hist_kwargs = {'density':True}, # Was 'normed':True, but normed is a depricated parameter for histograms in matplotlib... I think https://github.com/dirac-institute/CMNN_Photoz_Estimator/pull/17' found a workaround
                   use_math_text = True,
                   plot_contours = True,
                   levels = ci_levels,
                   max_n_ticks = 5,
                   truths = MAP,
                   truth_color = 'C0')
length_sides = np.arange(0,len(MAP),1)
histogram_indexes = (len(MAP)+1)*length_sides
N_samples = len(mcmc_posterior_chain_obj)

for idx, ax in enumerate(cp.get_axes()): # Loop through all the axes and set the fontsize of the tick parameters
    ax.grid(True)

    ax.yaxis.set_major_formatter(tck.ScalarFormatter())
    ax.xaxis.set_major_formatter(tck.ScalarFormatter())
    ax.ticklabel_format(axis='x',style='sci', useMathText=True, scilimits=(0,0))
    ax.ticklabel_format(axis='y',style='plain', useMathText=True, scilimits=(0,0))
    ax.tick_params(axis='both', labelsize=tickFontSize)
    ax.yaxis.offsetText.set_fontsize(tickFontSize)
    ax.xaxis.offsetText.set_fontsize(tickFontSize)

    if idx in histogram_indexes:
        ax2 = ax.twinx()
        ax2.tick_params(axis='both', labelsize=tickFontSize)
        ax2.ticklabel_format(axis='both', style='sci', useMathText=True, scilimits=(0, 0))
        ax2.yaxis.offsetText.set_fontsize(tickFontSize)
        ax2.xaxis.offsetText.set_fontsize(tickFontSize)
        ax2.set_ylim((ax.get_ylim()[0]/N_samples*100, ax.get_ylim()[1]/N_samples*100))
        ax2.set_xlim(ax.get_xlim())
        
        ax.yaxis.set_major_locator(plt.LinearLocator(5))
        ax2.yaxis.set_major_locator(plt.LinearLocator(5))
        
        ax2.yaxis.set_major_formatter(plt.FormatStrFormatter(':.1g'))
        ax2.set_ylabel('1D Norm. PDF \n (Percent per bin)',fontsize=tickFontSize)
        
        tick_labels = np.array(np.round(np.array(ax2.get_yticks(),dtype=float),2),dtype=str)
        tick_labels[0] = ''
        ax2.set_yticklabels(tick_labels)
    
    # Handling for if the subplot is not on the far-left side
    if idx not in len(MAP)*length_sides or idx == 0:
        ax.set_yticklabels([])
        ax.yaxis.offsetText.set_visible(False)
    if idx not in length_sides + len(MAP)*(len(MAP)-1):
        ax.set_xticklabels([])
        ax.xaxis.offsetText.set_visible(False)

# Generating the legend
alphas = np.ones(len(ci_levels)+1)
for i, l in enumerate(ci_levels):
        alphas[i] *= float(i) / (len(ci_levels)+1)
alphas = alphas[1:][::-1]

legend_symbols = [mplpatches(facecolor=shadingColor, edgecolor=shadingColor, label=theLegend[i], alpha=alphas[i]) for i in range(len(ci_levels))]
legend_symbols.append(box([0],[0], color='C9', lw=2))
legend_symbols.append(mplpatches(facecolor='C0', edgecolor='C0', label=theLegend[-1], alpha=0))

legend = cp.legend(\
    legend_symbols,\
    theLegend,\
    fontsize=tickFontSize,\
    title='2D PDF Contours',\
    loc='upper right',\
    bbox_to_anchor=(1.0, 0.8))
plt.setp(legend.get_title(),fontsize=tickFontSize)

cp.suptitle(\
    '{}SUS{} {} Actuation Strength Measurement\n MCMC Corner Plot: {}'\
        .format(\
            IFO,\
            optic,\
            actuatorStage,\
            measDate)\
    ,fontsize=titleFontSize)
cp.subplots_adjust(top=0.85, wspace=0.20, hspace=0.20)

thisfig = fignames(resultsDir+figTag+'_mcmcModel_paramCornerPlot.pdf')
allfigures.append(thisfig)
if printFigs:
    cp.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Saved corner plot at {resultsDir+figTag}_mcmcModel_paramCornerPlot.pdf")
