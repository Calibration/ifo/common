import numpy as np
import os
import pydarm
import argparse
from scipy import signal

# Point to where all the calibration files live
os.environ['CAL_DATA_ROOT'] = '/home/cal/svncommon/aligocalibration/trunk/'
directory = 'results'

#  ============================ Setting up the argparser ===============================
parser = argparse.ArgumentParser()

# Required arguments

parser.add_argument("--model_file", help="Model file")
parser.add_argument("--meas_type", help='Specify x or y arm actuation')
parser.add_argument("--stage", help="Actuation stage being considered")
parser.add_argument("--excitation", help="XML file for stage excitation", type=pydarm.measurement.Measurement)
parser.add_argument("--pcal_to_darm", help="XML file for PCAL to DARM measurement", type=pydarm.measurement.Measurement)
parser.add_argument("--excitation_channels", help="Channels for the CG measurement", nargs=2)
parser.add_argument("--pcal_to_darm_channels", help="Channels for the PCAL to DARM measurement", nargs=2)

# Optional arguments
parser.add_argument("--fmin", help="Minimum MCMC frequency", type=int, default=30)
parser.add_argument("--fmax", help="Maximimum MCMC frequency", type=int, default=800)
parser.add_argument("--burn_in_steps", help="Steps for burn in", type=int, default=200)
parser.add_argument("--steps", help="Steps for MCMC sampling", type=int, default=200)

args = parser.parse_args()

# getting the measurement date
date = args.excitation.filename.split('/')[-1][:10]

cohThresh = 0.9

# Running & saving the MCMC
meas = pydarm.measurement.ProcessActuationMeasurement(args.model_file, args.meas_type, args.excitation, args.pcal_to_darm,
                                                    args.excitation_channels, args.pcal_to_darm_channels,
                                                    meas1_cohThresh=cohThresh, meas2_cohThresh=cohThresh)

filename = f'{date}_{args.stage}_MCMC.json'
filename_chain = f'{date}_{args.stage}_MCMC_chain.hdf5'

chain = meas.run_mcmc(fmin=args.fmin, fmax=args.fmax, burn_in_steps=args.burn_in_steps, steps=args.steps, 
                      save_map_to_file=f'{directory}/{filename}', save_chain_to_file=f'{directory}/{filename_chain}')
MAP = np.median(chain, axis=0)
print(f'Actuation {args.stage} MCMC MAP values: {MAP}')

#  ============================ Plots ===============================
    
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
mpl.rcParams.update({'text.usetex': True,
                     'axes.linewidth': 1,
                     'axes.grid': True,
                     'axes.labelweight': 'normal',
                     'font.family': 'DejaVu Sans',
                     'font.size': 20})
from matplotlib import ticker

frange_plot = [5, 2000]
frequencies, tf, unc = meas.get_processed_measurement_response()

ref_tf = MAP[0] * np.exp(-2j*np.pi*frequencies*MAP[1]) 

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(15, 12))
ax0, ax1 = axes.flat

ax0.plot(frequencies, np.abs(ref_tf), label='MAP', color='magenta')
ax0.errorbar(frequencies,np.abs(tf), marker='o', markersize=10, 
             linestyle='', yerr=unc*np.abs(tf), label='Meas.')
ax0.set_xlim(frange_plot)
ax0.legend(loc='lower center', ncol=2, handlelength=2)
ax0.set_ylabel(r'Mag (N/ct)')

ax1.plot(frequencies, np.angle(ref_tf, deg=True), label='MAP', color='magenta')
ax1.errorbar(frequencies,np.angle(tf, deg=True),marker='o', markersize=10, linestyle='', yerr=unc*180.0/np.pi)
ax1.set_xlim(frange_plot)
ax1.set_xlabel(r'Frequency (Hz)')
ax1.set_ylabel(r'Phase (deg)')

for ax in axes.flat:
    ax.grid(which='major',color='black')
    ax.grid(which='minor',ls='--')
    ax.set_xscale('log')
    ax.axvline(args.fmin,ls='--',color='C06')
    ax.axvline(args.fmax,ls='--',color='C06')

plt.savefig(f'{directory}/{date}_{args.stage}_MCMC_plot.pdf', bbox_inches='tight', pad_inches=0.2)
