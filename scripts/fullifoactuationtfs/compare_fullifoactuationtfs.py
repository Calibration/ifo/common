from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner
import os
import sys
from pathlib import Path
plt.ion()

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

run = 'O3'
IFO = 'H1'
optic = "ETMX"
# actuatorStage = "L3"
refPCAL = 'PCALY_RX' 
todayDate =  dt.today().strftime('%Y%m%d')
model_parameters_file = cal_ifo_root + 'pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'

results_dir_default = '{}/Runs/{}/{}/Results/FullIFOActuationTFs/' \
    .format(cal_data_root, run, IFO)

measDate = ['2022-05-27',
            '2022-08-03',
            '2022-11-23']

if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('-m', '--modelfile', type=str,
                   default=model_parameters_file,
                   help=f'pyDARM model parameter file. Default is {model_parameters_file}')
    p.add_argument('-s', '--stage', choices=['L1','L2','L3'], required=True,
                   help='Actuation Stage to process. Choices are L1, L2, L3.')
    p.add_argument('-v', '--verbose', action='store_true',
                   help="Verbose output")
    p.add_argument('--printFigs', action='store_true',
                   help="Save output figures.")
    p.add_argument('-o', '--outdir', default=results_dir_default,
                   help=f"Output directory. Default is {results_dir_default}")
    args = p.parse_args()
    verbose = args.verbose
    printFigs = args.printFigs
    results_dir = args.outdir
    actuatorStage = args.stage

    COHTHRESH_DARMOLGTF = 0.9
    COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

    # extract date from model filename
    modelDate = model_parameters_file.split('.')[0][-8:]  # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*


    anythingExtraInTheFigTag = ''

    figTag ='actuationFunction_comparison_{}_{}_proc{}_model{}_meas{}-{}'.format(IFO,
        actuatorStage,
        todayDate,
        modelDate,
        measDate[0].replace('-', ''),
        measDate[-1].replace('-', ''),
        anythingExtraInTheFigTag)

    plotFreqRange = [4, 2e3]
    modelfrequencies = np.logspace(0,4,250)

    # Build the model:
    # Make sure the model parameter set exists first
    if not os.path.exists(model_parameters_file):
        print('The model parameter file')
        print('    {}'.format(model_parameters_file))
        print('does not exist. Check the file name, path, and whether path is up to date.')
        sys.exit()
    C = pydarm.sensing.SensingModel(model_parameters_file)
    A = pydarm.actuation.DARMActuationModel(model_parameters_file)
    darm = pydarm.darm.DARMModel(model_parameters_file)

    A_arm = A.xarm if optic=='ETMX' else A.yarm

    if actuatorStage=='L1':
        model_ActuatorStrengthParam = abs(A_arm.uim_npa)
        model_Npct = abs(A_arm.uim_dc_gain_Npct())
        model_ActuatorStrengthParam_Scale_to_Npct = abs(A_arm.uim_dc_gain_Apct())
        actuatorStrengthParam_units = '[N/A]'
    elif actuatorStage=='L2':
        model_ActuatorStrengthParam = abs(A_arm.pum_npa)
        model_Npct = abs(A_arm.pum_dc_gain_Npct())
        model_ActuatorStrengthParam_Scale_to_Npct = abs(A_arm.pum_dc_gain_Apct())
        actuatorStrengthParam_units = '[N/A]'
    else:
        model_ActuatorStrengthParam = abs(A_arm.tst_npv2)
        model_Npct = abs(A_arm.tst_dc_gain_Npct())
        model_ActuatorStrengthParam_Scale_to_Npct = abs(A_arm.tst_dc_gain_V2pct())
        actuatorStrengthParam_units = '[N/V**2]'
    # END BUILDING MODEL

    fignames = namedtuple('fignames',['filename'])
    results = namedtuple('results', ['modelDate','measDate','frequencies',
        'processed_response_tf', 'processed_unc',
        'residual_tf','residual_unc'])

    allresults = []
    allfigures = []
    meas_filedir = \
        Path('{}/Runs/{}/{}/Measurements/FullIFOActuationTFs/' \
             .format(cal_data_root, run, IFO))
    for iDate, thisDate in enumerate(measDate):
        try:
            # These are the xml files we want to get our data from.
            exc_glob_str = f"{thisDate}_{IFO}SUS{optic}_{actuatorStage}_iEXC2DARM*.xml"
            susexctf_filename = str(list(meas_filedir.glob(exc_glob_str))[0].resolve())
            pcal_glob_str = f"{thisDate}_{IFO}SUS{optic}_{actuatorStage}_PCAL2DARM*.xml"
            pcaltf_filename = str(list(meas_filedir.glob(pcal_glob_str))[0].resolve())

            susexctf_obj = pydarm.measurement.Measurement(susexctf_filename)
            pcaltf_obj  = pydarm.measurement.Measurement(pcaltf_filename)
        except IOError as e:
            err_msg = f"Unable to find files for {thisDate}: {e.filename}"

        # Create an actuation function measurement object
        process_actuator_obj = pydarm.measurement.ProcessActuationMeasurement(
            model_parameters_file, 'actuation_{}_arm'.format(optic[-1].lower()),
            susexctf_obj, pcaltf_obj,
            (f'{IFO}:SUS-{optic}_{actuatorStage}_CAL_EXC', f'{IFO}:LSC-DARM_IN1_DQ'),
            (f'{IFO}:CAL-{refPCAL}_PD_OUT_DQ', f'{IFO}:LSC-DARM_IN1_DQ'),
            COHTHRESH_DARMOLGTF, COHTHRESH_PCALTF)

        frequencies, processed_actuator_response_tf, processed_actuator_unc = \
            process_actuator_obj.get_processed_measurement_response()

        residual_tf = processed_actuator_response_tf / model_ActuatorStrengthParam

        allresults.append(results(modelDate, thisDate, frequencies,
                                  processed_actuator_response_tf,
                                  processed_actuator_unc, residual_tf,
                                  processed_actuator_unc))

    expscale = int(np.floor(np.log10(model_ActuatorStrengthParam)))

    fig = plt.figure()
    s1 = fig.add_subplot(221)
    s2 = fig.add_subplot(223)
    s3 = fig.add_subplot(222)
    s4 = fig.add_subplot(224)

    #s1.plot(modelfrequencies, model_ActuatorStrengthParam,label='{} Model'.format(modelDate),color='magenta')
    s1.axhline(y=model_ActuatorStrengthParam, label=f'{modelDate} Model', color='magenta')
    for iMeas, thisMeas in enumerate(measDate):
        s1.errorbar(allresults[iMeas].frequencies,
                    abs(allresults[iMeas].processed_response_tf),
                    yerr=allresults[iMeas].processed_unc*abs(allresults[iMeas].processed_response_tf),
                     fmt='.', label='{}'.format(thisMeas))
    s1.set_ylabel('Magnitude (ct/m)'.format(0),usetex=True)
    s1.set_xscale('log')
    #s1.set_yscale('log')
    s1.grid(which='major',color='black')
    s1.grid(which='minor', ls='--')
    s1.set_xlim(plotFreqRange)
    #s1.xaxis.set_major_locator(tck.LogLocator(base=10))
    #s1.set_ylim(0.5,10)
    #s1.yaxis.set_major_locator(tck.LogLocator(base=10))
    s1.legend(ncol=1)


    #s2.plot(modelfrequencies, np.angle(model_ActuatorStrengthParam,deg=True),color='magenta')
    s2.axhline(np.angle(model_ActuatorStrengthParam, deg=True), color='magenta')
    for iMeas, thisMeas in enumerate(measDate):
        s2.errorbar(allresults[iMeas].frequencies,
                    np.angle(allresults[iMeas].processed_response_tf, deg=True),
                    yerr=allresults[iMeas].processed_unc*(180.0/np.pi),
                    fmt='.')
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Phase (deg)')
    s2.set_xscale('log')
    s2.grid(which='major',color='black')
    s2.grid(which='minor', ls='--')
    s2.set_xlim(plotFreqRange)
    s2.xaxis.set_major_locator(tck.LogLocator(base=10))
    #s2.set_ylim(-135,45)
    s2.set_ylim(-180,180)
    s2.yaxis.set_major_locator(tck.MultipleLocator(45))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(15))

    for iMeas, thisMeas in enumerate(measDate):
        s3.errorbar(allresults[iMeas].frequencies,
                    abs(allresults[iMeas].residual_tf),
                    yerr=allresults[iMeas].residual_unc, fmt='.',
                    label='{} / {} Model'.format(thisMeas,modelDate))
    s3.set_ylabel('|meas./model|')
    s3.set_xscale('log')
    s3.legend(ncol=1)
    s3.grid(which='major',color='black')
    s3.grid(which='minor',ls='--')
    s3.set_xlim(plotFreqRange)
    s3.xaxis.set_major_locator(tck.LogLocator(base=10))
    s3.set_ylim([0.90,1.10])
    s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))

    for iMeas, thisMeas in enumerate(measDate):
        s4.errorbar(allresults[iMeas].frequencies,
                    np.angle(allresults[iMeas].residual_tf, deg=True),
                    yerr=allresults[iMeas].residual_unc*(180.0/np.pi),
                    fmt='.')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('Phase diff. (meas./model)')
    s4.set_xscale('log')
    s4.grid(which='major',color='black')
    s4.grid(which='minor',ls='--')
    s4.set_xlim(plotFreqRange)
    s4.xaxis.set_major_locator(tck.LogLocator(base=10))
    s4.set_ylim([-10,10])
    s4.yaxis.set_major_locator(tck.MultipleLocator(2))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

    suptitle = f'{IFO}SUS{optic} {actuatorStage} Actuation Strength Reference Model Params: '
    suptitle += f'$H_A=${model_ActuatorStrengthParam:.3e} {actuatorStrengthParam_units}'
    plt.suptitle(suptitle)

    plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    if printFigs:
        thisfig = fignames(results_dir+figTag+'.pdf')
        allfigures.append(thisfig)
        plt.savefig(thisfig.filename,bbox_inches='tight')
        print(f'Results saved as {thisfig.filename}.')



