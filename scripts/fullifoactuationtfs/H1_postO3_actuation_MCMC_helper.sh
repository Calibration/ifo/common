python3 generate_MCMC_results_actuation.py --meas_type actuation_x_arm --model_file params/pydarm_modelparams_PostO3_H1_20221118.ini --stage TST \
 --excitation /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L3_iEXC2DARM_12min.xml \
 --pcal_to_darm /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L3_PCAL2DARM_6min.xml \
 --excitation_channels H1:SUS-ETMX_L3_CAL_EXC H1:LSC-DARM_IN1_DQ --pcal_to_darm_channels H1:CAL-PCALY_RX_PD_OUT_DQ H1:LSC-DARM_IN1_DQ --fmin 10 --fmax 800 --burn_in_steps 100 --steps 900

python3 generate_MCMC_results_actuation.py --meas_type actuation_x_arm --model_file params/pydarm_modelparams_PostO3_H1_20221118.ini --stage PUM \
 --excitation /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L2_iEXC2DARM_12min.xml \
 --pcal_to_darm /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L2_PCAL2DARM_6min.xml \
 --excitation_channels H1:SUS-ETMX_L2_CAL_EXC H1:LSC-DARM_IN1_DQ --pcal_to_darm_channels H1:CAL-PCALY_RX_PD_OUT_DQ H1:LSC-DARM_IN1_DQ --fmin 10 --fmax 500 --burn_in_steps 100 --steps 900

python3 generate_MCMC_results_actuation.py --meas_type actuation_x_arm --model_file params/pydarm_modelparams_PostO3_H1_20221118.ini --stage UIM \
 --excitation /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L1_iEXC2DARM_25min.xml \
 --pcal_to_darm /home/cal/svncommon/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOActuationTFs/$1_H1SUSETMX_L1_PCAL2DARM_8min.xml \
 --excitation_channels H1:SUS-ETMX_L1_CAL_EXC H1:LSC-DARM_IN1_DQ --pcal_to_darm_channels H1:CAL-PCALY_RX_PD_OUT_DQ H1:LSC-DARM_IN1_DQ --fmin 10 --fmax 50 --burn_in_steps 100 --steps 900
