from collections import namedtuple
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
import scipy.signal as sig
import numpy as np
#import IIRrational.v2
import wavestate.iirrational
import os
import sys
import h5py


#Making the plots look nicer!

plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (20,18),
                    'savefig.dpi': 100,
                    'pdf.compression': 9})


SVNROOT = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'
dataDir = SVNROOT+'Common/Electronics/H1/DCPDWhitening/OMCA/S2101608/20220215/Data/'
#resultsDir = SVNROOT+'Common/Electronics/H1/DCPDWhitening/OMCA/S2101608/20220215/Results/'
resultsDir = "/ligo/home/louis.dartez/projects/omc_whitening_fits/"
meas = "20220215_H1_OMCA_DCPDWhitening_S2101608_"

#Reading in the data for measurement set up

freq, meassetup_mag_VpV = np.loadtxt(dataDir+meas+'MeasSetup_mag.TXT', delimiter='\t',usecols=(0,1),unpack=True)
freq, meassetup_pha_deg = np.loadtxt(dataDir+meas+'MeasSetup_pha.TXT', delimiter='\t',usecols=(0,1),unpack=True)

tf_meassetup = meassetup_mag_VpV * np.exp(1j*np.pi/180.0*meassetup_pha_deg)

DCPDName = ['CH1_DCPDA', 'CH2_DCPDB']
gain = ['HiGain', 'LoGain']

LoadResistor = 100000 # in ohm from https://dcc.ligo.org/DocDB/0172/D2000592/003/D2000592-v3.pdf
#fitOrder = 7

izeros = [20 ,20, 10000]
ipoles = [100, 100, 8000, 8000]# 157000]


results = namedtuple('results',['OMC_DCPD','WCgain','freq','meas','fit','residual','zeros','poles','gain','fotonstring'])
fignames = namedtuple('fignames',['fname'])


summary = []
allresults = []
allfigures = []
for i in DCPDName: 
         for j in gain:
                  fname = dataDir+meas+i+'_'+j
                  #fname = dataDir+'2022-02-15_OMCDCPDTIA_OMCA_' + i # Reading in the data
                  freq, mag_VpV = np.loadtxt(fname+'_mag.TXT',delimiter='\t',usecols=(0,1),unpack=True)
                  freq, phase_deg = np.loadtxt(fname+'_pha.TXT',delimiter='\t',usecols=(0,1),unpack=True)
                  mag_VpA = mag_VpV*LoadResistor
                  tf_meas = mag_VpA*np.exp(1j*np.pi/180.0*phase_deg)/tf_meassetup
                  selectVector = (freq > 0.1) & (freq < 102.4e3)
                  delayMax=1e-3
                  snrWeight = abs(100/(1+1j*freq/5000)**1.2)
                  #the fit
                  fit=wavestate.iirrational.v2.data2filter(
                           data=tf_meas.reshape(-1),
                           F_Hz=freq.reshape(-1),
                           F_nyquist_Hz=None,
                          # order = fitOrder,
                           delay_s_max=delayMax, # sec
                           select = selectVector,
                           mode = 'fit',
                           poles = -1*np.array(ipoles),
                           zeros = -1*np.array(izeros),
                           SNR = snrWeight,
                           coding_map = wavestate.iirrational.fitters_ZPK.codings_s.coding_maps.nlFBW)
                  tf_fit = fit.fitter.xfer_eval(freq)
                  fotonString = fit.as_foton_str_ZPKsf()
                  residual = tf_meas / tf_fit
                  residual_mag = abs(tf_meas / tf_fit)
                  residual_pha = phase_deg - 180/np.pi*np.angle(tf_fit)
                  fitZeros = -1 * np.array(fit.as_ZPKrep().zeros.fullplane)
                  fitPoles = -1 * np.array(fit.as_ZPKrep().poles.fullplane)
                  fitGain  = np.array(fit.as_ZPKrep().gain)
                  print('DCPD:%s; Gain: %s'%(i,j))
                  print('Fit Zeros: {} Hz'.format(np.array2string(fitZeros,precision=3)))
                  print('Fit Poles: {} Hz'.format(np.array2string(fitPoles,precision=3)))
                  print('Fit Gain: {}'.format(np.array2string(fitGain,precision=3)))
                  #Plotting the fits and the residuals
                  fig = plt.figure()
                  s1 = fig.add_subplot(221)
                  s2 = fig.add_subplot(223)
                  s3 = fig.add_subplot(222)
                  s4 = fig.add_subplot(224)
                  s1.loglog(freq,mag_VpA,label='Data')
                  s1.loglog(freq,abs(tf_fit),'--',label='Fit')
                  s1.set_ylabel('Magnitude (V/A)')
                  s1.grid(which='minor', ls='--')
                  s1.legend(ncol=2)
                  s1.set_title(meas+"OMCA_%s_%s"%(i,j))
                  s2.semilogx(freq,phase_deg,label='Data')
                  s2.semilogx(freq,180.0/np.pi*np.angle(tf_fit),'--',label='Fit')
                  s2.set_xlabel('Frequency (Hz)')
                  s2.set_ylabel('Phase (deg)')
                  s2.grid(which='minor', ls='--')
                  s2.set_title('z:{}'.format(np.array2string(np.sort_complex(fitZeros),precision=2)))
                  s3.semilogx(freq,residual_mag)
                  #s3.set_xlim(1e-1, 1.1e4)
                  s3.set_ylim(0.995,1.005)
                  s3.yaxis.set_major_locator(tck.MultipleLocator(0.0025))
                  s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
                  s3.grid(which='minor',ls='--')
                  s3.set_ylabel('abs(Data / Fit)')
                  s4.semilogx(freq,residual_pha)
                  s4.set_title('p:{}'.format(np.array2string(np.sort_complex(fitPoles),precision=2)))
                  s4.grid(which='minor',ls='--')
                  s4.set_ylabel('angle(Data / Fit) (deg)')
                  s4.set_xlabel('Frequency (Hz)')
                  s4.set_ylim([-1.0,1.0])
                  s4.yaxis.set_major_locator(tck.MultipleLocator(0.25))
                  s4.yaxis.set_minor_locator(tck.MultipleLocator(0.05))
                  #Saving the figure
                  thisfig = fignames(resultsDir+meas+i+j+"_FitResult.pdf")
                  allfigures.append(thisfig);
                  plt.savefig(thisfig.fname,bbox_inches='tight')
                  plt.close()
                  #Appending the data
                  thisresult = results(i,j,freq,tf_meas,tf_fit,residual,fitZeros,fitPoles,fitGain,fotonString)
                  allresults.append(thisresult)
                  summary.append(thisresult)
                  #Saving the measurement data
                  hdf5FileName = resultsDir+meas+ i + "_"+j+"_measData.hdf5"
                  with h5py.File(hdf5FileName, 'w') as f:
                           f.create_dataset('freq', data=freq)
                           f.create_dataset('tf_meas', data=tf_meas)

