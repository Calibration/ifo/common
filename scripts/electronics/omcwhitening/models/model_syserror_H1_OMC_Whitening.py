# ########### #
# ENVIRONMENT #
# ########### #
import pydarm
import numpy as np
from collections import namedtuple
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
import scipy.signal as signal
import numpy as np
import h5py
from cycler import cycler

#making the plots nice

plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (20,14),
                    'savefig.dpi': 100,
                    'pdf.compression': 9})


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array-value)).argmin()
    return idx, array[idx], value


def zpRead(sec):

    zpArr = []

    for key, value in config.items(sec):

        keyList = []

        if len(value) == 0:
            print('Missing zps')
            sys.exit()

        elif ':' in value:
            array = value.split(':')

            for index, innerArr in enumerate(array):
                innerList = []
                for entry in innerArr.split(','):
                    if('j' in entry):
                        innerList.append(complex(entry))
                    else:
                        innerList.append(float(entry))
                keyList.append(innerList)
            zpArr.append(keyList)

    return zpArr



# ############ #                                                                                                                                                                                                     
# FILE READ IN #                                                                                                                                                                                                     
# ############ #                                                                                                                                                                                                     

if len(sys.argv) != 2:
    print(">>> USAGE: {} <config file>".format(sys.argv[0]))
    sys.exit(1)

configFILE = sys.argv[1]

config = configparser.ConfigParser()

config.read(configFILE)

for sec in config.sections():

    if(sec == 'metadata'):

        meta = config['metadata']

        SVNROOT = meta['SVNROOT']
        pyDARMconfig = meta['pyDARMconfig']

    elif(sec == 'measINFO'):

        meas = config['measINFO']

        measDate = meas['measDate']
        ifo = meas['ifo']
        dataDir = SVNROOT+ifo+'DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Data/'
        #dataDir = SVNROOT+'Common/Electronics/'+ifo+'DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Data/'                                                                                                      
        resultsDir = SVNROOT+ifo+'/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Results/'
        #resultsDir = SVNROOT+'Common/Electronics/'+ifo+'/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Results/'                                                                                               
        fileTag = measDate + 'H1_OMCA_DCPDTIA_S2100832_SN02_'

    elif(sec == 'zpFull'): zp = zpRead(sec)
    elif(sec == 'zpRdx'): zp_rdx = zpRead(sec)
    elif(sec == 'zpOrig'): zp_org = zpRead(sec)


DCPD = ['CH1_DCPDA', 'CH2_DCPDB']
WCgain = ['HiGain', 'LoGain']

#zp = [[[[1.810e+01, 2.228e+01], [91.377, 11054.698, 11030.144, 10389.659,   111.47]], #DCPDA HiGain                                                                   
#      [[1.813e+01, 2.228e+01], [11177.813, 10160.005, 11180.644,    91.442,   111.634]]], #DCPDA LoGain                                                               
#      [[[ 1.789e+01, 2.178e+01], [90.59,  10627.498, 10932.758,   108.655, 10919.739]], #DCPDB HiGain                                                                 
#      [[ 1.791e+01, 2.176e+01], [11119.008, 10305.19,  11095.386,    90.485,   108.862]]]] #DCPDB LoGain 


#zp = [[[[1.57e+07,1.810e+01, 2.228e+01], [91.377, 11054.698, 11030.144, 10389.659,   111.47]], #DCPDA HiGain
#       [[2.03e+07,1.813e+01, 2.228e+01], [11177.813, 10160.005, 11180.644,    91.442,   111.634]]], #DCPDA LoGain
#      [[[1.59e+07, 1.789e+01, 2.178e+01], [90.59,  10627.498, 10932.758,   108.655, 10919.739]], #DCPDB HiGain
#       [[1.92e+07, 1.791e+01, 2.176e+01], [11119.008, 10305.19,  11095.386,    90.485,   108.862]]]] #DCPDB LoGain

#Using only sub-Nyquist frequencies
#zp_rdx = [[[[1.810e+01, 2.228e+01], [91.377, 111.47]], #DCPDA HiGain                                                             
#           [[1.813e+01, 2.228e+01], [91.442, 111.634]]], #DCPDA LoGain                                                             
#          [[[1.789e+01, 2.178e+01], [90.59, 108.655]], #DCPDB HiGain                                                            
#           [[1.791e+01, 2.176e+01], [90.485, 108.862]]]] #DCPDB LoGain

#Using existing poles and zeros from O3

#zp_org = [[[19.9, 19.9], [99.47, 99.47]], #DCPDA
#          [[19.9, 19.9], [99.47, 99.47]]] #DCPDB


fignames = namedtuple('fignames',['filename'])
WCtuple = namedtuple('WCgain', ['WCgain','freq', 'DCPDfullzarray_Hz', 'DCPDfullparray_Hz', 'DCPDrdxzarray_Hz','DCPDrdxparray_Hz',
                                'DCPDorgzarray_Hz', 'DCPDorgparray_Hz', 'meas_tf', 'normmeas_tf',
                                'dcpdfullmodel_tf', 'dcpdrdxmodel_tf', 'dcpdorgmodel_tf',
                                'fullcomp_tf', 'rdxcomp_tf','orgcomp_tf','fullresid_tf', 'rdxresid_tf', 'orgresid_tf'])
DCPDtuple = namedtuple('DCPDs', ['DCPD','WCgain'])
errortuple = namedtuple('errorModel',['WCgain','DCPD_wisyserror_full','DCPD_wisyserror_rdx','DCPD_wisyserror_org',
                                  'G_wipsyserror_full','G_wipsyserror_rdx', 'G_wipsyserror_org','etaR_full', 'etaR_rdx', 'etaR_org'])
                        
DCPDs = []

for x,i in enumerate(DCPD):
    thisDCPDGain = []
    for y,j in enumerate(WCgain):
        hdf5FileName_TC = resultsDir+fileTag+i+'_'+j+'_measData.hdf5'
        meas_h5 = h5py.File(hdf5FileName_TC, 'r')
        thisfreq = np.array(meas_h5.get('freq')[()], dtype=float)
        thismeas_tf = np.array(meas_h5.get('tf_meas')[()], dtype=complex)
        thisnormmeas_tf = thismeas_tf /abs(thismeas_tf[0])
        thisdcpdfullzlist_Hz = np.asarray(zp[x][y][0])
        thisdcpdfullplist_Hz = np.asarray(zp[x][y][1])
        thisdcpdrdxzlist_Hz = np.asarray(zp_rdx[x][y][0])
        thisdcpdrdxplist_Hz = np.asarray(zp_rdx[x][y][1])
        thisdcpdorgzlist_Hz = np.asarray(zp_org[x][0])
        thisdcpdorgplist_Hz = np.asarray(zp_org[x][1])
        omega = 2.0*np.pi*thisfreq
        thisdcpdfullmodel_tf = signal.freqresp(
            signal.ZerosPolesGain(-2.0*np.pi*thisdcpdfullzlist_Hz,
                                  -2.0*np.pi*thisdcpdfullplist_Hz,
                                  np.prod(2*np.pi*thisdcpdfullplist_Hz) /
                                  np.prod(2*np.pi*thisdcpdfullzlist_Hz)),
            omega)[1]
        thisdcpdrdxmodel_tf = signal.freqresp(
            signal.ZerosPolesGain(-2.0*np.pi*thisdcpdrdxzlist_Hz,
                                  -2.0*np.pi*thisdcpdrdxplist_Hz,
                                  np.prod(2*np.pi*thisdcpdrdxplist_Hz) /
                                  np.prod(2*np.pi*thisdcpdrdxzlist_Hz)),
            omega)[1]
        thisdcpdorgmodel_tf = signal.freqresp(
            signal.ZerosPolesGain(-2.0*np.pi*thisdcpdorgzlist_Hz,
                                  -2.0*np.pi*thisdcpdorgplist_Hz,
                                  np.prod(2*np.pi*thisdcpdorgplist_Hz) /
                                  np.prod(2*np.pi*thisdcpdorgzlist_Hz)),
            omega)[1]
        thisOneHzInd, __, __ = find_nearest(thisfreq,1.0)
        thisMeas_OneHzGain = abs(thisnormmeas_tf[thisOneHzInd])
        thisModel_OneHzGain = abs(thisdcpdfullmodel_tf[thisOneHzInd])
        thisOneHzGainRatio = thisModel_OneHzGain / thisMeas_OneHzGain
        thisfullcomp_tf = 1.0 / thisdcpdfullmodel_tf
        thisrdxcomp_tf = 1.0 / thisdcpdrdxmodel_tf
        thisorgcomp_tf = 1.0 / thisdcpdorgmodel_tf
        thisfullresid_tf = thisOneHzGainRatio * thisfullcomp_tf * thisnormmeas_tf
        thisrdxresid_tf = thisOneHzGainRatio * thisrdxcomp_tf * thisnormmeas_tf
        thisorgresid_tf = thisOneHzGainRatio * thisorgcomp_tf * thisnormmeas_tf
        thisWCgain = WCtuple(j, thisfreq,                                                                                                           
                             thisdcpdfullzlist_Hz, thisdcpdfullplist_Hz, thisdcpdrdxzlist_Hz,thisdcpdrdxplist_Hz, thisdcpdorgzlist_Hz, thisdcpdorgplist_Hz,     
                             thismeas_tf, thisnormmeas_tf,
                             thisdcpdfullmodel_tf,thisdcpdrdxmodel_tf,thisdcpdorgmodel_tf,thisfullcomp_tf, thisrdxcomp_tf, thisorgcomp_tf,
                             thisfullresid_tf, thisrdxresid_tf, thisorgresid_tf)
        thisDCPDGain.append(thisWCgain)
        #Saving the measurement data                                                                                                                 
        hdf5FileName = resultsDir + fileTag + i + "_"+j+"_residual.hdf5"
        with h5py.File(hdf5FileName, 'w') as f:
            f.create_dataset('freq', data=thisfreq)
            f.create_dataset('eta_full', data=thisfullresid_tf)
            f.create_dataset('eta_rdx', data=thisrdxresid_tf)
            f.create_dataset('eta_org', data=thisorgresid_tf)
    thisDCPD = DCPDtuple(i, thisDCPDGain)
    DCPDs.append(thisDCPD)
etaHiGain_full = (DCPDs[0].WCgain[0].fullresid_tf + DCPDs[1].WCgain[0].fullresid_tf)/2
etaLoGain_full = (DCPDs[0].WCgain[1].fullresid_tf + DCPDs[1].WCgain[1].fullresid_tf)/2
etaHiGain_rdx = (DCPDs[0].WCgain[0].rdxresid_tf + DCPDs[1].WCgain[0].rdxresid_tf)/2
etaLoGain_rdx = (DCPDs[0].WCgain[1].rdxresid_tf + DCPDs[1].WCgain[1].rdxresid_tf)/2
eta_org = (DCPDs[0].WCgain[1].orgresid_tf + DCPDs[1].WCgain[1].orgresid_tf)/2

# All frequency vectors are the same, or else none of this would work
freq = thisfreq
#eta_avg = (DCPDs[0].fullresid_tf + DCPDs[1].fullresid_tf)/2
# ######################### #
# REFERENCE DARM LOOP MODEL #
# ######################### #

C = pydarm.sensing.SensingModel(config)
A = pydarm.actuation.DARMActuationModel(config)
darm = pydarm.darm.DARMModel(config)

A_UIM_ref = A.xarm.compute_actuation_single_stage(freq,'UIM')
A_PUM_ref = A.xarm.compute_actuation_single_stage(freq,'PUM')
A_TST_ref = A.xarm.compute_actuation_single_stage(freq,'TST')
A_ref = A_UIM_ref + A_PUM_ref + A_TST_ref

D_ref = darm.digital.compute_response(freq)
C_ref = C.compute_sensing(freq)
R_ref = darm.compute_response_function(freq)

contri_C = (1.0/C_ref) / R_ref

# ##################### #
# DARM MODEL WITH ERROR #
# ##################### #
errorModel = []

eta_full = [etaHiGain_full, etaLoGain_full]
eta_rdx = [etaHiGain_rdx, etaLoGain_rdx]
for n,p,q in zip(WCgain,eta_full, eta_rdx):
    DCPD_wisyserror_full = C_ref * p
    DCPD_wisyserror_rdx = C_ref * q
    DCPD_wisyserror_org = C_ref * eta_org
    G_wipsyserror_full = A_ref*D_ref*DCPD_wisyserror_full
    G_wipsyserror_rdx = A_ref*D_ref*DCPD_wisyserror_rdx
    G_wipsyserror_org = A_ref*D_ref*DCPD_wisyserror_org
    G = A_ref*D_ref*C_ref
    etaR_full = (1+G_wipsyserror_full)/(p*(1+G))
    etaR_rdx = (1+G_wipsyserror_rdx)/(q*(1+G))
    etaR_org = (1+G_wipsyserror_org)/(eta_org*(1+G))
    thisErrorModel = errortuple(n, DCPD_wisyserror_full,DCPD_wisyserror_rdx,DCPD_wisyserror_org,
                                G_wipsyserror_full,G_wipsyserror_rdx,G_wipsyserror_org,
                                etaR_full, etaR_rdx, etaR_org)
    errorModel.append(thisErrorModel)

#print (errorModel[0].WCgain, errorModel[2].WCgain)

# ######################### #
#         FIGURES           #
# ######################### #
plotFreqRange = [1e-1, 1.1e4]

allfigures = []


plotFreqRange = [1e-1, 1.1e4]
xTicks = np.logspace(-1,4,6)

for i in range(len(DCPDs)):
    fig = plt.figure()
    s1 = fig.add_subplot(221)
    s2 = fig.add_subplot(223)
    s3 = fig.add_subplot(222)
    s4 = fig.add_subplot(224)
    s1.loglog(freq,abs(DCPDs[i].WCgain[0].dcpdorgmodel_tf),label=DCPDs[i].DCPD+'Model Existing', ls = '-.')
    s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[0].dcpdorgmodel_tf), ls = '-.', label=DCPDs[i].DCPD+'Model Existing')
    s3.semilogx(freq,abs(DCPDs[i].WCgain[0].orgresid_tf), label=DCPDs[i].DCPD+' Norm. Data * (1 / Fit) Existing', ls = '-.')
    s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[0].orgresid_tf), label=DCPDs[i].DCPD+' Norm. Data * (1 / Fit) Existing', ls = '-.')
    for j in range(len(WCgain)):
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].normmeas_tf),label=DCPDs[i].DCPD+WCgain[j]+'Norm. Data')
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].dcpdfullmodel_tf),label=DCPDs[i].DCPD+WCgain[j]+'Model', ls = '--')
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].dcpdrdxmodel_tf),label=DCPDs[i].DCPD+WCgain[j]+'Model Rdx', ls = ':')
        s1.set_ylabel('Magnitude (V/A)')
        s1.grid(which='minor', ls='--')
        s1.set_xlim(plotFreqRange)
        s1.set_xticks(xTicks)
        s1.set_ylim(1.0e-1, 50)
        s1.yaxis.set_major_locator(tck.LogLocator(base=10))
        s1.legend(ncol=1)
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].normmeas_tf), label=DCPDs[i].DCPD+WCgain[j]+'Data')
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].dcpdfullmodel_tf), ls = '--', label=DCPDs[i].DCPD+WCgain[j]+'Model')
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].dcpdrdxmodel_tf), ls = ':', label=DCPDs[i].DCPD+WCgain[j]+'Model Rdx')
        s2.set_ylabel('Phase')
        s2.grid(which='minor', ls='--')
        s2.set_xlim(plotFreqRange)
        s2.set_ylim([-60,100])
        s2.set_xticks(xTicks)
        s2.yaxis.set_major_locator(tck.MultipleLocator(15))
        s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
        s2.legend(ncol=1)
        custom_cycler = (cycler('linestyle',['--',':'])+cycler('color', ['red', 'magenta'])) #to mark full and rdx in diffferent linestyles for different gain
        s3.semilogx(freq,abs(DCPDs[i].WCgain[j].fullresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
        s3.semilogx(freq,abs(DCPDs[i].WCgain[j].rdxresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx')
        s3.set_prop_cycle(custom_cycler)
        s3.set_ylabel('abs(Data/Fit) (dimensionless)')
        s3.grid(which='major',color='black')
        s3.grid(which='minor',ls='--')
        s3.set_xlim(plotFreqRange)
        s3.set_xticks(xTicks)
        s3.set_ylim([0.98,1.015])
        s3.yaxis.set_major_locator(tck.MultipleLocator(0.005))
        s3.yaxis.set_minor_locator(tck.MultipleLocator(0.001))
        s3.legend(ncol=1)
        s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].fullresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
        s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].rdxresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx')
        s4.set_prop_cycle(custom_cycler)
        s4.set_xlabel('Frequency (Hz)')
        s4.set_ylabel('angle(Data/Fit) (deg)')
        s4.grid(which='major',color='black')
        s4.grid(which='minor',ls='--')
        s4.set_xlim(plotFreqRange)
        s4.set_xticks(xTicks)
        s4.set_ylim([-2.0,1.0])
        s4.yaxis.set_major_locator(tck.MultipleLocator(0.5))
        s4.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
        s4.legend(ncol=1)
    thisfig = fignames(resultsDir+ fileTag +DCPDs[i].DCPD+'_TF.pdf')
    #thisfig = fignames(fileTag +DCPDs[i].DCPD+'_TF.pdf') 
    plt.savefig(thisfig.filename,bbox_inches='tight')
    allfigures.append(thisfig)
    plt.close(fig)


#Plotting \eta_C
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)
s1.semilogx(freq,abs(eta_org),label='(\eta_A+\eta_B)/2'+'Existing', ls = '-.')
s2.semilogx(freq,180.0/np.pi*np.angle(eta_org), ls = '-.', label='Existing')
for i in range(len(eta_full)):
        s1.semilogx(freq,abs(eta_full[i]),label='(\eta_A+\eta_B)/2'+WCgain[i]+'Full')
        s1.semilogx(freq,abs(eta_rdx[i]),label='(\eta_A+\eta_B)/2'+WCgain[i]+'Rdx', ls = '--')
        s1.set_ylabel('Magnitude(Dimensionless)')
        s1.grid(which='minor', ls='--')
        s1.set_ylim([0.99,1.01])
        s1.set_xlim(plotFreqRange)
        s1.set_xticks(xTicks)
        #s1.yaxis.set_major_locator(tck.LogLocator(base=10))
        s1.legend(ncol=1)
        s2.semilogx(freq,180.0/np.pi*np.angle(eta_full[i]), label=WCgain[i]+'full')
        s2.semilogx(freq,180.0/np.pi*np.angle(eta_rdx[i]), ls = '--', label=WCgain[j]+'Rdx')
        s2.set_ylabel('Phase')
        s2.grid(which='minor', ls='--')
        s2.set_xlim(plotFreqRange)
        #s2.set_ylim([-10,10])
        s2.set_xticks(xTicks)
        s2.yaxis.set_major_locator(tck.MultipleLocator(20))
        s2.yaxis.set_minor_locator(tck.MultipleLocator(10))
        s2.legend(ncol=1)
        custom_cycler = (cycler('linestyle',['--',':'])+cycler('color', ['red', 'green'])) #to mark full and rdx in diffferent linestyles for different gain 
        s3.semilogx(freq,abs(eta_full[i]),label='(\eta_A+\eta_B)/2'+WCgain[i]+'Full')
        s3.semilogx(freq,abs(eta_rdx[i]),label='(\eta_A+\eta_B)/2'+WCgain[i]+'Rdx')
        s3.set_prop_cycle(custom_cycler)
        s3.set_ylabel('Magnitude (Dimensionless)')
        s3.grid(which='minor', ls='--')
        s3.set_ylim([0.998,1.003])                                                                                                                             
        s3.set_xlim(plotFreqRange)
        s3.set_xticks(xTicks)
        #s1.yaxis.set_major_locator(tck.LogLocator(base=10))                                                                                                    
        s3.legend(ncol=1)
        s4.semilogx(freq,180.0/np.pi*np.angle(eta_full[i]), label=WCgain[i]+'full')
        s4.semilogx(freq,180.0/np.pi*np.angle(eta_rdx[i]), label=WCgain[j]+'Rdx')
        s4.set_prop_cycle(custom_cycler)
        s4.set_ylabel('Phase')
        s4.grid(which='minor', ls='--')
        s4.set_xlim(plotFreqRange)
        s4.set_ylim([-10,10])                                                                                                                                  
        s4.set_xticks(xTicks)
        s4.yaxis.set_major_locator(tck.MultipleLocator(5))
        s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
        s4.legend(ncol=1)
        s4.set_xlabel('Frequency (Hz)')
thisfig = fignames(resultsDir + fileTag + '_CompensationComparison.pdf')
#thisfig = fignames(fileTag + '_CompensationComparison.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)


#Plotting \eta_R

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)
s1.semilogx(freq,abs(etaR_org),label='Existing', ls = '-.')
s2.semilogx(freq,180.0/np.pi*np.angle(etaR_org), ls = '-.', label='Existing')
for i in range(len(errorModel)):
    s1.semilogx(freq,abs(errorModel[i].etaR_full),label=errorModel[i].WCgain+'Full')
    s1.semilogx(freq,abs(errorModel[i].etaR_rdx),label=errorModel[i].WCgain+'Rdx', ls = '--')
    s1.set_ylabel('Magnitude (Dimensionless)')
    s1.grid(which='minor', ls='--')
    s1.set_ylim([0.95,1.05])
    s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
    s1.set_xlim(plotFreqRange)
    s1.set_xticks(xTicks)
    s1.legend(ncol=1)
    s2.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_full), label=errorModel[i].WCgain+'full')
    s2.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_rdx), ls = '--', label=errorModel[i].WCgain+'Rdx')
    s2.set_ylabel('Phase')
    s2.grid(which='minor', ls='--')
    s2.set_xlim(plotFreqRange)
    s2.set_xticks(xTicks)
    s2.set_ylim(-10, 30)
    s2.yaxis.set_major_locator(tck.MultipleLocator(10))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
    s2.legend(ncol=1)
    s3.semilogx(freq,abs(errorModel[i].etaR_full),label=errorModel[i].WCgain+'Full')
    s3.semilogx(freq,abs(errorModel[i].etaR_rdx),label=errorModel[i].WCgain+'Rdx', ls = '--')
    s3.set_ylabel('Magnitude (Dimensionless)')
    s3.grid(which='minor', ls='--')
    s3.set_ylim([0.93,1.03])
    s3.yaxis.set_major_locator(tck.MultipleLocator(0.01))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
    s3.set_xlim(plotFreqRange)
    s3.set_xticks(xTicks)
    s3.legend(ncol=1)
    s4.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_full), label=errorModel[i].WCgain+'full')
    s4.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_rdx), ls = '--', label=errorModel[i].WCgain+'Rdx')
    s4.set_ylabel('Phase')
    s4.grid(which='minor', ls='--')
    s4.set_xlim(plotFreqRange)
    s4.set_ylim([-10,10])
    s4.set_xticks(xTicks)
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.legend(ncol=1)
thisfig = fignames(resultsDir+fileTag +'etaR_w_etaC_comparison.pdf')
#thisfig = fignames(fileTag +'etaR_w_etaC_comparison.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)


#Plotting Fig 1 w/o "existing" curve zooming in

for i in range(len(DCPDs)):
    fig = plt.figure()
    s1 = fig.add_subplot(221)
    s2 = fig.add_subplot(223)
    s3 = fig.add_subplot(222)
    s4 = fig.add_subplot(224)
    for j in range(len(WCgain)):
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].normmeas_tf),label=DCPDs[i].DCPD+WCgain[j]+'Norm. Data')
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].dcpdfullmodel_tf),label=DCPDs[i].DCPD+WCgain[j]+'Model', ls = '--')
        s1.loglog(freq,abs(DCPDs[i].WCgain[j].dcpdrdxmodel_tf),label=DCPDs[i].DCPD+WCgain[j]+'Model Rdx', ls = ':')
        s1.set_ylabel('Magnitude (V/A)')
        s1.grid(which='minor', ls='--')
        s1.set_xlim(plotFreqRange)
        s1.set_xticks(xTicks)
        s1.set_ylim(5.0e-1, 50)
        s1.yaxis.set_major_locator(tck.LogLocator(base=10))
        s1.legend(ncol=1)
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].normmeas_tf), label=DCPDs[i].DCPD+WCgain[j]+'Data')
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].dcpdfullmodel_tf), ls = '--', label=DCPDs[i].DCPD+WCgain[j]+'Model')
        s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].dcpdrdxmodel_tf), ls = ':', label=DCPDs[i].DCPD+WCgain[j]+'Model Rdx')
        s2.set_ylabel('Phase')
        s2.grid(which='minor', ls='--')
        s2.set_xlim(plotFreqRange)
        s2.set_ylim([-60,100])
        s2.set_xticks(xTicks)
        s2.yaxis.set_major_locator(tck.MultipleLocator(15))
        s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
        s2.legend(ncol=1)
        custom_cycler = (cycler('linestyle',['--',':'])+cycler('color', ['red', 'green'])) #to mark full and rdx in diffferent linestyles 
        s3.semilogx(freq,abs(DCPDs[i].WCgain[j].fullresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
        s3.semilogx(freq,abs(DCPDs[i].WCgain[j].rdxresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx')
        s3.set_prop_cycle(custom_cycler)
        s3.set_ylabel('abs(Data/Fit) (dimensionless)')
        s3.grid(which='major',color='black')
        s3.grid(which='minor',ls='--')
        s3.set_xlim(plotFreqRange)
        s3.set_xticks(xTicks)
        s3.set_ylim([0.995,1.003])
        s3.yaxis.set_major_locator(tck.MultipleLocator(0.001))
        s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
        s3.legend(ncol=1)
        s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].fullresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
        s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[i].WCgain[j].rdxresid_tf), label=DCPDs[i].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx')
        s4.set_prop_cycle(custom_cycler)
        s4.set_xlabel('Frequency (Hz)')
        s4.set_ylabel('angle(Data/Fit) (deg)')
        s4.grid(which='major',color='black')
        s4.grid(which='minor',ls='--')
        s4.set_xlim(plotFreqRange)
        s4.set_xticks(xTicks)
        s4.set_ylim([-1.5,0.2])
        s4.yaxis.set_major_locator(tck.MultipleLocator(0.5))
        s4.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
        s4.legend(ncol=1)
    thisfig = fignames(resultsDir+fileTag+DCPDs[i].DCPD+'_TF_Rdx.pdf')
    #thisfig = fignames(fileTag+DCPDs[i].DCPD+'_TF_Rdx.pdf')
    plt.savefig(thisfig.filename,bbox_inches='tight')
    allfigures.append(thisfig)
    plt.close(fig)

#Plotting A and B residuals together in a quad plot
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)
for j in range(len(WCgain)):
    s1.semilogx(freq,abs(DCPDs[0].WCgain[j].fullresid_tf), label=DCPDs[0].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s1.semilogx(freq,abs(DCPDs[0].WCgain[j].rdxresid_tf), label=DCPDs[0].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx', ls = '--')
    #s1.semilogx(freq,abs(DCPDs[1].WCgain[j].fullresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s1.set_ylabel('abs(Data/Fit) (dimensionless)')
    s1.grid(which='major',color='black')
    s1.grid(which='minor',ls='--')
    s1.set_xlim(plotFreqRange)
    s1.set_xticks(xTicks)
    s1.set_ylim([0.995,1.003])
    s1.yaxis.set_major_locator(tck.MultipleLocator(0.001))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
    s1.legend(ncol=1)
    s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[0].WCgain[j].fullresid_tf), label=DCPDs[0].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[0].WCgain[j].rdxresid_tf), label=DCPDs[0].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx', ls = '--')
    #s2.semilogx(freq,180.0/np.pi*np.angle(DCPDs[1].WCgain[j].fullresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('angle(Data/Fit) (deg)')
    s2.grid(which='major',color='black')
    s2.grid(which='minor',ls='--')
    s2.set_xlim(plotFreqRange)
    s2.set_xticks(xTicks)
    s2.set_ylim([-1.5,0.2])
    s2.yaxis.set_major_locator(tck.MultipleLocator(0.5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
    s2.legend(ncol=1)
    s3.semilogx(freq,abs(DCPDs[1].WCgain[j].fullresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s3.semilogx(freq,abs(DCPDs[1].WCgain[j].rdxresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx', ls = '--')
    s3.set_ylabel('abs(Data/Fit) (dimensionless)')
    s3.grid(which='major',color='black')
    s3.grid(which='minor',ls='--')
    s3.set_xlim(plotFreqRange)
    s3.set_xticks(xTicks)
    s3.set_ylim([0.995,1.003])
    s3.yaxis.set_major_locator(tck.MultipleLocator(0.001))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
    s3.legend(ncol=1)
    s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[1].WCgain[j].fullresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Full')
    s4.semilogx(freq,180.0/np.pi*np.angle(DCPDs[1].WCgain[j].rdxresid_tf), label=DCPDs[1].DCPD+WCgain[j]+' Norm. Data * (1 / Fit) Rdx', ls = '--')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('angle(Data/Fit) (deg)')
    s4.grid(which='major',color='black')
    s4.grid(which='minor',ls='--')
    s4.set_xlim(plotFreqRange)
    s4.set_xticks(xTicks)
    s4.set_ylim([-1.5,0.2])
    s4.yaxis.set_major_locator(tck.MultipleLocator(0.5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
    s4.legend(ncol=1)
thisfig = fignames(resultsDir+ fileTag+'AandB.pdf')
#thisfig = fignames(fileTag+'AandB.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)

