close all
clear all
clc

set(0,'DefaultLineLineWidth',2) 
set(0,'DefaultLineMarkerSize',12) 
set(0,'DefaultAxesFontSize',12) 
set(0,'DefaultAxesXGrid','on') 
set(0,'DefaultAxesYGrid','on') 
set(0,'DefaultAxesZGrid','on') 

%%
printFigs = 1;
resultsDir = '/Users/kissel/Desktop/';

%%
nPoints = 1001;
freq = sort([logspace(-2,6,1000) 1000]);
w = 2*pi*freq;
onekHzInd = find(freq == 1000);
onekHz = freq(onekHzInd);

%% Enumerated names of components taken from -v3 of D2100630.

%% Differential Receiver
% Slew Rate limiting filter at the input
R33 = 1e3;
C6 = 6.8e-9;

Z_C6_diff = (1/2)*(1./(1i*w*C6));

V_IA_diff_over_V_in = Z_C6_diff ./ (R33 + Z_C6_diff);

% Instrumentation Amplifier (Ingoring ~11 MHz pole from C2 or C6)
R32 = 1.5e3;
R31 = 3e3;
V_IA_SE_over_V_IA_diff = (R32 / R31) * ones(nPoints,1);

%% Whitening Stages
% Inverting Amp 1
R96 = 8.06e3;
C60 = 1e-6;
Z_WRC1_para =  R96 .* (1./(1i*w*C60)) ./ (R96 + (1./(1i*w*C60)));

R98 = 2e3;

Z_WRC1_paraseries = Z_WRC1_para + R98;

R93 = 10e3;
C52 = 1.6e-9;
Z_WRC2_para = R93 .* (1./(1i*w*C52)) ./ (R93 + (1./(1i*w*C52)));

V_W1_over_V_IA_SE = -1 * Z_WRC2_para ./ Z_WRC1_paraseries;

% Interting Amp 2
R97 = 80.6e3;
C61 = 100e-9;
Z_WRC3_para =  R97 .* (1./(1i*w*C61)) ./ (R97 + (1./(1i*w*C61)));

R99 = 20e3;

Z_WRC3_paraseries = Z_WRC3_para + R99;

R94 = 100e3;
C54 = 150e-12;
Z_WRC4_para = R94 .* (1./(1i*w*C54)) ./ (R94 + (1./(1i*w*C54)));

V_W2_over_V_W1 = -1 * Z_WRC4_para ./ Z_WRC3_paraseries;

% Switchable Interting Amp 3
R100 = 20e3;

% Lo Gain
R92 = 20e3;
C53 = 10e-12;
Z_sw_lower = R92 * (1./(1i*w*C53)) ./ (R92 + (1./(1i*w*C53)));

% Hi Gain
R91 = 40.2e3;
C51 = 10e-12;
Z_sw_upper = R91 * (1./(1i*w*C51)) ./ (R91 + (1./(1i*w*C51)));

V_SW_over_V_W2(1).tf = -1 * Z_sw_lower ./ R100; 
V_SW_over_V_W2(2).tf = -1 * Z_sw_upper ./ R100;

% Inverting Amp 4
R101 = 100e3;

R95 = 100e3;
C55 = 10e-12; 
Z_WRC5_para = R95 .* (1./(1i*w*C55)) ./ (R95 + (1./(1i*w*C55)));

V_W3_over_V_SW = -1 .* Z_WRC5_para ./ R101;

%% Differential ADC Driver
R23 = 10e3;
R24 = 10e3;

V_AA_diff_over_V_W3 = 2 * (R24/R23) * ones(nPoints,1);

%% Overal all transfer function
V_out_over_V_in(1).tf = [V_IA_diff_over_V_in(:) ...
                         V_IA_SE_over_V_IA_diff(:) ...
                         V_W1_over_V_IA_SE(:) ...
                         V_W2_over_V_W1(:) ...
                         V_SW_over_V_W2(1).tf(:) ...
                         V_W3_over_V_SW(:) ...
                         V_AA_diff_over_V_W3(:)];

V_out_over_V_in(2).tf = [V_IA_diff_over_V_in(:) ...
                         V_IA_SE_over_V_IA_diff(:) ...
                         V_W1_over_V_IA_SE(:) ...
                         V_W2_over_V_W1(:) ...
                         V_SW_over_V_W2(2).tf(:) ...
                         V_W3_over_V_SW(:) ...
                         V_AA_diff_over_V_W3(:)];
                    
theLegend = {'V_{TIA,Out}^{DIFF} / V_{IA}^{DIFF}';...
             'V_{IA}^{SE} / V_{IA}^{DIFF}';...
             'V_{W1} / V_{IA}^{SE}';...
             'V_{W2} / V_{W1}';...
             'V_{SW} / V_{W2}';...
             'V_{W3} / V_{SW}';...
             'V_{AA}^{DIFF} / V_{W3}^{SE}';...
             'Product'};        
         
theSwitch = {'Lo';'Hi'};
%%
figNum = 1;
for iSwitchState = 1:length(V_out_over_V_in)
    for iTerm = 1:size(V_out_over_V_in(1).tf,2)
        thePlot = V_out_over_V_in(iSwitchState).tf(:,1:iTerm);
        thePlot = [thePlot prod(thePlot,2)];
        onekHzMag = abs(thePlot(onekHzInd,end));
        
        figure(figNum)
        figNames{figNum} = [resultsDir 'simpleanalyticmodel_OMC_DCPD_Whitening_gainx' num2str(iSwitchState) '_step' num2str(iTerm)];
        figNum = figNum + 1;
        subplot(211)
        ll = loglog(freq(:),abs(thePlot));
        set(ll(end),'LineStyle','--','Color','black')
        leg = legend(theLegend{[1:iTerm end]});
        set(leg,'Location','SouthWest')
        title('OMC DCPD Whitening Chassis Response Component Breakdown')
        xlabel('Frequency [Hz]')
        ylabel('Magnitude [V/V]')
        xlim([1e-2 1e6])
        ylim([1e-2 1e2])
        set(gca,'XTick',10.^(-2:6),'YTick',10.^(-2:2))
        
        subplot(212)
        ll = semilogx(freq(:),180/pi*angle(thePlot));
        set(ll(end),'LineStyle','--','Color','black')
        if iTerm > 4
            title(['Gain Switch Setting: ' theSwitch{iSwitchState} ', SW Gain: x' num2str(abs(V_SW_over_V_W2(iSwitchState).tf(1))) ', Overall Gain @ 1 kHz: ' num2str(onekHzMag,4) ' [V/V]'])
        else
            title(['Overall Gain @ 1 kHz: ' num2str(onekHzMag,4) ' [V/V]'])
        end
        xlabel('Frequency [Hz]')
        ylabel('Phase [deg]')
        xlim([1e-2 1e6])
        ylim([-185 185])
        set(gca,'XTick',10.^(-2:6),'YTick',-180:45:180,'YMinorTick','on','YMinorGrid','on')
    end
end

%%
figure(figNum)
figNames{figNum} = [resultsDir 'simpleanalyticmodel_OMC_DCPD_Whitening_Final'];
figNum = figNum + 1;
subplot(211)
ll = loglog(freq(:),abs([prod(V_out_over_V_in(1).tf,2) ...
                         prod(V_out_over_V_in(2).tf,2)]));
leg = legend('LO Gain',...
             'HI Gain');
set(leg,'Location','SouthWest')
title('OMC DCPD Whitening Chassis Total Response')
xlabel('Frequency [Hz]')
ylabel('Magnitude [V_{AA}^{DIFF} / V_{TIA,Out}^{DIFF}]')
xlim([1e-2 1e6])
ylim([1e-2 1e2])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-2:2))

subplot(212)
ll = semilogx(freq(:),180/pi*angle([prod(V_out_over_V_in(1).tf,2) ...
                                    prod(V_out_over_V_in(2).tf,2)]));
if iTerm > 4
    title(['Gain Switch Setting: ' theSwitch{iSwitchState} ', SW Gain: x' num2str(abs(V_SW_over_V_W2(iSwitchState).tf(1))) ', Overall Gain @ 1 kHz: ' num2str(onekHzMag,4) ' [V/V]'])
else
    title(['Overall Gain @ 1 kHz: ' num2str(onekHzMag,4) ' [V/V]'])
end
xlabel('Frequency [Hz]')
ylabel('Phase [deg]')
xlim([1e-2 1e6])
ylim([-185 185])
set(gca,'XTick',10.^(-2:6),'YTick',-180:45:180,'YMinorTick','on','YMinorGrid','on')

%%

if printFigs
    for iFig = 1:length(figNames)
        figure(iFig)
        FillPage('w')
        IDfig
        saveas(gcf,[figNames{iFig} '.png'])
    end
end
                    