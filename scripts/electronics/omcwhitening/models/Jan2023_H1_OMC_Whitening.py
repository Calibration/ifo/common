import numpy as np
import pydarm
import h5py
import os

os.environ['CAL_DATA_ROOT'] = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'

# Load in the 20221118 DARM model
config = '/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'
C = pydarm.sensing.SensingModel(config)
A = pydarm.actuation.DARMActuationModel(config)
darm = pydarm.darm.DARMModel(config)

OMC_wh_serial = 'S2300003'
# Load in the recent DCPD systematic error files (thanks Louis D.!)
results_dir = f'/ligo/svncommon/CalSVN/aligocalibration/trunk/Common/Electronics/H1/DCPDWhitening/OMCA/{OMC_wh_serial}/20230130/Results'
omc_dcpda_data = np.load(f'{results_dir}/OMCA_DCPDA_WhiteningON_0p1to102p4e3Hz_1Vsrcinput_residuals.npz')
omc_dcpdb_data = np.load(f'{results_dir}/OMCA_DCPDB_WhiteningON_0p1to102p4e3Hz_1Vsrcinput_residuals.npz')

# Load in the trans-impedance amplifier systematic error files
tia_dcpda_data = h5py.File(os.environ['CAL_DATA_ROOT']+'Common/Electronics/H1/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Results/20220215_H1_OMCA_DCPDTIA_S2100832_SN02_CH1_DCPDA_residual.hdf5','r')
tia_dcpdb_data = h5py.File(os.environ['CAL_DATA_ROOT']+'Common/Electronics/H1/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Results/20220215_H1_OMCA_DCPDTIA_S2100832_SN02_CH2_DCPDB_residual.hdf5','r')

# compute the individual DCPD systematic errors
eta_C_full_a = omc_dcpda_data['residual'] * tia_dcpda_data['eta_full']
eta_C_rdx_a = omc_dcpda_data['residual'] * tia_dcpda_data['eta_rdx']
eta_C_full_b = omc_dcpdb_data['residual'] * tia_dcpdb_data['eta_full']
eta_C_rdx_b = omc_dcpdb_data['residual'] * tia_dcpdb_data['eta_rdx']

# Compute the combination
eta_C_full = (eta_C_full_a + eta_C_full_b)/2
eta_C_rdx = (eta_C_rdx_a + eta_C_rdx_b)/2

# Compute the corrected responses
freq = omc_dcpda_data['freq']
_, _, eta_R_full = darm.compute_etas(freq, sensing_syserr=eta_C_full)
_, _, eta_R_rdx = darm.compute_etas(freq, sensing_syserr=eta_C_rdx)

# eta_C figures
eta_C_fig = pydarm.plot.BodePlot()
eta_C_fig.plot(freq, eta_C_full, label='eta_C_full', color='C0')
eta_C_fig.plot(freq, eta_C_rdx, label='eta_C_rdx', color='C1')
eta_C_fig.legend()
eta_C_fig.fig.savefig(f'{results_dir}/eta_C.pdf', bbox_inches='tight')

eta_C_fig = pydarm.plot.BodePlot()
eta_C_fig.plot(freq, eta_C_full, label='eta_C_full', color='C0')
eta_C_fig.legend()
eta_C_fig.fig.savefig(f'{results_dir}/eta_C_full.pdf', bbox_inches='tight')

# eta_R figures
eta_R_fig = pydarm.plot.BodePlot()
eta_R_fig.plot(freq, eta_R_full, label='eta_R_full', color='C0')
eta_R_fig.plot(freq, eta_R_rdx, label='eta_R_rdx', color='C1')
eta_R_fig.legend()
eta_R_fig.fig.savefig(f'{results_dir}/eta_R.pdf', bbox_inches='tight')

eta_R_fig = pydarm.plot.BodePlot()
eta_R_fig.plot(freq, eta_R_full, label='eta_R_full', color='C0')
eta_R_fig.legend()
eta_R_fig.fig.savefig(f'{results_dir}/eta_R_full.pdf', bbox_inches='tight')

eta_R_fig = pydarm.plot.BodePlot()
eta_R_fig.plot(freq, eta_R_full, label='eta_R_full', color='C0')
eta_R_fig.xlim(20,2000)
eta_R_fig.ylim(0.995, 1.005, -2, 2)
eta_R_fig.ax_mag.set_yscale('linear')
eta_R_fig.ax_mag.set_yticks([0.995, 0.9975, 1, 1.0025, 1.005])
eta_R_fig.ax_phase.set_yticks([-2,-1,0,1,2])
eta_R_fig.legend()
eta_R_fig.fig.savefig(f'{results_dir}/eta_R_full_reduced_freq_range.pdf', bbox_inches='tight')
