# ########### #                                                                                                                                                  
# ENVIRONMENT #                                                                                                                                                  
# ########### #                                                                                                                                                  
import pydarm
import numpy as np
from collections import namedtuple
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
import scipy.signal as signal
import numpy as np
import h5py
from cycler import cycler
import configparser
import sys

#making the plots nice                                                                                                                                           

plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (20,14),
                    'savefig.dpi': 100,
                    'pdf.compression': 9})


# ############ #                                                                                                                                                              
# FILE READ IN #                                                                                                                                                              
# ############ #                                                                                                                                                              

if len(sys.argv) != 2:
    print(">>> USAGE: {} <config file>".format(sys.argv[0]))
    sys.exit(1)

ifo = sys.argv[1]

#SVNROOT = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'
resultsdir1 = SVNROOT + 'Common/Electronics/'+ifo+'/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220215/Results/'
resultsdir2 = SVNROOT + 'Common/Electronics/'+ifo+'/DCPDWhitening/OMCA/S2101608/20220215/Results/'
resultsDir3 = SVNROOT + 'Common/Electronics/'+ifo+'/SensingFunction/OMCA/Results/'

measDate = '20220215_'
fileTag1 = '_OMCA_DCPDTIA_S2100832_SN02_'
fileTag2 = '_OMCA_DCPDWhitening_S2101608_'

#config = "/ligo/gitcommon/calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20210423.ini"
config = 'pydarm_modelparams_PostO3_H1_20210423.ini'

fignames = namedtuple('fignames',['filename'])

DCPD = ['CH1_DCPDA', 'CH2_DCPDB']

etaTIAtuple = namedtuple('eta', ['DCPD','freq','eta_full', 'eta_rdx', 'eta_org'])
etaTIA = []
for i in DCPD:
    hdf5FileName_TC = resultsDir1 + measDate + ifo + fileTag1 + i +"_residual.hdf5"
    meas_h5 = h5py.File(hdf5FileName_TC, 'r')
    thisfreq = np.array(meas_h5.get('freq')[()], dtype=float)
    thisfullresid_tf = np.array(meas_h5.get('eta_full')[()], dtype=complex)
    thisrdxresid_tf = np.array(meas_h5.get('eta_rdx')[()], dtype=complex)
    thisorgresid_tf = np.array(meas_h5.get('eta_org')[()], dtype=complex)
    thiseta = etaTIAtuple(i, thisfreq, thisfullresid_tf, thisrdxresid_tf, thisorgresid_tf)
    etaTIA.append(thiseta)


WCgain = ['HiGain', 'LoGain']
WCgaintuple = namedtuple('WCgain', ['WCgain', 'freq','eta_full', 'eta_rdx', 'eta_org'])
DCPDtuple = namedtuple('DCPDs', ['DCPD','WCgain'])

DCPDs = []
for i in (DCPD):
    thisDCPDGain = []
    for j in (WCgain):
        hdf5FileName_TC = resultsDir2 + measDate + ifo + fileTag2 + i + "_"+j+"_residual.hdf5"
        meas_h5 = h5py.File(hdf5FileName_TC, 'r')
        thisfreq = np.array(meas_h5.get('freq')[()], dtype=float)
        thisfullresid_tf = np.array(meas_h5.get('eta_full')[()], dtype=complex)
        thisrdxresid_tf = np.array(meas_h5.get('eta_rdx')[()], dtype=complex)
        thisorgresid_tf = np.array(meas_h5.get('eta_org')[()], dtype=complex)
        thisWCgain = WCgaintuple(j, thisfreq, thisfullresid_tf, thisrdxresid_tf,thisorgresid_tf)
        thisDCPDGain.append(thisWCgain)
    thisDCPD = DCPDtuple(i, thisDCPDGain)
    DCPDs.append(thisDCPD)
    

etaA_HiGain_full = (DCPDs[0].WCgain[0].eta_full * etaTIA[0].eta_full)
etaA_LoGain_full = (DCPDs[0].WCgain[1].eta_full * etaTIA[0].eta_full)
etaB_HiGain_full = (DCPDs[1].WCgain[0].eta_full * etaTIA[1].eta_full)
etaB_LoGain_full = (DCPDs[1].WCgain[1].eta_full * etaTIA[1].eta_full)
etaA_HiGain_rdx = (DCPDs[0].WCgain[0].eta_rdx * etaTIA[0].eta_rdx)
etaA_LoGain_rdx = (DCPDs[0].WCgain[1].eta_rdx * etaTIA[0].eta_rdx)
etaB_HiGain_rdx = (DCPDs[1].WCgain[0].eta_rdx * etaTIA[1].eta_rdx)
etaB_LoGain_rdx = (DCPDs[1].WCgain[1].eta_rdx * etaTIA[1].eta_rdx)
etaA_org = (DCPDs[0].WCgain[0].eta_org * etaTIA[0].eta_org)
etaB_org = (DCPDs[1].WCgain[0].eta_org * etaTIA[1].eta_org)

eta_HiGain_full = (etaA_HiGain_full + etaB_HiGain_full)/2 
eta_HiGain_rdx = (etaA_HiGain_rdx + etaB_HiGain_rdx)/2
eta_LoGain_full = (etaA_LoGain_full + etaB_LoGain_full)/2
eta_LoGain_rdx = (etaA_LoGain_rdx + etaB_LoGain_rdx)/2
eta_org = (etaA_org + etaB_org)/2

freq = thisfreq

# ######################### #                                                                                                                                 
# REFERENCE DARM LOOP MODEL #                                                                                                                                 
# ######################### #                                                                                                                                 

C = pydarm.sensing.SensingModel(config)
A = pydarm.actuation.DARMActuationModel(config)
darm = pydarm.darm.DARMModel(config)

A_UIM_ref = A.xarm.compute_actuation_single_stage(freq,'UIM')
A_PUM_ref = A.xarm.compute_actuation_single_stage(freq,'PUM')
A_TST_ref = A.xarm.compute_actuation_single_stage(freq,'TST')
A_ref = A_UIM_ref + A_PUM_ref + A_TST_ref

D_ref = darm.digital.compute_response(freq)
C_ref = C.compute_sensing(freq)
R_ref = darm.compute_response_function(freq)

contri_C = (1.0/C_ref) / R_ref

# ##################### #                                                                                                                                     
# DARM MODEL WITH ERROR #                                                                                                                                     
# ##################### #                                                                                                                                     
errorModel = []

errortuple = namedtuple('errorModel',['WCgain','DCPD_wisyserror_full','DCPD_wisyserror_rdx','DCPD_wisyserror_org',
                                  'G_wipsyserror_full','G_wipsyserror_rdx', 'G_wipsyserror_org','etaR_full', 'etaR_rdx', 'etaR_org'])

eta_full = [eta_HiGain_full, eta_LoGain_full]
eta_rdx = [eta_HiGain_rdx, eta_LoGain_rdx]

for n,p,q in zip(WCgain,eta_full, eta_rdx):
    DCPD_wisyserror_full = C_ref * p
    DCPD_wisyserror_rdx = C_ref * q
    DCPD_wisyserror_org = C_ref * eta_org
    G_wipsyserror_full = A_ref*D_ref*DCPD_wisyserror_full
    G_wipsyserror_rdx = A_ref*D_ref*DCPD_wisyserror_rdx
    G_wipsyserror_org = A_ref*D_ref*DCPD_wisyserror_org
    G = A_ref*D_ref*C_ref
    etaR_full = (1+G_wipsyserror_full)/(p*(1+G))
    etaR_rdx = (1+G_wipsyserror_rdx)/(q*(1+G))
    etaR_org = (1+G_wipsyserror_org)/(eta_org*(1+G))
    thisErrorModel = errortuple(n, DCPD_wisyserror_full,DCPD_wisyserror_rdx,DCPD_wisyserror_org,
				G_wipsyserror_full,G_wipsyserror_rdx,G_wipsyserror_org,
                                etaR_full, etaR_rdx, etaR_org)
    errorModel.append(thisErrorModel)

    
# ######################### #                                                                                                                                          
#         FIGURES           #                                                                                                                                          
# ######################### # 


allfigures = []


plotFreqRange = [1e-1, 1.1e4]
xTicks = np.logspace(-1,4,6)

#Plotting \eta_C = (\eta_C|A + \eta_C|B)/2

fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)
s1.semilogx(freq,abs(eta_HiGain_full),label='HiGain Full')
s1.semilogx(freq,abs(eta_LoGain_full),label='LoGain Full')
s1.semilogx(freq,abs(eta_HiGain_rdx),label='HiGain Rdx', ls = '--')
s1.semilogx(freq,abs(eta_LoGain_rdx),label='LoGain Rdx', ls = '--')
s1.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.001))
s1.set_ylabel('Magnitude (Dimensionless)')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.set_xticks(xTicks)
s1.set_ylim(0.995,1.005)
s1.yaxis.set_major_locator(tck.MultipleLocator(0.001))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
s1.legend(ncol=1)
s2.semilogx(freq,180.0/np.pi*np.angle(eta_HiGain_full), label='HiGain Full')
s2.semilogx(freq,180.0/np.pi*np.angle(eta_LoGain_full), label='LoGain Full')
s2.semilogx(freq,180.0/np.pi*np.angle(eta_HiGain_rdx), label='HiGain Rdx', ls = '--')
s2.semilogx(freq,180.0/np.pi*np.angle(eta_LoGain_rdx), label='LoGain Rdx', ls = '--')
s2.set_ylabel('Phase')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.set_ylim(-4, 0.5)
s2.yaxis.set_major_locator(tck.MultipleLocator(0.5))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
s2.set_xticks(xTicks)
s2.legend(ncol=1)
thisfig = fignames(resultsDir3+measDate+ifo+fileTag1+fileTag2+'_eta_combined.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)



#Plotting \eta_R

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)
for i in range(len(errorModel)):
    custom_cycler = (cycler('linestyle',['--',':'])+cycler('color', ['red', 'green'])) #to mark full and rdx in diffferent linestyles for different gain
    s1.semilogx(freq,abs(errorModel[i].etaR_full),label=errorModel[i].WCgain+'Full')
    s1.semilogx(freq,abs(errorModel[i].etaR_rdx),label=errorModel[i].WCgain+'Rdx')
    s1.set_prop_cycle(custom_cycler)
    s1.set_ylabel('Magnitude (Dimensionless)')
    s1.grid(which='minor', ls='--')
    s1.set_ylim([0.95,1.05])
    s1.set_xlim(plotFreqRange)
    s1.set_xticks(xTicks)
    s1.legend(ncol=1)
    s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
    s2.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_full), label=errorModel[i].WCgain+'full')
    s2.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_rdx), label=errorModel[i].WCgain+'Rdx')
    s2.set_prop_cycle(custom_cycler)
    s2.set_ylabel('Phase')
    s2.grid(which='minor', ls='--')
    s2.set_xlim(plotFreqRange)
    s2.set_xticks(xTicks)
    s2.set_ylim(-5, 30)
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s2.legend(ncol=1)
    s3.semilogx(freq,abs(errorModel[i].etaR_full),label=errorModel[i].WCgain+'Full')
    s3.semilogx(freq,abs(errorModel[i].etaR_rdx),label=errorModel[i].WCgain+'Rdx')
    s3.set_ylabel('Magnitude (Dimensionless)')
    s3.grid(which='minor', ls='--')
    s3.set_ylim([0.997,1.003])
    s3.yaxis.set_major_locator(tck.MultipleLocator(0.0005))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0001))
    s3.set_xlim(plotFreqRange)
    s3.set_xticks(xTicks)
    s3.legend(ncol=1)
    s4.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_full), label=errorModel[i].WCgain+'full')
    s4.semilogx(freq,180.0/np.pi*np.angle(errorModel[i].etaR_rdx), label=errorModel[i].WCgain+'Rdx')
    s4.set_ylabel('Phase')
    s4.grid(which='minor', ls='--')
    s4.set_xlim(plotFreqRange)
    s4.set_ylim([-0.5,4])
    s4.set_xticks(xTicks)
    s4.yaxis.set_major_locator(tck.MultipleLocator(0.5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
    s4.legend(ncol=1)
thisfig = fignames(resultsDir3+measDate+ifo+fileTag1+fileTag2+'_etaR.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)


#Plotting \eta_C for TIA and Whitening Chassis together
#for DCPDA and DCPDB

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)


custom_cycler = (cycler('linestyle',['-',':'])+cycler('color', ['red', 'green'])) #to mark full and rdx in diffferent linestyles for different gain 
s1.semilogx(freq,abs(etaA_HiGain_full),label='DCPDA Hi Gain Full')
s1.semilogx(freq,abs(etaA_LoGain_full),label='DCPDA Lo Gain Full', ls = '--')
s1.semilogx(freq,abs(etaA_HiGain_rdx),label='DCPDA Hi Gain Rdx')
s1.semilogx(freq,abs(etaA_LoGain_rdx),label='DCPDA Lo Gain Rdx', ls = '--')
s1.set_ylabel('Magnitude (Dimensionless)')
s1.grid(which='minor', ls='--')
s1.set_ylim([0.995,1.005])
s1.set_xlim(plotFreqRange)
s1.set_xticks(xTicks)
s1.legend(ncol=1)
s1.set_prop_cycle(custom_cycler)
s1.yaxis.set_major_locator(tck.MultipleLocator(0.001))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
s2.semilogx(freq,180.0/np.pi*np.angle(etaA_HiGain_full), label='DCPDA HiGain Full')
s2.semilogx(freq,180.0/np.pi*np.angle(etaA_LoGain_full), label='DCPDA LoGain Full', ls = '--')
s2.semilogx(freq,180.0/np.pi*np.angle(etaA_HiGain_rdx), label='DCPDA HiGain Rdx')
s2.semilogx(freq,180.0/np.pi*np.angle(etaA_HiGain_rdx), label='DCPDA LoGain Rdx', ls = '--')
s2.set_ylabel('Phase')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.set_xticks(xTicks)
s2.set_ylim(-1.5, 1.5)
s2.set_prop_cycle(custom_cycler)
s2.yaxis.set_major_locator(tck.MultipleLocator(0.5))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
s2.legend(ncol=1)
s3.semilogx(freq,abs(etaB_HiGain_full),label='DCPDB HiGain Full')
s3.semilogx(freq,abs(etaB_LoGain_full),label='DCPDB LoGain Full', ls = '--')
s3.semilogx(freq,abs(etaB_HiGain_rdx),label='DCPDB HiGain Rdx')
s3.semilogx(freq,abs(etaB_LoGain_rdx),label='DCPDB LoGain Rdx', ls = '--')
s3.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.001))
s3.set_prop_cycle(custom_cycler)
s3.yaxis.set_major_locator(tck.MultipleLocator(0.001))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
s3.set_ylabel('Magnitude (Dimensionless)')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.set_xticks(xTicks)
s3.set_ylim(0.995,1.005)
s3.legend(ncol=1)
s4.semilogx(freq,180.0/np.pi*np.angle(etaB_HiGain_full), label='DCPDB HiGain Full')
s4.semilogx(freq,180.0/np.pi*np.angle(etaB_LoGain_full), label='DCPDB LoGain Full', ls = '--')
s4.semilogx(freq,180.0/np.pi*np.angle(etaB_HiGain_rdx), label='DCPDB HiGain Rdx')
s4.semilogx(freq,180.0/np.pi*np.angle(etaB_LoGain_rdx), label='DCPDB LoGain Rdx', ls = '--')
s4.set_prop_cycle(custom_cycler)
s4.set_ylabel('Phase')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.set_ylim(-1.5, 1.5)
s4.yaxis.set_major_locator(tck.MultipleLocator(0.5))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.1))
s4.set_xticks(xTicks)
s4.legend(ncol=1)
thisfig = fignames(resultsDir3+measDate+ifo+fileTag1+fileTag2+'_etaC_DCPDAandB.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)

#Plotting ratio of \eta_C for TIA and Whitening Chassis together                                                                                                       
#for DCPDA and DCPDB                                                                                                                                                   
plotFreqRange = [1, 2e3]


fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

custom_cycler = (cycler('linestyle',['-',':'])+cycler('color', ['red', 'green'])) #to mark full and rdx in diffferent linestyles for different gain                    
s1.semilogx(freq,20*np.log10(abs(etaB_HiGain_full)/abs(etaA_HiGain_full)),label='eta_C(DCPDB)/eta_C(DCPDA) Hi Gain Full')
s1.semilogx(freq,20*np.log10(abs(etaB_LoGain_full)/abs(etaA_LoGain_full)),label='eta_C(DCPDB)/eta_C(DCPDA) Lo Gain Full', ls = '--')
s1.semilogx(freq,20*np.log10(abs(etaB_HiGain_rdx)/abs(etaA_HiGain_rdx)),label='eta_C(DCPDB)/eta_C(DCPDA) Hi Gain Rdx')
s1.semilogx(freq,20*np.log10(abs(etaB_LoGain_rdx)/abs(etaA_LoGain_rdx)),label='eta_C(DCPDB)/eta_C(DCPDA) Lo Gain Rdx')
s1.set_ylabel('Magnitude (dB)')
s1.grid(which='minor', ls='--')
s1.set_ylim([-0.2,0.2])
s1.set_xlim(plotFreqRange)
s1.legend(ncol=1)
s1.set_prop_cycle(custom_cycler)
s1.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.01))
s2.semilogx(freq,180.0/np.pi*np.angle(etaB_HiGain_full/etaA_HiGain_full), label='eta_C(DCPDB)/eta_C(DCPDA) HiGain Full')
s2.semilogx(freq,180.0/np.pi*np.angle(etaB_LoGain_full/etaA_LoGain_full), label='eta_C(DCPDB)/eta_C(DCPDA) LoGain Full')
s2.semilogx(freq,180.0/np.pi*np.angle(etaB_HiGain_rdx/etaA_HiGain_rdx), label='eta_C(DCPDB)/eta_C(DCPDA) HiGain Rdx')
s2.semilogx(freq,180.0/np.pi*np.angle(etaB_LoGain_rdx/etaA_LoGain_rdx), label='eta_C(DCPDB)/eta_C(DCPDA) LoGain Rdx')
s2.set_ylabel('Phase')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.set_ylim(-0.8, 1.2)
s2.set_prop_cycle(custom_cycler)
s2.yaxis.set_major_locator(tck.MultipleLocator(0.2))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.05))
s2.legend(ncol=1)
thisfig = fignames(resultsDir3+measDate+ifo+fileTag1+fileTag2+'_etaC_DCPDBbyAratio.pdf')
plt.savefig(thisfig.filename,bbox_inches='tight')
allfigures.append(thisfig)
plt.close(fig)
