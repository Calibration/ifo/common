from collections import namedtuple
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
import scipy.signal as sig
import numpy as np
import wavestate.iirrational
import os
import sys
import h5py


#Making the plots look nicer!

plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (14,10),
                    'savefig.dpi': 100,
                    'pdf.compression': 9})


SVNROOT = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'
dataDir = SVNROOT+'Common/Electronics/H1/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20230306/Data/'
resultsDir = SVNROOT + 'Common/Electronics/H1/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20230306/Results/'
meas = "20230306_H1_OMCA_DCPDTIA_S2100832_SN02_"
meas = "20230306_H1_DCPDTransimpedanceAmp_OMCA_{chan_name}_1VSRC"


#Reading in the data for measurement set up from previous measurement (20220419)
meas_setup = os.path.join(
        SVNROOT,
        'Common/Electronics/H1/DCPDTransimpedanceAmp/OMCA/S2100832_SN02/20220419/Data',
        "20220419_H1_DCPDTransimpedanceAmp_OMCA_"
        )

freq, meassetup_mag = np.loadtxt(meas_setup+'MeasSetup_mag.TXT', delimiter='\t',usecols=(0,1),unpack=True)
freq, meassetup_pha_deg = np.loadtxt(meas_setup+'MeasSetup_pha.TXT', delimiter='\t',usecols=(0,1),unpack=True)
tf_meassetup = meassetup_mag * np.exp(1j*np.pi/180.0*meassetup_pha_deg)

DCPDName = ['DCPDA', 'DCPDB']

fitOrder = 4

results = namedtuple('results',['OMC_DCPD','freq','meas','fit','residual','zeros','poles','gain','fotonstring'])
fignames = namedtuple('fignames',['fname'])

summary = []
allresults = []
allfigures = []

for i in DCPDName:
    fname = os.path.join(dataDir, meas.format(chan_name=i)) # Reading in the data
    freq, mag = np.loadtxt(fname+'_mag.TXT',delimiter='\t',usecols=(0,1),unpack=True)
    freq, phase_deg = np.loadtxt(fname+'_pha.TXT',delimiter='\t',usecols=(0,1),unpack=True)
    tf_meas = mag*np.exp(1j*np.pi/180.0*phase_deg)/tf_meassetup
    selectVector = (freq > 0.1) & (freq < 12e4)
    delayMax=1e-3
    #the fit
    fit=wavestate.iirrational.v2.data2filter(
        data=tf_meas.reshape(-1),
        F_Hz=freq.reshape(-1),
        F_nyquist_Hz=None,
        order = fitOrder,
        delay_s_max=delayMax, # sec
        select = selectVector)
    tf_fit = fit.fitter.xfer_eval(freq)
    fotonString = fit.as_foton_str_ZPKsf()
    residual = tf_meas / tf_fit
    residual_mag = mag / abs(tf_fit)
    residual_pha = phase_deg - 180/np.pi*np.angle(tf_fit)
    fitZeros = -1 * np.array(fit.as_ZPKrep().zeros.fullplane)
    fitPoles = -1 * np.array(fit.as_ZPKrep().poles.fullplane)
    fitGain  = np.array(fit.as_ZPKrep().gain)
    print('DCPD:%s'%i)
    print('Fit Zeros: {} Hz'.format(np.array2string(fitZeros,precision=3)))
    print('Fit Poles: {} Hz'.format(np.array2string(fitPoles,precision=3)))
    print('Fit Gain: {}'.format(np.array2string(fitGain,precision=3)))
    #Plotting the fits and the residuals
    fig = plt.figure()
    s1 = fig.add_subplot(221)
    s2 = fig.add_subplot(223)
    s3 = fig.add_subplot(222)
    s4 = fig.add_subplot(224)
    s1.loglog(freq,mag,label='Data')
    s1.loglog(freq,abs(tf_fit),'--',label='Fit')
    s1.set_ylabel('Magnitude')
    s1.grid(which='minor', ls='--')
    s1.legend(ncol=2)
    s1.set_title(meas.format(chan_name=i))
    s2.semilogx(freq,phase_deg,label='Data')
    s2.semilogx(freq,180.0/np.pi*np.angle(tf_fit),'--',label='Fit')
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Phase (deg)')
    s2.grid(which='minor', ls='--')
    s2.set_title('Measurement setup TF divided out')
    s3.semilogx(freq,residual_mag)
    s3.set_ylim([0.990,1.010])
    s3.yaxis.set_major_locator(tck.MultipleLocator(0.0025))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(0.0005))
    s3.grid(which='minor',ls='--')
    s3.set_ylabel('abs(Data / Fit)')
    s4.semilogx(freq,residual_pha)
    s4.set_title('{}:{}:{}'.format(np.array2string(fitZeros,precision=3),\
                                np.array2string(fitPoles,precision=3),\
                                np.array2string(fitGain,precision=4)))
    #s4.grid(which='major',color='black')
    s4.grid(which='minor',ls='--')
    s4.set_ylabel('angle(Data / Fit) (deg)')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylim([-1.0,1.0])
    s4.yaxis.set_major_locator(tck.MultipleLocator(0.25))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(0.05))
    #Saving the figure
    thisfig = fignames(resultsDir+meas.format(chan_name=i)+"_FitResult.pdf")
    allfigures.append(thisfig)
    plt.savefig(thisfig.fname,bbox_inches='tight')
    plt.close()
    #Appending the data
    thisresult = results(i,freq,tf_meas,tf_fit,residual,fitZeros,fitPoles,fitGain,fotonString)
    allresults.append(thisresult)
    summary.append(thisresult)
    #Saving the measurement data
    hdf5FileName = resultsDir + meas.format(chan_name=i) + "_measData.hdf5"
    with h5py.File(hdf5FileName, 'w') as f:
        f.create_dataset('freq', data=freq)
        f.create_dataset('tf_meas', data=tf_meas)


#Plotting the sum and ratio
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

for n in range(0,len(allresults)):
    s1.loglog(allresults[n].freq,abs(allresults[n].meas),label=allresults[n].OMC_DCPD+' Data')
    #s1.loglog(allresults[n].freq,abs(allresults[n].fit),'--',label=allresults[n].OMC_DCPD+' Fit')
s1.set_ylabel('Magnitude')
s1.grid(which='minor', ls='--')
s1.legend(ncol=2)
s1.set_title('H1 OMCA DCPD Transimpedance')
for n in range(0,len(allresults)):
    s2.semilogx(allresults[n].freq,180.0/np.pi*np.angle(allresults[n].fit),label=allresults[n].OMC_DCPD+' Data')
    #s2.loglog(allresults[n].freq,abs(allresults[n].fit),'--',label=allresults[n].OMC_DCPD+' Fit')
s2.set_ylabel('Phase (deg)')
s2.grid(which='minor', ls='--')
s2.legend(ncol=2)
s2.set_title('Measurement set up TF divided out')
# s3.semilogx(allresults[0].freq, abs(allresults[0].meas+allresults[1].meas)/(2*LoadResistor), label = 'Sum/2*R1; R1 = 100000 Ohm')
s3.semilogx(allresults[0].freq, abs(allresults[0].meas)/abs(allresults[1].meas), label = 'A/B Ratio')
s3.set_ylim(0.5, 1.5)
s3.grid(which='minor', ls='--')
s3.legend(ncol=2)
# s4.semilogx(allresults[0].freq, (180.0/np.pi*np.angle(allresults[0].fit + allresults[1].fit)), label = 'Sum')
s4.semilogx(allresults[0].freq, (180.0/np.pi*np.angle(allresults[0].meas/allresults[1].meas)), label = 'A/B Ratio')
s4.grid(which='minor', ls='--')
s4.legend(ncol=2)
#Saving the figure                                                                                                                                           
thisfig = fignames(resultsDir+meas.format(chan_name=i)+"_Ratio.pdf")
allfigures.append(thisfig)
plt.savefig(thisfig.fname,bbox_inches='tight')
plt.close()        
