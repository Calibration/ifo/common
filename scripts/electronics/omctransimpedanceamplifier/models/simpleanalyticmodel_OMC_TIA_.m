close all
clear all
clc

set(0,'DefaultLineLineWidth',2) 
set(0,'DefaultLineMarkerSize',12) 
set(0,'DefaultAxesFontSize',12) 
set(0,'DefaultAxesXGrid','on') 
set(0,'DefaultAxesYGrid','on') 
set(0,'DefaultAxesZGrid','on') 

addpath('/ligo/svncommon/SusSVN/sus/trunk/Common/MatlabTools/')

%%
printFigs = 0;
resultsDir = '/Users/kissel/Desktop/';

%%
freq = logspace(-2,8,10000);
w = 2*pi*freq;

%% Test Path
R1 = 100e3;
Z_TST = R1*ones(1,length(freq));

%% RF Path
C1 = 4.7e-9;
MAR_6SM.R = 50;

Z_RF = MAR_6SM.R + 1./(1i*w*C1);

Z_RFTST_para = Z_TST .* Z_RF ./ (Z_TST + Z_RF);

%% AF Path
% All AF current goes through L2...
L2 = 82e-6;
Z_L2 = 1i*w*L2;

% But the current then gets divided again, between the DC path through L4,
% and the GW path through the Z_RC1 network 
L4.L = 2.6;
L4.R = 24; % From PDR T1900678, Section 2 Parenthetically in first sentence of Paragraph 2
Z_L4 = L4.R + 1i*w*L4.L;

% Z_RC1
R8 = 3.1e3;
Zupper_in1 = R8;

R11 = 100;
C11 = 10e-6;
C14 = 10e-6;
Cpara1 = C11 + C14;

Zlower_in1 = R11 + 1./(1i*w*(Cpara1));
% GW current goes this way,
Zpara_in1 = Zupper_in1 * Zlower_in1 ./ (Zupper_in1 + Zlower_in1);

% But all AF current goes through both GW and DC paths to ground
Z_para_in1_and_L4 = Zpara_in1 .* Z_L4 ./ (Zpara_in1 + Z_L4);

% And don't for get L2
Z_AF = Z_L2 + Z_para_in1_and_L4;

Z_AFTST_para = Z_AF .* Z_TST ./ (Z_AF + Z_TST);

% So the current divided between the RF path and the AF path is
PDIAF_over_PDIT = Z_RFTST_para ./ (Z_AF + Z_RFTST_para);
PDIRF_over_PDIT = Z_AFTST_para ./ (Z_AFTST_para + Z_RF);

%% GW Path
% Now that we have the AF portion of the total current,
% What's the GW portion of that AF current but just another current divider
PDIGW_over_PDIAF = Z_L4 ./ (Z_L4 + Zpara_in1);
PDIDC_over_PDIAF = Zpara_in1 ./ (Z_L4 + Zpara_in1);

%% Transimpedance: Turn that IGW current into a Voltage!
C6 = 1.5e-9;
R5 = 10e3;

Zpara_f1 = R5 * (1./(1i*w*C6)) ./ (R5 + 1./(1i*w*C6));

Vout1_over_PDIGW = - Zpara_f1;

%% 
R9 = 1e3;
Zupper_in2 = R9;

R12 = 250;
C12 = 10e-6;
C16 = 10e-6;
Cpara2 = C12 + C16;

Zlower_in2 = R12 + 1./(1i*w*(Cpara2));

Zpara_in2 = Zupper_in2 * Zlower_in2 ./ (Zupper_in2 + Zlower_in2);

R6 = 1e3;
C7 = 15e-9;

Zpara_f2 = R6 * 1./(1i*w*C7) ./ (R6 + 1./(1i*w*C7));

G_inv2 = - Zpara_f2 ./ Zpara_in2;

%%
G_DiffOut = 2 * ones(1,length(freq));

%%
VoutDIFF_over_IT = PDIAF_over_PDIT .* PDIGW_over_PDIAF .* Vout1_over_PDIGW .* G_inv2 .* G_DiffOut;

%%
figure(1)
subplot(211)
ll = loglog(freq(:),abs([PDIAF_over_PDIT(:)]));
set(ll(end),'Color','Black','LineStyle','--')
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([0.95 1.15])
set(gca,'XTick',10.^(-2:6),'YTick',0.95:0.05:1.15,'FontSize',16,'YMinorGrid','on')
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle([PDIAF_over_PDIT(:)])));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-10 0.5])
set(gca,'XTick',10.^(-2:6),'YTick',-10:1:1,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(2)
subplot(211)
ll = loglog(freq(:),abs(PDIAF_over_PDIT(:)));
set(ll(end),'Color','Black','LineStyle','--')                     
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([5e-3 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-3:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle(PDIAF_over_PDIT(:))));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(3)
subplot(211)
ll = loglog(freq(:),abs([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) ...
                         PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:)]));
set(ll(end),'Color','Black','LineStyle','--')                     
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'I_{GW} / I_{AF} [A/A]',...
             'Product: I_{GW} / I_{Total} [A/A]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([5e-3 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-3:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) ...
                                           PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:)])));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(4)
subplot(211)
ll = loglog(freq(:),abs([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) ...
                    PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:).*Vout1_over_PDIGW(:)]));
set(ll(end),'Color','Black','LineStyle','--')
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'I_{GW} / I_{AF} [A/A]',...
             'V_{1} / I _{GW} [V/A]',...
             'Product: V_{1} / I_{Total} [V/A]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([5e-3 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-3:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) ...
                                      PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:).*Vout1_over_PDIGW(:)])));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(5)
subplot(211)
ll = loglog(freq(:),abs([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) G_inv2(:)...
                         PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:).*Vout1_over_PDIGW(:).*G_inv2(:)]));
set(ll(end),'Color','Black','LineStyle','--')
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'I_{GW} / I_{AF} [A/A]',...
             'V_{1} / I _{GW} [V/A]',...
             'V_{2} / V_{1} [V/V]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([5e-3 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-3:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) G_inv2(:)...
                                           PDIAF_over_PDIT(:).*PDIGW_over_PDIAF(:).*Vout1_over_PDIGW(:).*G_inv2(:)])));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(6)
subplot(211)
ll = loglog(freq(:),abs([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) G_inv2(:) G_DiffOut(:) VoutDIFF_over_IT(:)]));
set(ll(end),'Color','Black','LineStyle','--')
leg = legend('I_{AF} / I_{Total} [A/A]',...
             'I_{GW} / I_{AF} [A/A]',...
             'V_{1} / I _{GW} [V/A]',...
             'V_{2} / V_{1} [V/V]',...
             'V_{out}^{DIFF} [V/V]',...
             'Product [V/A]',...
             'Location','NorthWest');
set(leg,'FontSize',8)
ylabel('Magnitude (see legend)')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([5e-3 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(-3:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Response Component Breakdown')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle([PDIAF_over_PDIT(:) PDIGW_over_PDIAF(:) Vout1_over_PDIGW(:) G_inv2(:) G_DiffOut(:) VoutDIFF_over_IT(:)])));
set(ll(end),'Color','Black','LineStyle','--')
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%%
figure(7)
subplot(211)
ll = loglog(freq(:),abs(VoutDIFF_over_IT(:)));
set(leg,'FontSize',8)
ylabel('Magnitude ([V_{out}^{DIFF} / I_{total}])')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([1e0 2e5])
set(gca,'XTick',10.^(-2:6),'YTick',10.^(0:5),'FontSize',16)
title('OMC DCPD Transimpedance Amplifier Total Response')
grid on;

subplot(212)
ll = semilogx(freq(:),180/pi*unwrap(angle(VoutDIFF_over_IT(:))));
ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
xlim([1e-2 1e6])
ylim([-275 185])
set(gca,'XTick',10.^(-2:6),'YTick',-270:45:180,'YMinorTick','on','YMinorGrid','on','FontSize',16)
grid on;

%% Investigate the current to all paths, and the impact of Z_TST
figure(20);
subplot(221)
ll = loglog(freq(:),abs([Z_TST(:) ...
                         Z_RF(:) ...
                         Z_AF(:) ...
                         Z_RFTST_para(:) ...
                         Z_AFTST_para(:) ...
                         PDIAF_over_PDIT(:) ...
                         PDIRF_over_PDIT(:)]));
set(ll([2 3]),'LineWidth',3,'LineStyle','--')
set(ll([6 7]),'LineWidth',3)
leg = legend('Z_{TST}',...
       'Z_{RF}',...
       'Z_{AF}',...
       'Z_{TST} || Z_{RF}',...
       'Z_{TST} || Z_{AF}',...
       'I_{AF} / I_{Total}',...
       'I_{RF} / I_{Total}',...
       'Location','SouthEast');
set(leg,'FontSize',6)   
xlabel('Frequency [Hz]')
ylabel('Magnitude ([V/A] or [A/A])')
xlim([1e-2 1e8])
ylim([1e-8 1e8])
set(gca,'XTick',10.^(-2:8),'YTick',10.^(-8:8))
title('OMC DCPD Transimpendance Amplifier')

subplot(223)
ll = semilogx(freq(:),180/pi*angle([Z_TST(:) ...
                                    Z_RF(:) ...
                                    Z_AF(:) ...
                                    Z_RFTST_para(:) ...
                                    Z_AFTST_para(:) ...
                                    PDIAF_over_PDIT(:) ...
                                    PDIRF_over_PDIT(:)]));
set(ll([2 3]),'LineWidth',3,'LineStyle','--')
xlabel('Frequency [Hz]')
ylabel('Phase [deg]')
xlim([1e-2 1e8])
ylim([-185 185])
set(gca,'XTick',10.^(-2:8),'YTick',-180:45:180,'YMinorGrid','ON','YMinorTick','ON')
title('Current Divider Component Breakdown')

subplot(222)
ll = semilogx(freq(:),abs([Z_TST(:) ...
                           Z_RF(:) ...
                           Z_AF(:) ...
                           Z_RFTST_para(:) ...
                           Z_AFTST_para(:) ...
                           PDIAF_over_PDIT(:) ...
                           PDIRF_over_PDIT(:)]));
set(ll([2 3]),'LineWidth',3,'LineStyle','--')
xlabel('Frequency [Hz]')
ylabel('Magnitude ([A/A])')
xlim([1e-1 1e4])
ylim([0.99 1.01])
set(gca,'XTick',10.^(-1:4),'YTick',0.99:0.002:1.01,'YMinorGrid','ON','YMinorTick','ON')
title('I_{AF} / I_{Total} Magnitude ZOOM')

subplot(224)
ll = semilogx(freq(:)/1e6,abs([Z_TST(:) ...
                           Z_RF(:) ...
                           Z_AF(:) ...
                           Z_RFTST_para(:) ...
                           Z_AFTST_para(:) ...
                           PDIAF_over_PDIT(:) ...
                           PDIRF_over_PDIT(:)]));
set(ll([2 3]),'LineWidth',3,'LineStyle','--')
xlabel('Frequency [MHz]')
ylabel('Magnitude ([A/A])')
xlim([1e4/1e6 1e7/1e6])
ylim([0.95 1.25])
set(gca,'XTick',10.^((4-6):(7-6)),'YTick',0.95:0.05:1.25,'YMinorGrid','ON','YMinorTick','ON')
title('I_{RF} / I_{Total} Magnitude ZOOM')

%%
if printFigs
    for iFig = 1:6
        figure(iFig)
        FillPage('w')
        IDfig
        saveas(gcf,[resultsDir 'simpleanalyticmodel_OMC_DCPD_TIA_Step' num2str(iFig) '.png'])
    end
    
    figure(7)
    FillPage('w')
    IDfig
    saveas(gcf,[resultsDir 'simpleanalyticmodel_OMC_DCPD_TIA_Final.png'])
    
    figure(20)
    FillPage('w')
    IDfig
    saveas(gcf,[resultsDir 'simpleanalyticmodel_OMC_DCPD_TIA_CurrentDividerComponents_ITotal.png'])
end
