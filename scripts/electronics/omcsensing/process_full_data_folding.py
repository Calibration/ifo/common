import numpy as np
from scipy.signal import welch, decimate
from matplotlib import pyplot as plt


avgs = 10
t_fft = 8
t_obs = t_fft * (1+(avgs-1)/2)
fs = 2**19
times = np.arange(0, t_obs, 1/2**19)


def read_dtt_ts(fp, avgs=avgs):
    data = np.loadtxt(fp)
    values = np.zeros((int(len(data) * (1+(avgs-1)/2))), dtype=np.float64)
    for n in range(avgs):
        if n == 0:
            values[:len(data)] = data[:, n+1]
        else:
            values[len(data)+int((n-1)*(len(data)/2)):len(data)+int(n*(len(data)/2))] = data[int(len(data)/2):len(data), n+1]

    return values


if __name__ == "__main__":
    values = read_dtt_ts('/ligo/svncommon/CalSVN/aligocalibration/trunk/Common/Electronics/H1/SensingFunction/OMCA/Data/2025-01-15_OMC_DCPD_B1TestChans_TimeSeries.txt')
    f, psd = welch(values, fs=fs, nperseg=t_fft*fs, average='mean')

    times2 = times[::8]
    values2 = values[::8]
    f2, psd2 = welch(values2, fs=fs/8, nperseg=t_fft*fs/8, average='mean')
    f3, psd3 = welch(decimate(values, 8), fs=fs/8, nperseg=t_fft*fs/8, average='mean')
    plt.figure()
    plt.loglog(f, np.sqrt(psd), linewidth=4, label='Raw')
    plt.loglog(f2, np.sqrt(psd2), linewidth=2, label='Decimate 8x, w/o AA')
    plt.loglog(f3, np.sqrt(psd3), linewidth=1, label='Decimate 8x, w/ AA')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('ASD')
    plt.grid()
    plt.legend()

    times3 = times[::32]
    values3 = values[::32]
    f4, psd4 = welch(values3, fs=fs/32, nperseg=t_fft*fs/32, average='mean')
    f5, psd5 = welch(decimate(decimate(values, 8), 4), fs=fs/32, nperseg=t_fft*fs/32, average='mean')
    plt.figure()
    plt.loglog(f, np.sqrt(psd), linewidth=4, label='Raw')
    plt.loglog(f4, np.sqrt(psd4), linewidth=2, label='Decimate 32x, w/o AA')
    plt.loglog(f5, np.sqrt(psd5), linewidth=1, label='Decimate 32x, w/ AA')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('ASD')
    plt.grid()
    plt.legend()

    plt.figure()
    plt.loglog(f4, psd4/psd[:len(psd4)])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('PSD ratio')
    plt.grid()

    plt.show()
