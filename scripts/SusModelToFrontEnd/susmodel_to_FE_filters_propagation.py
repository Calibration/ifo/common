# Refer to this diagram for what this file is:      https://dcc.ligo.org/LIGO-G1902134
# For specific help, read this:                     https://dcc.ligo.org/LIGO-T2000022
# Or contact:                                       Vladimir Bossilkov

import numpy as np
import scipy.io as spio
import scipy.signal as signal
import sys, os
import foton
import matplotlib.pyplot as plt

###########
# Constants
###########
IFO = os.environ['IFO']

# You *should* backup the old fitler files before writing the new ones. Just in case.
writeFilters = False

susModelPath = f'/ligo/groups/cal/{IFO}/arx/susmodels/'
filterfilepath = '/opt/rtcds/userapps/release/cal/l1/filterfiles/' if IFO == 'L1' else'/opt/rtcds/userapps/release/cal/h1/filterfiles/'

# Specify which susmodel files you are using here.
if IFO == 'H1':
    EX_file = '20200107_H1_EX_O3_susdata.mat'
    EY_file = '20200107_H1_EY_O3_susdata.mat'
elif IFO == 'L1':
    EX_file = '20230202_L1_EX_O3_susdata.mat'
    EY_file = '20230202_L1_EY_O3_susdata.mat'

cal_freq = 20.0
pcal_freq = 0.0
samplerate = 16384

# Tolerances of minreal function, and addition of whitening
# These need adjusting on an model-per-model basis.
if IFO == 'H1':
    tol_X = {
    'TST_LF_tolerance' : 2e-5,
    'PUM_LF_tolerance' : 2e-4,
    'PUM_viol_tolerance' : 1.4e-6,
    'UIM_LF_tolerance' : 4e-3,
    'UIM_HF_tolerance' : 7e-4,
    'UIM_viol_tolerance' : 1e-3}
    tol_Y = {
    'TST_LF_tolerance' : 1.25e-4,
    'PUM_LF_tolerance' : 1.5e-4,
    'PUM_viol_tolerance' : 1e-6,
    'UIM_LF_tolerance' : 4e-3,
    'UIM_HF_tolerance' : 7e-4,
    'UIM_viol_tolerance' : 1e-3}
elif IFO == 'L1':
    tol_X = {
    'TST_LF_tolerance' : 2e-5,
    'PUM_LF_tolerance' : 1e-4,
    'PUM_viol_tolerance' : 7.0e-5,
    'UIM_LF_tolerance' : 4e-3,
    'UIM_viol_tolerance' : 7e-5}

    tol_Y = {
    'TST_LF_tolerance' : 2e-5,
    'PUM_LF_tolerance' : 1e-4,
    'PUM_viol_tolerance' : 1e-3,
    'UIM_LF_tolerance' : 4e-3,
    'UIM_viol_tolerance' : 1.0e-3}


TST_whiteFreq = np.array([1,1], dtype=np.complex128) # 1 Hz (Poles) to remove 1/f^2 rolloff
PUM_whiteFreq = np.array([1,1,1,1], dtype=np.complex128)
UIM_whiteFreq = np.array([1,1,1,1,1,1], dtype=np.complex128)

# Max Cut of frequencies for LF, HF and viol sections
if IFO == 'H1':
    f_LF = 45
    f_PUM_viol = 1850
    f_UIM_HF = 134 
    f_UIM_viol = 1250
elif IFO == 'L1':
    f_LF = 45
    f_PUM_viol = 1850
    f_UIM_viol = 1850

maxQ = 1e3
# For Plotting
plotFRange = [0.01,2000]
fullfreq = np.geomspace(plotFRange[0], plotFRange[1], 10000)

###########
# Functions
###########

def minreal(z,p,k, tol=1e-9):
    """
    Minimum realisation of a zpk model. Similar implementation as MATLAB,
    excpet that I remove the aboslute smallest tolerance match, rather than the first,
    and moreover operates in Log10 space, so matching is actually much better.
    
    Shock, horror! This gives better residuals than matlab. Good riddence.
    """
    z = np.sort(z)
    p = np.sort(p)
    z_good = np.ones(len(z))
    for zi in range(len(z)):
        minimumInd = np.argmin(np.abs(np.log10(p) - np.log10(z[zi]))) # Technically, to make this "fast", this can stop as soon as the next one calculated is larger than the last, since  the list is sorted [PS needs to be sorted by absolute as by default it might sort wrong]
        minimum = np.abs(np.log10(p) - np.log10(z[zi]))[minimumInd]
        if minimum < tol:
            z_good[zi] = 0
            p = np.delete(p, minimumInd)
    z = z[(z_good == 1)]
    finalDCgain = gainAtFreq(z,p,k, 0.0)# Technicall, to handle TFs with differentiators or integrators, this should check 1 Hz. But ~1Hz is where we actually remove many poles and zeroes, so in our particular case, that;s not ideal.
    k /= finalDCgain # Effective gain is normalised to 1
    return z, p, k

def freqrespZPK(syst, w):
    s = s = 1j * np.complex128(w)
    numPoles = len(syst.poles)
    g = syst.gain * np.ones(len(w),dtype='complex128')
    
    sortedPolesIdx = np.argsort(abs(syst.poles))
    sortedZerosIdx = np.argsort(abs(syst.zeros))
    
    for i in range(0, numPoles):
        if i < len(syst.zeros):
            tmp = s - syst.zeros[sortedZerosIdx[i]]
        else:
            tmp = 1
        g = g * tmp / (s - syst.poles[sortedPolesIdx[i]])
    return g

def check(z, p, k, name):
    if (len(z) > 20 or len(p) > 20):
        print(f'\ntolerance variable needs to be increased \nYou have {len(z)} Zeros and {len(p)} Poles for {name}. \nYou must have <=20 each, for foton to generate <=10 SOS functions, the hardcoded limit.\n')
        sys.exit(1) #Don't really need to halt script here, as foton output will tell you what is wrong (too many SOS) - this is just handy because it tells you why this would go wrong
    else:
        print(f'\nYou have {len(z)} Zeros and {len(p)} Poles for {name}. \nYou should consider altering the tolerance variable \nto have as near to 20 for each as possible.\n')

def gainAtFreq(z,p,k, f_ref):
    '''
    This returns the gain at the specified reference frequency.
    '''
    mpN_f_reduced = np.abs(freqrespZPK(signal.ZerosPolesGain(z,p,k), [f_ref])[0])
    return mpN_f_reduced

def qReduce(z, p, k, max_allowed_Q):
    '''
    This function runs the subfunction qCut over the zeroes and poles to trim the Q to a maximim level.
    This front end prevents filters from ringing.
    '''
    def qCut(arr):
        for i in range(len(arr)):
            zp = arr[i]
            if np.isreal(zp):
                continue
            else:
                Q = -1*np.abs(zp) / (2 * np.real(zp))
                if Q > max_allowed_Q:
                    conj = -1*np.sign( np.imag(zp)) # gets sign of which conjugate pair this is. conj = 1 if imaginary component is negative. i.e. apply conjugate operation later.
                    new_zp =  -1*np.abs(zp) * np.exp(1j*np.arccos(1/(2*max_allowed_Q)))
                    arr[i] = np.conjugate(new_zp) if conj == 1 else new_zp
        return arr
    
    z = np.sort(z)
    p = np.sort(p)
    znew = qCut(z)
    pnew = qCut(p)
    return znew, pnew, k

########################
# Loop over the two arms
########################
armArray = [EX_file, EY_file]

for susmodelIndx in range(len(armArray)):
    arm = 'X' if susmodelIndx == 0 else 'Y'
    tolDict = eval(f'tol_{arm}')
    ###################
    # Import ZPK filter
    ###################
    
    quad = spio.loadmat(f'{susModelPath}{armArray[susmodelIndx]}')
    UIMz = quad['UIMz'].T[0]/(2*np.pi)
    UIMp = quad['UIMp'].T[0]/(2*np.pi)
    UIMk = quad['UIMk'].T[0][0]
    PUMz = quad['PUMz'].T[0]/(2*np.pi)
    PUMp = quad['PUMp'].T[0]/(2*np.pi)
    PUMk = quad['PUMk'].T[0][0]
    TSTz = quad['TSTz'].T[0]/(2*np.pi)
    TSTp = quad['TSTp'].T[0]/(2*np.pi)
    TSTk = quad['TSTk'].T[0][0]
    
    ###############################################################################
    # Normalisation coefficient of the original filter at Cal Line Frequency Region
    ###############################################################################
    
    #Whiten
    TSTz = np.concatenate((TSTz,-1*TST_whiteFreq),axis=0)
    # TSTk *= 1/np.prod(-1*2*np.pi*TST_whiteFreq)
    TSTk *= 1/(2*np.pi)**2
    PUMz = np.concatenate((PUMz,-1*PUM_whiteFreq),axis=0)
    # PUMk *= 1/np.prod(-1*2*np.pi*PUM_whiteFreq)
    PUMk *= 1/(2*np.pi)**4
    UIMz = np.concatenate((UIMz,-1*UIM_whiteFreq),axis=0)
    # UIMk *= 1/np.prod(-1*2*np.pi*UIM_whiteFreq)
    UIMk *= 1/(2*np.pi)**6
    
    mpN_f_ref_pcal= gainAtFreq(TSTz,TSTp,TSTk, pcal_freq)
    mpN_f_ref_TST = gainAtFreq(TSTz,TSTp,TSTk, cal_freq)
    mpN_f_ref_PUM = gainAtFreq(PUMz,PUMp,PUMk, cal_freq)
    mpN_f_ref_UIM = gainAtFreq(UIMz,UIMp,UIMk, cal_freq)
    
    ######################################################################
    # Produce a normalised, minimum realisation ZPK array to hand to foton
    ######################################################################
    
    #Break into small sections
    PUMz_LF = PUMz[np.abs(PUMz) < f_LF]
    PUMp_LF = PUMp[np.abs(PUMp) < f_LF]
    PUMz_viol = PUMz[(f_LF <= np.abs(PUMz)) & (np.abs(PUMz) < f_PUM_viol)]
    PUMp_viol = PUMp[(f_LF <= np.abs(PUMp)) & (np.abs(PUMp) < f_PUM_viol)]
    UIMz_LF = UIMz[np.abs(UIMz) < f_LF]
    UIMp_LF = UIMp[np.abs(UIMp) < f_LF]
    if IFO == 'H1':
        UIMz_HF = UIMz[(f_LF <= np.abs(UIMz)) & (np.abs(UIMz) < f_UIM_HF)]
        UIMp_HF = UIMp[(f_LF <= np.abs(UIMp)) & (np.abs(UIMp) < f_UIM_HF)]
        UIMz_viol = UIMz[(f_UIM_HF <= np.abs(UIMz)) & (np.abs(UIMz) < f_UIM_viol)]
        UIMp_viol = UIMp[(f_UIM_HF <= np.abs(UIMp)) & (np.abs(UIMp) < f_UIM_viol)]
    else:
        UIMz_viol = UIMz[(f_LF <= np.abs(UIMz)) & (np.abs(UIMz) < f_UIM_viol)]
        UIMp_viol = UIMp[(f_LF <= np.abs(UIMp)) & (np.abs(UIMp) < f_UIM_viol)]
    
    # Minreal
    zT, pT, kT =            minreal(TSTz,       TSTp,       np.sign(TSTk),  tol=tolDict['TST_LF_tolerance'])
    zTpcal, pTpcal, kTpcal =minreal(TSTz,       TSTp,       np.sign(TSTk),  tol=tolDict['TST_LF_tolerance'])
    zPlf, pPlf, kPlf =      minreal(PUMz_LF,    PUMp_LF,    np.sign(PUMk),  tol=tolDict['PUM_LF_tolerance'])
    zPv, pPv, kPv =         minreal(PUMz_viol,  PUMp_viol,  1,              tol=tolDict['PUM_viol_tolerance'])
    zUlf, pUlf, kUlf =      minreal(UIMz_LF,    UIMp_LF,    np.sign(UIMk),  tol=tolDict['UIM_LF_tolerance'])
    if IFO == 'H1':
        zUhf, pUhf, kUhf =  minreal(UIMz_HF,    UIMp_HF,    1,              tol=tolDict['UIM_HF_tolerance'])
    else:
        zUhf, pUhf, kUhf =  [], [], 1
    zUv, pUv, kUv =         minreal(UIMz_viol,  UIMp_viol,  1,              tol=tolDict['UIM_viol_tolerance'])
    
    # Print on screen the status of each block, w.r.t. whether foton will give an error - so you know why.
    check(zT,       pT,     kT,     f'TST_zpk E{arm}')
    check(zPlf,     pPlf,   kPlf,   f'PUM_LF_zpk E{arm}')
    check(zPv,      pPv,    kPv,    f'PUM_viol_zpk E{arm}')
    check(zUlf,     pUlf,   kUlf,   f'UIM_LF_zpk E{arm}')
    if IFO == 'H1':
        check(zUhf, pUhf,   kUhf,   f'UIM_HF_zpk E{arm}')
    check(zUv,      pUv,    kUv,    f'UIM_viol_zpk E{arm}')
    
    # Find correction gain required to restore response at target frequency.
    TST_k_norm =        gainAtFreq(    zT,                                      pT,                                         kT,             cal_freq)
    TST_k_pcal_norm =   gainAtFreq(    zTpcal,                                  pTpcal,                                     kTpcal,         pcal_freq)
    PUM_k_norm =        gainAtFreq(    np.concatenate((zPlf,zPv),axis=0),       np.concatenate((pPlf,pPv),axis=0),          kPlf*kPv,       cal_freq)
    UIM_k_norm =        gainAtFreq(    np.concatenate((zUlf,zUhf,zUv),axis=0),  np.concatenate((pUlf,pUhf,pUv),axis=0),     kUlf*kUhf*kUv,  cal_freq)
    
    # Violin Mode Q reduction
    zPv, pPv, kPv =     qReduce(zPv, pPv, kPv, maxQ)
    zUv, pUv, kUv =     qReduce(zUv, pUv, kUv, maxQ)
    
    ########################
    # Plot for sanity Checks
    ########################
    for sus in ['tst','pum','uim']:
        if sus == 'tst':
            original_white = signal.ZerosPolesGain(TSTz, TSTp, TSTk)
            title = 'TST original vs minreal comparison'
            susnorm_d_resp = freqrespZPK(signal.ZerosPolesGain(zT, pT, kT * mpN_f_ref_TST / TST_k_norm), fullfreq)
        elif sus == 'pum':
            original_white = signal.ZerosPolesGain(PUMz, PUMp, PUMk)
            title = 'PUM original vs minreal comparison'
            susnorm_d_resp = freqrespZPK(signal.ZerosPolesGain(np.concatenate((zPlf,zPv),axis=0), np.concatenate((pPlf,pPv),axis=0), kPlf * kPv * mpN_f_ref_PUM / PUM_k_norm), fullfreq)
        elif sus == 'uim':
            original_white = signal.ZerosPolesGain(UIMz, UIMp, UIMk)
            title = 'UIM original vs minreal comparison'
            susnorm_d_resp = freqrespZPK(signal.ZerosPolesGain(np.concatenate((zUlf,zUhf,zUv),axis=0), np.concatenate((pUlf,pUhf,pUv),axis=0), kUlf * kUv * kUhf * mpN_f_ref_UIM / UIM_k_norm), fullfreq)
        original_white_resp = freqrespZPK(original_white, fullfreq)
        
        # Plot the data against the fit output
        fig = plt.figure(figsize=(15, 10))
        s1 = fig.add_subplot(221)
        s2 = fig.add_subplot(223)
        s3 = fig.add_subplot(222)
        s4 = fig.add_subplot(224)
            
        # s1.loglog(fullfreq,abs(original_d_resp),'-',label='Original_discrete',zorder=1)
        s1.loglog(fullfreq,np.abs(original_white_resp),'-',label='Original',zorder=2)
        s1.loglog(fullfreq,np.abs(susnorm_d_resp),'--',label='Reduced',zorder=3)
        s1.set_ylabel('Magnitude')
        s1.grid(True,which='both')
        # s1.set_xticks(np.logspace(1,3,5))
        # s1.set_yticks(np.logspace(-26,-18,9))
        # s1.set_title('')
        s1.legend(ncol=2)
                
        # s2.semilogx(fullfreq,np.angle(original_d_resp, deg=True),'-',label='Original_discrete',zorder=1)
        s2.semilogx(fullfreq,np.angle(original_white_resp, deg=True),'-',label='Original',zorder=2)
        s2.semilogx(fullfreq,np.angle(susnorm_d_resp, deg=True),'--',label='Reduced',zorder=3)
        s2.set_xlabel('Frequency (Hz)')
        s2.set_ylabel('Phase (deg)')
        s2.grid(True,which='both')
        # s2.set_xticks(np.logspace(1,3,5))
        s2.set_ylim(-185,185)
        s2.set_yticks(range(-180,180+45,45))
        # s2.set_title('')
                
        s3.semilogx(fullfreq,np.abs(susnorm_d_resp/original_white_resp),'.')
        # s3.set_xticks(np.logspace(1,3,5))
        s3.set_ylim(0.99,1.02)
        s3.set_yticks(np.arange(0.98,1.02,0.005))
        if IFO =='H1' and sus == 'uim':
            s3.set_ylim(0.85,1.15)
            s3.set_yticks(np.arange(0.85,1.15,0.01))
        s3.set_ylabel('Magnitude (Minreal / Original)')
        s3.grid(True,which='both')
                
        s4.semilogx(fullfreq,np.angle(susnorm_d_resp/original_white_resp, deg=True),'.')
        # s4.set_xlim([0.005,5])
        # s4.set_xticks(np.logspace(1,3,5))
        s4.set_ylim(-2,2)
        s4.set_yticks(np.arange(-2,2,0.2))
        s4.set_ylabel('Phase (Minreal / Original) (deg)')
        s4.set_xlabel('Frequency (Hz)')
        s4.grid(True,which='both')
        
        plt.suptitle(f'ETM{arm} {title}')
        if sus == 'uim':
            plt.show()
    
    ####################
    # Write out to foton
    ####################
    
    LLObanksDict = {'TST_susnorm': 0, 'TST_1f2': 1, 'TST_mpN': 2, 'PUM_susnorm': 0, 'PUM_1f4': 1, 'PUM_mpN': 2, 'PUM_viol': 4, 'UIM_susnorm': 0, 'UIM_1f6': 1, 'UIM_mpN': 2, 'UIM_viol': 4}
    LHObanksDict = {'TST_susnorm': 0, 'TST_1f2': 9, 'TST_mpN': 5, 'PUM_susnorm': 0, 'PUM_1f4': 9, 'PUM_mpN': 5, 'PUM_viol': 4, 'UIM_susnorm': 0, 'UIM_1f6': 9, 'UIM_mpN': 5, 'UIM_viol': 4, 'UIM_HF': 2}
    
    if IFO == 'H1':
        banks = LHObanksDict
    elif IFO == 'L1':
        banks = LLObanksDict
    
    CALCSfilterfilepath= f'{filterfilepath}{IFO}CALCS.txt'
    PCALfilterfilepath= f'{filterfilepath}{IFO}CALE{arm}.txt'
    
    if writeFilters:
        ff = foton.FilterFile(CALCSfilterfilepath)
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_susnorm']].set_zpk(    zUlf,   pUlf,           kUlf/UIM_k_norm,    plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_susnorm']].name = 'susn_LF1'   # Note this is indexed 1, just in case you ever want to add more blocks into the spacre filter banks
        if IFO == 'H1':
            ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_HF']].set_zpk(     zUhf,   pUhf,           kUhf,               plane = 'f')
            ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_HF']].name = 'susn_HF1'    # Note this is indexed 1, just in case you ever want to add more blocks into the spacre filter banks
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_viol']].set_zpk(       zUv,    pUv,            kUv,                plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_viol']].name = 'susn_VM'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_1f6']].set_zpk(        [],     UIM_whiteFreq,  1,                  plane = 'n')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_1f6']].name = '1/f^6'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_mpN']].set_zpk(        [],     [],             mpN_f_ref_UIM,      plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L1'][banks['UIM_mpN']].name = 'mpN_20Hz'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_susnorm']].set_zpk(    zPlf,   pPlf,           kPlf/PUM_k_norm,    plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_susnorm']].name = 'susn_LF1'   # Note this is indexed 1, just in case you ever want to add more blocks into the spacre filter banks
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_viol']].set_zpk(       zPv,    pPv,            kPv,                plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_viol']].name = 'susn_VM'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_1f4']].set_zpk(        [],     PUM_whiteFreq,  1,                  plane = 'n')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_1f4']].name = '1/f^4'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_mpN']].set_zpk(        [],     [],             mpN_f_ref_PUM,      plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L2'][banks['PUM_mpN']].name = 'mpN_20Hz'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_susnorm']].set_zpk(    zT,     pT,             kT/TST_k_norm,      plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_susnorm']].name = 'susn_LF1'   # Note this is indexed 1, just in case you ever want to add more blocks into the spacre filter banks
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_1f2']].set_zpk(        [],     TST_whiteFreq,  1,                  plane = 'n')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_1f2']].name = '1/f^2'
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_mpN']].set_zpk(        [],     [],             mpN_f_ref_TST,      plane = 'f')
        ff[f'CS_DARM_ANALOG_ETM{arm}_L3'][banks['TST_mpN']].name = 'mpN_20Hz'
        ff.write()
        
        ff = foton.FilterFile(PCALfilterfilepath)
        ff[f'PCAL{arm}_RX_PD'][5].set_zpk(  zTpcal,     pTpcal,         kTpcal/TST_k_pcal_norm,     plane = 'f')
        ff[f'PCAL{arm}_RX_PD'][5].name = 'susnorm'
        ff[f'PCAL{arm}_RX_PD'][6].set_zpk(  [],         TST_whiteFreq,  1,                          plane = 'n')
        ff[f'PCAL{arm}_RX_PD'][6].name = ':1,1'
        ff[f'PCAL{arm}_RX_PD'][7].set_zpk(  [],         [],             mpN_f_ref_pcal,             plane = 'f')
        ff[f'PCAL{arm}_RX_PD'][7].name = 'mpN_DC'
        ff.write()
