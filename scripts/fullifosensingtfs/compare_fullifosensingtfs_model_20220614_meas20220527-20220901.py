from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner
import os
import sys

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

run = 'O3'
IFO = 'H1'
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')      


measDate = ['2022-05-25',
            '2022-06-02',
            '2022-07-22',
            '2022-08-03',
            '2022-09-01']   
            
verbose = True
printFigs = True

COHTHRESH_DARMOLGTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20220614.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

resultsDir = '{}/Runs/{}/{}/Results/FullIFOSensingTFs/'\
    .format(cal_data_root,\
        run,\
        IFO)       
        
anythingExtraInTheFigTag = ''

figTag ='sensingFunction_comparison_{}_proc{}_model{}_meas{}-{}'.format(IFO,
    todayDate,
    modelDate,
    measDate[0].replace('-',''),
    measDate[-1].replace('-',''),
    anythingExtraInTheFigTag)       
    
plotFreqRange = [4, 2e3]
modelfrequencies = np.logspace(0,4,250)
    
# Build the model:
# Make sure the model parameter set exists first
if not os.path.exists(model_parameters_file):
    print('The model parameter file')
    print('    {}'.format(model_parameters_file))
    print('does not exist. Check the file name, path, and whether path is up to date.')
    sys.exit()
#
C = pydarm.sensing.SensingModel(model_parameters_file)
A = pydarm.actuation.DARMActuationModel(model_parameters_file)
darm = pydarm.darm.DARMModel(model_parameters_file)   

fignames = namedtuple('fignames',['filename'])
results = namedtuple('results',['modelDate','measDate','frequencies','model_optical_response_tf','processed_optical_response_tf','processed_optical_response_unc','residual_tf','residual_unc'])

allresults = []
allfigures = []
for iDate, thisDate in enumerate(measDate):
    # These are the xml files we want to get our data from.
    darmolgtf_filename = \
        '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_DARM_OLGTF_LF_SS_5to1100Hz_15min.xml'\
        .format(cal_data_root,\
            run,\
            IFO,\
            thisDate,\
            IFO)
    pcaltf_filename = \
        '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCAL2DARMTF_LF_SS_5to1100Hz_10min.xml'\
        .format(cal_data_root,\
        run,\
        IFO,\
        thisDate,\
        IFO)
        
    darmolgtf_obj = pydarm.measurement.Measurement(darmolgtf_filename)
    pcaltf_obj  = pydarm.measurement.Measurement(pcaltf_filename)
        
    # Create a sensing function measurement object
    process_sensing_obj = pydarm.measurement.ProcessSensingMeasurement(
        model_parameters_file, darmolgtf_obj, pcaltf_obj,
        ('H1:LSC-DARM1_IN2', 'H1:LSC-DARM1_EXC'),
        ('H1:CAL-PCALY_RX_PD_OUT_DQ', 'H1:LSC-DARM_IN1_DQ'),
        COHTHRESH_DARMOLGTF, COHTHRESH_PCALTF)

    # Extract the processed sening function measurement, with C_res removed
    frequencies, processed_optical_response_tf, processed_optical_response_unc = \
        process_sensing_obj.get_processed_measurement_response()
        
        
    normalized_model_optical_response_tf = signal.freqresp(darm.sensing.optical_response(C.coupled_cavity_pole_frequency,
                                                                                         C.detuned_spring_frequency,
                                                                                         C.detuned_spring_q,
                                                                                         C.is_pro_spring),
                                                           2*np.pi*frequencies)[1]
    model_optical_response_tf = (C.coupled_cavity_optical_gain*normalized_model_optical_response_tf)
    
    residual_tf = processed_optical_response_tf/model_optical_response_tf
    residual_unc = processed_optical_response_unc
        
    allresults.append(results(modelDate,\
                              thisDate,\
                              frequencies,\
                              model_optical_response_tf,\
                              processed_optical_response_tf,\
                              processed_optical_response_unc,\
                              residual_tf,\
                              residual_unc))
                              
normalized_model_optical_response_tf = signal.freqresp(darm.sensing.optical_response(C.coupled_cavity_pole_frequency,
                                                                                         C.detuned_spring_frequency,
                                                                                         C.detuned_spring_q,
                                                                                         C.is_pro_spring),
                                                           2*np.pi*modelfrequencies)[1]
model_optical_response_tf = (C.coupled_cavity_optical_gain*normalized_model_optical_response_tf)

#One more model on its own broad frequency vector plotting alone
                          
expscale = int(np.floor(np.log10(C.coupled_cavity_optical_gain)))


fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.plot(modelfrequencies, abs(model_optical_response_tf)/10**expscale,label='{} Model'.format(modelDate),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s1.errorbar(allresults[iMeas].frequencies,\
                abs(allresults[iMeas].processed_optical_response_tf)/10**expscale,\
                yerr=allresults[iMeas].processed_optical_response_unc*abs(allresults[iMeas].processed_optical_response_tf)/10**expscale,\
                 fmt='.', label='{}'.format(thisMeas))
s1.set_ylabel('Magnitude (ct/m) x 10$^{{{}}}$'.format(expscale),usetex=True)
s1.set_xscale('log')
s1.set_yscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(0.5,10)
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend(ncol=1)


s2.plot(modelfrequencies, np.angle(model_optical_response_tf,deg=True),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s2.errorbar(allresults[iMeas].frequencies,\
                np.angle(allresults[iMeas].processed_optical_response_tf, deg=True),\
                yerr=allresults[iMeas].processed_optical_response_unc*(180.0/np.pi),\
                fmt='.')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-135,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))

for iMeas, thisMeas in enumerate(measDate):
    s3.errorbar(frequencies, \
                abs(allresults[iMeas].residual_tf),\
                yerr=allresults[iMeas].residual_unc, fmt='.',\
                label='{} / {} Model'.format(thisMeas,modelDate))
s3.set_ylabel('|meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.90,1.10])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))

for iMeas, thisMeas in enumerate(measDate):
    s4.errorbar(allresults[iMeas].frequencies, \
                np.angle(allresults[iMeas].residual_tf, deg=True),\
                yerr=allresults[iMeas].residual_unc*(180.0/np.pi),\
                fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase diff. (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,5])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

fig.suptitle('{} Sensing Function Reference Model Params: $H_C=${:.3e} ct/m, $f_{{cc}}=${:.1f} Hz, $f_s=${:.3} Hz, $Q$={:.3}'.format(IFO,\
                                                                                                                  C.coupled_cavity_optical_gain,\
                                                                                                                  C.coupled_cavity_pole_frequency,\
                                                                                                                  C.detuned_spring_frequency,\
                                                                                                                  C.detuned_spring_q),\
                                                                                                                  fontsize='x-large')

plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
        
    
        
