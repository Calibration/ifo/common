from mpl_toolkits.mplot3d import axes3d
from matplotlib import ticker as tck
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import cm
import numpy as np
from gwpy.time import tconvert

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })

#segmentgps = ['1367630118-1367641818',
#              '1367743278-1367748738',
#              '1367752758-1367762418']
segmentgps = ['1367752698-1367767098',
              '1368087318-1368099918',
              '1368161418-1368175818',
              '1368247638-1368262038',
              '1368384498-1368396018',
              '1368442818-1368457218',
              '1368559818-1368574218',
              '1368603918-1368618318',
              '1368632418-1368646818',
              '1368655218-1368669618']

for thisSeg in segmentgps:
    # dataDir = '/ligo/gitcommon/Calibration/ifo/scripts/fullifosensingtfs/'
    dataDir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/Measurements/FullIFOSensingTFs/Thermalization/'
    timetime = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_timetime.txt'.format(dataDir,thisSeg))
    freqfreq = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_freqfreq.txt'.format(dataDir,thisSeg))
    pwrpwr = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pwrpwr.txt'.format(dataDir,thisSeg))
    magmag = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_mag.txt'.format(dataDir,thisSeg))
    phapha = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pha.txt'.format(dataDir,thisSeg))
    
    startgps = str(round(timetime[0,0]))
    stopgps = str(round(timetime[-1,-1]))

    startutc = tconvert(timetime[0,0]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    endutc = tconvert(timetime[-1,-1]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    
    duration_hrs = (timetime[-1,-1] - timetime[0,0])/3600
    
    print('Processing data for {:.3f} hr segment spanning {} to {} ...'.format(duration_hrs,startutc,endutc))

    timetime = (timetime - timetime[0,0])/60+1

    time = timetime[0,:]
    freq = freqfreq[:,0]
    pwr = pwrpwr[0,:]

    columnStride = 1

    normalbode_pitch = 0
    normalbode_yaw = 0

    timeforward_pitch = 15
    timeforward_yaw = 80

    freqforward_pitch = 15
    freqforward_yaw = 5

    zlabelpad = 10
    width_pad = 0.1
    height_pad = 0.1
    edges_pad = 0.0

    timelim_min = [0, 250]
    timelim_lnmin = [0, 5.5]
    pwrlim = [390,440]
    freqlim = [5, 30]
    maglim = [0.1, 14]
    phalim = [-100, 100]
    zphalim = [-30, 30]

    titleTag = '\n(blue = {}; yellow = {}, Duration = {:.3f} hrs)'.format(startutc, endutc, duration_hrs)

    normalcolors = [cm.viridis(x) for x in np.linspace(0, 1, len(time))]

    norm = plt.Normalize(timetime.min(), timetime.max())
    colors = cm.viridis(norm(timetime))
    rowCount, columnCount, _ = colors.shape

    ###
    ### Replica of standard bode plot
    ###
    fig, (s1,s2) = plt.subplots(2,1,figsize=(12,8))

    for n, __ in enumerate(time):
        s1.plot(freqfreq[:,n], magmag[:,n],ls='-',marker='.', color=normalcolors[n])   
    s1.set_xlim(freqlim)
    s1.set_ylim(maglim)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.minorticks_on()
    s1.set_xlabel('Frequency (Hz)')
    s1.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.xaxis.set_major_locator(tck.MultipleLocator(5))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s1.yaxis.set_major_locator(tck.MultipleLocator(2))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

    for n, __ in enumerate(time):
        s2.plot(freqfreq[:,n], phapha[:,n],ls='-',marker='.', color=normalcolors[n])           
    s2.set_xlim(freqlim)
    s2.set_ylim(phalim)
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.minorticks_on()
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Phase Residual (deg)')
    s2.xaxis.set_major_locator(tck.MultipleLocator(5))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s2.yaxis.set_major_locator(tck.MultipleLocator(45))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
    plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)


    ###
    ### 3D Bode plot, frequency axis forward, linear in time
    ###
    fig, ((s1,s2), (s3,s4)) = plt.subplots(2,2,figsize=(12,8),subplot_kw={'projection':'3d'})

    ss1 = s1.plot_surface(timetime, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss1.set_facecolor((0,0,0,0))                       
    s1.view_init(freqforward_pitch,freqforward_yaw)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.set_xlabel('Time (min)')
    s1.set_ylabel('Frequency (Hz)')
    s1.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.set_xlim(timelim_min)
    s1.set_ylim(freqlim)
    s1.set_zlim(maglim)
    s1.xaxis.set_major_locator(tck.MultipleLocator(60))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s1.yaxis.set_major_locator(tck.MultipleLocator(5))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss3 = s3.plot_surface(timetime, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss3.set_facecolor((0,0,0,0))   
    s3.view_init(freqforward_pitch,freqforward_yaw)
    s3.set_xlabel('Time (min)')
    s3.set_ylabel('Frequency (Hz)')
    s3.set_zlabel('Phase Residual (deg)')    
    s3.set_xlim(timelim_min)
    s3.set_ylim(freqlim)
    s3.set_zlim(phalim)
    s3.xaxis.set_major_locator(tck.MultipleLocator(60))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.yaxis.set_major_locator(tck.MultipleLocator(5))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.zaxis.set_major_locator(tck.MultipleLocator(45))
    s3.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.set_title('3D Bode Plot: Frequency Axis Forward',fontsize=16)

    ss2 = s2.plot_surface(timetime, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss2.set_facecolor((0,0,0,0))   
    s2.view_init(freqforward_pitch,freqforward_yaw)
    s2.set_xlabel('Time (min)')
    s2.set_ylabel('Frequency (Hz)')
    s2.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s2.set_xlim(timelim_min)
    s2.set_ylim(freqlim)
    s2.set_zlim([1.0,1.25])
    s2.xaxis.set_major_locator(tck.MultipleLocator(60))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss4 = s4.plot_surface(timetime, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss4.set_facecolor((0,0,0,0))   
    s4.view_init(freqforward_pitch,freqforward_yaw)
    s4.set_xlabel('Time (min)')
    s4.set_ylabel('Frequency (Hz)')
    s4.set_zlabel('Phase Residual (deg)')
    s4.set_xlim(timelim_min)
    s4.set_ylim(freqlim)
    s4.set_zlim(zphalim)
    s4.xaxis.set_major_locator(tck.MultipleLocator(60))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.zaxis.set_major_locator(tck.MultipleLocator(15))
    s4.zaxis.set_minor_locator(tck.MultipleLocator(5))
    s4.set_title('3D Bode Plot: Frequency Axis Forward -- Zoomed Z Axes',fontsize=16)

    plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)

    plt.tight_layout(pad=edges_pad, w_pad=width_pad, h_pad=height_pad)


    ###
    ### 3D Bode plot, time axis forward, linear in time
    ###
    fig, ((s1,s2), (s3,s4)) = plt.subplots(2,2,figsize=(12,8),subplot_kw={'projection':'3d'})

    ss1 = s1.plot_surface(timetime, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss1.set_facecolor((0,0,0,0))                       
    s1.view_init(timeforward_pitch,timeforward_yaw)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.set_xlabel('Time (min)')
    s1.set_ylabel('Frequency (Hz)')
    s1.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.set_xlim(timelim_min)
    s1.set_ylim(freqlim)
    s1.set_zlim(maglim)
    s1.xaxis.set_major_locator(tck.MultipleLocator(60))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s1.yaxis.set_major_locator(tck.MultipleLocator(5))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss3 = s3.plot_surface(timetime, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss3.set_facecolor((0,0,0,0))   
    s3.view_init(timeforward_pitch,timeforward_yaw)
    s3.set_xlabel('Time (min)')
    s3.set_ylabel('Frequency (Hz)')
    s3.set_zlabel('Phase Residual (deg)')
    s3.set_xlim(timelim_min)
    s3.set_ylim(freqlim)
    s3.set_zlim(phalim)
    s3.xaxis.set_major_locator(tck.MultipleLocator(60))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.yaxis.set_major_locator(tck.MultipleLocator(5))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.zaxis.set_major_locator(tck.MultipleLocator(45))
    s3.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.set_title('3D Bode Plot: Time Axis Forward',fontsize=16)

    ss2 = s2.plot_surface(timetime, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss2.set_facecolor((0,0,0,0))   
    s2.view_init(timeforward_pitch,timeforward_yaw)
    s2.set_xlabel('Time (min)')
    s2.set_ylabel('Frequency (Hz)')
    s2.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s2.set_xlim(timelim_min)
    s2.set_ylim(freqlim)
    s2.set_zlim([1.0,1.25])
    s2.xaxis.set_major_locator(tck.MultipleLocator(60))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss4 = s4.plot_surface(timetime, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss4.set_facecolor((0,0,0,0))   
    s4.view_init(timeforward_pitch,timeforward_yaw)
    s4.set_xlabel('Time (min)')
    s4.set_ylabel('Frequency (Hz)')
    s4.set_zlabel('Phase Residual (deg)')
    s4.set_xlim(timelim_min)
    s4.set_ylim(freqlim)
    s4.set_zlim(zphalim)
    s4.xaxis.set_major_locator(tck.MultipleLocator(60))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.zaxis.set_major_locator(tck.MultipleLocator(15))
    s4.zaxis.set_minor_locator(tck.MultipleLocator(5))
    s4.set_title('3D Bode Plot: Time Axis Forward -- Zoomed Z Axes',fontsize=16)

    plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)

    plt.tight_layout(pad=edges_pad, w_pad=width_pad, h_pad=height_pad)

    ###
    ### 3D Bode plot, power forward, linear in power
    ###
    fig, ((s1,s2), (s3,s4)) = plt.subplots(2,2,figsize=(12,8),subplot_kw={'projection':'3d'})

    ss1 = s1.plot_surface(pwrpwr, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss1.set_facecolor((0,0,0,0))                       
    s1.view_init(timeforward_pitch,timeforward_yaw)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.set_xlabel('Power (kW)')
    s1.set_ylabel('Frequency (Hz)')
    s1.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.set_xlim(pwrlim)
    s1.set_ylim(freqlim)
    s1.set_zlim(maglim)
    s1.xaxis.set_major_locator(tck.MultipleLocator(10))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s1.yaxis.set_major_locator(tck.MultipleLocator(5))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss3 = s3.plot_surface(pwrpwr, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss3.set_facecolor((0,0,0,0))   
    s3.view_init(timeforward_pitch,timeforward_yaw)
    s3.set_xlabel('Power (kW)')
    s3.set_ylabel('Frequency (Hz)')
    s3.set_zlabel('Phase Residual (deg)')
    s3.set_xlim(pwrlim)
    s3.set_ylim(freqlim)
    s3.set_zlim(phalim)
    s3.xaxis.set_major_locator(tck.MultipleLocator(10))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.yaxis.set_major_locator(tck.MultipleLocator(5))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.zaxis.set_major_locator(tck.MultipleLocator(45))
    s3.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.set_title('3D Bode Plot: Power Axis Forward',fontsize=16)

    ss2 = s2.plot_surface(pwrpwr, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss2.set_facecolor((0,0,0,0))   
    s2.view_init(timeforward_pitch,timeforward_yaw)
    s2.set_xlabel('Power (kW)')
    s2.set_ylabel('Frequency (Hz)')
    s2.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s2.set_xlim(pwrlim)
    s2.set_ylim(freqlim)
    s2.set_zlim([1.0,1.25])
    s2.xaxis.set_major_locator(tck.MultipleLocator(10))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss4 = s4.plot_surface(pwrpwr, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss4.set_facecolor((0,0,0,0))   
    s4.view_init(timeforward_pitch,timeforward_yaw)
    s4.set_xlabel('Power (kW)')
    s4.set_ylabel('Frequency (Hz)')
    s4.set_zlabel('Phase Residual (deg)')
    s4.set_xlim(pwrlim)
    s4.set_ylim(freqlim)
    s4.set_zlim(zphalim)
    s4.xaxis.set_major_locator(tck.MultipleLocator(10))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.zaxis.set_major_locator(tck.MultipleLocator(15))
    s4.zaxis.set_minor_locator(tck.MultipleLocator(5))
    s4.set_title('3D Bode Plot: Power Axis Forward -- Zoomed Z Axes',fontsize=16)

    plt.suptitle('H1 Sensing Function Ratio vs Power: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)

    plt.tight_layout(pad=edges_pad, w_pad=width_pad, h_pad=height_pad)

    ###
    ### 3D Bode plot, time axis forward, natural logarithmic in time
    ###
    fig, ((s1,s2), (s3,s4)) = plt.subplots(2,2,figsize=(12,8),subplot_kw={'projection':'3d'})

    ss1 = s1.plot_surface(np.log(timetime), freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss1.set_facecolor((0,0,0,0))                       
    s1.view_init(timeforward_pitch,timeforward_yaw)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.set_xlabel('Time ln(min)')
    s1.set_ylabel('Frequency (Hz)')
    s1.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.set_xlim(timelim_lnmin)
    s1.set_ylim(freqlim)
    s1.set_zlim(maglim)
    s1.yaxis.set_major_locator(tck.MultipleLocator(5))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss3 = s3.plot_surface(np.log(timetime), freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss3.set_facecolor((0,0,0,0))   
    s3.view_init(timeforward_pitch,timeforward_yaw)
    s3.set_xlabel('Time ln(min)')
    s3.set_ylabel('Frequency (Hz)')
    s3.set_zlabel('Phase Residual (deg)')
    s3.set_xlim(timelim_lnmin)
    s3.set_ylim(freqlim)
    s3.set_zlim(phalim)
    s3.yaxis.set_major_locator(tck.MultipleLocator(5))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.zaxis.set_major_locator(tck.MultipleLocator(45))
    s3.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.set_title('3D Bode Plot: Natural Log-based Time Axis Forward',fontsize=16)

    ss2 = s2.plot_surface(np.log(timetime), freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss2.set_facecolor((0,0,0,0))   
    s2.view_init(timeforward_pitch,timeforward_yaw)
    s2.set_xlabel('Time ln(min)')
    s2.set_ylabel('Frequency (Hz)')
    s2.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s2.set_xlim(timelim_lnmin)
    s2.set_ylim(freqlim)
    s2.set_zlim([1.0,1.25])
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss4 = s4.plot_surface(np.log(timetime), freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss4.set_facecolor((0,0,0,0))   
    s4.view_init(timeforward_pitch,timeforward_yaw)
    s4.set_xlabel('Time ln(min)')
    s4.set_ylabel('Frequency (Hz)')
    s4.set_zlabel('Phase Residual (deg)')
    s4.set_xlim(timelim_lnmin)
    s4.set_ylim(freqlim)
    s4.set_zlim(zphalim)
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.zaxis.set_major_locator(tck.MultipleLocator(15))
    s4.zaxis.set_minor_locator(tck.MultipleLocator(5))
    s4.set_title('3D Bode Plot: Natural Log-based Time Axis Forward\nZoomed Z Axes',fontsize=16)

    plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)

    plt.tight_layout(pad=edges_pad, w_pad=width_pad, h_pad=height_pad)

    pp = PdfPages('sensingFunctionResidual_meas_over_model_wTDCFs_'+startgps+'-'+stopgps+'.pdf')
    figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf')
    pp.close()
    plt.close('all')
    

print('    Done!')
