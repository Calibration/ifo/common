from pydarm.cmd.measurement_set import MeasurementSet
from pydarm.measurement import Measurement, ProcessSensingMeasurement
from pydarm.plot import BodePlot
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

import matplotlib.ticker as tck
import json
import numpy as np
from scipy import signal
import os
import sys
import logging


log_format = "%(asctime)s::%(levelname)s::%(name)s::%(message)s"
ff = logging.Formatter(log_format)
# fh = logging.FileHandler("/ligo/home/louis.dartez/projects/20230410/srcl_offset_measurement/process_log.txt")
fh = logging.FileHandler(os.path.join(os.getcwd(),"process_log.txt"))
sh = logging.StreamHandler(sys.stderr)
fh.setFormatter(ff)

colors = list(mcolors.TABLEAU_COLORS)
#colors = ['red','orange','yellow','green','mediumaquamarine','blue','purple','black', 'cyan']
plt.rcParams.update({'text.usetex': False,
                     'lines.linewidth': 1.1,
                     'font.family': 'sans-serif',
                     'font.serif': 'Helvetica',
                     'font.size': 16,
                     'xtick.labelsize': 'x-large',
                     'ytick.labelsize': 'x-large',
                     'axes.labelsize': 'x-large',
                     'axes.titlesize': 'x-large',
                     'axes.grid': True,
                     'grid.alpha': 0.5,
                     'lines.markersize': 12,
                     'legend.borderpad': 0.2,
                     'legend.fancybox': True,
                     'legend.fontsize': 'large',
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.1,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'figure.figsize': (15, 12),
                     'savefig.dpi': 100,
                     'pdf.compression': 9,
                     'pdf.fonttype': 42,
                     })


pcal_rx_pd_chan_name = "H1:CAL-PCALY_RX_PD_OUT_DQ"
deltal_ext_chan_name = "H1:CAL-DELTAL_EXTERNAL_DQ"
# param_modelfile_20230125 = "/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20230125.ini"
param_modelfile = "/ligo/groups/cal/ifo/H1/pydarm_H1.ini"
DATA_ROOT = "/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs"
results_dir = "/ligo/home/elenna.capote/DARM_SPRING"
class Sensing_Bundle:
    def __init__(self, name,
                 olg_xml_path,
                 pcal2darm_xml_path,
                 data_root=DATA_ROOT):
        self.name = name
        # self.paramfile = param_modelfile_20230125
        self.paramfile = param_modelfile
        self.olg_measurement = Measurement(os.path.join(data_root, olg_xml_path))
        self.p2d_measurement = Measurement(os.path.join(data_root, pcal2darm_xml_path))
        self.pcal_rx_pd_chan_name = "H1:CAL-PCALY_RX_PD_OUT_DQ"
        self.darm_exc_chan_name = "H1:LSC-DARM1_EXC"
        self.darm_in2_chan_name = "H1:LSC-DARM1_IN2"
        self.darm_in1_chan_name = "H1:LSC-DARM_IN1_DQ"
        self.processed_measurement = \
            ProcessSensingMeasurement(param_modelfile,
                                      self.olg_measurement,
                                      self.p2d_measurement,
                                      (self.darm_in2_chan_name,
                                       self.darm_exc_chan_name),
                                      (self.pcal_rx_pd_chan_name,
                                       self.darm_in1_chan_name),
                                      0.75, 0.75)



def get_latest_mcmc_entry(fpath):
    with open(fpath) as f:
        json_contents = json.load(f)
    latest_job = json_contents[sorted(list(json_contents.keys()))[-1]]
    mcmc_map_vals = latest_job['mcmc_map_vals']
    fmin, fmax = float(latest_job['fmin']), float(latest_job['fmax'])
    return (mcmc_map_vals, (fmin, fmax))


def scale_optical_response(freqs, opt_resp_norm, MAP):
    MAP_opticalGain_ct_p_m = MAP[0]
    MAP_residualTimeDelay_sec = MAP[4]
    return (MAP_opticalGain_ct_p_m * opt_resp_norm *
            np.exp(-2*np.pi*1j*MAP_residualTimeDelay_sec*freqs*1e-6))


def get_optical_response_modeled(mset, mapv, freqs):
    norm_resp = signal.freqresp(mset.model.sensing.optical_response(
        mapv[1], mapv[2], 1/mapv[3], mset.model.sensing.is_pro_spring),
        2*np.pi*freqs)[1]
    return scale_optical_response(freqs, norm_resp, mapv)


def test_plot_sensing():
    measurements = [Sensing_Bundle(*x) for x in [
        # ("-250 ct / -0.761 deg / -2.25 nm",
        #  "2023-02-24_2222UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2230UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-200 ct / -0.609 deg / -1.80 nm",
        #  "2023-02-24_2159UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2207UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-100 ct / -0.305 deg / -0.90 nm",
        #  "2023-02-24_2109UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2118UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("  -50 ct / -0.152 deg / -0.45 nm ",
        #  "2023-02-24_2047UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2055UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("  -25 ct / -0.076 deg / -0.225 nm",
        #  "2023-02-24_2024UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2032UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("      0 ct / +0.000 deg / 0.0 nm",
        #  "2023-02-24_1907UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_1924UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # (" +25 ct / +0.076 deg / +0.225 nm",
        #  "2023-02-24_1936UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_1944UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # (" +50 ct / +0.152 deg / +0.405 nm",
        #  "2023-02-24_2000UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-02-24_2008UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # new data 2023-04-10
        # ("SRCL offset: +100 ct 20230410",
        #  "2023-04-10_1904UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-10_1921UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-175 ct 20230410",
        #  "2023-04-10_1939UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-10_1944UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-160 ct 20230410",
        #  "2023-04-10_1950UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-10_1954UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-150 ct 20230410",
        #  "2023-04-10_1959UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-10_2001UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        #("-170 ct 20230410",
        ## "2023-04-10_2004UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        # "2023-04-10_2006UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        #("-165 ct 20230410",
        # "2023-04-10_2010UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        # "2023-04-10_2012UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        #("SRCL offset: -165 ct 20230410",
        # "2023-04-10_2015UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        # "2023-04-10_2025UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml"),
        # ("-200 ct 20230410",
        #  "2023-04-10_2039UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-10_2042UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml")
        # ("20230410T205525Z",
        #  "DARMOLG_SS/DARMOLG_SS_20230410T155018Z.xml",
        #  "PCALY2DARM_SS/PCALY2DARM_SS_20230410T171645Z.xml",
        #  "/ligo/groups/cal/data/H1/measurements"),
        # ("20230410T205525Z",
        #  "DARMOLG_SS/DARMOLG_SS_20230410T155018Z.xml",
        #  "PCALY2DARM_SS/PCALY2DARM_SS_20230410T171645Z.xml",
        #  "/ligo/groups/cal/data/H1/measurements")
        # ("SRCL offset: -165 20230413",
        #  "DARMOLG_SS/DARMOLG_SS_20230413T205629Z.xml",
        #  "PCALY2DARM_SS/PCALY2DARM_SS_20230413T203912Z.xml",
        #  "/ligo/groups/cal/data/H1/measurements"),
        # ("TJ",
        #  "DARMOLG_SS/DARMOLG_SS_20230414T195338Z.xml",
        #  "PCALY2DARM_SS/PCALY2DARM_SS_20230414T193621Z.xml"),
        #  "/ligo/groups/cal/data/H1/measurements"),
        # ("after ETMX RH",
        #  "2023-04-14_0626UTC_H1_DARM_OLGTF_LF_SS_5to70Hz_8min.xml",
        #  "2023-04-14_0628UTC_H1_PCAL2DARMTF_LF_SS_5to70Hz_9min.xml")
        #("20230419, CO2s 1.7W",
        # "DARMOLG_SS/DARMOLG_SS_20230420T014029Z.xml",
        # "PCALY2DARM_SS/PCALY2DARM_SS_20230420T012314Z.xml",
        # "/ligo/groups/cal/data/H1/measurements"),
        # ('20230424, CO2X 2.2W',
        #  '2023-04-24_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
        #  '2023-04-24_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
        #  '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
        #  ('20230425, CO2X 2.2W',
        #   '2023-04-25_0356_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
        #   '2023-04-25_0405_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
        #   '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
           ('20230425, CO2X 4W',
            '2023-04-25_dan_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
            '2023-04-25_dan_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
            '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
            ('20230425, Thermalization test 1, SRCL -165',
             '2023-04-25_2234_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
             '2023-04-25_2234_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
             '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
             ('20230425, Thermalization test 2, SRCL 0',
              '2023-04-25_2249_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
              '2023-04-25_2249_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
              '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
              ('20230425, Thermalization test 3, SRCL -80',
               '2023-04-25_2323_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
               '2023-04-25_2323_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
               '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
               ('20230425, Thermalization test 4, SRCL -50',
                '2023-04-25_2350_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
                '2023-04-25_2350_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
                '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs'),
                ('2023-04-26, Thermalization test 5, SRCL -100',
                 '2023-04-26_0007_H1DARM_OLGTF_5to23Hz_TCS_Changes.xml',
                 '2023-04-26_0007_H1_PCAL2DARM_TF_5to23Hz_TCS_Changes.xml',
                 '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs')
    ]]

    fig = plt.figure()
    fig.suptitle("Sensing Function Measurements (04-10-2023)",
                 fontsize='x-large')
    bp = BodePlot(fig=fig, spspec=[211, 212], title='2023-04-10 H1 Sensing Function Measurements vs. SRCL Offset')
    # load sensing measurement
    # mset = MeasurementSet()
    # mset.load_model()
    # freqs, resp, _ = \
    #     mset.processed_measurements['Sensing']\
    #     .get_processed_measurement_response()

    # exp_scale = np.floor(np.log10(np.median(np.abs(resp))))
    # bp.plot(freqs, resp*10**(-1*exp_scale), marker='.', linestyle='',
    #              c=colors[0], label=mset.id)

    # iterate over models
    for i, meas_bundle in enumerate(measurements):
        freqs, resp, resp_unc = meas_bundle.processed_measurement.get_processed_measurement_response()
        if i==0:
            exp_scale = np.floor(np.log10(np.mean(np.abs(resp))))
        bp.error(freqs, resp/10**(exp_scale), resp_unc,
                 color=colors[i],
                 marker='.',
                 label=meas_bundle.name)
        bp.ax_mag.set_xscale('log')
        bp.ax_mag.grid(which='major', color='black')
        bp.ax_mag.grid(which='minor', ls='--')
        bp.ax_mag.set_ylabel(f"Magnitude (m/ct) x$10^{int(exp_scale):d}$")
        bp.ax_mag.yaxis.set_major_locator(tck.LogLocator(base=10))
        bp.ax_phase.yaxis.set_major_locator(tck.MultipleLocator(15))
        bp.ax_phase.set_xscale('log')
        bp.ax_phase.grid(which='major', color='black')
        bp.ax_phase.grid(which='minor', ls='--')
        bp.ax_phase.xaxis.set_major_locator(tck.LogLocator(base=10))
        bp.ax_phase.yaxis.set_minor_locator(tck.MultipleLocator(5))
        bp.ax_phase.set_ylim(-90, 45)
    bp.legend()
    outfile = os.path.join(results_dir, 'sensing.pdf')
    fig.savefig(outfile)
    print(f"saved {outfile}")
    
if __name__ == "__main__":
    test_plot_sensing()
