from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner
import glob
import os
import sys

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

run = 'O3'
IFO = 'H1'
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')      

# Default is 1e3, 9e3. That takes ~20 minutes on Jeff's laptop. No thanks.
MCMC_PARAMS_BURNINSTEPS = 100;
MCMC_PARAMS_STEPS = 900;

REFMEASINDEX = 0 # 20221118 model uses MCMC results from 2022-05-25 measurement
REFMEASINDEX = 1 # #FIXME HOWEVER, even though we never intend to use a detuned optical spring again, MCMC results from 2022-05-25 have a spring with high f_s and Q, and GPR uses this reference measurement's MCMC results to generate the "reference model" rather than actually using the reference model. So, for now, let's try using the 2022-06-02 measurement, which has a similar H_c and f_cc as the reference model, but has f_s at ~0 Hz. See pydarm Issue 167. 

measDate = ['2022-05-25',
            '2022-06-02',
            '2022-07-22',
            '2022-08-03',
            '2022-09-01',
            '2022-11-23_2129UTC',
            '2022-11-23_2243UTC']   
            
mcmc_params_fitregion_Hz = [[80,1.2e3],
                            [80,1.2e3],
                            [80,1.2e3],
                            [80,1.2e3],
                            [80,1.2e3],
                            [80,1.2e3],
                            [80,1.2e3]]
                            
GPR_PROJECTION_FREQ = np.logspace(np.log10(1), np.log10(5000), 100)
GPR_PARAMS_FITREGION_HZ = [30,1100]
GPR_RBF_LENGTHSCALE_PRIOR = 0.35
GPR_RBF_LENGTHSCALE_RANGE_PRIOR = (0.33, 0.37)
            
verbose = True
printFigs = True

COHTHRESH_DARMOLGTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

resultsDir = '{}/Runs/{}/{}/Results/FullIFOSensingTFs/'\
    .format(cal_data_root,\
        run,\
        IFO)       
        
anythingExtraInTheFigTag = ''

figTag ='sensingFunction_comparison_{}_proc{}_model{}_meas{}-{}'.format(IFO,
    todayDate,
    modelDate,
    measDate[0].replace('-',''),
    measDate[-1].replace('-',''),
    anythingExtraInTheFigTag)       
    
MCMC_JSONRESULTSCOLLECTIONFILE = '{}/{}_mcmcresults.json'.format(resultsDir,figTag)

GPR_RESULTSFILE = '{}/{}_gprresults.hdf5'.format(resultsDir,figTag)
    
plotFreqRange = [4, 2e3]
modelfrequencies = np.logspace(0,4,250)
    
# Build the model:
# Make sure the model parameter set exists first
if not os.path.exists(model_parameters_file):
    print('The model parameter file')
    print('    {}'.format(model_parameters_file))
    print('does not exist. Check the file name, path, and whether path is up to date.')
    sys.exit()
#
C = pydarm.sensing.SensingModel(model_parameters_file)
A = pydarm.actuation.DARMActuationModel(model_parameters_file)
darm = pydarm.darm.DARMModel(model_parameters_file)   

fignames = namedtuple('fignames',['filename'])
results = namedtuple('results',['modelDate','measDate','frequencies','model_optical_response_tf','processed_optical_response_tf','processed_optical_response_unc','residual_tf','residual_unc'])

allfigures = []
allunstackedresults = []
allunstackedsensingobjects = []

print('    ')
print('Starting post-processing and MCMC fitting of measDate collection:')
print('    {}'.format(measDate))
print('    ...')
############################
#         MCMC FIT         #
############################

# Look for pre-processed MCMC fits of this particular collection of data
oldMCMCJSONRESULTSCOLLECTIONSearchString = MCMC_JSONRESULTSCOLLECTIONFILE.replace(todayDate,'*')
oldMCMCJSONRESULTSCOLLECTIONFileNameMatch = sorted(glob.glob(oldMCMCJSONRESULTSCOLLECTIONSearchString))

if oldMCMCJSONRESULTSCOLLECTIONFileNameMatch:
    oldMCMCJSONRESULTSCOLLECTIONprocDate = oldMCMCJSONRESULTSCOLLECTIONFileNameMatch[-1].split('/')[-1].split('proc')[-1].split('_')[0]
    print('    ')
    print('A .json record of MCMC fit results for this collection already exists, made on {}'.format(oldMCMCJSONRESULTSCOLLECTIONprocDate))
    print('Loading in that file, which is')
    print('    {}'.format(resultsDir))
    print('        {}'.format(oldMCMCJSONRESULTSCOLLECTIONFileNameMatch[-1].split('/')[-1]))
    print('    ')
    
    MCMC_JSONRESULTSCOLLECTIONFILE = oldMCMCJSONRESULTSCOLLECTIONFileNameMatch[-1]
    
else:
    print('    ')
    print('No prior .json file of MCMC fits for this data collection was found using the following glob search string:')
    print('    {}'.format(oldMCMCJSONRESULTSCOLLECTIONSearchString))
    print('    ')
    print('If a set of MCMC fits need screating, then their results will be saved to')
    print('    {}'.format(MCMC_JSONRESULTSCOLLECTIONFILE))
    print('    ')
    MCMC_JSONRESULTSCOLLECTIONFILE = MCMC_JSONRESULTSCOLLECTIONFILE

for iDate, thisDate in enumerate(measDate):
    # These are the xml files we want to get our data from.
    darmolgtf_filename = \
        '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_DARM_OLGTF_LF_SS_5to1100Hz_15min.xml'\
        .format(cal_data_root,\
            run,\
            IFO,\
            thisDate,\
            IFO)
    pcaltf_filename = \
        '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCAL2DARMTF_LF_SS_5to1100Hz_10min.xml'\
        .format(cal_data_root,\
        run,\
        IFO,\
        thisDate,\
        IFO)
        
    print('DTT File Names used for measDate: {}'.format(thisDate))
    print('    {}'.format(darmolgtf_filename))
    print('    {}'.format(pcaltf_filename))
        
    darmolgtf_obj = pydarm.measurement.Measurement(darmolgtf_filename)
    pcaltf_obj  = pydarm.measurement.Measurement(pcaltf_filename)
        
    # Create a sensing function measurement object
    processed_sensing_obj = pydarm.measurement.ProcessSensingMeasurement(
        model_parameters_file, darmolgtf_obj, pcaltf_obj,
        ('H1:LSC-DARM1_IN2', 'H1:LSC-DARM1_EXC'),
        ('H1:CAL-PCALY_RX_PD_OUT_DQ', 'H1:LSC-DARM_IN1_DQ'),
        COHTHRESH_DARMOLGTF, COHTHRESH_PCALTF,
        json_results_file = MCMC_JSONRESULTSCOLLECTIONFILE)
    allunstackedsensingobjects.append(processed_sensing_obj)

    # Extract the processed sensing function measurement, with C_res removed
    frequencies, processed_optical_response_tf, processed_optical_response_unc = \
        processed_sensing_obj.get_processed_measurement_response()
        
    thisMCMCMAPJSONResultsFileName = '{}{}_{}_mcmcmaps.json'.format(resultsDir,figTag,thisDate)
    thisMCMCPosteriorChainHDF5ResultsFileName = '{}{}_{}_mcmcposteriors.hdf5'.format(resultsDir,figTag,thisDate) 
    
    oldMCMCPosteriorChainHDF5ResultsFileNameSearchString = thisMCMCPosteriorChainHDF5ResultsFileName.replace(todayDate,'*')
    oldMCMCPosteriorChainHDF5ResultsFileNameMatch = sorted(glob.glob(oldMCMCPosteriorChainHDF5ResultsFileNameSearchString))
    
    if oldMCMCPosteriorChainHDF5ResultsFileNameMatch:
        print('    ')
        print('MCMC Posteriors already exist for {}'.format(thisDate))
        print('Loading in the most recent match, which is')
        print('    {}'.format(resultsDir))
        print('        {}'.format(oldMCMCPosteriorChainHDF5ResultsFileNameMatch[-1].split('/')[-1]))
        print('    ')
        _, _, _, _, mcmc_posterior_chain = pydarm.utils.read_chain_from_hdf5(oldMCMCPosteriorChainHDF5ResultsFileNameMatch[-1])
        
    else:
        # Fit optical processed optical response to our sensing function model
        # Fit only the optical response to our sensing function model optical parameters
        
        print('    ')
        print('No prior MCMC posterior chain for this data found, using the following glob search string:')
        print('    {}'.format(oldMCMCPosteriorChainHDF5ResultsFileNameSearchString))
        print('    ')
        print('Fitting MCMC from scratch instead, and saving results to')
        print('    {}'.format(thisMCMCPosteriorChainHDF5ResultsFileName))
        print('    ')
        
        mcmc_posterior_chain = processed_sensing_obj.run_mcmc(
            fmin=mcmc_params_fitregion_Hz[iDate][0],
            fmax=mcmc_params_fitregion_Hz[iDate][1],
            burn_in_steps=MCMC_PARAMS_BURNINSTEPS,
            steps=MCMC_PARAMS_STEPS,
            save_map_to_file=MCMC_JSONRESULTSCOLLECTIONFILE,
            save_chain_to_file=thisMCMCPosteriorChainHDF5ResultsFileName)
        
    MAP = np.median(mcmc_posterior_chain, axis=0)
        
    # Make a reference model of the sensing function from the original model parameters
    #     on the measurement frequency vector, 
    #     so it can be divided out of the measurement    
    normalized_model_optical_response_tf = signal.freqresp(darm.sensing.optical_response(C.coupled_cavity_pole_frequency,
                                                                                         C.detuned_spring_frequency,
                                                                                         C.detuned_spring_q,
                                                                                         C.is_pro_spring),
                                                           2*np.pi*frequencies)[1]
    model_optical_response_tf = C.coupled_cavity_optical_gain*normalized_model_optical_response_tf
    
    # Divide the reference model out of the measurement
    residual_tf = processed_optical_response_tf/model_optical_response_tf
    residual_unc = processed_optical_response_unc
        
    allunstackedresults.append(results(modelDate,\
                                       thisDate,\
                                       frequencies,\
                                       model_optical_response_tf,\
                                       processed_optical_response_tf,\
                                       processed_optical_response_unc,\
                                       residual_tf,\
                                       residual_unc))

print('    DONE with all MCMC fitting (or loading in of previous fits).')
print('    ')
print('Beginning measurement stacking of measurement collection...')

# Assume that the REFMEAS MCMC results are 
# the same as the model parameter file's values, 
# treat its values as the reference value to create 
# TDCFs derived from 
#      iMeas MCMC value / REFMEAS MCMC value, 
# and "stack" them.

# stackedmeasarray is a list of lists, of dimension 
#    [[freq,tf]_0thMeas,[freq,tf]_1thMeas,...,[freq,tf]_nthMeas]. 
# This returns the processed sensing function multiplied by
# sensing_model_reference / sensing_model_MCMC_for_each_measurement
# i.e. the processed sensing function with kappas applied.

# From pydarm/measurement.py after Issue #161 closed with the merge of Merge Req !172 
#    stackedmeaslist_tfs.append([this_freq, this_resp, this_resp_r, this_resp_unc, this_resid])
#  stackedmeaslist_tdcfs.append([this_kappa_c, this_f_cc, fixed_model_springfs, fixed_model_springQ])

stackedmeaslist_tfs, stackedmeaslist_tdcfs = allunstackedsensingobjects[REFMEASINDEX].stack_measurements(
    allunstackedsensingobjects,  
    mcmc_params_fitregion_Hz[REFMEASINDEX][0], 
    mcmc_params_fitregion_Hz[REFMEASINDEX][1], 
    [thisRange[0] for thisRange in mcmc_params_fitregion_Hz],
    [thisRange[1] for thisRange in mcmc_params_fitregion_Hz])
   
# FIXME -- As of pydarm githash a7376cfd,
# The following concatination of all the MCMC fit, TDCF scaled, data into one vector 
# (i.e.*actually* stacking the measurements) is done inside the _gpr method, but not 
# returned. 
# I want a plot of this single vector of many measurements, to look at 
# the input of the GPR.
# The example from https://git.ligo.org/Calibration/pydarm/-/blob/master/examples/pydarm_example.ipynb
# around In [93]: plots only one measurement from the stacked_meas, which isn't enough.
# So I recreate what's done inside the method to concatenate the measurments into one vector here. 
# The fix is that we should just expose / return fdata, meas, and unc to the user.

### START _gpr METHOD CODE COPY ###
for idx, this_meas in enumerate(stackedmeaslist_tfs):
    # Create a mask array where True means the data values should be
    # ignored while False indicates the data should be included
    if GPR_PARAMS_FITREGION_HZ is not None:
        mask = np.ma.masked_outside(
            this_meas[0], GPR_PARAMS_FITREGION_HZ[0], GPR_PARAMS_FITREGION_HZ[1]).mask
    else:
        mask = np.array([False] * len(this_meas[0]))
    # Create / append to an array of frequencies, residuals, and
    # uncertainties
    if idx == 0:
        residuals = np.array(
            [this_meas[0][:],
             this_meas[4][:],
             this_meas[3][:]]).T
    else:
        residuals = np.vstack(
            (residuals, np.array(
                [this_meas[0][:],
                 this_meas[4][:],
                 this_meas[3][:]]).T))

# Now clean up, extract, and get in the right shape for the GPR
# NOTE, I want the sorting and stacking, but I don't want 
# - a pre-logarithmic frequency vector, or
# - the tf collection pre-split into real and imaginary parts, 
# so I change the method's code from this:
#    fdata = np.log10(np.real(residuals[None, :, 0].T))
#    meas = np.hstack((np.real(residuals[None, :, 1].T), np.imag(residuals[None, :, 1].T)))
#    unc = np.real(residuals[None, :, 2])

# FIXME why does the method add an extra dimension by using None in the array handling?
gpr_input_fdata = abs(residuals[:, 0].T) #FIXME why is the frequency complex?
gpr_input_meastf = residuals[:, 1].T
gpr_input_measunc = abs(residuals[:, 2].T) #FIXME ... why doesn't the method's code need to transpose the uncertainty vector? I transpose it here as a guess.

### END _gpr METHOD CODE COPY ###
    
print('    DONE with measurement stacking.')
print('    ')
print('Beginning GPR fit of stacked measurement collection...')
############################
#          GPR FIT         #
############################

# GPR residual_tf is the processed sensing function 
#    with kappas applied *and* the reference model divided out


gpr_fit_median_tf, gpr_fit_unc_tf, gpr_covariance_mtrx, gpr_residual_tfs, gpr_tdcfs = allunstackedsensingobjects[REFMEASINDEX].run_gpr(
    GPR_PROJECTION_FREQ,
    allunstackedsensingobjects,  
    fmin=mcmc_params_fitregion_Hz[REFMEASINDEX][0], 
    fmax=mcmc_params_fitregion_Hz[REFMEASINDEX][1], 
    fmin_list=[thisRange[0] for thisRange in mcmc_params_fitregion_Hz],
    fmax_list=[thisRange[1] for thisRange in mcmc_params_fitregion_Hz],
    RBF_length_scale=GPR_RBF_LENGTHSCALE_PRIOR, 
    RBF_length_scale_limits=GPR_RBF_LENGTHSCALE_RANGE_PRIOR, 
    gpr_flim = GPR_PARAMS_FITREGION_HZ,
    save_to_file=GPR_RESULTSFILE)

print('    DONE with GPR fit of stacked measurements.')
print('    ')
print('Making plots of the results...')

# Make a model of the sensing function from the original model parameters
#     on a nicely spaced, more dense than the measurement, frequency vector, 
#     just for show on the plots                                
normalized_model_optical_response_tf = signal.freqresp(
    darm.sensing.optical_response(
        C.coupled_cavity_pole_frequency,
        C.detuned_spring_frequency,
        C.detuned_spring_q,
        C.is_pro_spring),
        2*np.pi*modelfrequencies)[1]
model_optical_response_tf = C.coupled_cavity_optical_gain*normalized_model_optical_response_tf
                          
expscale = int(np.floor(np.log10(C.coupled_cavity_optical_gain)))

############################
#          PLOTS           #
############################
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.plot(modelfrequencies, abs(model_optical_response_tf)/10**expscale,label='{} Model'.format(modelDate),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s1.errorbar(allunstackedresults[iMeas].frequencies,\
                abs(allunstackedresults[iMeas].processed_optical_response_tf)/10**expscale,\
                yerr=allunstackedresults[iMeas].processed_optical_response_unc*abs(allunstackedresults[iMeas].processed_optical_response_tf)/10**expscale,\
                 fmt='.', label='{}'.format(thisMeas))
s1.set_ylabel('Magnitude (ct/m) x 10$^{{{}}}$'.format(expscale),usetex=True)
s1.set_xscale('log')
s1.set_yscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(0.5,10)
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend(ncol=1)


s2.plot(modelfrequencies, np.angle(model_optical_response_tf,deg=True),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s2.errorbar(allunstackedresults[iMeas].frequencies,\
                np.angle(allunstackedresults[iMeas].processed_optical_response_tf, deg=True),\
                yerr=allunstackedresults[iMeas].processed_optical_response_unc*(180.0/np.pi),\
                fmt='.')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-135,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))

for iMeas, thisMeas in enumerate(measDate):
    s3.errorbar(allunstackedresults[iMeas].frequencies, \
                abs(allunstackedresults[iMeas].residual_tf),\
                yerr=allunstackedresults[iMeas].residual_unc, fmt='.',\
                label='{} / {} Model'.format(thisMeas,modelDate))
s3.set_ylabel('Magnitude |meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.90,1.10])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))

for iMeas, thisMeas in enumerate(measDate):
    s4.errorbar(allunstackedresults[iMeas].frequencies, \
                np.angle(allunstackedresults[iMeas].residual_tf, deg=True),\
                yerr=allunstackedresults[iMeas].residual_unc*(180.0/np.pi),\
                fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase diff. (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,5])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

fig.suptitle('{} Sensing Function Measurement Collection, Without Scaling for TDCFs\nReference Model Params: $H_C=${:.3e} ct/m, $f_{{cc}}=${:.1f} Hz, $f_s=${:.3} Hz, $Q$={:.3}'.format(IFO,\
                                                                                                                  C.coupled_cavity_optical_gain,\
                                                                                                                  C.coupled_cavity_pole_frequency,\
                                                                                                                  C.detuned_spring_frequency,\
                                                                                                                  C.detuned_spring_q),\
                                                                                                                  fontsize='x-large')

plt.tight_layout(rect=[0, 0.03, 1, 0.98])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_postprocessed_unstacked_MCMCinput.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Stored results as {thisfig.filename}")
    
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

# each element of the list of stackedmeaslist_tfs has columns
#    [this_freq, this_resp, this_resp_r, this_resp_unc, this_resid]
#          this_freq (column 0) is frequency vector of the measurement for all subsequent columns
#          this_resp (column 1) is *not* scaled by TDCFs,
#          this_resp_r (column 2) *is* scaled by TDCFs, 
#          this_resid (column 4) is the residual between model and scaled meas TF
#          we assert that this_resp_unc (column 3), the error bars on the measurement, are the 
#             same for all of these. #FIXME I'm not sure that's right, given that we're dividing out MAP values which have uncertainty themselves...
# We already plotted column 1 above, so now we plot column 2 (and 4).

s1.plot(modelfrequencies, abs(model_optical_response_tf)/10**expscale,label='{} Model'.format(modelDate),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s1.errorbar(stackedmeaslist_tfs[iMeas][0],\
                abs(stackedmeaslist_tfs[iMeas][2])/10**expscale,\
                yerr=stackedmeaslist_tfs[iMeas][3]*abs(stackedmeaslist_tfs[iMeas][2])/10**expscale,\
                 fmt='.', label='{} (kappaC, fCC) = ({:.3f}, {:.4g} Hz)'.format(thisMeas,stackedmeaslist_tdcfs[iMeas][0],stackedmeaslist_tdcfs[iMeas][1]))
s1.set_ylabel('Magnitude (ct/m) x 10$^{{{}}}$'.format(expscale),usetex=True)
s1.set_xscale('log')
s1.set_yscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(0.5,10)
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend(ncol=1)


s2.plot(modelfrequencies, np.angle(model_optical_response_tf,deg=True),color='magenta')
for iMeas, thisMeas in enumerate(measDate):
    s2.errorbar(stackedmeaslist_tfs[iMeas][0],\
                np.angle(stackedmeaslist_tfs[iMeas][2], deg=True),\
                yerr=stackedmeaslist_tfs[iMeas][3]*(180.0/np.pi),\
                fmt='.')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-135,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))

for iMeas, thisMeas in enumerate(measDate):
    s3.errorbar(stackedmeaslist_tfs[iMeas][0], \
                abs(stackedmeaslist_tfs[iMeas][4]),\
                yerr=stackedmeaslist_tfs[iMeas][3], fmt='.',\
                label='{} / {} Model'.format(thisMeas,modelDate))
s3.set_ylabel('Magnitude |meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.90,1.10])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))

for iMeas, thisMeas in enumerate(measDate):
    s4.errorbar(stackedmeaslist_tfs[iMeas][0], \
                np.angle(stackedmeaslist_tfs[iMeas][4], deg=True),\
                yerr=stackedmeaslist_tfs[iMeas][3]*(180.0/np.pi),\
                fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,5])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

fig.suptitle('{} Sensing Function Measurement Collection, Scaled by kappaC and fCC TDCFs from MCMC Fit\n Reference Model Params: $H_C=${:.3e} ct/m, $f_{{cc}}=${:.1f} Hz, $f_s=${:.3} Hz, $Q$={:.3}'.format(IFO,\
                                                                                                                  C.coupled_cavity_optical_gain,\
                                                                                                                  C.coupled_cavity_pole_frequency,\
                                                                                                                  C.detuned_spring_frequency,\
                                                                                                                  C.detuned_spring_q),\
                                                                                                                  fontsize='x-large')

plt.tight_layout(rect=[0, 0.03, 1, 0.98])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_postprocessed_stacked_MCMCMAPdividedout.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Stored results as {thisfig.filename}")
    
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)  

for iMeas, thisMeas in enumerate(measDate):
    s1.errorbar(stackedmeaslist_tfs[iMeas][0], \
                abs(stackedmeaslist_tfs[iMeas][4]),\
                yerr=stackedmeaslist_tfs[iMeas][3], fmt='.',\
                label='{} / {} Model'.format(thisMeas,modelDate))
s1.set_ylabel('Magnitude |meas./model|')
s1.set_xscale('log')
s1.legend(ncol=1)
s1.grid(which='major',color='black')
s1.grid(which='minor',ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.90,1.10])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_title('output of stack_measurements method')

for iMeas, thisMeas in enumerate(measDate):
    s2.errorbar(stackedmeaslist_tfs[iMeas][0], \
                np.angle(stackedmeaslist_tfs[iMeas][4], deg=True),\
                yerr=stackedmeaslist_tfs[iMeas][3]*(180.0/np.pi),\
                fmt='.')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (meas./model)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor',ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim([-10,5])
s2.yaxis.set_major_locator(tck.MultipleLocator(2))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

for iMeas, thisMeas in enumerate(measDate):
    s3.errorbar(gpr_residual_tfs[iMeas][0], \
                abs(gpr_residual_tfs[iMeas][4]),\
                yerr=gpr_residual_tfs[iMeas][3], fmt='.',\
                label='{} / {} Model'.format(thisMeas,modelDate))
s3.set_ylabel('Magnitude |meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.90,1.10])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.set_title('output of run_gpr method')

for iMeas, thisMeas in enumerate(measDate):
    s4.errorbar(gpr_residual_tfs[iMeas][0], \
                np.angle(gpr_residual_tfs[iMeas][4], deg=True),\
                yerr=gpr_residual_tfs[iMeas][3]*(180.0/np.pi),\
                fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,5])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
      
fig.suptitle('The Left and Right Bode Plots Should be Identical')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_methodoutputcheck_stack_vs_gprstack.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Stored results as {thisfig.filename}")
    
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.errorbar(gpr_input_fdata,abs(gpr_input_meastf), yerr=gpr_input_measunc,fmt='.',label='Raw Input Data')
s1.plot(GPR_PROJECTION_FREQ,abs(gpr_fit_median_tf), 'b-',label='GPR Fit Median')
s1.fill_between(
    GPR_PROJECTION_FREQ,
    abs(gpr_fit_median_tf) - gpr_fit_unc_tf, 
    y2=abs(gpr_fit_median_tf) + gpr_fit_unc_tf,
    alpha=0.5,fc='b',ec='None',label='GPR Fit 68% C.I.')
s1.axvline(GPR_PARAMS_FITREGION_HZ[0],ls='--',color='C06',label='GPR Fit Region ({}-{} Hz)'.format(
    GPR_PARAMS_FITREGION_HZ[0],
    GPR_PARAMS_FITREGION_HZ[1]))
s1.axvline(GPR_PARAMS_FITREGION_HZ[1],ls='--',color='C06')
s1.set_xscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor',ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.MultipleLocator(0.2))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.05))
s1.set_ylabel('Magnitude |meas./model|')
s1.legend(ncol=1)

s2.errorbar(gpr_input_fdata,np.angle(gpr_input_meastf,deg=True), yerr=gpr_input_measunc*(180.0/np.pi),fmt='.')
s2.plot(GPR_PROJECTION_FREQ,np.angle(gpr_fit_median_tf,deg=True), 'b-',label='GPR Fit Median')
s2.fill_between(
    GPR_PROJECTION_FREQ,
    np.angle(gpr_fit_median_tf,deg=True) - gpr_fit_unc_tf*180.0/np.pi, 
    y2=np.angle(gpr_fit_median_tf,deg=True) + gpr_fit_unc_tf*180.0/np.pi,
    alpha=0.5,fc='b',ec='None',label='GPR Fit 68% C.I.')
s2.axvline(GPR_PARAMS_FITREGION_HZ[0],ls='--',color='C06')
s2.axvline(GPR_PARAMS_FITREGION_HZ[1],ls='--',color='C06')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor',ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (meas./model)') 

s1.set_title('Sensing Function GPR Fit (fit range: {}-{} Hz)\n{}, model: {}, measDate range: {}-{}'.format(
    GPR_PARAMS_FITREGION_HZ[0],
    GPR_PARAMS_FITREGION_HZ[1],
    IFO,
    modelDate,
    measDate[0].replace('-',''),
    measDate[-1].replace('-','')))
GPR_PROJECTION_FREQ = np.logspace(np.log10(1), np.log10(5000), 100)
GPR_PARAMS_FITREGION_HZ = [10,1000]
GPR_RBF_LENGTHSCALE_PRIOR = 0.35
GPR_RBF_LENGTHSCALE_RANGE_PRIOR = (0.33, 0.37)
s2.set_title('GPR RBF Length Scale Prior: {:.3f} (+{:.3f}/-{:.3f})'.format(
    GPR_RBF_LENGTHSCALE_PRIOR,
    GPR_RBF_LENGTHSCALE_RANGE_PRIOR[1] - GPR_RBF_LENGTHSCALE_PRIOR,
    GPR_RBF_LENGTHSCALE_PRIOR - GPR_RBF_LENGTHSCALE_RANGE_PRIOR[0]))    
    
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_actualstackedandscalemeas_gprfit.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    print(f"Stored results as {thisfig.filename}")
