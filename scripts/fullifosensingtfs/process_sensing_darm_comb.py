# This is a draft so far to compute the response and sensing functions over time
# usually a couple of hours during IFO thermalization

import argparse
import numpy as np
import os
from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.backends.backend_pdf import PdfPages
from scipy import signal
from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.time import tconvert
from pydarm.calcs import CALCSModel
from pydarm.uncertainty import DARMUncertainty


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i', '--ifo', type=str, required=True, choices=['H1', 'L1'], help='Interferometer')
parser.add_argument('-s', '--start', type=str, required=True, help='UTC start time of analysis')
parser.add_argument('-e', '--end', type=str, required=True, help='UTC end time of analysis')
parser.add_argument('--pydarm_ini', type=str, required=True, help='Path and filename of pyDARM configuration file')
parser.add_argument('--pydarm_unc_ini', type=str, help='Path and filename of pyDARM uncertainty configuration file')
parser.add_argument('-t', '--fftlength', type=float, default=40, help='FFT length (seconds)')
parser.add_argument('-o', '--overlap', type=float, default=20, help='FFT overlap (seconds), if % overlap is desired, enter in ((% overlap) / 100) * fft_length')
parser.add_argument('-w', '--window', type=str, default='hann', help='FFT window function, hann window is default')
parser.add_argument('-r', '--stride', type=float, default=120, help='Duration over which FFTs are averaged for the transfer function')
parser.add_argument('--darm_in1', type=str, default='LSC-DARM_IN1_DQ')
parser.add_argument('--darm_in2', type=str, default='LSC-DARM1_IN2_DQ')
parser.add_argument('--darm_exc', type=str, default='LSC-DARM1_EXC_DQ')
parser.add_argument('--pcalx', type=str, default='CAL-PCALX_RX_PD_OUT_DQ')
parser.add_argument('--pcaly', type=str, default='CAL-PCALY_RX_PD_OUT_DQ')
parser.add_argument('--deltal', type=str, default='CAL-DELTAL_EXTERNAL_DQ')
parser.add_argument('--pcalx_freq', type=float, nargs='*', default=[33.43, 53.67, 77.73, 102.13, 283.91])
parser.add_argument('--pcaly_freq', type=float, nargs='*', default=[8.925, 11.575, 15.275, 17.1, 24.5, 410.3, 1083.7])
parser.add_argument('--darm_freq', type=float, nargs='*', default=[8.825, 11.475, 15.175, 24.4])
parser.add_argument('--output-dir', type=str, default='.')
parser.add_argument('-d', '--data-dir', type=str, default='.')
parser.add_argument('--cal_data_root', type=str, help='Set an environment variable to override the cal_data_root path')
args = parser.parse_args()


if args.cal_data_root:
    os.environ['CAL_DATA_ROOT'] = args.cal_data_root

args.pcalx_freq = np.asarray(args.pcalx_freq)
args.pcaly_freq = np.asarray(args.pcaly_freq)
args.darm_freq = np.asarray(args.darm_freq)

"""
# Set these parameters as needed
os.environ['CAL_DATA_ROOT'] = '/Users/egoetz/lscrepos/cal-H1'
config = '/Users/egoetz/lscrepos/cal-H1/pydarm_H1.ini'
unc_config = '/Users/egoetz/lscrepos/cal-H1/pydarm_uncertainty_H1.ini'
pcalx_freq = np.array([33.43, 53.67, 77.73, 102.13, 283.91, 410.2])
pcaly_freq = np.array([8.925, 11.575, 15.275, 17.1, 24.5, 410.3, 1083.7])
darm_freq = np.array([8.825, 11.475, 15.175, 24.4])
fftlength = 80 # seconds (df = 0.025 Hz bin width)
overlap = 40 # seconds (must be half of fft_length to achieve 50% overlap, which is best for a hann window)
window = 'hann'
stride = 240 # seconds (5 averages)
fft_averages = 5 = (stride/fft_length)+(fft_length/overlap_length)
chList = ['H1:LSC-DARM_IN1_DQ',
          'H1:LSC-DARM1_IN2_DQ',
          'H1:LSC-DARM1_EXC_DQ',
          'H1:CAL-PCALX_RX_PD_OUT_DQ',
          'H1:CAL-PCALY_RX_PD_OUT_DQ',
          'H1:CAL-DELTAL_EXTERNAL_DQ',]
"""

# Covert to GPS
start_gps = tconvert(args.start)
end_gps = tconvert(args.end)

fft_averages = (args.stride/args.fftlength)+(args.fftlength/args.overlap)

# All the PCAL frequencies and a index array that would sort the PCAL frequencies
pcal_freq = np.hstack((args.pcalx_freq, args.pcaly_freq))
sort_indicies = np.argsort(pcal_freq)

darm_indices = []
for f in args.darm_freq:
    idx = np.argmin(np.abs(args.pcaly_freq - f))
    darm_indices.append(idx)

# Initialize model
calcs = CALCSModel(args.pydarm_ini)
uncertainty = DARMUncertainty(args.pydarm_ini, args.pydarm_unc_ini)
model_freq = np.logspace(np.log10(8), np.log10(2000), 200)

response_ref = calcs.compute_response_function(pcal_freq)
response_model = calcs.compute_response_function(model_freq)
sensing_ref = calcs.sensing.compute_sensing(args.darm_freq)
sensing_model = calcs.sensing.compute_sensing(model_freq)

pcalx_corr = calcs.pcal.compute_pcal_correction(args.pcalx_freq, endstation=True, arm='X')
pcaly_corr = calcs.pcal.compute_pcal_correction(args.pcaly_freq, endstation=True, arm='Y')
pcalx_over_deltal_corr = 1/calcs.deltal_ext_pcal_correction(args.pcalx_freq, endstation=True, arm='X')
pcaly_over_deltal_corr = 1/calcs.deltal_ext_pcal_correction(args.pcaly_freq, endstation=True, arm='Y')

# Create channel list
chList = []
for ch in [args.darm_in1, args.darm_in2, args.darm_exc, args.pcalx, args.pcaly, args.deltal]:
    chList.append(f'{args.ifo}:{ch}')
    
armPwrChList = [f'{args.ifo}:ASC-X_PWR_CIRC_OUT16',
                f'{args.ifo}:ASC-Y_PWR_CIRC_OUT16']


# Loop over the 2-minute times and process each 2-minute average
start = start_gps
end = start_gps + args.stride
times = []
response_tf_x = []
response_unc_x = []
response_tf_y = []
response_unc_y = []
sensing_tf = []
sensing_unc = []
response_nom = []
sensing_nom = []
syserr_x = []
syserr_y = []
syserr_unc_x = []
syserr_unc_y = []
kappa_c = []
f_cc = []
kappa_u = []
kappa_p = []
kappa_t = []
armpwrs = []
while end <= end_gps:

    if os.path.exists(f"{args.data_dir}/armpwr_{start}-{end}.hdf5"):
        data = tsd.read(f"{args.data_dir}/sensing_comb_{start}-{end}.hdf5", chList)
        pwrdata = tsd.read(f"{args.data_dir}/armpwr_{start}-{end}.hdf5")
    else:
        data = tsd.get(chList, start, end, frametype='R', verbose=True)
        data.resample(4096)
        data.write(f"{args.data_dir}/sensing_comb_{start}-{end}.hdf5")
        
        pwrdata = tsd.get(armPwrChList, start, end, frametype='R',verbose=True)
        pwrdata.write(f"{args.data_dir}/armpwr_{start}-{end}.hdf5")
        
    # Compute the average of the two arm powers at each 16 Hz data point,
    # then compute the median across the stride
    pwr = np.median(np.mean(np.array(
                                     [pwrdata[armPwrChList[0]].value, 
                                      pwrdata[armPwrChList[1]].value]),
                            axis=0))

    # Response function PCALX / DARM = (1 + G) / C
    tf = data[f'{args.ifo}:{args.darm_in1}'].transfer_function(
        data[f'{args.ifo}:{args.pcalx}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window,
        average='mean')[(args.pcalx_freq*args.fftlength).astype(int)]
    coh = data[f'{args.ifo}:{args.darm_in1}'].coherence(
        data[f'{args.ifo}:{args.pcalx}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window)[(args.pcalx_freq*args.fftlength).astype(int)]
    unc = np.sqrt((1-coh.value)/(2*(coh.value+1e-6)*fft_averages))

    response_tf_x.append(tf.value * pcalx_corr)
    response_unc_x.append(unc)

    # Response function PCALY / DARM = (1 + G) / C
    tf = data[f'{args.ifo}:{args.darm_in1}'].transfer_function(
        data[f'{args.ifo}:{args.pcaly}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window,
        average='mean')[(args.pcaly_freq*args.fftlength).astype(int)]
    coh = data[f'{args.ifo}:{args.darm_in1}'].coherence(
        data[f'{args.ifo}:{args.pcaly}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window)[(args.pcaly_freq*args.fftlength).astype(int)]
    unc = np.sqrt((1-coh.value)/(2*(coh.value+1e-6)*fft_averages))

    response_tf_y.append(tf.value * pcaly_corr)
    response_unc_y.append(unc)

    # Nominal response (response corrected for time dependence)
    times.append((start + (end-start)/2).gpsSeconds)
    armpwrs.append(pwr)
    r_nom, tdcf_data = uncertainty.nominal_response(
        times[-1], pcal_freq)
    response_nom.append(r_nom)

    kappa_c.append(tdcf_data[f"H1:{uncertainty.tdcf_channels_dict['sensing']['kappa_c']}"].mean().value)
    f_cc.append(tdcf_data[f"H1:{uncertainty.tdcf_channels_dict['sensing']['f_cc']}"].mean().value)
    kappa_u.append(tdcf_data[f"H1:{uncertainty.tdcf_channels_dict['xarm']['kappa_uim']}"].mean().value)
    kappa_p.append(tdcf_data[f"H1:{uncertainty.tdcf_channels_dict['xarm']['kappa_pum']}"].mean().value)
    kappa_t.append(tdcf_data[f"H1:{uncertainty.tdcf_channels_dict['xarm']['kappa_tst']}"].mean().value)

    # Closed loop response EXC / IN2 = (1 + G)
    tf = data[f'{args.ifo}:{args.darm_in2}'].transfer_function(
        data[f'{args.ifo}:{args.darm_exc}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window,
        average='mean')[(args.darm_freq*args.fftlength).astype(int)]
    coh = data[f'{args.ifo}:{args.darm_in2}'].coherence(
        data[f'{args.ifo}:{args.darm_exc}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window)[(args.darm_freq*args.fftlength).astype(int)]
    unc = np.sqrt((1-coh.value)/(2*(coh.value+1e-6)*fft_averages))

    sensing_tf.append(tf.value / response_tf_y[-1][darm_indices])
    sensing_unc.append(np.sqrt(unc**2 + response_unc_y[-1][darm_indices]**2))

    C_nom = (kappa_c[-1] *
             calcs.sensing.coupled_cavity_optical_gain *
             signal.freqresp(calcs.sensing.optical_response(
                 f_cc[-1],
                 calcs.sensing.detuned_spring_frequency,
                 calcs.sensing.detuned_spring_q), 2*np.pi*args.darm_freq)[1])
    sensing_nom.append(C_nom)

    # Response function PCALX / DELTAL = m/m
    tf = data[f'{args.ifo}:{args.deltal}'].transfer_function(
        data[f'{args.ifo}:{args.pcalx}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window,
        average='mean')[(args.pcalx_freq*args.fftlength).astype(int)]
    coh = data[f'{args.ifo}:{args.deltal}'].coherence(
        data[f'{args.ifo}:{args.pcalx}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window)[(args.pcalx_freq*args.fftlength).astype(int)]
    unc = np.sqrt((1-coh.value)/(2*(coh.value+1e-6)*fft_averages))

    syserr_x.append(tf.value * pcalx_over_deltal_corr)
    syserr_unc_x.append(unc)

    # Response function PCALY / DELTAL = m/m
    tf = data[f'{args.ifo}:{args.deltal}'].transfer_function(
        data[f'{args.ifo}:{args.pcaly}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window,
        average='mean')[(args.pcaly_freq*args.fftlength).astype(int)]
    coh = data[f'{args.ifo}:{args.deltal}'].coherence(
        data[f'{args.ifo}:{args.pcaly}'],
        fftlength=args.fftlength,
        overlap=args.overlap,
        window=args.window)[(args.pcaly_freq*args.fftlength).astype(int)]
    unc = np.sqrt((1-coh.value)/(2*(coh.value+1e-6)*fft_averages))

    syserr_y.append(tf.value * pcaly_over_deltal_corr)
    syserr_unc_y.append(unc)

    # step to next time
    start += args.stride
    end += args.stride
    
#import ipdb; ipdb.set_trace()
duration_hrs = (end_gps.gpsSeconds - start_gps.gpsSeconds)/3600

print('  ')
print('  ')
print('  {:.3f}'.format(len(syserr_y)))
print('  {:.3f}'.format(args.stride))
print('  {:.3f}'.format(duration_hrs))
print('There are {:.3f} chunks of data, each {:.3f} seconds long over the {:.3f} hour duration chosen.'.format(len(syserr_y), args.stride, duration_hrs))
print('  ')
print('  ')

# Plotting
titleTag = '\n(blue = {} UTC; yellow = {} UTC, Duration = {:.3f} hrs)'.format(args.start, args.end, duration_hrs)
colors = [cm.viridis(x) for x in np.linspace(0, 1, len(response_tf_x))]

plt.figure(figsize=[8,6])
plt.suptitle('H1 Sensing Function: Meas vs. (Model w/o TDCFs)'+titleTag)
plt.subplot(211)
plt.plot(model_freq, np.abs(sensing_model))
for n, tf in enumerate(sensing_tf):
    plt.plot(args.darm_freq, np.abs(tf), marker='.', linestyle='', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude (ct/m)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.yscale('log')
plt.xscale('log')
plt.xlim(5, 500)
plt.ylim([1e6,1e8])

plt.subplot(212)
plt.plot(model_freq, np.angle(sensing_model, deg=True))
for n, tf in enumerate(sensing_tf):
    plt.plot(args.darm_freq, np.angle(tf, deg=True), marker='.', linestyle='', color=colors[n])    
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xlim(5, 500)
plt.xscale('log')
plt.ylim([-90,90])



plt.figure(figsize=[8,6])
plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/o TDCFs)'+titleTag)
plt.subplot(211)
for n, tf in enumerate(sensing_tf):
    ratio = tf/sensing_ref
    plt.plot(args.darm_freq, np.abs(ratio), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude Residual (ct/m)/(ct/m)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xlim([5,30])
plt.ylim([0.1,14])

plt.subplot(212)
for n, tf in enumerate(sensing_tf):
    ratio = tf/sensing_ref
    plt.plot(args.darm_freq, np.angle(ratio, deg=True), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase Residual (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xlim([5,30])
plt.ylim([-25,100])


plt.figure(figsize=[8,6])
plt.suptitle('H1 Sensing Function Ratio: Meas / (Model w/ TDCFs)'+titleTag)
plt.subplot(211)
sensingMeas_over_sensingModelwTDCFs = []
for n, (tf, nom) in enumerate(zip(sensing_tf, sensing_nom)):
    ratio = tf/nom
    sensingMeas_over_sensingModelwTDCFs.append(ratio)
    plt.plot(args.darm_freq, np.abs(ratio), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude Residual (ct/m)/(ct/m)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xlim([5,30])
plt.ylim([0.1,14])

plt.subplot(212)
for n, (tf, nom) in enumerate(zip(sensing_tf, sensing_nom)):
    ratio = tf/nom
    plt.plot(args.darm_freq, np.angle(ratio, deg=True), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase Residual (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xlim([5,30])
plt.ylim([-25,100])

out_mag = abs(np.array(sensingMeas_over_sensingModelwTDCFs)).T
out_pha = np.angle(np.array(sensingMeas_over_sensingModelwTDCFs), deg=True).T
out_unc = np.array(sensing_unc).T
out_timetime, out_freqfreq = np.meshgrid(times,args.darm_freq)
out_pwrpwr, out_freqfreq = np.meshgrid(armpwrs,args.darm_freq)
outDir = args.output_dir
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_mag.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_mag, fmt='%.7g')
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_pha.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_pha, fmt='%.7g')
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_unc.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_unc, fmt='%.7g')
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_timetime.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_timetime, fmt='%.12g')
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_freqfreq.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_freqfreq, fmt='%.7g')
np.savetxt('{}/sensingFunctionResidual_meas_over_model_wTDCFs_{}-{}_pwrpwr.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_pwrpwr, fmt='%.7g')

plt.figure(figsize=[8,6])
plt.suptitle('H1 Response Function Time Evolution (1+G)/C'+titleTag)
plt.subplot(211)
plt.plot(model_freq, np.abs(response_model),label='Model')
for n, (tf_x, tf_y) in enumerate(zip(response_tf_x, response_tf_y)):
    tf = np.hstack((tf_x, tf_y))
    plt.plot(pcal_freq[sort_indicies], np.abs(tf[sort_indicies]), marker='.', linestyle='', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude (m/ct)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.yscale('log')
plt.ylim([1e-7,1e-5])
plt.legend()

plt.subplot(212)
plt.plot(model_freq, np.angle(response_model, deg=True))
for n, (tf_x, tf_y) in enumerate(zip(response_tf_x, response_tf_y)):
    tf = np.hstack((tf_x, tf_y))
    plt.plot(pcal_freq[sort_indicies], np.angle(tf[sort_indicies], deg=True), marker='.', linestyle='', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')

plt.figure(figsize=[8,6])
plt.suptitle('H1 Response Function Ratio: (PCAL/DARM ERR) / (Model w/ TDCFs) '+titleTag)
plt.subplot(211)
responseMeas_over_responseModelwTDCFs_tf = []
responseMeas_over_responseModelwTDCFs_unc = []
for n, (tf_x, tf_y, tf_nom, unc_x, unc_y) in enumerate(zip(response_tf_x, response_tf_y, response_nom, response_unc_x, response_unc_y)):
    tf = np.hstack((tf_x, tf_y))
    unc = np.hstack((unc_x,unc_y))
    ratio = tf/tf_nom
    responseMeas_over_responseModelwTDCFs_tf.append(ratio[sort_indicies])
    responseMeas_over_responseModelwTDCFs_unc.append(unc[sort_indicies])
    plt.plot(pcal_freq[sort_indicies], np.abs(ratio[sort_indicies]), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude Residual (m/ct)/(m/ct)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.ylim([0.8,1.2])

plt.subplot(212)
for n, (tf_x, tf_y, tf_nom) in enumerate(zip(response_tf_x, response_tf_y, response_nom)):
    tf = np.hstack((tf_x, tf_y))
    ratio = tf/tf_nom
    plt.plot(pcal_freq[sort_indicies], np.angle(ratio[sort_indicies], deg=True), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase Residual (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.ylim([-15,15])

out_mag = abs(np.array(responseMeas_over_responseModelwTDCFs_tf)).T
out_pha = np.angle(np.array(responseMeas_over_responseModelwTDCFs_tf), deg=True).T
out_unc = np.array(responseMeas_over_responseModelwTDCFs_unc).T
out_timetime, out_freqfreq = np.meshgrid(times,pcal_freq[sort_indicies])
out_pwrpwr, out_freqfreq = np.meshgrid(armpwrs,pcal_freq[sort_indicies])
outDir = args.output_dir
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_mag.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_mag, fmt='%.7g')
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_pha.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_pha, fmt='%.7g')
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_unc.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_unc, fmt='%.7g')
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_timetime.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_timetime, fmt='%.12g')
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_freqfreq.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_freqfreq, fmt='%.7g')
np.savetxt('{}/responseFunctionResidual_meas_over_model_wTDCFs_{}-{}_pwrpwr.txt'.format(args.data_dir,start_gps.gpsSeconds,end_gps.gpsSeconds), out_pwrpwr, fmt='%.7g')


plt.figure(figsize=[8,6])
plt.suptitle('H1 Response Function Ratio: (PCAL/DARM ERR) / (Model w/o TDCFs) '+titleTag)
plt.subplot(211)
for n, (tf_x, tf_y) in enumerate(zip(response_tf_x, response_tf_y)):
    tf = np.hstack((tf_x, tf_y))
    ratio = tf/response_ref
    plt.plot(pcal_freq[sort_indicies], np.abs(ratio[sort_indicies]), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude Residual (m/ct)/(m/ct)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.ylim([0.8,1.2])
plt.subplot(212)
for n, (tf_x, tf_y) in enumerate(zip(response_tf_x, response_tf_y)):
    tf = np.hstack((tf_x, tf_y))
    ratio = tf/response_ref
    plt.plot(pcal_freq[sort_indicies], np.angle(ratio[sort_indicies], deg=True), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase Residual (deg)', fontsize='small')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.ylim([-15,15])

plt.figure(figsize=[8,6])
plt.suptitle('H1 Measured Systematic Error (PCAL / DELTAL EXT)'+titleTag)
plt.subplot(211)
for n, (tf_x, tf_y) in enumerate(zip(syserr_x, syserr_y)):
    tf = np.hstack((tf_x, tf_y))
    plt.plot(pcal_freq[sort_indicies], np.abs(tf[sort_indicies]), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Magnitude (m/m)', fontsize='small')
plt.ylim([0.8,1.20])
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')
plt.yscale('linear')

plt.subplot(212)
for n, (tf_x, tf_y) in enumerate(zip(syserr_x, syserr_y)):
    tf = np.hstack((tf_x, tf_y))
    plt.plot(pcal_freq[sort_indicies], np.angle(tf[sort_indicies], deg=True), marker='.', color=colors[n])
plt.xlabel('Frequency (Hz)', fontsize='small')
plt.ylabel('Phase (deg)', fontsize='small')
plt.ylim([-15,15])
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.xscale('log')









times = np.asarray(times)

plt.figure(figsize=[8,6])
plt.suptitle('H1 TDCF values')
plt.subplot(221)
plt.plot(times-times[0], kappa_c)
plt.xlabel(f'GPS (t0 = {times[0]})')
plt.ylabel('kappa_c')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')
plt.subplot(223)
plt.plot(times-times[0], f_cc)
plt.xlabel(f'GPS (t0 = {times[0]})')
plt.ylabel('f_cc')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')

plt.subplot(222)
plt.plot(times-times[0], kappa_u, label='UIM')
plt.plot(times-times[0], kappa_p, label='PUM')
plt.plot(times-times[0], kappa_t, label='TST')
plt.legend(fontsize='small')
plt.xlabel(f'GPS (t0 = {times[0]})')
plt.ylabel('kappa_*')
plt.minorticks_on()
plt.tick_params(axis='both', which='both', labelsize='small')
plt.grid(which='both')


pp = PdfPages(f'{args.output_dir}/plots-{start_gps}-{end_gps}.pdf')
figs = [plt.figure(n) for n in plt.get_fignums()]
for fig in figs:
    fig.savefig(pp, format='pdf')
pp.close()
