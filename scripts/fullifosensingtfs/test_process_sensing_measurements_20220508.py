from pydarm.cmd.measurement_set import MeasurementSet
from pydarm.measurement import Measurement, ProcessSensingMeasurement
from pydarm.plot import BodePlot
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

import matplotlib.ticker as tck
import json
import numpy as np
from scipy import signal
import os
import sys
import logging

log_format = "%(asctime)s::%(levelname)s::%(name)s::%(message)s"
ff = logging.Formatter(log_format)
fh = logging.FileHandler("/ligo/home/louis.dartez/projects/20230224_srcl_offset_darm_measurements/log.txt")
sh = logging.StreamHandler(sys.stderr)
fh.setFormatter(ff)

#colors = list(mcolors.TABLEAU_COLORS)
colors = ['red','orange','yellow','green','mediumaquamarine','blue','purple','black']
plt.rcParams.update({'text.usetex': False,
                     'lines.linewidth': 1.1,
                     'font.family': 'sans-serif',
                     'font.serif': 'Helvetica',
                     'font.size': 16,
                     'xtick.labelsize': 'x-large',
                     'ytick.labelsize': 'x-large',
                     'axes.labelsize': 'x-large',
                     'axes.titlesize': 'x-large',
                     'axes.grid': True,
                     'grid.alpha': 0.5,
                     'lines.markersize': 12,
                     'legend.borderpad': 0.2,
                     'legend.fancybox': True,
                     'legend.fontsize': 'large',
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.1,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'figure.figsize': (15, 12),
                     'savefig.dpi': 100,
                     'pdf.compression': 9,
                     'pdf.fonttype': 42,
                     })


pcal_rx_pd_chan_name = "H1:CAL-PCALY_RX_PD_OUT_DQ"
deltal_ext_chan_name = "H1:CAL-DELTAL_EXTERNAL_DQ"
param_modelfile_20230125 = "/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20230125.ini"
param_modelfile_20230508 = "/ligo/groups/cal/H1/ifo/pydarm_H1.ini"
DATA_ROOT = "/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs"
BB_ROOT = "/ligo/gitcommon/noise_recorder/data/darm_noise/"
RESULTS_DIR = "/ligo/home/jeffrey.kissel/2023-05-08/"
class Sensing_Bundle:
    def __init__(self, name,
                 olg_xml_path,
                 pcal2darm_xml_path,
                 bbsensingfile):
        self.name = name
        self.paramfile = param_modelfile_20230508
        self.olg_measurement = Measurement(os.path.join(DATA_ROOT, olg_xml_path))
        self.p2d_measurement = Measurement(os.path.join(DATA_ROOT, pcal2darm_xml_path))
        self.pcal_rx_pd_chan_name = "H1:CAL-PCALY_RX_PD_OUT_DQ"
        self.darm_exc_chan_name = "H1:LSC-DARM1_EXC"
        self.darm_in2_chan_name = "H1:LSC-DARM1_IN2"
        self.darm_in1_chan_name = "H1:LSC-DARM_IN1_DQ"
        self.processed_measurement = \
            ProcessSensingMeasurement(param_modelfile_20230508,
                                      self.olg_measurement,
                                      self.p2d_measurement,
                                      (self.darm_in2_chan_name,
                                       self.darm_exc_chan_name),
                                      (self.pcal_rx_pd_chan_name,
                                       self.darm_in1_chan_name),
                                      0.25, 0.25)
        self.bbsensingfile = bbsensingfile



def get_latest_mcmc_entry(fpath):
    with open(fpath) as f:
        json_contents = json.load(f)
    latest_job = json_contents[sorted(list(json_contents.keys()))[-1]]
    mcmc_map_vals = latest_job['mcmc_map_vals']
    fmin, fmax = float(latest_job['fmin']), float(latest_job['fmax'])
    return (mcmc_map_vals, (fmin, fmax))


def scale_optical_response(freqs, opt_resp_norm, MAP):
    MAP_opticalGain_ct_p_m = MAP[0]
    MAP_residualTimeDelay_sec = MAP[4]
    return (MAP_opticalGain_ct_p_m * opt_resp_norm *
            np.exp(-2*np.pi*1j*MAP_residualTimeDelay_sec*freqs*1e-6))


def get_optical_response_modeled(mset, mapv, freqs):
    norm_resp = signal.freqresp(mset.model.sensing.optical_response(
        mapv[1], mapv[2], 1/mapv[3], mset.model.sensing.is_pro_spring),
        2*np.pi*freqs)[1]
    return scale_optical_response(freqs, norm_resp, mapv)


def test_plot_sensing():
    measurements = [Sensing_Bundle(*x) for x in [
        ("-265 ct",
         "2023-05-08_2120UTC_H1_DARM_OLGTF_LF_SS_SRCL_m265.xml",
         "2023-05-08_2120UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m265.xml",
         "202305081410/sensing_function_tf_1367615618_May-8th-2023.txt"),
        ("-275 ct",
         "2023-05-08_2139UTC_H1_DARM_OLGTF_LF_SS_SRCL_m275.xml",
         "2023-05-08_2139UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m275.xml",
         "202305081432/sensing_function_tf_1367616988_May-8th-2023.txt"),  
        ("-290 ct",
         "2023-05-08_2157UTC_H1_DARM_OLGTF_LF_SS_SRCL_m290.xml",
         "2023-05-08_2157UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m290.xml",
         "202305081451/sensing_function_tf_1367618068_May-8th-2023.txt"), 
        ("-310 ct",
         "2023-05-08_2215UTC_H1_DARM_OLGTF_LF_SS_SRCL_m310.xml",
         "2023-05-08_2215UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m310.xml",
         "202305081508/sensing_function_tf_1367619127_May-8th-2023.txt"), 
        ("-325 ct",
         "2023-05-08_2232UTC_H1_DARM_OLGTF_LF_SS_SRCL_m325.xml",
         "2023-05-08_2232UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m325.xml",
         "202305081525/sensing_function_tf_1367620133_May-8th-2023.txt"), 
        ("-350 ct",
         "2023-05-08_2250UTC_H1_DARM_OLGTF_LF_SS_SRCL_m350.xml",
         "2023-05-08_2250UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m350.xml",
         "202305081542/sensing_function_tf_1367621256_May-8th-2023.txt"), 
        ("-400 ct",
         "2023-05-08_2307UTC_H1_DARM_OLGTF_LF_SS_SRCL_m400.xml",
         "2023-05-08_2307UTC_H1_PCAL2DARMTF_LF_SS_SRCL_m400.xml",
         "202305081601/sensing_function_tf_1367622297_May-8th-2023.txt"), 
    ]]

    fig = plt.figure()
    fig.suptitle("Sensing Function Measurements (05-08-2023)",
                 fontsize='x-large')
    bp = BodePlot(fig=fig, spspec=[211, 212], title='2023-05-08 H1 Sensing Function Measurements vs. SRCL Offset')
    # load sensing measurement
    # mset = MeasurementSet()
    # mset.load_model()
    # freqs, resp, _ = \
    #     mset.processed_measurements['Sensing']\
    #     .get_processed_measurement_response()

    # exp_scale = np.floor(np.log10(np.median(np.abs(resp))))
    # bp.plot(freqs, resp*10**(-1*exp_scale), marker='.', linestyle='',
    #              c=colors[0], label=mset.id)

    # iterate over models
    for i, meas_bundle in enumerate(measurements):
        freqs, resp, resp_unc = meas_bundle.processed_measurement.get_processed_measurement_response()
        
        meas = np.loadtxt(BB_ROOT + meas_bundle.bbsensingfile);
        bbfreq = meas[:,0]
        bbmag = meas[:,1]
        bbpha_deg = meas[:,2]
        
        bbtf = bbmag * np.exp(1j*(np.pi/180.0)*bbpha_deg)
        
        if i==0:
            exp_scale = np.floor(np.log10(np.mean(np.abs(resp))))
        bp.error(freqs, resp/10**(exp_scale), resp_unc,
                 color=colors[i],
                 marker='.',
                 label=meas_bundle.name)
        bp.plot(bbfreq,(3/1e4)*bbtf/10**(exp_scale),
                color=colors[i],
                label=meas_bundle.name + ' BB')
        bp.ax_mag.set_xscale('log')
        bp.ax_mag.grid(which='major',color='black')
        bp.ax_mag.grid(which='minor', ls='--')
        bp.legend(loc='upper right')
        bp.ax_mag.set_ylabel(f"Magnitude (ct/m) x$10^{int(exp_scale):d}$")
        bp.ax_mag.yaxis.set_major_locator(tck.LogLocator(base=10))
        bp.ax_mag.set_xlim([4,30])
        bp.ax_phase.set_xscale('log')
        bp.ax_phase.grid(which='major', color='black')
        bp.ax_phase.grid(which='minor', ls='--')
        bp.ax_phase.xaxis.set_major_locator(tck.LogLocator(base=10))
        bp.ax_phase.yaxis.set_major_locator(tck.MultipleLocator(5))
        bp.ax_phase.yaxis.set_minor_locator(tck.MultipleLocator(1))
        bp.ax_phase.set_ylim(-30, 30)
        bp.ax_phase.set_xlim([4,30])
    bp.legend()
    fig.savefig(RESULTS_DIR + '2023-05-08_H1_sensingfunction_vs_srcloffset.pdf')

if __name__ == "__main__":
    test_plot_sensing()
