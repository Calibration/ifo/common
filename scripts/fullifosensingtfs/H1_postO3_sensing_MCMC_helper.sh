python3 generate_MCMC_results_sensing.py \
 --model_file ../../pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini \
 --clg_measurement /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs/$1_H1_DARM_OLGTF_LF_SS_5to1100Hz_15min.xml \
 --pcal_to_darm /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/FullIFOSensingTFs/$1_H1_PCAL2DARMTF_LF_SS_5to1100Hz_10min.xml \
 --clg_channels H1:LSC-DARM1_IN2 H1:LSC-DARM1_EXC --pcal_to_darm_channels H1:CAL-PCALY_RX_PD_OUT_DQ H1:LSC-DARM_IN1_DQ \
 --fmin 80 --fmax 1200 --burn_in_steps 100 --steps 900

