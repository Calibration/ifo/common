import numpy as np
import sys, os
from glob import glob
import json
import pydarm

directory = 'results'  ## FIXME: for automation, need to specify some location
sensing_mcmc_files = glob(f'{directory}/*sensing_MCMC.json')
sensing_gpr_frange = [30,1100]

frequencies = np.logspace(np.log10(1), np.log10(5000), 100)
ref_idx = 0
cohThresh = 0.9

sensing_mcmc_files = glob(f'{directory}/*sensing_MCMC.json')

measurement_list = []
fmin_list = []
fmax_list = []

for file in sensing_mcmc_files:
    with open(file, 'r') as f:
        data = json.load(f)

    key = list(data.keys())[-1]

    meas_obj_1 = pydarm.measurement.Measurement(data[key]['loop_meas'])
    meas_obj_2 = pydarm.measurement.Measurement(data[key]['pcal_meas'])
    meas = pydarm.measurement.ProcessSensingMeasurement(data[key]['model_config_file'], 
        meas_obj_1, meas_obj_2,
        data[key]['loop_tup'],
        data[key]['pcal_tup'],
        meas1_cohThresh=data[key]['loop_cohthresh'], meas2_cohThresh=data[key]['pcal_cohthresh'],
        json_results_file=file)

    fmin_list.append(data[key]['fmin'])
    fmax_list.append(data[key]['fmax'])
    measurement_list.append(meas)

median, unc, cov, residuals, tdcfs = measurement_list[ref_idx].run_gpr(frequencies, measurement_list, 
    fmin_list[ref_idx], fmax_list[ref_idx], fmin_list=fmin_list, fmax_list=fmax_list, 
    gpr_flim=(sensing_gpr_frange[0], sensing_gpr_frange[1]),
    save_to_file=f'{directory}/sensing_GPR.hdf5')

#  ============================ Plots ===============================
    
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
mpl.rcParams.update({'text.usetex': True,
                     'axes.linewidth': 1,
                     'axes.grid': True,
                     'axes.labelweight': 'normal',
                     'font.family': 'DejaVu Sans',
                     'font.size': 20})
from matplotlib import ticker

frange_plot = [5, 2000]
mag = np.abs(median)
phase = np.angle(median)*180.0/np.pi

stacked_meas, tdcfs = measurement_list[ref_idx].stack_measurements(measurement_list, 
                    fmin_list[ref_idx], fmax_list[ref_idx], fmin_list, fmax_list)

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(15, 12))
ax0, ax1 = axes.flat

ax0.plot(frequencies, mag, 'b-', label='Median')
ax0.fill_between(frequencies, mag - unc, mag + unc, alpha=0.5, fc='b', label='68\% C.I.')
for idx in range(0,len(measurement_list)):
    ax0.errorbar(stacked_meas[idx][0],np.abs(stacked_meas[idx][4]), marker='o', markersize=10, linestyle='', yerr=stacked_meas[idx][3])
ax0.set_xlim(frange_plot)
ax0.set_ylim([0.9,1.1])
ax0.yaxis.set_minor_locator(ticker.MultipleLocator(0.01))
ax0.set_ylabel(r'Mag (meas/model)')

ax1.plot(frequencies, phase, 'b-', label='Median')
ax1.fill_between(frequencies, phase - unc*180.0/np.pi, phase + unc*180.0/np.pi, alpha=0.5, fc='b', label='68\% C.I.')
for idx in range(0,len(stacked_meas)):
    ax1.errorbar(stacked_meas[idx][0],np.angle(stacked_meas[idx][4])*180.0/np.pi,marker='o', markersize=10, linestyle='', yerr=stacked_meas[idx][3]*180.0/np.pi)
ax1.set_xlim(frange_plot)
ax1.set_ylim([-5,5])
ax1.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
ax1.set_xlabel(r'Frequency (Hz)')
ax1.set_ylabel(r'Phase (meas/model) [deg]')


for ax in axes.flat:
    ax.grid(which='major',color='black')
    ax.grid(which='minor',ls='--')
    ax.set_xscale('log')
    ax.legend(loc='lower center', ncol=2, handlelength=2)
    ax.axvline(sensing_gpr_frange[0],ls='--',color='C06')
    ax.axvline(sensing_gpr_frange[1],ls='--',color='C06')

plt.savefig(f'{directory}/sensing_GPR_plot.pdf', bbox_inches='tight', pad_inches=0.2)