from mpl_toolkits.mplot3d import axes3d
from matplotlib import ticker as tck
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import cm
import numpy as np
from gwpy.time import tconvert

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
columnStride = 1
timeforward_pitch = 15
timeforward_yaw = 80


zlabelpad = 10
width_pad = 0.1
height_pad = 0.1
edges_pad = 0.0

timelim_min = [0, 250]
timelim_lnmin = [0, 5.5]
pwrlim = [395,440]
freqlim3d = [5,30]
freqlim = [5, 50]
maglim = [0.1, 25]
phalim = [-100, 100]
uzfreqlim = [5, 100]
zmaglim = [0.85, 1.25]
zphalim = [-15, 15]                    

segmentgps = ['1367752698-1367767098',
              '1368087318-1368099918',
              '1368161418-1368175818',
              #'1368247638-1368262038', has glitch
              '1368384498-1368396018',
              '1368442818-1368457218',
              '1368559818-1368574218',
              '1368603918-1368618318',
              '1368632418-1368646818',
              '1368655218-1368669618']   
              
# process_sensing_darm_comb.py had bug in it up until 
# 2023-05-23 where fft_averages, or N, used to compute the
# uncertainty from the coherence C, via the usual 
#      unc = sqrt(1-C / 2NC)
# was computed as the total duration of the data set
# (e.g. 4 hours) divided by 2 minutes, with 50% overlap, 
# i.e. 4 hrs * 60 mins/hr * (2 + (2*0.5)) = 720 averages
# It should be 4, that each 2 minute, or 120 sec, time span was 
# informed by 40 sec ffts, with 50% overlap
# i.e. 
baadNavgs = [719,629,719,575,719,719,719,719,719]  
goodNavgs = [  5,  5,  5,  5,  5,  5,  5,  5,  5]         

times = []
freqs = []
pwrs = []
all_timetime = []
all_freqfreq = []
all_pwrpwr = []
all_magmag = []
all_phapha = []
all_uncunc = []


for nSeg, thisSeg in enumerate(segmentgps):    
    # dataDir = '/ligo/gitcommon/Calibration/ifo/scripts/fullifosensingtfs/'
    dataDir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/Measurements/FullIFOSensingTFs/Thermalization/'
    timetime = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_timetime.txt'.format(dataDir,thisSeg))
    freqfreq = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_freqfreq.txt'.format(dataDir,thisSeg))
    pwrpwr = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pwrpwr.txt'.format(dataDir,thisSeg))
    magmag = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_mag.txt'.format(dataDir,thisSeg))
    phapha = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pha.txt'.format(dataDir,thisSeg))
    uncunc = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_unc.txt'.format(dataDir,thisSeg))
    
    
    startutc = tconvert(timetime[0,0]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    endutc = tconvert(timetime[-1,-1]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    
    duration_hrs = (timetime[-1,-1] - timetime[0,0])/3600
    timetime = (timetime - timetime[0,0])/60+1
    
    # to correct for baadNavgs without re-downloading and saving the data,
    # sqrt(baadNavgs/goodNavgs) * uncunc = sqrt(baadNavgs/goodNavgs) * sqrt((1 - C) / (2 baadNavg C))
    #                                  = sqrt( (baadNavgs/goodNavgs) * (1 - C) / (2 baadNavg C))
    #                                  = sqrt( (1 - C) / (2 goodNavgs C) )
    uncunc = uncunc * np.sqrt(baadNavgs[nSeg] / goodNavgs[nSeg])
    
    all_timetime.append(timetime)
    all_freqfreq.append(freqfreq)
    all_pwrpwr.append(pwrpwr)
    all_magmag.append(magmag)
    all_phapha.append(phapha)
    all_uncunc.append(uncunc)
    
    times.append(timetime[0,:])
    freqs.append(freqfreq[:,0])
    pwrs.append(pwrpwr[0,:])
    
    titleTag = '\n(blue = {}; yellow = {}, Duration = {:.3f} hrs)'.format(startutc, endutc, duration_hrs)
    normalcolors = [cm.viridis(x) for x in np.linspace(0, 1, len(timetime[0,:]))]        
    norm = plt.Normalize(timetime.min(), timetime.max())
    colors = cm.viridis(norm(timetime))
    rowCount, columnCount, _ = colors.shape
    
    ###
    ### 3D Bode plot, power forward, linear in power
    ###
    fig, ((s1,s2), (s3,s4)) = plt.subplots(2,2,figsize=(12,8),subplot_kw={'projection':'3d'})

    ss1 = s1.plot_surface(pwrpwr, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss1.set_facecolor((0,0,0,0))                       
    s1.view_init(timeforward_pitch,timeforward_yaw)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.set_xlabel('Power (kW)')
    s1.set_ylabel('Frequency (Hz)')
    s1.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s1.set_xlim(pwrlim)
    s1.set_ylim(freqlim3d)
    s1.set_zlim(maglim)
    s1.xaxis.set_major_locator(tck.MultipleLocator(10))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s1.yaxis.set_major_locator(tck.MultipleLocator(5))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss3 = s3.plot_surface(pwrpwr, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss3.set_facecolor((0,0,0,0))   
    s3.view_init(timeforward_pitch,timeforward_yaw)
    s3.set_xlabel('Power (kW)')
    s3.set_ylabel('Frequency (Hz)')
    s3.set_zlabel('Phase Residual (deg)')
    s3.set_xlim(pwrlim)
    s3.set_ylim(freqlim3d)
    s3.set_zlim(phalim)
    s3.xaxis.set_major_locator(tck.MultipleLocator(10))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.yaxis.set_major_locator(tck.MultipleLocator(5))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.zaxis.set_major_locator(tck.MultipleLocator(45))
    s3.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s3.set_title('3D Bode Plot: Power Axis Forward',fontsize=16)

    ss2 = s2.plot_surface(timetime, freqfreq, magmag, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss2.set_facecolor((0,0,0,0))                       
    s2.view_init(timeforward_pitch,timeforward_yaw)
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.set_xlabel('Time (min)')
    s2.set_ylabel('Frequency (Hz)')
    s2.set_zlabel('Magnitude Residual \n(ct/m)/(ct/m)',labelpad=zlabelpad)
    s2.set_xlim(timelim_min)
    s2.set_ylim(freqlim3d)
    s2.set_zlim(maglim)
    s2.xaxis.set_major_locator(tck.MultipleLocator(60))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s2.yaxis.set_major_locator(tck.MultipleLocator(5))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(1))

    ss4 = s4.plot_surface(timetime, freqfreq, phapha, rcount=rowCount, ccount=columnCount,
                       facecolors=colors, shade=False)
    ss4.set_facecolor((0,0,0,0))   
    s4.view_init(timeforward_pitch,timeforward_yaw)
    s4.set_xlabel('Time (min)')
    s4.set_ylabel('Frequency (Hz)')
    s4.set_zlabel('Phase Residual (deg)')
    s4.set_xlim(timelim_min)
    s4.set_ylim(freqlim3d)
    s4.set_zlim(phalim)
    s4.xaxis.set_major_locator(tck.MultipleLocator(60))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(15))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.zaxis.set_major_locator(tck.MultipleLocator(45))
    s4.zaxis.set_minor_locator(tck.MultipleLocator(15))
    s4.set_title('3D Bode Plot: Time Axis Forward',fontsize=16)


    plt.suptitle('H1 Sensing Function Ratio vs Power: Meas / (Model w/ TDCFs)'+titleTag,fontsize=12)

    plt.tight_layout(pad=edges_pad, w_pad=width_pad, h_pad=height_pad)
    
fig, (s1) = plt.subplots(1,1,figsize=(12,8))

for n, __ in enumerate(all_pwrpwr):
    s1.plot(times[n]/60.0,(pwrs[n]),ls='-',marker='.',label='{} UTC'.format(tconvert(segmentgps[n][0:10])))   
s1.set_ylim(pwrlim)
s1.grid(which='major', color='black')
s1.grid(which='minor', ls='--',color='grey')
s1.set_xlabel('Time (hrs)')
s1.set_ylabel('Power (kW)')
s1.xaxis.set_major_locator(tck.MultipleLocator(1))
s1.xaxis.set_minor_locator(tck.MultipleLocator(0.25))
s1.yaxis.set_major_locator(tck.MultipleLocator(5))
s1.yaxis.set_minor_locator(tck.MultipleLocator(1))
s1.legend()
s1.set_title('2-minute Median of Average X&Y Arm Power vs. Time\n{} Data Sets'.format(len(segmentgps)))



pp = PdfPages('sensingFunction_syserror_vs_power.pdf')
figs = [plt.figure(n) for n in list(reversed(plt.get_fignums()))]
for fig in figs:
    fig.savefig(pp, format='pdf')
pp.close()
plt.close('all')

pwrbinlowerlim = 404
pwrbinupperlim = 436 
pwrbinstep = 1
pwrbins = [[n,n+pwrbinstep] for n in range(pwrbinlowerlim,pwrbinupperlim,pwrbinstep)]
pwrbins[0] = [0,pwrbinlowerlim+pwrbinstep]

fig, (s1) = plt.subplots(1,1,figsize=(12,8))

for n, __ in enumerate(all_pwrpwr):
    s1.plot(times[n]/60.0,(pwrs[n]),ls='-',marker='.',label='{} UTC'.format(tconvert(segmentgps[n][0:10]))) 
s1.hlines(y=pwrbinlowerlim, xmin=times[n][0]/60, xmax=times[n][-1]/60, linewidth=2, color='r',ls='--',label='Lowest Bin Upper Limit: {} kW'.format(pwrbinlowerlim))
s1.hlines(y=pwrbinupperlim, xmin=times[n][0]/60, xmax=times[n][-1]/60, linewidth=2, color='b',ls='--',label='Highest Bin Upper Limit: {} kW'.format(pwrbinupperlim))
s1.set_ylim(pwrlim)
s1.grid(which='major', color='black')
s1.grid(which='minor', ls='--',color='grey')
s1.set_xlabel('Time (hrs)')
s1.set_ylabel('Power (kW)')
s1.xaxis.set_major_locator(tck.MultipleLocator(1))
s1.xaxis.set_minor_locator(tck.MultipleLocator(0.25))
s1.yaxis.set_major_locator(tck.MultipleLocator(5))
s1.yaxis.set_minor_locator(tck.MultipleLocator(pwrbinstep))
s1.legend()
s1.set_title('2-minute Median of Average X&Y Arm Power vs. Time\n{} Data Sets, Power Bin Step Size = {} kW (Same as Minor Grid)'.format(len(segmentgps),pwrbinstep))

for thispwrbin in pwrbins:
    pwrbin_pwr = []
    pwrbin_freqxmag = []
    pwrbin_freqxpha = []
    pwrbin_freqxunc = []
    
    for iseg, _ in enumerate(segmentgps): # for each segment
        ind = np.where(np.logical_and(all_pwrpwr[iseg][0,:] > thispwrbin[0], all_pwrpwr[iseg][0,:] <= thispwrbin[1])) # find indices where power is within the bin boundaries
        pwrbin_pwr.append(all_pwrpwr[iseg][0,ind])      # append the all those powers onto a list
        pwrbin_freqxmag.append(all_magmag[iseg][:,ind]) # append all ":" frequency values of the tf magnitude values at that power onto a list
        pwrbin_freqxpha.append(all_phapha[iseg][:,ind]) # append all ":" frequency values of the tf phase values at that power onto a list
        pwrbin_freqxunc.append(all_uncunc[iseg][:,ind]) # append all ":" frequency values of the tf phase values at that power onto a list
    
    fig, ((s1,s2),(s3,s4)) = plt.subplots(2,2,figsize=(12,8))

    ntraces = 0
    for iseg, __ in enumerate(segmentgps): # for each segment
        for __, thispwrbinspwrs in  enumerate(pwrbin_pwr[iseg]): # grab the list of power values within a given bin
            for kpwr, thispwr in enumerate(thispwrbinspwrs): # and plot each transfer function at that power value
                thismag = pwrbin_freqxmag[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thismagunc = thismag * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s1.errorbar(freqs[0], thismag, yerr=thismagunc, 
                            ls='-',marker='.',label='{} UTC; pwr = {:.3f} kW'.format(tconvert(segmentgps[iseg][0:10]),thispwr))
                ntraces+=1
    s1.legend(fontsize=8)
    s1.set_xlabel('Frequency (Hz)')
    s1.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s1.set_xlim(freqlim)
    s1.set_ylim(maglim)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.minorticks_on()
    s1.xaxis.set_major_locator(tck.MultipleLocator(5))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s1.yaxis.set_major_locator(tck.MultipleLocator(2))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

    for iseg in range(len(segmentgps)):
        for thispwrbinspwrs in pwrbin_pwr[iseg]:
            for kpwr, thispwr in enumerate(thispwrbinspwrs):
                thispha = pwrbin_freqxpha[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thisphaunc = (180.0/np.pi) * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s3.errorbar(freqs[0], thispha, yerr=thisphaunc, 
                            ls='-',marker='.')
    s3.set_xlabel('Frequency (Hz)')
    s3.set_ylabel('Phase Residual (deg)')
    s3.set_xlim(freqlim)
    s3.set_ylim(phalim)
    s3.grid(which='major', color='black')
    s3.grid(which='minor', ls='--',color='grey')
    s3.minorticks_on()
    s3.xaxis.set_major_locator(tck.MultipleLocator(5))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.yaxis.set_major_locator(tck.MultipleLocator(45))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(15))
    
    for iseg, __ in enumerate(segmentgps): # for each segment
        for __, thispwrbinspwrs in  enumerate(pwrbin_pwr[iseg]): # grab the list of power values within a given bin
            for kpwr, thispwr in enumerate(thispwrbinspwrs): # and plot each transfer function at that power value
                thismag = pwrbin_freqxmag[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thismagunc = thismag * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s2.errorbar(freqs[0], thismag, yerr=thismagunc, 
                            ls='-',marker='.')
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s2.set_xlim(uzfreqlim)
    s2.set_ylim(zmaglim)
    s2.set_xscale('log')
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.minorticks_on()
    s2.xaxis.set_major_locator(tck.LogLocator(base=10))
    s2.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s2.yaxis.set_major_locator(tck.MultipleLocator(0.05))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(0.01))

    for iseg in range(len(segmentgps)):
        for thispwrbinspwrs in pwrbin_pwr[iseg]:
            for kpwr, thispwr in enumerate(thispwrbinspwrs):
                thispha = pwrbin_freqxpha[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thisphaunc = (180.0/np.pi) * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s4.errorbar(freqs[0], thispha, yerr=thisphaunc, 
                            ls='-',marker='.')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('Phase Residual (deg)')
    s4.set_xlim(uzfreqlim)
    s4.set_ylim(zphalim)
    s4.set_xscale('log')
    s4.grid(which='major', color='black')
    s4.grid(which='minor', ls='--',color='grey')
    s4.minorticks_on()
    s4.xaxis.set_major_locator(tck.LogLocator(base=10))
    s4.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    
    plt.suptitle('Sensing Function Systematic Error vs. Power Slice\n Power Bin: ({:.0f},{:.0f}] kW, containing {} TFs in that bin'.format(thispwrbin[0],thispwrbin[1],ntraces),fontsize=16)
                        
pp = PdfPages('sensingFunction_syserror_vs_powerslices.pdf')
figs = [plt.figure(n) for n in plt.get_fignums()]
for fig in figs:
    fig.savefig(pp, format='pdf')
pp.close()
plt.close('all')
