
#######################
# ENVIRONMENT SETUP   #
#######################

import math as m
from mpl_toolkits.mplot3d import axes3d
from matplotlib import ticker as tck
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import cm
import numpy as np
from gwpy.time import tconvert
from pydarm.utils import save_gpr_to_hdf5

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })

#Homemade version of matlab tic and toc functions                    
def tic():
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        tocout = time.time() - startTime_for_tictoc
        if tocout < 0.1: # seconds; i.e. peanuts
            tocoout = 0.1
        print('Elapsed time is {:.1f} seconds.'.format(tocout))
    else:
        print('Toc: start time not set')

#######################
# HARD-CODED PARAMS   #
#######################                    

zlabelpad = 10
width_pad = 0.1
height_pad = 0.1
edges_pad = 0.0

timelim_min = [0, 250]
timelim_lnmin = [0, 5.5]
pwrlim = [395,440]
freqlim = [5, 50]
outfreqlim = [5, 5000]
maglim = [0.1, 25]
phalim = [-60, 60]
uzfreqlim = [5, 100]
zmaglim = [0.85, 1.25]
zphalim = [-15, 15]   
histbinlim = [0,60]

pwrbinlowerlim = 404
pwrbinupperlim = 436 
pwrbinstep = 1
pwrbins = [[n,n+pwrbinstep] for n in range(pwrbinlowerlim,pwrbinupperlim,pwrbinstep)]
pwrbins[0] = [0,pwrbinlowerlim+pwrbinstep]

#pwrbins = [[0,405]]

#pwrbins = [[0,405],[405,406],[432,433],[434,435]] 

lower_flim_MCMC = 40
upper_flim_rrnom = 6000
nExtraPoints = 30
extrapointval = 1.0+1j*0.0
extrapointunc = 1e-4   

# GPR Kernel is F * RBF(\ell) + C
#     F is the "amount of correlated frequency dependence"
#     RBF is a radial basis function, with length scale parameter \ell
#     C is the "flat" transfer function we expect residuals to be if measurement == model

# F
amountoffreqdependencecoeff_value = 0.5 # Some middle value for amount of frequency dependence
amountoffreqdependencecoeff_range = (0.4,1.0) # allow for "little-to-no," i.e. 0.1, or "a lot," i.e. 2.0, of frequency dependence  

# \ell
rbf_lengthscale_value = 0.25 # freq * (10 ** \ell - 1)
rbf_lengthscale_bound = rbf_lengthscale_value/2.0 # Don't give it that much play to move around from the desired length scale. 
     
# C
whennoresidualtf_value = 1.0
whennoresidualtfunc_bound = 0.10

fitfreqlim = [4,upper_flim_rrnom]


ciSigmas = np.array([0, -1, -2, -3, 1, 2, 3])
ciColors = ['red','mediumvioletred','hotpink','lightpink','mediumvioletred','deeppink','lightpink']

segmentgps = ['1367752698-1367767098',
              '1368087318-1368099918',
              '1368161418-1368175818',
              #'1368247638-1368262038', has glitch
              '1368384498-1368396018',
              '1368442818-1368457218',
              '1368559818-1368574218',
              '1368603918-1368618318',
              '1368632418-1368646818',
              '1368655218-1368669618']   
              
# process_sensing_darm_comb.py had bug in it up until 
# 2023-05-23 where fft_averages, or N, used to compute the
# uncertainty from the coherence C, via the usual 
#      unc = sqrt(1-C / 2NC)
# was computed as the total duration of the data set
# (e.g. 4 hours) divided by 2 minutes, with 50% overlap, 
# i.e. 4 hrs * 60 mins/hr * (2 + (2*0.5)) = 720 averages
# It should be 4, that each 2 minute, or 120 sec, time span was 
# informed by 40 sec ffts, with 50% overlap
# i.e. 
baadNavgs = [719,629,719,575,719,719,719,719,719]  
goodNavgs = [  5,  5,  5,  5,  5,  5,  5,  5,  5]     

########################
# INITIALIZE VARIABLES #
########################

extrafreqs = np.round(np.logspace(np.log10(lower_flim_MCMC),np.log10(upper_flim_rrnom),nExtraPoints))  
#extrafreqs = np.round(np.linspace(lower_flim_MCMC,upper_flim_rrnom,nExtraPoints))
#extrafreqs = np.array([40, 50, 60, 80, 100, 200, 500, 1000, 1500, 4000])
extraunc = extrapointunc * np.ones(extrafreqs.shape)

# https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.kernels.RBF.html
# Radial basis function, as we use it here, is assuming that each frequency point is correlated 
# with its neighbor in log10(frequency) in via a random guassian manner, where the "sigma" of the 
# Guassian is the "length scale" and the "length" is the Euclidean distance between the complex
# data points.
rbf_lengthscale_range = (rbf_lengthscale_value - rbf_lengthscale_bound,rbf_lengthscale_value + rbf_lengthscale_bound) 

whennoresidualtfunc_range = (whennoresidualtf_value - whennoresidualtfunc_bound, whennoresidualtf_value + whennoresidualtfunc_bound)

kernel_test = ConstantKernel(amountoffreqdependencecoeff_value, amountoffreqdependencecoeff_range)\
                             * RBF(rbf_lengthscale_value, rbf_lengthscale_range)\
                             + ConstantKernel(whennoresidualtf_value, whennoresidualtfunc_range)
              
freqout = np.logspace(np.log10(8),np.log10(5000),128)
logfreqout = np.log10(freqout[None, :].T)

ciQuantiles = np.asarray(([abs(m.erf(x/np.sqrt(2))/2-(1/2)) for x in ciSigmas])) # https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule

times = []
freqs = []
pwrs = []
all_timetime = []
all_freqfreq = []
all_pwrpwr = []
all_magmag = []
all_phapha = []
all_uncunc = []

########################
# LOAD DATA            #
########################

for nSeg, thisSeg in enumerate(segmentgps):    
    # dataDir = '/ligo/gitcommon/Calibration/ifo/scripts/fullifosensingtfs/'
    dataDir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/Measurements/FullIFOSensingTFs/Thermalization/'
    timetime = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_timetime.txt'.format(dataDir,thisSeg))
    freqfreq = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_freqfreq.txt'.format(dataDir,thisSeg))
    pwrpwr = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pwrpwr.txt'.format(dataDir,thisSeg))
    magmag = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_mag.txt'.format(dataDir,thisSeg))
    phapha = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_pha.txt'.format(dataDir,thisSeg))
    uncunc = np.loadtxt('{}sensingFunctionResidual_meas_over_model_wTDCFs_{}_unc.txt'.format(dataDir,thisSeg))
    
    
    startutc = tconvert(timetime[0,0]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    endutc = tconvert(timetime[-1,-1]).strftime('%Y%m%d %H:%M:%S')+' UTC'
    
    duration_hrs = (timetime[-1,-1] - timetime[0,0])/3600
    timetime = (timetime - timetime[0,0])/60+1
    
    # to correct for baadNavgs without re-downloading and saving the data,
    # sqrt(baadNavgs/goodNavgs) * uncunc = sqrt(baadNavgs/goodNavgs) * sqrt((1 - C) / (2 baadNavg C))
    #                                  = sqrt( (baadNavgs/goodNavgs) * (1 - C) / (2 baadNavg C))
    #                                  = sqrt( (1 - C) / (2 goodNavgs C) )
    uncunc = uncunc * np.sqrt(baadNavgs[nSeg] / goodNavgs[nSeg])
    
    all_timetime.append(timetime)
    all_freqfreq.append(freqfreq)
    all_pwrpwr.append(pwrpwr)
    all_magmag.append(magmag)
    all_phapha.append(phapha)
    all_uncunc.append(uncunc)
    
    times.append(timetime[0,:])
    freqs.append(freqfreq[:,0])
    pwrs.append(pwrpwr[0,:])
    
################################
# SORT INTO POWER BINS AND FIT #
################################  
    
for thispwrbin in pwrbins:   
    tic() 
    pwrbin_pwr = []
    pwrbin_freqxmag = []
    pwrbin_freqxpha = []
    pwrbin_freqxunc = []
    
    allsegs_thispwrbin_pwr = np.empty((0,))
    allsegs_thispwrbin_magxfreq = np.empty((0,4))
    allsegs_thispwrbin_phaxfreq = np.empty((0,4))
    allsegs_thispwrbin_uncxfreq = np.empty((0,4))
    
    for iseg, _ in enumerate(segmentgps): # for each segment
        ind = np.where(np.logical_and(all_pwrpwr[iseg][0,:] > thispwrbin[0], all_pwrpwr[iseg][0,:] <= thispwrbin[1])) # find indices where power is within the bin boundaries
        pwrbin_pwr.append(all_pwrpwr[iseg][0,ind])      # append the all those powers onto a list
        pwrbin_freqxmag.append(all_magmag[iseg][:,ind]) # append all ":" frequency values of the tf magnitude values at that power onto a list
        pwrbin_freqxpha.append(all_phapha[iseg][:,ind]) # append all ":" frequency values of the tf phase values at that power onto a list
        pwrbin_freqxunc.append(all_uncunc[iseg][:,ind]) # append all ":" frequency values of the tf phase values at that power onto a list
        
        # also append to an arbitrarily long nparray that doesn't preserve what segment it came from (what GPR needs)
        # note the completely obscene dimensional juggling to turn the all_blahblah list of arrays back into a useable
        # appendable array to tack on to the allsegs_blahxfreq array.
        #      all_magmag[5].shape is (4,120) 
        #          i.e. freq x mag for all 120 two-minute answers over 4 hours)
        #      all_magmag[5][:,ind].shape is (4,1,17) 
        #          i.e. freq x ?? x mag for only the 17 two-minute answers within that 4 hours that were within this pwrbin
        #               not sure why this extra dimension gets added, but we need to prune it out
        #      all_magmag[5][:,ind].transpose(1,2,0).shape is (1,17,4) 
        #          i.e. re-arranges the dimensions to move the ?? dimension up front for pruning, leaving ?? x mag x freq
        #               with the goal of a mag x freq matrix where we can add more mag rows on a fixed freq vector from the next 
        #               segment
        #      all_magmag[5][:,ind].transpose(1,2,0)[0].shape is (17,4) i.e. prunes off that first dimension
        allsegs_thispwrbin_pwr = np.append(allsegs_thispwrbin_pwr,all_pwrpwr[iseg][0,ind]) 
        allsegs_thispwrbin_magxfreq = np.append(allsegs_thispwrbin_magxfreq,all_magmag[iseg][:,ind].transpose(1,2,0)[0],axis=0) 
        allsegs_thispwrbin_phaxfreq = np.append(allsegs_thispwrbin_phaxfreq,all_phapha[iseg][:,ind].transpose(1,2,0)[0],axis=0)
        allsegs_thispwrbin_uncxfreq = np.append(allsegs_thispwrbin_uncxfreq,all_uncunc[iseg][:,ind].transpose(1,2,0)[0],axis=0)
        
    
    # Convert real data to a complex transfer function 
    allsegs_thispwrbin_tfxfreq = allsegs_thispwrbin_magxfreq * np.exp(1j*(np.pi/180.0)*allsegs_thispwrbin_phaxfreq)
    ntraces = allsegs_thispwrbin_tfxfreq.shape[0]
    
    # Grab statistics on the quantiles of the data within the power bin
    thispwrbin_magcixfreq = np.empty((freqs[0].shape[0],ciQuantiles.shape[0]))
    thispwrbin_phacixfreq = np.empty((freqs[0].shape[0],ciQuantiles.shape[0]))
    for nfreq, thisfreq in enumerate(freqs[0]):
        thispwrbin_magcixfreq[nfreq,:] = np.quantile(abs(allsegs_thispwrbin_tfxfreq[:,nfreq]), ciQuantiles)
        thispwrbin_phacixfreq[nfreq,:] = np.quantile(np.angle(allsegs_thispwrbin_tfxfreq[:,nfreq],deg=True), ciQuantiles)  
    
    # Create arrays of extra, fake, uncertain complex tfs on the extra, high-frequency vector to be added to each power's tf value
    allsegs_thispwrbin_extratfxfreq = extrapointval*np.ones((allsegs_thispwrbin_tfxfreq.shape[0],extrafreqs.shape[0]))
    allsegs_thispwrbin_extrauncxfreq = extraunc*np.ones((allsegs_thispwrbin_tfxfreq.shape[0],extrafreqs.shape[0]))
    
    # Merge the real data and extra, fake data 
    thispwrbin_freqxfreq = np.hstack((freqs[0],extrafreqs)) * np.ones((allsegs_thispwrbin_tfxfreq.shape[0],len(freqs[0])+len(extrafreqs)))
    thispwrbin_tfxfreq = np.hstack((allsegs_thispwrbin_tfxfreq,allsegs_thispwrbin_extratfxfreq))
    thispwrbin_uncxfreq = np.real(np.hstack((allsegs_thispwrbin_uncxfreq,allsegs_thispwrbin_extrauncxfreq)))
    
    # Don't think. Just reshape the data into something that we know works
    # in previous Evan/Craig code.
    # Re-arrange the data to be all in one long dimension, such that
    # e.g. 14 powers' worth of tfs over a 9 frequency vector become
    # 14 * 6 = 126 element array (while still preserving the match
    # between any given frequency point and its TF value because the
    # frequency vector is repeated 14 times).
    thispwrbin_full_freq = thispwrbin_freqxfreq.flatten()
    thispwrbin_full_tf = thispwrbin_tfxfreq.flatten()
    thispwrbin_full_tfunc = thispwrbin_uncxfreq.flatten()
    
    fitInd = np.logical_and(thispwrbin_full_freq>=fitfreqlim[0],thispwrbin_full_freq<=fitfreqlim[1])
    thispwrbin_freq = thispwrbin_full_freq[fitInd]
    thispwrbin_tf = thispwrbin_full_tf[fitInd]
    thispwrbin_tfunc = thispwrbin_full_tfunc[fitInd]
    
    # Add a meaningless dimension such that each element is its own array
    thispwrbin_logffreqdata = np.log10(thispwrbin_freq[None,:].T)
    thispwrbin_tfreal = np.real(thispwrbin_tf)[None,:].T
    thispwrbin_tfimag = np.imag(thispwrbin_tf)[None,:].T
    
    # Put the real and imaginary parts in the same 2D array
    thispwrbin_gprinput = np.hstack((thispwrbin_tfreal, thispwrbin_tfimag))
    
    
    # Fit the data to a guassian process
    print('Fitting data from power bin {} kW with {} TFs in it ...'.format(thispwrbin, ntraces))
    thispwrbin_gpr_obj = GaussianProcessRegressor(kernel=kernel_test, alpha=thispwrbin_tfunc, n_restarts_optimizer=10)
    thispwrbin_gpr_obj.fit(thispwrbin_logffreqdata, thispwrbin_gprinput)
    
    # Extract the output of the fit
    y_pred, cov = thispwrbin_gpr_obj.predict(logfreqout, return_cov=True)
    y_pred, sigma = thispwrbin_gpr_obj.predict(logfreqout, return_std=True)
    thispwrbin_tfgprmodel = y_pred[:,0] + 1j*y_pred[:,1]
    thispwrbin_tfgpr68ci = sigma[:,0] # Assumes that 68% ci is the same for real and imaginary part. Confirmed true with one slice.
    
    # Save the GPR
    thispwrbin_filename = 'supplemental_sensing_gpr_{}_kW.hdf5'.format(thispwrbin[-1]) # chosing the *upper* bound of the power bin
    pydarmparametermodel = '' # nominally the .ini file, but in this case it's an empty string, because the model's not being used.
    measurementtype = 'sensing' 
    save_gpr_to_hdf5(thispwrbin_filename, pydarmparametermodel, measurementtype, y_pred, cov, freqout)
    
    print('Done fitting.')
    toc()
    print(' ')
    print(' ')
    
    #############################
    # PLOTS                     #
    #############################
    '''
    # Plot to make sure I've re-organized the each segment's power slice into power bins
    # correctly.
    fig, ((s1,s2),(s3,s4)) = plt.subplots(2,2,figsize=(12,8))
    
    for iseg, __ in enumerate(segmentgps): # for each segment
        for __, thispwrbinspwrs in  enumerate(pwrbin_pwr[iseg]): # grab the list of power values within a given bin
            for kpwr, thispwr in enumerate(thispwrbinspwrs): # and plot each transfer function at that power value
                thismag = pwrbin_freqxmag[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thismagunc = thismag * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s1.errorbar(freqs[0], thismag, yerr=thismagunc, 
                            ls='-',marker='.',label='{} UTC; pwr = {:.3f} kW'.format(tconvert(segmentgps[iseg][0:10]),thispwr))
    s1.legend(fontsize=8)
    s1.set_xlabel('Frequency (Hz)')
    s1.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s1.set_xlim(freqlim)
    s1.set_ylim(maglim)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.minorticks_on()
    s1.xaxis.set_major_locator(tck.MultipleLocator(5))
    s1.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s1.yaxis.set_major_locator(tck.MultipleLocator(2))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
    s1.set_title('{} traces'.format(ntraces))

    for iseg in range(len(segmentgps)):
        for thispwrbinspwrs in pwrbin_pwr[iseg]:
            for kpwr, thispwr in enumerate(thispwrbinspwrs):
                thispha = pwrbin_freqxpha[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                thisphaunc = (180.0/np.pi) * pwrbin_freqxunc[iseg].transpose(1,0,2)[0,:,:].T[kpwr]
                s3.errorbar(freqs[0], thispha, yerr=thisphaunc, 
                            ls='-',marker='.')
    s3.set_xlabel('Frequency (Hz)')
    s3.set_ylabel('Phase Residual (deg)')
    s3.set_xlim(freqlim)
    s3.set_ylim(phalim)
    s3.grid(which='major', color='black')
    s3.grid(which='minor', ls='--',color='grey')
    s3.minorticks_on()
    s3.xaxis.set_major_locator(tck.MultipleLocator(5))
    s3.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s3.yaxis.set_major_locator(tck.MultipleLocator(45))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(15))
    
    for ntf, thispwr in enumerate(allsegs_thispwrbin_pwr):
        thismag = allsegs_thispwrbin_magxfreq[ntf]
        thismagunc = thismag * allsegs_thispwrbin_uncxfreq[ntf]
        s2.errorbar(freqs[0], thismag, yerr=thismagunc, 
                    ls='-',marker='.',label='{:.3f} kW'.format(thispwr))
    s2.legend(fontsize=8)
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s2.set_xlim(freqlim)
    s2.set_ylim(maglim)
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.minorticks_on()
    s2.xaxis.set_major_locator(tck.MultipleLocator(5))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s2.yaxis.set_major_locator(tck.MultipleLocator(2))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
    s2.set_title('{} traces'.format(ntraces))

    for ntf, thispwr in enumerate(allsegs_thispwrbin_pwr):
        thispha = allsegs_thispwrbin_phaxfreq[ntf]
        thisphaunc = (180.0/np.pi) * allsegs_thispwrbin_uncxfreq[ntf]
        s4.errorbar(freqs[0], thispha, yerr=thisphaunc, 
                    ls='-',marker='.')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('Phase Residual (deg)')
    s4.set_xlim(freqlim)
    s4.set_ylim(phalim)
    s4.grid(which='major', color='black')
    s4.grid(which='minor', ls='--',color='grey')
    s4.minorticks_on()
    s4.xaxis.set_major_locator(tck.MultipleLocator(5))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.yaxis.set_major_locator(tck.MultipleLocator(45))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
    
    plt.suptitle('Sensing Function Systematic Error vs. Power Slice\n Power Bin: ({:.0f},{:.0f}] kW'.format(thispwrbin[0],thispwrbin[1]),fontsize=16)
    
       
    # Plot to make sure that I've re-organized the data into what the 
    # GPR fit wants
    fig, ((s1,s2),(s3,s4)) = plt.subplots(2,2,figsize=(12,8))
    
    for ntf, thispwr in enumerate(allsegs_thispwrbin_pwr):
        thismag = abs(thispwrbin_tfxfreq[ntf])
        thismagunc = thismag * thispwrbin_uncxfreq[ntf]
        s1.errorbar(thispwrbin_freqxfreq[0], thismag, yerr=thismagunc, 
                    ls='-',marker='.',label='{:.3f} kW'.format(thispwr))
    s1.legend(fontsize=8,loc='upper right')
    s1.set_xlabel('Frequency (Hz)')
    s1.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s1.set_xlim(outfreqlim)
    s1.set_xscale('log')
    s1.set_ylim(maglim)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.minorticks_on()
    s1.xaxis.set_major_locator(tck.LogLocator(base=10))
    s1.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s1.yaxis.set_major_locator(tck.MultipleLocator(2))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
    s1.set_title('{} traces'.format(ntraces))

    for ntf, thispwr in enumerate(allsegs_thispwrbin_pwr):
        thispha = np.angle(thispwrbin_tfxfreq[ntf],deg=True)
        thisphaunc = (180.0/np.pi) * thispwrbin_uncxfreq[ntf]
        s3.errorbar(thispwrbin_freqxfreq[0], thispha, yerr=thisphaunc, 
                    ls='-',marker='.')
    s3.set_xlabel('Frequency (Hz)')
    s3.set_ylabel('Phase Residual (deg)')
    s3.set_xlim(outfreqlim)
    s3.set_xscale('log')
    s3.set_ylim(phalim)
    s3.grid(which='major', color='black')
    s3.grid(which='minor', ls='--',color='grey')
    s3.minorticks_on()
    s3.xaxis.set_major_locator(tck.LogLocator(base=10))
    s3.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s3.yaxis.set_major_locator(tck.MultipleLocator(45))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(15))
    
    s2.errorbar(thispwrbin_freq,abs(thispwrbin_tf),yerr=abs(thispwrbin_tf)*thispwrbin_tfunc,fmt='.')
    s2.set_xlabel('Frequency (Hz)')
    s2.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s2.set_xlim(outfreqlim)
    s2.set_xscale('log')
    s2.set_ylim(maglim)
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.minorticks_on()
    s2.xaxis.set_major_locator(tck.LogLocator(base=10))
    s2.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s2.yaxis.set_major_locator(tck.MultipleLocator(2))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
    s2.set_title('Fake data: {} freq. points of value {}+/-{},\n log-spaced between {} and {} Hz'.format(nExtraPoints,extrapointval, extrapointunc, lower_flim_MCMC, upper_flim_rrnom))
    
    s4.errorbar(thispwrbin_freq,np.angle(thispwrbin_tf,deg=True),yerr=(180.0/np.pi)*thispwrbin_tfunc,fmt='.')
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('Phase Residual (deg)')
    s4.set_xlim(outfreqlim)
    s4.set_xscale('log')
    s4.set_ylim(phalim)
    s4.grid(which='major', color='black')
    s4.grid(which='minor', ls='--',color='grey')
    s4.minorticks_on()
    s4.xaxis.set_major_locator(tck.LogLocator(base=10))
    s4.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s4.yaxis.set_major_locator(tck.MultipleLocator(45))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
    
    plt.suptitle('Sensing Function Systematic Error Power Slice Data Reorganization for GPR Input\n Power Bin: ({:.0f},{:.0f}] kW'.format(thispwrbin[0],thispwrbin[1]),fontsize=16)
    
    
    # Plot to show the distribution of data in each frequency slice within a power bin
    fig, axshndls = plt.subplots(2,4,figsize=(12,8))

    for nfreq, thisfreq in enumerate(freqs[0]):
        (bincount, thesebins, patches) = axshndls[0,nfreq].hist(abs(allsegs_thispwrbin_tfxfreq[:,nfreq]),bins='auto',orientation='horizontal')
        axshndls[0,nfreq].hlines(y=thispwrbin_magcixfreq[nfreq,:],xmin=histbinlim[0],xmax=histbinlim[1],linestyles='--',colors=ciColors)
        nbins = len(thesebins)
        axshndls[1,nfreq].hist(np.angle(allsegs_thispwrbin_tfxfreq[:,nfreq],deg=True),bins=nbins,orientation='horizontal')
        axshndls[1,nfreq].hlines(y=thispwrbin_phacixfreq[nfreq,:],xmin=histbinlim[0],xmax=histbinlim[1],linestyles='--',colors=ciColors)
        axshndls[1,nfreq].set_title('f = {} Hz'.format(thisfreq),fontsize='xx-large')
        
        for mhndl in [0,1]:
            if mhndl == 0:
                axshndls[mhndl,0].set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)',fontsize=10)
                axshndls[mhndl,nfreq].set_ylim([thispwrbin_magcixfreq[nfreq,-1]*(1-0.2),thispwrbin_magcixfreq[nfreq,3]*(1+0.2)])
            elif mhndl == 1:
                axshndls[mhndl,0].set_ylabel('Phase Residual (deg)',fontsize=10)
                axshndls[mhndl,nfreq].set_ylim([thispwrbin_phacixfreq[nfreq,-1]*(1-np.sign(thispwrbin_phacixfreq[nfreq,-1])*0.2),thispwrbin_phacixfreq[nfreq,3]*(1+np.sign(thispwrbin_phacixfreq[nfreq,3])*0.2)])
                axshndls[mhndl,nfreq].set_xlabel('Bin Count ( )')
            axshndls[mhndl,nfreq].grid(which='major', color='black')
            axshndls[mhndl,nfreq].grid(which='minor', ls='--',color='grey')
            axshndls[mhndl,nfreq].minorticks_on()
            axshndls[mhndl,nfreq].set_xlim(histbinlim)
    
    plt.suptitle('Sensing Function Systematic Error Power Slice Frequency Point Histogram\n Power Bin: ({:.0f},{:.0f}] kW, having N = {} traces put into {} bins'.format(thispwrbin[0],thispwrbin[1],ntraces,nbins),fontsize=16)
    
    '''
    # Plot show the GPR fit
    fig, ((s1,s2),(s3,s4)) = plt.subplots(2,2,figsize=(12,8))
    
    lnstl = 'none'
    mkr = 'o'
    mkrclr = 'g'
    mkrsz = 10
    
    s1.errorbar(thispwrbin_full_freq,abs(thispwrbin_full_tf),yerr=abs(thispwrbin_full_tf)*thispwrbin_full_tfunc,fmt='b.',label='Raw Data')
    #s1.plot(thispwrbin_freq,abs(thispwrbin_tf),color=mkrclr,marker=mkr,markersize=mkrsz,ls=lnstl,fillstyle='none',alpha=0.5,label='Data Used in Fit (f = ({:.1f},{:.1f}) Hz'.format(fitfreqlim[0],fitfreqlim[1]))
    s1.plot(freqout, abs(thispwrbin_tfgprmodel), 'c-', zorder=4,label='GPR Fit Median')
    s1.fill(np.concatenate([freqout, freqout[::-1]]), np.concatenate([ abs(thispwrbin_tfgprmodel) - thispwrbin_tfgpr68ci, (abs(thispwrbin_tfgprmodel) + thispwrbin_tfgpr68ci)[::-1] ]), alpha=.5, fc='c', ec='None', label='GPR Fit 68% C.I.', zorder=3)
    for nfreq, thisfreq in enumerate(freqs[0]):
        for ici in [1,2,3]:
            thismedian = thispwrbin_magcixfreq[nfreq,0]
            thisci=[[thismedian-thispwrbin_magcixfreq[nfreq,ici+3]],[thispwrbin_magcixfreq[nfreq,ici]-thismedian]]
            #thislabel = '{:.3f} +{:.3f}/-{:.3f} ({}-sigma C.I.)'.format(thismedian,thisci[0][0],thisci[1][0],ici)
            thislabel = 'median +/- {}-sigma C.I.'.format(ici) if nfreq == 0 else None
            s1.errorbar(thisfreq,thismedian,yerr=thisci,fmt='o', markersize=8, capsize=10,color=ciColors[ici],markeredgecolor=ciColors[0],fillstyle='none',label=thislabel)
    s1.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s1.set_xlim(outfreqlim)
    s1.set_xscale('log')
    s1.set_ylim(maglim)
    s1.grid(which='major', color='black')
    s1.grid(which='minor', ls='--',color='grey')
    s1.minorticks_on()
    s1.xaxis.set_major_locator(tck.LogLocator(base=10))
    s1.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s1.yaxis.set_major_locator(tck.MultipleLocator(2))
    s1.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
    s1.set_title('{} traces'.format(ntraces))
    s1.legend(loc='upper right')
    
    s3.errorbar(thispwrbin_full_freq,np.angle(thispwrbin_full_tf,deg=True),yerr=(180.0/np.pi)*thispwrbin_full_tfunc,fmt='b.')
    #s3.plot(thispwrbin_freq,np.angle(thispwrbin_tf,deg=True),color=mkrclr,marker=mkr,markersize=mkrsz,ls=lnstl,fillstyle='none',alpha=0.5)
    s3.plot(freqout, np.angle(thispwrbin_tfgprmodel, deg=True), 'c-', zorder=4)
    s3.fill(np.concatenate([freqout, freqout[::-1]]), np.concatenate([ np.angle(thispwrbin_tfgprmodel, deg=True) - (180.0/np.pi)*thispwrbin_tfgpr68ci, (np.angle(thispwrbin_tfgprmodel, deg=True) + (180.0/np.pi)*thispwrbin_tfgpr68ci)[::-1] ]), alpha=.5, fc='c', ec='None', zorder=3)
    for nfreq, thisfreq in enumerate(freqs[0]):
        for ici in [1,2,3]:
            thismedian = thispwrbin_phacixfreq[nfreq,0]
            thisci=[[thispwrbin_phacixfreq[nfreq,ici]-thismedian],[thismedian-thispwrbin_phacixfreq[nfreq,ici+3]]]
            #thislabel = '{:.3f} +{:.3f}/-{:.3f} ({}-sigma C.I.)'.format(thismedian,thisci[0][0],thisci[1][0],ici)
            s3.errorbar(thisfreq,thismedian,yerr=thisci,fmt='o', markersize=8, capsize=10,color=ciColors[ici],markeredgecolor=ciColors[0],fillstyle='none',label=thislabel)
    s3.set_title('Prior Kernel = {}**2 * RBF(length_scale=({:.2f}, {:.2f})) + ({:.2f}, {:.2f})**2'.format(str(amountoffreqdependencecoeff_range), rbf_lengthscale_range[0],rbf_lengthscale_range[1],whennoresidualtfunc_range[0],whennoresidualtfunc_range[1]),size='medium',wrap=True, y=1.01)
    s3.set_xlabel('Frequency (Hz)')
    s3.set_ylabel('Phase Residual (deg)')
    s3.set_xlim(outfreqlim)
    s3.set_xscale('log')
    s3.set_ylim(phalim)
    s3.grid(which='major', color='black')
    s3.grid(which='minor', ls='--',color='grey')
    s3.minorticks_on()
    s3.xaxis.set_major_locator(tck.LogLocator(base=10))
    s3.xaxis.set_minor_locator(tck.LogLocator(base=10,subs=list(np.arange(1,10))))
    s3.yaxis.set_major_locator(tck.MultipleLocator(15))
    s3.yaxis.set_minor_locator(tck.MultipleLocator(5))
    #s3.legend(loc='upper right')
    
    s2.errorbar(thispwrbin_full_freq,abs(thispwrbin_full_tf),yerr=abs(thispwrbin_full_tf)*thispwrbin_full_tfunc,fmt='b.')
    #s2.plot(thispwrbin_freq,abs(thispwrbin_tf),color=mkrclr,marker=mkr,markersize=mkrsz,ls=lnstl,fillstyle='none',alpha=0.5)
    s2.plot(freqout, abs(thispwrbin_tfgprmodel), 'c-', zorder=4)
    s2.fill(np.concatenate([freqout, freqout[::-1]]), np.concatenate([ abs(thispwrbin_tfgprmodel) - thispwrbin_tfgpr68ci, (abs(thispwrbin_tfgprmodel) + thispwrbin_tfgpr68ci)[::-1] ]), alpha=.5, fc='c', ec='None', label='68% confidence', zorder=3)
    for nfreq, thisfreq in enumerate(freqs[0]):
        for ici in [1,2,3]:
            thismedian = thispwrbin_magcixfreq[nfreq,0]
            thisci=[[thismedian-thispwrbin_magcixfreq[nfreq,ici+3]],[thispwrbin_magcixfreq[nfreq,ici]-thismedian]]
            s2.errorbar(thisfreq,thismedian,yerr=thisci,fmt='o', markersize=8, capsize=10,color=ciColors[ici],markeredgecolor=ciColors[0],fillstyle='none')
    s2.set_ylabel('Magnitude Residual \n(ct/m)/(ct/m)')
    s2.set_xlim(uzfreqlim)
    s2.set_ylim(zmaglim)
    s2.grid(which='major', color='black')
    s2.grid(which='minor', ls='--',color='grey')
    s2.minorticks_on()
    s2.xaxis.set_major_locator(tck.MultipleLocator(5))
    s2.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s2.yaxis.set_major_locator(tck.MultipleLocator(0.05))
    s2.yaxis.set_minor_locator(tck.MultipleLocator(0.01))
    
    s4.errorbar(thispwrbin_full_freq,np.angle(thispwrbin_full_tf,deg=True),yerr=(180.0/np.pi)*thispwrbin_full_tfunc,fmt='b.')
    #s4.plot(thispwrbin_freq,np.angle(thispwrbin_tf,deg=True),color=mkrclr,marker=mkr,markersize=mkrsz,ls=lnstl,fillstyle='none',alpha=0.5)
    s4.plot(freqout, np.angle(thispwrbin_tfgprmodel, deg=True), 'c-', zorder=4)
    s4.fill(np.concatenate([freqout, freqout[::-1]]), np.concatenate([ np.angle(thispwrbin_tfgprmodel, deg=True) - (180.0/np.pi)*thispwrbin_tfgpr68ci, (np.angle(thispwrbin_tfgprmodel, deg=True) + (180.0/np.pi)*thispwrbin_tfgpr68ci)[::-1] ]), alpha=.5, fc='c', ec='None', zorder=3)
    for nfreq, thisfreq in enumerate(freqs[0]):
        for ici in [1,2,3]:
            thismedian = thispwrbin_phacixfreq[nfreq,0]
            thisci=[[thispwrbin_phacixfreq[nfreq,ici]-thismedian],[thismedian-thispwrbin_phacixfreq[nfreq,ici+3]]]
            s4.errorbar(thisfreq,thismedian,yerr=thisci,fmt='o', markersize=8, capsize=10,color=ciColors[ici],markeredgecolor=ciColors[0],fillstyle='none')
    s4.set_title('Posterior Kernel = {}; Log-likelihood = {:.3f}'.format(thispwrbin_gpr_obj.kernel,thispwrbin_gpr_obj.log_marginal_likelihood(thispwrbin_gpr_obj.kernel_.theta)),size='medium',wrap=True, y=1.01)
    s4.set_xlabel('Frequency (Hz)')
    s4.set_ylabel('Phase Residual (deg)')
    s4.set_xlim(uzfreqlim)
    s4.set_ylim(zphalim)
    s4.grid(which='major', color='black')
    s4.grid(which='minor', ls='--',color='grey')
    s4.minorticks_on()
    s4.xaxis.set_major_locator(tck.MultipleLocator(5))
    s4.xaxis.set_minor_locator(tck.MultipleLocator(1))
    s4.yaxis.set_major_locator(tck.MultipleLocator(5))
    s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
    
    
    plt.suptitle('Sensing Function Systematic Error Power Slice GPR Fit\n Power Bin: ({:.0f},{:.0f}] kW'.format(thispwrbin[0],thispwrbin[1]),fontsize=16)
    
    
#pp = PdfPages('sensingFunction_syserror_vs_powerslices_originaldata_vs_binneddata.pdf')
#figs = [plt.figure(n) for n in plt.get_fignums()]
#for fig in figs:
#    fig.savefig(pp, format='pdf')
#pp.close()
#plt.close('all')

#pp = PdfPages('sensingFunction_syserror_vs_powerslices_binneddata_vs_gprinputdata.pdf')
#figs = [plt.figure(n) for n in plt.get_fignums()]
#for fig in figs:
#    fig.savefig(pp, format='pdf')
#pp.close()
#plt.close('all')

#pp = PdfPages('sensingFunction_syserror_vs_powerslices_binneddata_freqhist.pdf')
#figs = [plt.figure(n) for n in plt.get_fignums()]
#for fig in figs:
#    fig.savefig(pp, format='pdf')
#pp.close()
#plt.close('all')

pp = PdfPages('sensingFunction_syserror_vs_powerslices_gprfit_lengthscale_{}_nxtrapnts{}.pdf'.format(str(rbf_lengthscale_value).replace('.','p'),nExtraPoints))
figs = [plt.figure(n) for n in plt.get_fignums()]
for fig in figs:
    fig.savefig(pp, format='pdf')
pp.close()
plt.close('all')
