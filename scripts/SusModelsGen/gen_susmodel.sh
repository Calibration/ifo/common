#!/bin/bash

# VladBoss 20240110
# Bash script that generates a "python friendly" ZPK Length-to-Length transfer funtion that is called by pyDARM parameter file that lives in /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/params/
# The first arguement is the tagged model that you want to convert.
# usage example:
#
# ./gen_O3_susmodel.sh '/ligo/svncommon/SusSVN/sus/trunk/Common/SusModelTags/Matlab/quadmodelproduction-rev9944_ssmake4pv2eMB5f_fiber-rev8442_h1etmx-rev10098_h1etmx_options-rev10098_released-2019-12-16.mat'
#
# The output file is dated within the python script step, and uses UTC time.

if [[ $@ == *"h1"* ]]; then
        if [[ "${IFO}" == "L1" ]]; then
                read -p "Processing H1 file, but you are in L1! Are you sure? Press Y to confirm" -n 1 -r
                echo    
                if [[ ! $REPLY =~ ^[Yy]$ ]]
                then 
                        exit 1
                fi
        fi
        matlab -nodesktop -nosplash -r "ifo = 'h1'; originFile = '$@'; export_SUS; exit"
        if [[ $@ == *"etmx"* ]]; then
                python3 sus_ss2zpk.py 'H1' 'EX' $@
        elif [[ $@ == *"etmy"* ]]; then
                python3 sus_ss2zpk.py 'H1' 'EY' $@
        elif [[ $@ == *"itmx"* ]]; then
                python3 sus_ss2zpk.py 'H1' 'IX' $@
        elif [[ $@ == *"itmy"* ]]; then
                python3 sus_ss2zpk.py 'H1' 'IY' $@
        fi
elif [[ $@ == *"l1"* ]]; then
        if [[ "${IFO}" == "H1" ]]; then
                read -p "Processing L1 file, but you are in H1! Are you sure? Press Y to confirm" -n 1 -r
                echo    
                if [[ ! $REPLY =~ ^[Yy]$ ]]
                then 
                        exit 1
                fi
        fi
        matlab -nodesktop -nosplash -r "ifo = 'l1'; originFile = '$@'; export_SUS; exit"
        if [[ $@ == *"etmx"* ]]; then
                python3 sus_ss2zpk.py 'L1' 'EX' $@
        elif [[ $@ == *"etmy"* ]]; then
                python3 sus_ss2zpk.py 'L1' 'EY' $@
        elif [[ $@ == *"itmx"* ]]; then
                python3 sus_ss2zpk.py 'L1' 'IX' $@
        elif [[ $@ == *"itmy"* ]]; then
                python3 sus_ss2zpk.py 'L1' 'IY' $@
        fi
fi

# Clean up the matlab intermidate file
rm susdata_ss.mat
