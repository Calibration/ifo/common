#This script takes the intermidate SS TF and spits out a ZPK file with todays date out the front.

import os
import sys
import numpy as np
import scipy
import scipy.io as spio
import scipy.signal as signal
import datetime

#################################################################################################################################################################################
#Functions [ss2zpk Lifted, with modification, from iirrational as it exists today. https://git.ligo.org/lee-mcculler/iirrational/-/blob/master/IIRrational/TFmath/statespace.py ]
#          [freqrespZPK Lifted from old pyDARM]
#################################################################################################################################################################################

def freqrespZPK(syst, w):
    s = s = 1j * np.complex128(w)
    numPoles = len(syst.poles)
    g = syst.gain * np.ones(len(w),dtype='complex128')
    
    sortedPolesIdx = np.argsort(abs(syst.poles))
    sortedZerosIdx = np.argsort(abs(syst.zeros))
    
    for i in range(0, numPoles):
        if i < len(syst.zeros):
            tmp = s - syst.zeros[sortedZerosIdx[i]]
        else:
            tmp = 1
        g = g * tmp / (s - syst.poles[sortedPolesIdx[i]])

    return g

def ss2zpk(
    A, B, C, D, E = None,
    idx_in = None,
    idx_out = None,
    F_match_Hz = 1e-10
):
    if idx_in is None:
        if B.shape[1] == 1:
            idx_in = 0
        else:
            raise RuntimeError("Must specify idx_in if B indicates MISO/MIMO system")
    if idx_out is None:
        if C.shape[0] == 1:
            idx_out = 0
        else:
            raise RuntimeError("Must specify idx_in if C indicates SIMO/MIMO system")
    B = B[:, idx_in:idx_in+1]
    C = C[idx_out:idx_out+1, :]
    D = D[idx_out:idx_out+1, idx_in:idx_in+1]

    if E is None:
        p = scipy.linalg.eig(A, left = False, right = False)
    else:
        p = scipy.linalg.eig(A, E, left = False, right = False)
    SS = np.block([[A, B], [C, D]])
    if E is None:
        z = scipy.linalg.eig(
            a = SS,
            b = np.diag(np.concatenate([np.ones(A.shape[0]), np.zeros(1)])),
            left = False,
            right = False
        )
    else:
        SSE = np.block([
            [E                                  , np.zeros(E.shape[0]).reshape(-1, 1)],
            [np.zeros(E.shape[1]).reshape(1, -1), np.zeros(1).reshape(1, 1)]
        ])
        z = scipy.linalg.eig(
            a = SS,
            b = SSE,
            left = False,
            right = False
        )
    z = np.asarray([_ for _ in z if np.isfinite(_.real)])
    k = 1
    s_match_wHz = F_match_Hz * 2j * np.pi
    tf0 = (np.matmul(C, np.matmul(np.linalg.inv(np.eye(A.shape[0])*s_match_wHz - A), B)) + D)[..., 0, 0]
    zpk0 = freqrespZPK(signal.ZerosPolesGain(z,p,1),[2*np.pi*F_match_Hz])
    k = np.sign(tf0 / zpk0) * np.abs(tf0 / zpk0)
    return z, p, k

#########################
#Import StateSpace filter
#########################

susresp=spio.loadmat('./susdata_ss.mat')
TSTA = susresp['TSTA']
TSTB = susresp['TSTB']
TSTC = susresp['TSTC']
TSTD = susresp['TSTD']
PUMA = susresp['PUMA']
PUMB = susresp['PUMB']
PUMC = susresp['PUMC']
PUMD = susresp['PUMD']
UIMA = susresp['UIMA']
UIMB = susresp['UIMB']
UIMC = susresp['UIMC']
UIMD = susresp['UIMD']

####################################
#Calculate new zpks and Save
####################################

TSTz, TSTp, TSTk = ss2zpk(TSTA,TSTB,TSTC,TSTD, F_match_Hz = 0)
PUMz, PUMp, PUMk = ss2zpk(PUMA,PUMB,PUMC,PUMD, F_match_Hz = 0)
UIMz, UIMp, UIMk = ss2zpk(UIMA,UIMB,UIMC,UIMD, F_match_Hz = 0)

#Generate Date in UTC string, and IFO string:
IFO = sys.argv[1]
ARM = sys.argv[2]
modelDate = (sys.argv[3])[-14:-4]
dateinterp = datetime.datetime.strptime(modelDate, '%Y-%m-%d')
date = dateinterp.strftime("%Y%m%d")

outDir = f'/ligo/groups/cal/{IFO}/arx/susmodels/'

spio.savemat(f'{outDir}{date}_{IFO}_{ARM}_susdata.mat',{'UIMz':np.transpose([UIMz]),'UIMp':np.transpose([UIMp]),'UIMk':np.transpose([np.real(UIMk)]),'PUMz':np.transpose([PUMz]),'PUMp':np.transpose([PUMp]),'PUMk':np.transpose([np.real(PUMk)]),'TSTz':np.transpose([TSTz]),'TSTp':np.transpose([TSTp]),'TSTk':np.transpose([np.real(TSTk)]),'originFile':susresp['originFile'][0]})
