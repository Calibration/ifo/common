file = load(originFile);    
tstSUSresp = file.quadModel.dampedss(file.quadModel.dampedout.tst.disp.L,file.quadModel.dampedin.tst.drive.L);
pumSUSresp = file.quadModel.dampedss(file.quadModel.dampedout.tst.disp.L,file.quadModel.dampedin.pum.drive.L);
uimSUSresp = file.quadModel.dampedss(file.quadModel.dampedout.tst.disp.L,file.quadModel.dampedin.uim.drive.L);

TSTA = tstSUSresp.A; TSTB = tstSUSresp.B; TSTC = tstSUSresp.C; TSTD = tstSUSresp.D;
PUMA = pumSUSresp.A; PUMB = pumSUSresp.B; PUMC = pumSUSresp.C; PUMD = pumSUSresp.D;
UIMA = uimSUSresp.A; UIMB = uimSUSresp.B; UIMC = uimSUSresp.C; UIMD = uimSUSresp.D;

save susdata_ss.mat UIMA UIMB UIMC UIMD PUMA PUMB PUMC PUMD TSTA TSTB TSTC TSTD originFile
