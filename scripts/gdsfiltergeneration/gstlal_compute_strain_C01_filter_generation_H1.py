import pydarm

config = "../../../H1/pydarm_H1.ini"
filtersfilename = 'gstlal_compute_strain_C01_filters_H1.npz'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../H1', output_filename=filtersfilename, file_format='npz', mode='DCS', plots_directory='./' + filtersfilename + '_plots/')
