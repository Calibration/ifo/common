import pydarm

config = "../../../H1/pydarm_H1.ini"
filtersfilename = 'gstlal_compute_strain_C00_filters_H1'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../H1', output_filename=filtersfilename, file_format='npz', mode='GDS', plots_directory='./' + filtersfilename + '_plots/')
