import pydarm

config = "../../../L1/pydarm_L1.ini"
filtersfilename = 'gstlal_correct_strain_filters_L1.h5'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../L1', output_filename=filtersfilename, file_format='hdf5', mode='CALCS_corr', plots_directory='./' + filtersfilename + '_plots/')
