import pydarm

config = "../../../H1/pydarm_H1.ini"
filtersfilename = 'gstlal_correct_strain_filters_H1.h5'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../H1', output_filename=filtersfilename, file_format='hdf5', mode='CALCS_corr', plots_directory='./' + filtersfilename + '_plots/')
