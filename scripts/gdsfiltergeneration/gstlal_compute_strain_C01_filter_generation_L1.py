import pydarm

config = "../../../L1/pydarm_L1.ini"
filtersfilename = 'gstlal_compute_strain_C01_filters_L1.npz'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../L1', output_filename=filtersfilename, file_format='npz', mode='DCS', plots_directory='./' + filtersfilename + '_plots/')
