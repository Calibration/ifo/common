import pydarm

config = "../../../L1/pydarm_L1.ini"
filtersfilename = 'gstlal_compute_strain_C00_filters_L1'

pydarm.fir.FIRFilterFileGeneration(config, output_dir='../../../L1', output_filename=filtersfilename, file_format='npz', mode='GDS', plots_directory='./' + filtersfilename + '_plots/')
