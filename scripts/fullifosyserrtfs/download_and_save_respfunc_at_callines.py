## ################# ##
## Environment Setup ##
## ################# ##

# Do this in a terminal before running this script:
# $ kinit
# $ conda activate cds-pydarm-test
# $ python -m pydarm --version
# 0.1.2.dev85+gffcb6ba
# $ export NDSSERVER="nds.ligo-wa.caltech.edu,$NDSSERVER"
# $ svn up /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/
# $ git pull /ligo/gitcommon/Calibration/ifo/pydarmparams/
# $ git pull /ligo/gitcommon/Calibration/ifo/scripts/fullifosyserrtfs/

from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.time import * # tconvert, to_gps, from_gps
import numpy as np
import pydarm
import nds2

## ################# ##
## Hard-coded Params ##
## ################# ##

FFT_LENGTH = 40     # sec
FFT_OVERLAP = 20    # sec
FFT_STRIDE = 120    # sec
FFT_WINDOW = 'hann'

pcalx_chan = 'H1:CAL-PCALX_RX_PD_OUT_DQ'
pcaly_chan = 'H1:CAL-PCALY_RX_PD_OUT_DQ'
strain_chan = 'H1:GDS-CALIB_STRAIN'

isclockstate_chan = 'H1:GRD-ISC_LOCK_STATE_N'
k_C_ccs_chan = 'H1:CAL-CS_TDEP_KAPPA_C_OUTPUT'
f_C_ccs_chan = 'H1:CAL-CS_TDEP_F_C_OUTPUT'
k_U_ccs_chan = 'H1:CAL-CS_TDEP_KAPPA_UIM_REAL_OUTPUT'
k_P_ccs_chan = 'H1:CAL-CS_TDEP_KAPPA_PUM_REAL_OUTPUT'
k_T_ccs_chan = 'H1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT'
k_C_gds_chan = 'H1:GDS-CALIB_KAPPA_C'
f_C_gds_chan = 'H1:GDS-CALIB_F_CC'
k_U_gds_chan = 'H1:GDS-CALIB_KAPPA_UIM_REAL'
k_P_gds_chan = 'H1:GDS-CALIB_KAPPA_PUM_REAL'
k_T_gds_chan = 'H1:GDS-CALIB_KAPPA_TST_REAL'
xarmpwr_chan = 'H1:ASC-X_PWR_CIRC_OUT16'
yarmpwr_chan = 'H1:ASC-Y_PWR_CIRC_OUT16'

NDSSRVR = 'nds.ligo-wa.caltech.edu'
NDSPORT = 31200

pcalx_freq = np.array([33.43, 53.67, 77.73, 102.13, 283.91])
pcaly_freq = np.array([8.925, 11.575, 15.275, 17.1, 24.5, 284.01, 410.3, 1083.7, 1153.2])

gitdir = '/ligo/gitcommon/Calibration/ifo/pydarmparams/'
svndir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'

datadir = svndir + 'Runs/O4/H1/Measurements/SystematicErrorTFs/Thermalization/'

power_eras = [75,
              60]

pydarm_params_refmodels = [gitdir + 'pydarm_H1_20230510T062635Z.ini', # 75W
                           gitdir + 'pydarm_H1_20230621T211522Z.ini'] # 60W

pydarm_params_no3p2kmodels = [gitdir + 'pydarm_H1_20230510T062635Z_no3p2k.ini', # 75W
                              gitdir + 'pydarm_H1_20230621T211522Z_no3p2k.ini'] # 60W


# Eventually, use the whole list of 4 hour stretches at each power.            
gpstime_startstop = [np.array([[1367752698, 1367767098],    # 75 W
                               [1368087318, 1368099918],
                               [1368161418, 1368175818],
                               [1368247698, 1368262038],
                               [1368384498, 1368396018],
                               [1368442818, 1368457218],
                               [1368559818, 1368574218],
                               [1368603918, 1368618318],
                               [1368632418, 1368646818],
                               [1368655218, 1368669618]]),
                     np.array([[1371444918, 1371459318],    # 60 W
                               [1374536538, 1374550938],
                               [1374610218, 1374624618],
                               [1374720618, 1374735018],
                               [1374783798, 1374798198],
                               [1374831318, 1374845718],
                               [1374861918, 1374876318],
                               [1374997878, 1375012278],
                               [1375149798, 1375164198],
                               [1375275438, 1375289838],
                               [1375342818, 1375357218],
                               [1375566738, 1375581138]])]


'''
# Just two times for starters
gpstime_startstop = [np.array([[1368655218, 1368669618]]), # 75 W
                     np.array([[1374783798, 1374798198]])] # 60 W   
'''
          
'''
# Just 15 minutes of each of those two times for starter starters.
gpstime_startstop = [np.array([[1368655218, 1368656118]]), # 75 W
                     np.array([[1374783798, 1374784698]])] # 60 W 
'''

                               
## ####################### ##                               
## Parse Hard-coded Params ##
## ####################### ##
FFT_NAVGS = (FFT_STRIDE/FFT_LENGTH)+(FFT_LENGTH/FFT_OVERLAP)

pcal_freq = np.hstack((pcalx_freq, pcaly_freq))
sort_indicies = np.argsort(pcal_freq)

chanlist = [pcalx_chan,
            pcaly_chan,
            strain_chan,
            isclockstate_chan,
            k_C_ccs_chan,
            f_C_ccs_chan,
            k_U_ccs_chan,
            k_P_ccs_chan,
            k_T_ccs_chan,
            k_C_gds_chan,
            f_C_gds_chan,
            k_U_gds_chan,
            k_P_gds_chan,
            k_T_gds_chan,
            xarmpwr_chan,
            yarmpwr_chan]  
                 
nds2cxn = nds2.connection(NDSSRVR,NDSPORT)

## ################ ##
## Initialize Lists ##
## ################ ##
darmModel_ref_obj = []

pcalx_corr_m_per_ct = []
pcaly_corr_m_per_ct = []

# Use fancy nested generators to create an empty list of lists 
# where the transfer function of 
#    the ith Stride of 
#        the jth Lock Stretch at 
#            the kth Power 
# can be accessed as 
#    etaR_tf[kPower][jLockStretch][iStride]

# ABSOLUTE NIGHTMARE: I can't just generate one empty fancy list of lists
# to a dummy variable, and set all of my accumulating variables (tfs, times, 
# powers, tdcfs, etc) to be that dummy variables such that they all have 
# that dimensionality to it.
# Why? Because python treats that as I'm setting all those variables *to 
# the same list* and when I try later to write different values to 
# the [kPower][jLockStretch][iStride] element of each accumulating variables,
# all the *different* variables end up *getting set the same value*.
# so 
#     k_C_ccs[kPower][jLockStretch][iStride]  = this
#     f_C_ccs[kPower][jLockStretch][iStride]  = is
#     k_U_ccs[kPower][jLockStretch][iStride]  = how
#     k_P_ccs[kPower][jLockStretch][iStride]  = code
#     k_T_ccs[kPower][jLockStretch][iStride]  = should
#     etaR_tf[kPower][jLockStretch][iStride]  = work
#     etaR_unc[kPower][jLockStretch][iStride] = dammit
#     
# DOESN'T WORK.
# At the end of the loop, all of these variables get set to "should." That's 
# right, not "this" or "dammit," but one of the random variable setting in the
# middle. 
# WHO WRITES A CODE LANGUAGE LIKE THAT. WTF.

# So, I have to add one more layer of stupidity surrounding the super nested generator -- a for loop generate
# tuple that unpacks a *copy* of the super nested generator in each of the variables. 
# *WOW* PYTHON IS BAD AT SOPHISTICED MATRIX MANIPULATION

etaR_tf_x, etaR_tf_y, etaR_tf = ([[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] for i in range(3))

etaR_unc_x, etaR_unc_y, etaR_unc = ([[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] for i in range(3))

k_C_ccs, f_C_ccs, k_U_ccs, k_P_ccs, k_T_ccs = ([[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] for i in range(5))

k_C_gds, f_C_gds, k_U_gds, k_P_gds, k_T_gds = ([[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] for i in range(5))

times = [[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] 

armpwrs = [[[[] for iStride in range(int((gpstime_startstop[kPower][:,1][jLockStretch] - gpstime_startstop[kPower][:,0][jLockStretch])/FFT_STRIDE))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] 

## ################# ##
## Download the Data ##
## ################# ##

for kPower, thisPower in enumerate(power_eras):
    darmModel_ref_obj.append(pydarm.darm.DARMModel(pydarm_params_refmodels[kPower]))
   
    pcalx_corr_m_per_ct.append(darmModel_ref_obj[kPower].pcal.compute_pcal_correction(pcalx_freq,endstation=True, arm='X'))
    pcaly_corr_m_per_ct.append(darmModel_ref_obj[kPower].pcal.compute_pcal_correction(pcaly_freq,endstation=True, arm='Y'))
    
    L = darmModel_ref_obj[kPower].sensing.mean_arm_length()

    # Loop over each 4-hour data stretch
    for jLockStretch in range(len(gpstime_startstop[kPower][:,0])):
        stretch_startgps = gpstime_startstop[kPower][:,0][jLockStretch]
        stretch_stopgps = gpstime_startstop[kPower][:,1][jLockStretch]
       
        print('\nDownloading (Power {}, Lock Stretch {}): {} W data from {} UTC to {} UTC ...\n'.format(
            kPower,
            jLockStretch,
            power_eras[kPower],
            tconvert(stretch_startgps),
            tconvert(stretch_stopgps)))
       
        # Break stretch into FFT_STRIDE amount of time, and average the result
        iStride = 0
        stride_startgps = stretch_startgps
        stride_stopgps = stride_startgps + FFT_STRIDE
       
        while stride_stopgps <= stretch_stopgps: # stride start and stop are incremented by FFT_STRIDE at the end of this while loop far below
            
            percentdone = 100*(1-(stretch_stopgps - stride_stopgps)/(stretch_stopgps - stretch_startgps))
            print('    Getting stride {} of {} W Lock Stretch {} starting at {} UTC, {:.1f}% done ... \n'.format(
                iStride + 1,
                power_eras[kPower],
                jLockStretch,
                tconvert(stride_startgps),
                percentdone))
            
            # Here's were we actually get all the data
            data = tsd.get(chanlist, stride_startgps, stride_stopgps, frametype='R', verbose=True, connection=nds2cxn)
           
            # Append the easy slow channel values to arrays over time, 
            # assigning the median of the channel over the FFT_STRIDE
            # to a single time; the middle of the FFT_STRIDE.
            times[kPower][jLockStretch][iStride] = stride_startgps + FFT_STRIDE/2.0
            
            armpwrs[kPower][jLockStretch][iStride] = np.median(np.mean(np.array([data[xarmpwr_chan].value,data[yarmpwr_chan].value]),axis=0))

            k_C_ccs[kPower][jLockStretch][iStride] = data[k_C_ccs_chan].median().value
            f_C_ccs[kPower][jLockStretch][iStride] = data[f_C_ccs_chan].median().value
            k_U_ccs[kPower][jLockStretch][iStride] = data[k_U_ccs_chan].median().value
            k_P_ccs[kPower][jLockStretch][iStride] = data[k_P_ccs_chan].median().value
            k_T_ccs[kPower][jLockStretch][iStride] = data[k_T_ccs_chan].median().value
            
            k_C_gds[kPower][jLockStretch][iStride] = data[k_C_gds_chan].median().value
            f_C_gds[kPower][jLockStretch][iStride] = data[f_C_gds_chan].median().value
            k_U_gds[kPower][jLockStretch][iStride] = data[k_U_gds_chan].median().value
            k_P_gds[kPower][jLockStretch][iStride] = data[k_P_gds_chan].median().value
            k_T_gds[kPower][jLockStretch][iStride] = data[k_T_gds_chan].median().value
            
            # ################################################################### #
            # Construct the response function systematic error transfer function, #
            #     etaR = dL_PCAL / dL_GDS in [m/m]                                #
            # from the fast channels. Remember, we want etaR to be a              #
            # multiplicative transfer function with which one corrects            #
            # Delta L_GDS with (to create one less inverse in the sea of ever     #
            # confusing plethora of inverses)                                     #
            # ################################################################### #
            
            # Raw PCALX [ct] / STRAIN [ ] transfer function 
            etaR_tf_PCALct_per_STRAIN_x = data[strain_chan].transfer_function(
                data[pcalx_chan],
                fftlength=FFT_LENGTH,
                overlap=FFT_OVERLAP,
                window=FFT_WINDOW,
                average='mean')[(pcalx_freq*FFT_LENGTH).astype(int)]
            etaR_coh_dimensionless_x = data[strain_chan].coherence(
                data[pcalx_chan],
                fftlength=FFT_LENGTH,
                overlap=FFT_OVERLAP,
                window=FFT_WINDOW)[(pcalx_freq*FFT_LENGTH).astype(int)]
            etaR_relunc_rad_x = np.sqrt((1-etaR_coh_dimensionless_x.value)/(2*(etaR_coh_dimensionless_x.value+1e-6)*FFT_NAVGS)) # pad coh.value in denominator with a small amount of coh to avoid divide-by-zero issues
            
            # Raw PCALY [ct] / STRAIN [ ] transfer function
            etaR_tf_PCALct_per_STRAIN_y = data[strain_chan].transfer_function(
                data[pcaly_chan],
                fftlength=FFT_LENGTH,
                overlap=FFT_OVERLAP,
                window=FFT_WINDOW,
                average='mean')[(pcaly_freq*FFT_LENGTH).astype(int)]
            etaR_coh_dimensionless_y = data[strain_chan].coherence(
                data[pcaly_chan],
                fftlength=FFT_LENGTH,
                overlap=FFT_OVERLAP,
                window=FFT_WINDOW)[(pcaly_freq*FFT_LENGTH).astype(int)]
            etaR_relunc_rad_y = np.sqrt((1-etaR_coh_dimensionless_y.value)/(2*(etaR_coh_dimensionless_y.value+1e-6)*FFT_NAVGS)) # pad coh.value in denominator with a small amount of coh to avoid divide-by-zero issues
            
            # Convert raw PCAL [ct] to real Delta L_PCAL [m]
            #     dL PCAL [m]    PCAL [ct]    PCAL_CORR [m/ct]
            #     ----------- = ----------- * -----------------
            #     STRAIN [ ]     STRAIN [ ]           1
            etaR_tf_PCALm_per_STRAIN_x = etaR_tf_PCALct_per_STRAIN_x * pcalx_corr_m_per_ct[kPower]
            etaR_tf_PCALm_per_STRAIN_y = etaR_tf_PCALct_per_STRAIN_y * pcaly_corr_m_per_ct[kPower]

            # Convert STRAIN to DELTAL
            #             dL PCAL [m]    PCAL [m]      1
            #     eta_R = ----------- = ---------- * -----
            #             dL GDS [m]   STRAIN [ ]    L [m]
            etaR_tf_PCALm_per_dLGDSm_x = etaR_tf_PCALm_per_STRAIN_x / L
            etaR_tf_PCALm_per_dLGDSm_y = etaR_tf_PCALm_per_STRAIN_y / L
    
            etaR_tf_x[kPower][jLockStretch][iStride] = etaR_tf_PCALm_per_dLGDSm_x.value  
            etaR_tf_y[kPower][jLockStretch][iStride] = etaR_tf_PCALm_per_dLGDSm_y.value
            
            etaR_unc_x[kPower][jLockStretch][iStride] = etaR_relunc_rad_x
            etaR_unc_y[kPower][jLockStretch][iStride] = etaR_relunc_rad_y
            
            # Create a 2D, nfreq x 3 [freq, etaR_tf, etaR_unc] array that mashes the x and y answers together
            freqbyetaR_tf_unsorted = np.vstack(
                (np.hstack((pcalx_freq,pcaly_freq)),
                 np.hstack((etaR_tf_x[kPower][jLockStretch][iStride],etaR_tf_y[kPower][jLockStretch][iStride])),
                 np.hstack((etaR_unc_x[kPower][jLockStretch][iStride],etaR_unc_y[kPower][jLockStretch][iStride]))
                )).T
                
            # Sort the 2D, [freq, etaR_tf, etaR_unc] array by frequency, 
            # and separate back into frequency, tf, and unc vectors again
            pcal_freq = np.real(freqbyetaR_tf_unsorted[freqbyetaR_tf_unsorted[:,0].argsort()][:,0])
            etaR_tf[kPower][jLockStretch][iStride] = freqbyetaR_tf_unsorted[freqbyetaR_tf_unsorted[:,0].argsort()][:,1]
            etaR_unc[kPower][jLockStretch][iStride] = freqbyetaR_tf_unsorted[freqbyetaR_tf_unsorted[:,0].argsort()][:,2]
             

            
            # Step to next FFT_STRIDE
            iStride += 1
            stride_startgps += FFT_STRIDE
            stride_stopgps += FFT_STRIDE

        nStrides = iStride # (works because counting started at zero)
        
        # Save the data!
        #
        # Turn all the values over the lock stretch from a list into an np.array.
        
        fileTag = '{}{}W/etaR_PCALdLmeas_over_GDSdLmodel_{}W_{}-{}'.format(datadir,power_eras[kPower],power_eras[kPower],stretch_startgps,stretch_stopgps)
        
        out_timetime_x, out_freqfreq_x = np.meshgrid(np.array(times[kPower][jLockStretch]),pcalx_freq)
        out_pwrpwr_x, out_freqfreq_x = np.meshgrid(np.array(armpwrs[kPower][jLockStretch]),pcalx_freq)
        
        np.savetxt('{}_timetime_x.txt'.format(fileTag), out_timetime_x, fmt='%.12g')
        np.savetxt('{}_freqfreq_x.txt'.format(fileTag), out_freqfreq_x, fmt='%.7g')
        np.savetxt('{}_pwrpwr_x.txt'.format(fileTag), out_pwrpwr_x, fmt='%.7g')
        
        out_timetime_y, out_freqfreq_y = np.meshgrid(np.array(times[kPower][jLockStretch]),pcaly_freq)
        out_pwrpwr_y, out_freqfreq_y = np.meshgrid(np.array(armpwrs[kPower][jLockStretch]),pcaly_freq)
        
        np.savetxt('{}_timetime_y.txt'.format(fileTag), out_timetime_y, fmt='%.12g')
        np.savetxt('{}_freqfreq_y.txt'.format(fileTag), out_freqfreq_y, fmt='%.7g')
        np.savetxt('{}_pwrpwr_y.txt'.format(fileTag), out_pwrpwr_y, fmt='%.7g')
        
        out_timetime, out_freqfreq = np.meshgrid(np.array(times[kPower][jLockStretch]),pcal_freq)
        out_pwrpwr, out_freqfreq = np.meshgrid(np.array(armpwrs[kPower][jLockStretch]),pcal_freq)
        
        np.savetxt('{}_timetime.txt'.format(fileTag), out_timetime, fmt='%.12g')
        np.savetxt('{}_freqfreq.txt'.format(fileTag), out_freqfreq, fmt='%.7g')
        np.savetxt('{}_pwrpwr.txt'.format(fileTag), out_pwrpwr, fmt='%.7g')
        
        out_mag_x = abs(np.array(etaR_tf_x[kPower][jLockStretch]))
        out_pha_x = np.angle(np.array(etaR_tf_x[kPower][jLockStretch]))
        out_unc_x = abs(np.array(etaR_unc_x[kPower][jLockStretch])) # unc array is entirely real already, just using abs to cheat the formatting of the file
        
        np.savetxt('{}_mag_x.txt'.format(fileTag), out_mag_x, fmt='%.7g')
        np.savetxt('{}_pha_x.txt'.format(fileTag), out_pha_x, fmt='%.7g')
        np.savetxt('{}_unc_x.txt'.format(fileTag), out_unc_x, fmt='%.7g')
        
        out_mag_y = abs(np.array(etaR_tf_y[kPower][jLockStretch]))
        out_pha_y = np.angle(np.array(etaR_tf_y[kPower][jLockStretch]))
        out_unc_y = abs(np.array(etaR_unc_y[kPower][jLockStretch])) # unc array is entirely real already, just using abs to cheat the formatting of the file
        
        np.savetxt('{}_mag_y.txt'.format(fileTag), out_mag_y, fmt='%.7g')
        np.savetxt('{}_pha_y.txt'.format(fileTag), out_pha_y, fmt='%.7g')
        np.savetxt('{}_unc_y.txt'.format(fileTag), out_unc_y, fmt='%.7g')
        
        out_mag = abs(np.array(etaR_tf[kPower][jLockStretch]))
        out_pha = np.angle(np.array(etaR_tf[kPower][jLockStretch]))
        out_unc = abs(np.array(etaR_unc[kPower][jLockStretch])) # unc array is entirely real already, just using abs to cheat the formatting of the file
        
        np.savetxt('{}_mag.txt'.format(fileTag), out_mag, fmt='%.7g')
        np.savetxt('{}_pha.txt'.format(fileTag), out_pha, fmt='%.7g')
        np.savetxt('{}_unc.txt'.format(fileTag), out_unc, fmt='%.7g')
        
        out_tdcfs_ccs = np.vstack((np.array(k_C_ccs[kPower][jLockStretch]),
                                   np.array(f_C_ccs[kPower][jLockStretch]),
                                   np.array(k_U_ccs[kPower][jLockStretch]),
                                   np.array(k_P_ccs[kPower][jLockStretch]),
                                   np.array(k_T_ccs[kPower][jLockStretch]))).T
        out_tdcfs_gds = np.vstack((np.array(k_C_gds[kPower][jLockStretch]),
                                   np.array(f_C_gds[kPower][jLockStretch]),
                                   np.array(k_U_gds[kPower][jLockStretch]),
                                   np.array(k_P_gds[kPower][jLockStretch]),
                                   np.array(k_T_gds[kPower][jLockStretch]))).T
                                   
        np.savetxt('{}_tdcfs_ccs.txt'.format(fileTag), out_tdcfs_ccs, fmt='%.7g')
        np.savetxt('{}_tdcfs_gds.txt'.format(fileTag), out_tdcfs_gds, fmt='%.7g')

        print('   %%%% Done with (Power {}, Time {})\n'.format(kPower,jLockStretch))
