from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner

# This won't be needed for normal comparisons, I hope. See NASTINESS below.
from copy import deepcopy

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

run = 'O3'
IFO = 'H1'
measDate = '2022-11-24'
measTime = ['2022-11-23_2236UTC',
            '2022-11-23_2243UTC',
            '2022-11-24_0002UTC',
            '2022-11-24_0008UTC']
measType = ['BB',
            'SS',
            'BB',
            'SS']
measPlotAlpha = [0.5,
                 1.0,
                 0.5,
                 1.0]
measMarkerSize = [5,
                  10,
                  5,
                  10]
measColor = ['C02',
             'C03',
             'C06',
             'C07']
measCALModelVersion = ['Fudge',
                       'Fudge',
                       '20221118',
                       '20221118']
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')

verbose = True
printFigs = True

resultsDir = \
cal_data_root+'/Runs/O3/'+IFO+'/Results/Uncertainty/'

anythingExtraInTheFigTag = '_prevspostcalibrationchange'

COHTHRESH_DARMOLGTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

deltal_chan_name = '{}:CAL-DELTAL_EXTERNAL_DQ'\
    .format(\
        IFO)
pcal_chan_name = '{}:CAL-{}_PD_OUT_DQ'\
    .format(\
        IFO,\
        refPCAL)
        
figTag ='syserrFunction_{}_proc{}_model{}_meas{}-{}'\
    .format(\
        IFO,\
        todayDate,\
        modelDate,\
        measTime[0].replace('-',''),
        measTime[-1].replace('-',''),
        anythingExtraInTheFigTag)
        
        
syserrorresults = namedtuple('syserrresults',['freq', 'tf', 'deltaloverpcal_tf', 'corr_tf', 'eta_R_tf', 'coh', 'relunc','time','exctype','calversion'])
results = []
        
# Build the model:
calcsmodel_obj = pydarm.calcs.CALCSModel(model_parameters_file)
pcalmodel_obj = pydarm.pcal.PcalModel(model_parameters_file)    

# WARNING :: NASTINESS INCOMING
# There's currently no way for pyDARM to compute the deltaL / PCAL correction to be computed
# if the parameter file's optical gain and actuation gains don't match what's in the 
# *filter bank* gains for CALCS (i.e. the computation assumes these are he same). 
# The lack of match may be in two ways,
#     (1) The filter bank gain doesn't match the parameter file, and/or
#     (2) There's a fudged EPICs gain applied to the path.
# In case of a fudged calibration, these filter bank gains are modified by the *EPICS* gains in their
# respective path, a la LHO:64096 or LHO:64316. 
# Both things were true to varying degrees at LHO from 2022-07-22 to 2022-11-18.
# So here, to compute the deltaL / PCAL correction, we fudge the gains of *the parameter set itself* 
# to match the doubly-bad sitation. This is so we don't have to create a whole new parameter set 
# whose reason for existence we need to remember. Pros and cons.
# We do so using the numbers from the reconciliation aLOG, LHO:65497, noting that 
# the 20221118 model parameter set we're *actually* using has the same MCMC gain values as the 
# 20220527 parameter set:
# (1) start with the gain to its expected 20220527 value (admittedly redundant, but good for clarity)
# (2) multiply by the ratio of the CALCS / 20220527 values, to go "back in time" to what filter bank gains were
#      (note, this is especially necessary for the actuator gains, since the only numbers we have 
#       are in the N/ct units rather than what the paramter file needs, the N/A or N/V^2 units)
# (3) multiply by the EPICS gain fudge to that CALCS filter gain.
#      (note the ever present confusing *division* of the (ct/m) number by the EPICs gain fudge
#       which modifies 1/C (m/ct) path)

fudge_sensing_coupled_cavity_optical_gain  = 3.206e+06 * (3.473e+06 / 3.206e+06) * (1/1.08)
fudge_actuation_xarm_uim_npa               = 1.6222    * (7.650e-08 / 7.615e-08) * 1.00
fudge_actuation_xarm_pum_npa               = 0.03003   * (6.054e-10 / 6.150e-10) * 1.05
fudge_actuation_xarm_tst_npv2              = 4.669e-11 * (4.751e-12 / 4.985e-12) * 1.07

pcal_channel_is_from_endstation = True
include_pcal_dewhitening = True
include_deltal_whitening = True

for iMeas, thisMeas in enumerate(measTime):
    if measType[iMeas] == 'SS':
        filename = '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCAL2DARMTF_LF_SS_5to1100Hz_10min.xml'\
               .format(cal_data_root,
                       run,
                       IFO,
                       thisMeas,
                       IFO)
    elif measType[iMeas] == 'BB':
       filename = '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCALY2DARMTF_BB_3min.xml'\
                   .format(cal_data_root,
                           run,
                           IFO,
                           thisMeas,
                           IFO)
    elif measType[iMeas] == 'MS':
        raise IOError('Processing ability for measType {}, or multi-sine does not exist yet. Use BB or SS.'.format(measType[iMeas]))
    else:
        raise IOError('Unrecognized measType {}. Must be either BB, SS, MS'.format(measType[iMeas]))
    
    pcalmeas_obj  = pydarm.measurement.Measurement(filename)                              
                                  
    syserr_freq, syserr_tf,  syserr_coh, syserr_relunc = pcalmeas_obj.get_raw_tf(
        pcal_chan_name,
        deltal_chan_name,
        COHTHRESH_PCALTF)
    
    # Correct the transfer functions for the artifacts each channel has

    # Re-instantiate the calcsmodel_obj just to make sure the overall path gains
    # get reset to the model_paramaters_file values before conditionally modifying 
    # below
    calcsmodel_obj = pydarm.calcs.CALCSModel(model_parameters_file)
    
    if measCALModelVersion[iMeas] == 'Fudge':
        calcsmodel_obj.sensing.coupled_cavity_optical_gain  = fudge_sensing_coupled_cavity_optical_gain
        calcsmodel_obj.actuation.xarm.uim_npa               = fudge_actuation_xarm_uim_npa
        calcsmodel_obj.actuation.xarm.pum_npa               = fudge_actuation_xarm_pum_npa
        calcsmodel_obj.actuation.xarm.tst_npv2              = fudge_actuation_xarm_tst_npv2
    
    deltaloverpcal_corr_tf = calcsmodel_obj.deltal_ext_pcal_correction(
        syserr_freq,
        endstation = pcal_channel_is_from_endstation,
        include_dewhitening=include_pcal_dewhitening,
        include_whitening=include_deltal_whitening)
        
    syserr_corr_tf = syserr_tf * deltaloverpcal_corr_tf  
    
    # \eta_R is defined as a corrective factor to Model [m] to get us closer
    # to "R_true" or "[m]_true". In this transfer function, PCAL [m] is our
    # proxy for "[m]_true," so we flip the corrected DELTAL / PCAL TF to be
    # PCAL / DELTAL
    eta_R_tf = 1.0 / syserr_corr_tf
    
    thissyserrorresult = syserrorresults(
        syserr_freq, 
        syserr_tf, 
        deltaloverpcal_corr_tf, 
        syserr_corr_tf, 
        eta_R_tf, 
        syserr_coh, 
        syserr_relunc, 
        measTime[iMeas], 
        measType[iMeas],
        measCALModelVersion[iMeas])
    results.append(thissyserrorresult)


#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

plotFreqRange = [4, 2e3]

fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)
for iMeas, thisMeas in enumerate(results): 
    s1.errorbar(thisMeas.freq,abs(thisMeas.eta_R_tf),yerr = abs(thisMeas.eta_R_tf) * thisMeas.relunc,color=measColor[iMeas],marker='o',markersize=measMarkerSize[iMeas], linestyle='',label='{} ({}, {} model)'.format(thisMeas.time,thisMeas.exctype,thisMeas.calversion),alpha=measPlotAlpha[iMeas])
s1.set_xscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.90,1.10])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_ylabel('Magnitude $\eta_{{{}}}$ ($\Delta L_{{{}}}$ [m] / $\Delta L_{{{}}}$ [m])'.format('R','Pcal','Model'),usetex=True)
s1.set_xlabel('Frequency [Hz]')
s1.legend()

for iMeas, thisMeas in enumerate(results): 
    s2.errorbar(thisMeas.freq,180.0/np.pi*np.angle(thisMeas.eta_R_tf),yerr = 180.0/np.pi * thisMeas.relunc,color=measColor[iMeas],marker='o',markersize=measMarkerSize[iMeas], linestyle='',alpha=measPlotAlpha[iMeas])
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-6,6)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.25))
s2.set_xlabel('Frequency [Hz]')
s2.set_ylabel('Phase [deg]')

plt.suptitle(IFO + ' Calibration Systematic Error Comparison',fontsize='x-large')
plt.tight_layout(rect=[0, 0.03, 1, 0.98])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_pcal2deltal_comparison.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
