from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/calibration/ifo/'

run = 'O3'
IFO = 'H1'
measDate = '2022-06-02'
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')

verbose = True
printFigs = True

COHTHRESH_DARMOLGTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

responsetf_SS_filename = \
    '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCAL2DARMTF_LF_SS_5to1100Hz_10min.xml'\
        .format(cal_data_root,\
            run,\
            IFO,\
            measDate,\
            IFO)

responsetf_BB_filename = \
'{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}_{}_PCALY2DARMTF_BB_3min.xml'\
    .format(cal_data_root,\
        run,\
        IFO,\
        measDate,\
        IFO)

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20220527.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

deltal_chan_name = '{}:CAL-DELTAL_EXTERNAL_DQ'\
    .format(\
        IFO)
pcal_chan_name = '{}:CAL-{}_PD_OUT_DQ'\
    .format(\
        IFO,\
        refPCAL)

resultsDir = \
cal_data_root+'/Runs/O3/'+IFO+'/Results/Uncertainty/'

anythingExtraInTheFigTag = ''

figTag ='syserrFunction_{}_proc{}_model{}_meas{}'\
    .format(\
        IFO,\
        todayDate,\
        modelDate,\
        measDate.replace('-',''),\
        anythingExtraInTheFigTag)
        
        
# Build the model:
calcsmodel_obj = pydarm.calcs.CALCSModel(model_parameters_file)
pcalmodel_obj = pydarm.pcal.PcalModel(model_parameters_file)

# Extract the measurement from the DTT Template
pcalmeas_SS_obj  = pydarm.measurement.Measurement(responsetf_SS_filename)
pcalmeas_BB_obj  = pydarm.measurement.Measurement(responsetf_BB_filename)

# syserrtf_tf is DELTAL / PCAL, which is Model [m] / PCAL [m].
syserr_SS_freq, syserr_SS_tf,  syserr_SS_coh, syserr_SS_relunc = pcalmeas_SS_obj.get_raw_tf(\
    pcal_chan_name,\
    deltal_chan_name,\
    COHTHRESH_PCALTF)
    
syserr_BB_freq, syserr_BB_tf,  syserr_BB_coh, syserr_BB_relunc = pcalmeas_BB_obj.get_raw_tf(\
    pcal_chan_name,\
    deltal_chan_name,\
    COHTHRESH_PCALTF)
    
# Correct the transfer functions for the artifacts each channel has
pcalmeas_SS_corr_tf = pcalmodel_obj.compute_pcal_correction(syserr_SS_freq)
deltalmeas_SS_corr_tf = calcsmodel_obj.calcs_dtt_calibration(syserr_SS_freq)
deltalmeas_SS_corr_nowhitening_tf = calcsmodel_obj.calcs_dtt_calibration(syserr_SS_freq,include_whitening = False)
deltalmeas_SS_whitening_tf = deltalmeas_SS_corr_tf / deltalmeas_SS_corr_nowhitening_tf

pcalmeas_BB_corr_tf = pcalmodel_obj.compute_pcal_correction(syserr_BB_freq)
deltalmeas_BB_corr_tf = calcsmodel_obj.calcs_dtt_calibration(syserr_BB_freq)

syserr_SS_deltalwhiteningonly_corr_tf = syserr_SS_tf * (deltalmeas_SS_whitening_tf / pcalmeas_SS_corr_tf)

syserrtf_SS_corr_tf = syserr_SS_deltalwhiteningonly_corr_tf * deltalmeas_SS_corr_nowhitening_tf
syserrtf_BB_corr_tf = syserr_BB_tf * (deltalmeas_BB_corr_tf / pcalmeas_BB_corr_tf)

# \eta_R is defined as a corrective factor to Model [m] to get us closer
# to "R_true" or "[m]_true". In this transfer function, PCAL [m] is our
# proxy for "[m]_true," so we flip the corrected DELTAL / PCAL TF to be
# PCAL / DELTAL

eta_R_SS_deltalwhiteningonly_tf = 1.0 /syserr_SS_deltalwhiteningonly_corr_tf
eta_R_SS_corr_nowhitening_tf = 1.0 / deltalmeas_SS_corr_nowhitening_tf

eta_R_SS_tf = 1.0 / syserrtf_SS_corr_tf
eta_R_BB_tf = 1.0 / syserrtf_BB_corr_tf

# The relative uncertainty of the inverse of a function, f, i.e. 1/f is
# equal to that of that original function f.
eta_R_SS_deltalwhiteningonly_relunc = syserr_SS_relunc

eta_R_SS_relunc = syserr_SS_relunc
eta_R_BB_relunc = syserr_BB_relunc

#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

plotFreqRange = [4, 2e3]

fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.errorbar(syserr_BB_freq,abs(eta_R_BB_tf),yerr = abs(eta_R_BB_tf) * eta_R_BB_relunc,color='b',marker='o',markersize=10, linestyle='',label='Broad Band EXC')
s1.errorbar(syserr_SS_freq,abs(eta_R_SS_tf),yerr = abs(eta_R_SS_tf) * eta_R_SS_relunc,color='r',marker='o',markersize=10, linestyle='',label='Swept Sine EXC')
s1.set_xscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.85,1.25])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.01))
s1.set_ylabel('Magnitude $\eta_{{{}}}$ ($\Delta L_{{{}}}$ [m] / $\Delta L_{{{}}}$ [m])'.format('R','Pcal','Model'),usetex=True)
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.errorbar(syserr_BB_freq,180.0/np.pi*np.angle(eta_R_BB_tf),yerr = eta_R_BB_relunc,color='b',marker='o',markersize=10, linestyle='')
s2.errorbar(syserr_SS_freq,180.0/np.pi*np.angle(eta_R_SS_tf),yerr = eta_R_SS_relunc,color='r',marker='o',markersize=10, linestyle='')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim([-15,15])
s2.yaxis.set_major_locator(tck.MultipleLocator(5))
s2.yaxis.set_minor_locator(tck.MultipleLocator(1))
s2.set_xlabel('Frequency [Hz]')
s2.set_ylabel('Phase [deg]')

plt.suptitle(IFO + ' Calibration Systematic Error: ' + measDate,fontsize='x-large')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_pcal2deltal_correctedforpcalandcalcsartifacts.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
    
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.errorbar(syserr_SS_freq,abs(eta_R_SS_deltalwhiteningonly_tf),yerr = abs(eta_R_SS_deltalwhiteningonly_tf) * eta_R_SS_deltalwhiteningonly_relunc,color='b',marker='o',markersize=10, linestyle='',label='Uncorrected')
s1.errorbar(syserr_SS_freq,abs(eta_R_SS_tf),yerr = abs(eta_R_SS_tf) * eta_R_SS_relunc,color='r',marker='o',markersize=10, linestyle='',label='Corrected')
s1.semilogx(syserr_SS_freq,abs(eta_R_SS_corr_nowhitening_tf),label='Eq. 10 from T1900169')
s1.set_xscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.85,1.25])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.01))
s1.set_ylabel('Magnitude $\eta_{{{}}}$ ($\Delta L_{{{}}}$ [m] / $\Delta L_{{{}}}$ [m])'.format('R','Pcal','Model'),usetex=True)
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.errorbar(syserr_SS_freq,180.0/np.pi*np.angle(eta_R_SS_deltalwhiteningonly_tf),yerr = eta_R_SS_deltalwhiteningonly_relunc,color='b',marker='o',markersize=10, linestyle='')
s2.errorbar(syserr_SS_freq,180.0/np.pi*np.angle(eta_R_SS_tf),yerr = eta_R_SS_relunc,color='r',marker='o',markersize=10, linestyle='')
s2.semilogx(syserr_SS_freq,180.0/np.pi*np.angle(eta_R_SS_corr_nowhitening_tf))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim([-45,45])
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_xlabel('Frequency [Hz]')
s2.set_ylabel('Phase [deg]')

plt.suptitle(IFO + ' Calibration $\Delta L_{{{}}}$ Corrections: '.format('Model') + measDate,fontsize='x-large',usetex=True)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_pcal2deltal_demonstratedeltalcorrections.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')
