# $ conda activate cds-pydarm-test
# $ python -m pydarm --version
# 0.1.2.dev74+g872e9a9
# $ export NDSSERVER="nds.ligo-wa.caltech.edu,$NDSSERVER"
# $ svn up /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/Results/
# $ git pull /ligo/gitcommon/Calibration/ifo/pydarmparams/

from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.time import * # tconvert, to_gps, from_gps
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
from copy import deepcopy
import numpy as np
import pydarm
import nds2

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })

resultsDir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/Results/SystematicErrorTFs/'

freq = np.logspace(np.log10(5),np.log10(5000),1000)

chanList = ['H1:GRD-ISC_LOCK_STATE_N',
            'H1:CAL-CS_TDEP_KAPPA_C_OUTPUT',
            'H1:CAL-CS_TDEP_F_C_OUTPUT',
            'H1:CAL-CS_TDEP_KAPPA_UIM_REAL_OUTPUT',
            'H1:CAL-CS_TDEP_KAPPA_PUM_REAL_OUTPUT',
            'H1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT',
            'H1:GDS-CALIB_KAPPA_C',
            'H1:GDS-CALIB_F_CC',
            'H1:GDS-CALIB_KAPPA_UIM_REAL',
            'H1:GDS-CALIB_KAPPA_PUM_REAL',
            'H1:GDS-CALIB_KAPPA_TST_REAL']

#create two darm model objects, using "case study" parameter files from 
# https://git.ligo.org/Calibration/ifo/common/-/blob/main/pydarmparams/
# :: one with 3.2 kHz pole in place (i.e. as is)
darmModel_obj = pydarm.darm.DARMModel('/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_H1_20230621T211522Z.ini')
        
# :: one with the 3.2 kHz poles removed
darmModel_no3p2k_obj = pydarm.darm.DARMModel('/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_H1_20230621T211522Z_no3p2k.ini')
        
# extract compute_response_function from both
R_nom = darmModel_obj.compute_response_function(freq)
R_no3p2k = darmModel_no3p2k_obj.compute_response_function(freq)

#take the ratio.
eta_R_nom_over_no3p2k = R_nom / R_no3p2k

#compare against Vlad's fit
vladfit = np.loadtxt('{}71787_20230728082618_H1_uncertainty_systematic_correction.txt'.format(resultsDir))
vladtf = vladfit[:,1]*np.exp(1j*vladfit[:,2])

#Vlad's GPS time for R/(GDS/DARM_ERR):
utctime_LHO71787 = '2023-07-27 05:03:20 UTC'
gpstime_LHO71787 = 1374469418
start_LHO71787 = gpstime_LHO71787 - 300
end_LHO71787 = gpstime_LHO71787 + 1200

#Joe's GPS Time for bad kappas:
utctime_LHO72622 = '2023-07-26 01:10 UTC'
gpstime_LHO72622 = 1374369018
start_LHO72622 = gpstime_LHO72622 - 300
end_LHO72622 = gpstime_LHO72622 + 1200

NDSSRVR = 'nds.ligo-wa.caltech.edu'
NDSPORT = 31200
nds2cxn = nds2.connection(NDSSRVR,NDSPORT)

print('Getting data ...')
tdcfs_LHO71787 = tsd.get(chanList, start_LHO71787, end_LHO71787, frametype='R', verbose=True, connection=nds2cxn) 
tdcfs_LHO72622 = tsd.get(chanList, start_LHO72622, end_LHO72622, frametype='R', verbose=True, connection=nds2cxn) 
print('Got the data ...')     

time_min_LHO71787 = (tdcfs_LHO71787[chanList[0]].times.value - gpstime_LHO71787)/60

isclock_state_LHO71787 = tdcfs_LHO71787[chanList[0]].value

#I abbreviate calcs as ccs here
ccs_kappa_C_LHO71787 = tdcfs_LHO71787[chanList[1]].value
ccs_freq_CC_LHO71787 = tdcfs_LHO71787[chanList[2]].value
ccs_kappa_U_LHO71787 = tdcfs_LHO71787[chanList[3]].value
ccs_kappa_P_LHO71787 = tdcfs_LHO71787[chanList[4]].value
ccs_kappa_T_LHO71787 = tdcfs_LHO71787[chanList[5]].value

gds_kappa_C_LHO71787 = tdcfs_LHO71787[chanList[6]].value
gds_freq_CC_LHO71787 = tdcfs_LHO71787[chanList[7]].value
gds_kappa_U_LHO71787 = tdcfs_LHO71787[chanList[8]].value
gds_kappa_P_LHO71787 = tdcfs_LHO71787[chanList[9]].value
gds_kappa_T_LHO71787 = tdcfs_LHO71787[chanList[10]].value

tdcfvalues_at_gpstime_LHO71787 = [tdcfs_LHO71787[chanList[iChan]].value[tdcfs_LHO71787[chanList[0]].times.value == gpstime_LHO71787][0] for iChan in range(len(chanList))]

time_min_LHO72622 = (tdcfs_LHO72622[chanList[0]].times.value - gpstime_LHO72622)/60

isclock_state_LHO72622 = tdcfs_LHO72622[chanList[0]].value

ccs_kappa_C_LHO72622 = tdcfs_LHO72622[chanList[1]].value
ccs_freq_CC_LHO72622 = tdcfs_LHO72622[chanList[2]].value
ccs_kappa_U_LHO72622 = tdcfs_LHO72622[chanList[3]].value
ccs_kappa_P_LHO72622 = tdcfs_LHO72622[chanList[4]].value
ccs_kappa_T_LHO72622 = tdcfs_LHO72622[chanList[5]].value

gds_kappa_C_LHO72622 = tdcfs_LHO72622[chanList[6]].value
gds_freq_CC_LHO72622 = tdcfs_LHO72622[chanList[7]].value
gds_kappa_U_LHO72622 = tdcfs_LHO72622[chanList[8]].value
gds_kappa_P_LHO72622 = tdcfs_LHO72622[chanList[9]].value
gds_kappa_T_LHO72622 = tdcfs_LHO72622[chanList[10]].value

tdcfvalues_at_gpstime_LHO72622 = [tdcfs_LHO72622[chanList[iChan]].value[tdcfs_LHO72622[chanList[0]].times.value == gpstime_LHO72622][0] for iChan in range(len(chanList))]

#create a third and fourth instantiation of the DARM model, so we can apply TDCFs
darmModel_wTDCFs_CCS_LHO71787_obj = deepcopy(darmModel_obj)
darmModel_wTDCFs_CCS_LHO72622_obj = deepcopy(darmModel_obj)
darmModel_wTDCFs_GDS_LHO71787_obj = deepcopy(darmModel_obj)
darmModel_wTDCFs_GDS_LHO72622_obj = deepcopy(darmModel_obj)

# Multiple in kappas, replace cavity pole
darmModel_wTDCFs_CCS_LHO71787_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO71787[1]
darmModel_wTDCFs_CCS_LHO71787_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO71787[2]
darmModel_wTDCFs_CCS_LHO71787_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO71787[3]
darmModel_wTDCFs_CCS_LHO71787_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO71787[4]
darmModel_wTDCFs_CCS_LHO71787_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO71787[5]

darmModel_wTDCFs_CCS_LHO72622_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO72622[1]
darmModel_wTDCFs_CCS_LHO72622_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO72622[2]
darmModel_wTDCFs_CCS_LHO72622_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO72622[3]
darmModel_wTDCFs_CCS_LHO72622_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO72622[4]
darmModel_wTDCFs_CCS_LHO72622_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO72622[5]

darmModel_wTDCFs_GDS_LHO71787_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO71787[6]
darmModel_wTDCFs_GDS_LHO71787_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO71787[7]
darmModel_wTDCFs_GDS_LHO71787_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO71787[8]
darmModel_wTDCFs_GDS_LHO71787_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO71787[9]
darmModel_wTDCFs_GDS_LHO71787_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO71787[10]

darmModel_wTDCFs_GDS_LHO72622_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO72622[6]
darmModel_wTDCFs_GDS_LHO72622_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO72622[7]
darmModel_wTDCFs_GDS_LHO72622_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO72622[8]
darmModel_wTDCFs_GDS_LHO72622_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO72622[9]
darmModel_wTDCFs_GDS_LHO72622_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO72622[10]

# Compute updated response
R_wTDCFs_CCS_LHO71787 = darmModel_wTDCFs_CCS_LHO71787_obj.compute_response_function(freq)
R_wTDCFs_CCS_LHO72622 = darmModel_wTDCFs_CCS_LHO72622_obj.compute_response_function(freq)
R_wTDCFs_GDS_LHO71787 = darmModel_wTDCFs_GDS_LHO71787_obj.compute_response_function(freq)
R_wTDCFs_GDS_LHO72622 = darmModel_wTDCFs_GDS_LHO72622_obj.compute_response_function(freq)

eta_R_wTDCFs_CCS_over_GDS_LHO71787 = R_wTDCFs_CCS_LHO71787 / R_wTDCFs_GDS_LHO71787
eta_R_wTDCFs_CCS_over_GDS_LHO72622 = R_wTDCFs_CCS_LHO72622 / R_wTDCFs_GDS_LHO72622

eta_R_wTDCFs_CCS_LHO71787 = R_wTDCFs_CCS_LHO71787 / R_nom 
eta_R_wTDCFs_CCS_LHO72622 = R_wTDCFs_CCS_LHO72622 / R_nom 
eta_R_wTDCFs_GDS_LHO71787 = R_wTDCFs_GDS_LHO71787 / R_nom 
eta_R_wTDCFs_GDS_LHO72622 = R_wTDCFs_GDS_LHO72622 / R_nom 

# OK, now get crazy. Take the ratio of the nominal R and "missing 3.2 kHz pole, updated with CCS TDCFs" vs. "missing 3.2 kHz pole, updated with GDS TDCFs"
darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj = deepcopy(darmModel_no3p2k_obj)
darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj = deepcopy(darmModel_no3p2k_obj)
darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj = deepcopy(darmModel_no3p2k_obj)
darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj = deepcopy(darmModel_no3p2k_obj)

darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO71787[1]
darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO71787[2]
darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO71787[3]
darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO71787[4]
darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO71787[5]

darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO71787[6]
darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO71787[7]
darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO71787[8]
darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO71787[9]
darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO71787[10]

R_no3p2k_wTDCFs_CCS_LHO71787 = darmModel_no3p2k_wTDCFs_CCS_LHO71787_obj.compute_response_function(freq)
R_no3p2k_wTDCFs_GDS_LHO71787 = darmModel_no3p2k_wTDCFs_GDS_LHO71787_obj.compute_response_function(freq)

eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787 = R_wTDCFs_CCS_LHO71787 / R_no3p2k_wTDCFs_CCS_LHO71787
eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO71787 = R_wTDCFs_GDS_LHO71787 / R_no3p2k_wTDCFs_GDS_LHO71787

eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO71787 = R_wTDCFs_CCS_LHO71787 / R_no3p2k_wTDCFs_GDS_LHO71787

darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO72622[1]
darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO72622[2]
darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO72622[3]
darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO72622[4]
darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO72622[5]

darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.sensing.coupled_cavity_optical_gain *= tdcfvalues_at_gpstime_LHO72622[6]
darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.sensing.coupled_cavity_pole_frequency = tdcfvalues_at_gpstime_LHO72622[7]
darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.actuation.xarm.uim_npa *= tdcfvalues_at_gpstime_LHO72622[8]
darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.actuation.xarm.pum_npa *= tdcfvalues_at_gpstime_LHO72622[9]
darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.actuation.xarm.tst_npv2 *= tdcfvalues_at_gpstime_LHO72622[10]

R_no3p2k_wTDCFs_CCS_LHO72622 = darmModel_no3p2k_wTDCFs_CCS_LHO72622_obj.compute_response_function(freq)
R_no3p2k_wTDCFs_GDS_LHO72622 = darmModel_no3p2k_wTDCFs_GDS_LHO72622_obj.compute_response_function(freq)

eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622 = R_wTDCFs_CCS_LHO72622 / R_no3p2k_wTDCFs_CCS_LHO72622
eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO72622 = R_wTDCFs_GDS_LHO72622 / R_no3p2k_wTDCFs_GDS_LHO72622

eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO72622 = R_wTDCFs_CCS_LHO72622 / R_no3p2k_wTDCFs_GDS_LHO72622

#plot the TDCFs.
fig, ((s1,s4),(s2,s5),(s3,s6)) = plt.subplots(nrows=3,ncols=2)
fig.subplots_adjust(top=0.8)

s1.plot(time_min_LHO71787, isclock_state_LHO71787)
s1.set_ylabel('ISC_LOCK_STATE_N')
s1.set_ylim([590,610])
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s3.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s3.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s3.yaxis.set_major_locator(tck.MultipleLocator(2))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

s2.plot(time_min_LHO71787, ccs_kappa_C_LHO71787,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[1]))
s2.plot(time_min_LHO71787, gds_kappa_C_LHO71787,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[6]))
s2.plot(0, tdcfvalues_at_gpstime_LHO71787[1],color='C00',marker='o',markerfacecolor='none')
s2.plot(0, tdcfvalues_at_gpstime_LHO71787[6],color='C01',marker='o',markerfacecolor='none')
s2.set_ylabel('\kappa_C [ ]')
s2.set_ylim([0.95,1.00])
s2.legend()
s2.grid(which='major',color='black') 
s2.grid(which='minor', ls='--')
s2.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s2.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s2.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.002))


s3.plot(time_min_LHO71787, ccs_freq_CC_LHO71787,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[2]))
s3.plot(time_min_LHO71787, gds_freq_CC_LHO71787,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[7]))
s3.plot(0, tdcfvalues_at_gpstime_LHO71787[2],color='C00',marker='o',markerfacecolor='none')
s3.plot(0, tdcfvalues_at_gpstime_LHO71787[7],color='C01',marker='o',markerfacecolor='none')
s3.set_ylabel('f_CC [Hz]')
s3.set_xlabel('Time [min]')
s3.set_ylim([440,452])
s3.legend()
s3.grid(which='major',color='black') 
s3.grid(which='minor', ls='--')
s3.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s3.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s3.yaxis.set_major_locator(tck.MultipleLocator(2))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.5))


s4.plot(time_min_LHO71787, ccs_kappa_U_LHO71787,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[3]))
s4.plot(time_min_LHO71787, gds_kappa_U_LHO71787,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[8]))
s4.plot(0, tdcfvalues_at_gpstime_LHO71787[3],color='C00',marker='o',markerfacecolor='none')
s4.plot(0, tdcfvalues_at_gpstime_LHO71787[8],color='C01',marker='o',markerfacecolor='none')
s4.set_ylabel('\kappa_U [ ]')
s4.set_ylim([0.9,1.1])
s4.legend()
s4.grid(which='major',color='black') 
s4.grid(which='minor', ls='--')
s4.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s4.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s4.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.01))

s5.plot(time_min_LHO71787, ccs_kappa_P_LHO71787,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[4]))
s5.plot(time_min_LHO71787, gds_kappa_P_LHO71787,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[9]))
s5.plot(0, tdcfvalues_at_gpstime_LHO71787[4],color='C00',marker='o',markerfacecolor='none')
s5.plot(0, tdcfvalues_at_gpstime_LHO71787[9],color='C01',marker='o',markerfacecolor='none')
s5.set_ylabel('\kappa_P [ ]')
s5.set_ylim([0.99,1.01])
s5.legend()
s5.grid(which='major',color='black') 
s5.grid(which='minor', ls='--')
s5.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s5.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s5.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s5.yaxis.set_minor_locator(tck.MultipleLocator(0.001))

s6.plot(time_min_LHO71787, ccs_kappa_T_LHO71787,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[5]))
s6.plot(time_min_LHO71787, gds_kappa_T_LHO71787,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO71787[10]))
s6.plot(0, tdcfvalues_at_gpstime_LHO71787[5],color='C00',marker='o',markerfacecolor='none')
s6.plot(0, tdcfvalues_at_gpstime_LHO71787[10],color='C01',marker='o',markerfacecolor='none')
s6.set_ylabel('\kappa_T [ ]')
s6.set_xlabel('Time [min]')
s6.set_ylim([1.02,1.05])
s6.legend()
s6.grid(which='major',color='black') 
s6.grid(which='minor', ls='--')
s6.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s6.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s6.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s6.yaxis.set_minor_locator(tck.MultipleLocator(0.001))

fig.suptitle('H1 TDCFs around {}\nLHO:71787; THERMALIZED'.format(utctime_LHO71787),fontsize='x-large',y=0.98)
fig.tight_layout()

fig.savefig('{}2023-07-27_0503UTC_H1_TDCFs.pdf'.format(resultsDir), format='pdf')

fig, ((s1,s4),(s2,s5),(s3,s6)) = plt.subplots(nrows=3,ncols=2)
fig.subplots_adjust(top=0.8)

s1.plot(time_min_LHO72622, isclock_state_LHO72622)
s1.set_ylabel('ISC_LOCK_STATE_N')
s1.set_ylim([590,610])
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s3.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s3.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s3.yaxis.set_major_locator(tck.MultipleLocator(2))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

s2.plot(time_min_LHO72622, ccs_kappa_C_LHO72622,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[1]))
s2.plot(time_min_LHO72622, gds_kappa_C_LHO72622,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[6]))
s2.plot(0, tdcfvalues_at_gpstime_LHO72622[1],color='C00',marker='o',markerfacecolor='none')
s2.plot(0, tdcfvalues_at_gpstime_LHO72622[6],color='C01',marker='o',markerfacecolor='none')
s2.set_ylabel('\kappa_C [ ]')
s2.set_ylim([0.95,1.00])
s2.legend()
s2.grid(which='major',color='black') 
s2.grid(which='minor', ls='--')
s2.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s2.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s2.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.002))


s3.plot(time_min_LHO72622, ccs_freq_CC_LHO72622,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[2]))
s3.plot(time_min_LHO72622, gds_freq_CC_LHO72622,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[7]))
s3.plot(0, tdcfvalues_at_gpstime_LHO72622[2],color='C00',marker='o',markerfacecolor='none')
s3.plot(0, tdcfvalues_at_gpstime_LHO72622[7],color='C01',marker='o',markerfacecolor='none')
s3.set_ylabel('f_CC [Hz]')
s3.set_xlabel('Time [min]')
s3.set_ylim([440,452])
s3.legend()
s3.grid(which='major',color='black') 
s3.grid(which='minor', ls='--')
s3.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s3.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s3.yaxis.set_major_locator(tck.MultipleLocator(2))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.5))


s4.plot(time_min_LHO72622, ccs_kappa_U_LHO72622,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[3]))
s4.plot(time_min_LHO72622, gds_kappa_U_LHO72622,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[8]))
s4.plot(0, tdcfvalues_at_gpstime_LHO72622[3],color='C00',marker='o',markerfacecolor='none')
s4.plot(0, tdcfvalues_at_gpstime_LHO72622[8],color='C01',marker='o',markerfacecolor='none')
s4.set_ylabel('\kappa_U [ ]')
s4.set_ylim([0.9,1.1])
s4.legend()
s4.grid(which='major',color='black') 
s4.grid(which='minor', ls='--')
s4.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s4.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s4.yaxis.set_major_locator(tck.MultipleLocator(0.05))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.01))

s5.plot(time_min_LHO72622, ccs_kappa_P_LHO72622,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[4]))
s5.plot(time_min_LHO72622, gds_kappa_P_LHO72622,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[9]))
s5.plot(0, tdcfvalues_at_gpstime_LHO72622[4],color='C00',marker='o',markerfacecolor='none')
s5.plot(0, tdcfvalues_at_gpstime_LHO72622[9],color='C01',marker='o',markerfacecolor='none')
s5.set_ylabel('\kappa_P [ ]')
s5.set_ylim([0.99,1.01])
s5.legend()
s5.grid(which='major',color='black') 
s5.grid(which='minor', ls='--')
s5.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s5.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s5.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s5.yaxis.set_minor_locator(tck.MultipleLocator(0.001))

s6.plot(time_min_LHO72622, ccs_kappa_T_LHO72622,label='CAL-CS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[5]))
s6.plot(time_min_LHO72622, gds_kappa_T_LHO72622,label='GDS = {:.5f}'.format(tdcfvalues_at_gpstime_LHO72622[10]))
s6.plot(0, tdcfvalues_at_gpstime_LHO72622[5],color='C00',marker='o',markerfacecolor='none')
s6.plot(0, tdcfvalues_at_gpstime_LHO72622[10],color='C01',marker='o',markerfacecolor='none')
s6.set_ylabel('\kappa_T [ ]')
s6.set_xlabel('Time [min]')
s6.set_ylim([1.02,1.05])
s6.legend()
s6.grid(which='major',color='black') 
s6.grid(which='minor', ls='--')
s6.xaxis.set_major_locator(tck.MultipleLocator(5.0))
s6.xaxis.set_minor_locator(tck.MultipleLocator(1.0))
s6.yaxis.set_major_locator(tck.MultipleLocator(0.005))
s6.yaxis.set_minor_locator(tck.MultipleLocator(0.001))

fig.suptitle('H1 TDCFs around {}\nLHO:72622;  NOT THERMALIZED'.format(utctime_LHO72622),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_TDCFs.pdf'.format(resultsDir), format='pdf')
                       
# plot systematic error from excluding 3.2 kHz ESD pole.
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_nom_over_no3p2k),label='Direct from Model')
s1.semilogx(vladfit[:,0],abs(vladtf),label='LHO:71787 Fit')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.04])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_ylabel('Magnitude (R_nom / R_no3p2k) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(vladfit[:,0],np.angle(vladtf,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-3,3)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.25))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error if 3.2 kHz ESD is Not Included in A_T',fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_syserr_no3p2kHz.pdf'.format(resultsDir), format='pdf')

# plot systematic error incurred after applying static bad GDS TDCFs vs. the real CAL-CS TDCFs
# LHO:72622
# 2023-07-26_0110UTC
# NOT THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_LHO72622),label='R_(nominal updated w/ Live CAL-CS TDCFs) / R_nominal')
s1.semilogx(freq,abs(eta_R_wTDCFs_GDS_LHO72622),label='R_(nominal updated w/ Frozen GDS TDCFs) / R_nominal')
s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO72622),label='Ratio = R_(w/ Live CAL-CS TDCFs) / R_(w/ Frozen GDS TDCFs)')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.10])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_LHO72622,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_GDS_LHO72622,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO72622,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-3,5)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.25))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error from using Frozen GDS TDCF vs. Live CAL-CS Values at {}\nLHO:72622;  NOT THERMALIZED'.format(utctime_LHO72622),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_syserr_CALCS_vs_GDS_TDCFs.pdf'.format(resultsDir), format='pdf')

# plot systematic error incurred after applying static bad GDS TDCFs vs. the real CAL-CS TDCFs
# LHO:71787
# 2023-07-27_0503UTC
# THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_LHO71787),label='R_(nominal updated w/ Live CAL-CS TDCFs) / R_nominal')
s1.semilogx(freq,abs(eta_R_wTDCFs_GDS_LHO71787),label='R_(nominal updated w/ Frozen GDS TDCFs) / R_nominal')
s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO71787),label='Ratio = R_(w/ Live CAL-CS TDCFs) / R_(w/ Frozen GDS TDCFs)')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.10])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_LHO71787,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_GDS_LHO71787,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO71787,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-3,5)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.25))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error from using Frozen GDS TDCF vs. Live CAL-CS Values at {}\nLHO:71787; THERMALIZED'.format(utctime_LHO71787),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-27_0503UTC_H1_syserr_CALCS_vs_GDS_TDCFs.pdf'.format(resultsDir), format='pdf')

# Compare 
# - missing 3.2 kHz, 
# - missing 3.2 kHz updated with CALCS TDCFs, 
# - missing 3.2 kHz updated with GDS TDCFs
# LHO:71787
# 2023-07-27_0503UTC
# THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_nom_over_no3p2k),label=' R_nominal / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787),label=' R_(nominal updated w/ Live CAL-CS TDCFs) / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO71787),label=' R_(nominal updated w/ Frozen GDS TDCFs) / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(vladfit[:,0],abs(vladtf),label=' LHO:71787 Fit to R_(nominal updated w/ some Vlad TDCFs) / R_(installed, GDS-CALIB_STRAIN / DARM_ERR)')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.04])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend(fontsize='medium')

s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO71787,deg=True))
s2.semilogx(vladfit[:,0], np.angle(vladtf,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-3,3)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error in R updated with Frozen GDS TDCF vs. Live CAL-CS Values at {}, if missing 3.2 kHz ESD pole\nLHO:71787; THERMALIZED'.format(utctime_LHO71787),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-27_0503UTC_H1_syserr_no3p2kHz_and_CALCS_vs_GDS_TDCFs.pdf'.format(resultsDir), format='pdf')

# Compare 
# - missing 3.2 kHz, 
# - missing 3.2 kHz updated with CALCS TDCFs, 
# - missing 3.2 kHz updated with GDS TDCFs
# LHO:72622
# 2023-07-26_0110UTC
# NOT THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_nom_over_no3p2k),label=' R_nominal / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622),label=' R_(nominal updated w/ Live CAL-CS TDCFs) / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO72622),label=' R_(nominal updated w/ Frozen GDS TDCFs) / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(vladfit[:,0],abs(vladtf),label=' LHO:71787 Fit to R_(nominal updated w/ some Vlad TDCFs) / R_(installed, GDS-CALIB_STRAIN / DARM_ERR)')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.04])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend(fontsize='medium')

s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO72622,deg=True))
s2.semilogx(vladfit[:,0], np.angle(vladtf,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-3,3)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error in R updated with Frozen GDS TDCF vs. Live CAL-CS Values at {}, if missing 3.2 kHz ESD pole\nLHO:72622;  NOT THERMALIZED'.format(utctime_LHO72622),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_syserr_no3p2kHz_and_CALCS_vs_GDS_TDCFs.pdf'.format(resultsDir), format='pdf')


# Compare 
# - missing 3.2 kHz, 
# - missing 3.2 kHz updated with CALCS TDCFs, 
# - missing 3.2 kHz updated with GDS TDCFs
# LHO:71787
# 2023-07-27_0503UTC
# THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO71787),color='C02',label=' [ R_(w/ Live CAL-CS TDCFs) ] / [ R_(w/ Frozen GDS TDCFs) ] ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787 / eta_R_nom_over_no3p2k), label=' [ R_(w/ Live CAL-CS TDCFs)/R_(Missing 3.2 kHz) ] / [ R_nominal / R_(Missing 3.2 kHz ESD Pole) ]')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787 / eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO71787),label=' [ R_(w/ Live CAL-CS TDCFs)/R_(Missing 3.2 kHz) ] / [ R_(w/ Frozen GDS TDCFs)/R_(Missing 3.2 kHz) ] ')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.98,1.042])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO71787,deg=True),color='C02')
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787 / eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO71787 / eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO71787,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-1,2.2)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error Comparison: Ratio of R_(w/ CALCS TDCFs) / R_(w/ GDS TDCFs) with and without Missing 3.2 kHz ESD Pole\nLHO:71787; THERMALIZED'.format(utctime_LHO71787),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-27_0503UTC_H1_syserr_CALCS_vs_GDS_TDCFs_w_vs_wo_3p2kHzpole.pdf'.format(resultsDir), format='pdf')

# Compare 
# - missing 3.2 kHz, 
# - missing 3.2 kHz updated with CALCS TDCFs, 
# - missing 3.2 kHz updated with GDS TDCFs
# LHO:72622
# 2023-07-26_0110UTC
# NOT THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO72622),color='C02',label=' [ R_(w/ Live CAL-CS TDCFs) ] / [ R_(w/ Frozen GDS TDCFs) ] ')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622 / eta_R_nom_over_no3p2k), label=' [ R_(w/ Live CAL-CS TDCFs)/R_(Missing 3.2 kHz) ] / [ R_nominal / R_(Missing 3.2 kHz ESD Pole) ]')
s1.semilogx(freq,abs(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622 / eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO72622),label=' [ R_(w/ Live CAL-CS TDCFs)/R_(Missing 3.2 kHz) ] / [ R_(w/ Frozen GDS TDCFs)/R_(Missing 3.2 kHz) ] ')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.98,1.042])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO72622,deg=True),color='C02')
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622 / eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFS_over_R_wTDCFs_no3p2k_CCS_LHO72622 / eta_R_wTDCFS_over_R_wTDCFs_no3p2k_GDS_LHO72622,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-1,2.2)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error Comparison: Ratio of R_(w/ CALCS TDCFs) / R_(w/ GDS TDCFs) with and without Missing 3.2 kHz ESD Pole\nLHO:72622; NOT THERMALIZED'.format(utctime_LHO72622),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_syserr_CALCS_vs_GDS_TDCFs_w_vs_wo_3p2kHzpole.pdf'.format(resultsDir), format='pdf')





# Compare 
# - missing 3.2 kHz alone
# - replacing GDS TDCFs with CALCS TDCFs alone
# - product of the two
# - (R nominal updated with CALCS TDCFS) over (R missing 3.2 kHz updated with GDS TDCFs)
# LHO:71787
# 2023-07-27_0503UTC
# THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_nom_over_no3p2k),label='etaR_no3p2k = R_nominal / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO71787),label='etaR_tdcfs = [ R_(w/ Live CAL-CS TDCFs) ] / [ R_(w/ Frozen GDS TDCFs) ] ')
s1.semilogx(freq,abs(eta_R_nom_over_no3p2k * eta_R_wTDCFs_CCS_over_GDS_LHO71787),label='etaR_no3p2k * etaR_tdcfs')
s1.semilogx(freq,abs(eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO71787), label=' [ R_(w/ Live CAL-CS TDCFs) / R_(Missing 3.2 kHz, w/ Frozen GDS TDCFs) ]')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.06])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO71787,deg=True))
s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k * eta_R_wTDCFs_CCS_over_GDS_LHO71787,deg=True))
s2.semilogx(freq,np.angle(eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO71787,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-2,3)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error Comparison: Treating TDCF Update and No 3.2 kHz Pole as Separable Independent vs "The right thing" \nLHO:71787; THERMALIZED'.format(utctime_LHO71787),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-27_0503UTC_H1_syserr_TDCFsUpdate_and_No3p2kHzpole_separable_vs_indivisable.pdf'.format(resultsDir), format='pdf')

# Compare 
# - missing 3.2 kHz alone
# - replacing GDS TDCFs with CALCS TDCFs alone
# - product of the two
# - (R nominal updated with CALCS TDCFS) over (R missing 3.2 kHz updated with GDS TDCFs)
# LHO:72622
# 2023-07-26_0110UTC
# NOT THERMALIZED
fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq,abs(eta_R_nom_over_no3p2k),label='etaR_no3p2k = R_nominal / R_(Missing 3.2 kHz ESD Pole) ')
s1.semilogx(freq,abs(eta_R_wTDCFs_CCS_over_GDS_LHO72622),label='etaR_tdcfs = [ R_(w/ Live CAL-CS TDCFs) ] / [ R_(w/ Frozen GDS TDCFs) ] ')
s1.semilogx(freq,abs(eta_R_nom_over_no3p2k * eta_R_wTDCFs_CCS_over_GDS_LHO72622),label='etaR_no3p2k * etaR_tdcfs')
s1.semilogx(freq,abs(eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO72622), label=' [ R_(w/ Live CAL-CS TDCFs) / R_(Missing 3.2 kHz, w/ Frozen GDS TDCFs) ]')
s1.set_xscale('log')
s1.grid(which='major',color='black') 
s1.grid(which='minor', ls='--')
s1.set_xlim([5,5000])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.94,1.06])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.01))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.002))
s1.set_ylabel('Magnitude (see legend) \n [(m/m)/(m/m)]')
s1.set_xlabel('Frequency [Hz]')
s1.legend()

s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k,deg=True))
s2.semilogx(freq,np.angle(eta_R_wTDCFs_CCS_over_GDS_LHO72622,deg=True))
s2.semilogx(freq,np.angle(eta_R_nom_over_no3p2k * eta_R_wTDCFs_CCS_over_GDS_LHO72622,deg=True))
s2.semilogx(freq,np.angle(eta_R_wCCSTDCFs_over_etaR_wGDSTDCFs_no3p2k_LHO72622,deg=True))
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([5,5000])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-2,3)
s2.yaxis.set_major_locator(tck.MultipleLocator(1))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.2))
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

fig.suptitle('Systematic Error Comparison: Treating TDCF Update and No 3.2 kHz Pole as Separable Independent vs "The right thing" \nLHO:72622; NOT THERMALIZED'.format(utctime_LHO72622),fontsize='x-large',y=0.98)
fig.tight_layout()
fig.savefig('{}2023-07-26_0110UTC_H1_syserr_TDCFsUpdate_and_No3p2kHzpole_separable_vs_indivisable.pdf'.format(resultsDir), format='pdf')
