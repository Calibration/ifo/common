from pydarm.plot import BodePlot

import pydarm
import numpy as np
import matplotlib.pyplot as plt

config = '../../pydarmparams/pydarm_modelparams_PostO3_L1_20220513_superactuator_comparison.ini'
config2 = '../../pydarmparams/pydarm_modelparams_PostO3_L1_20220513_superactuator_comparison_pum_0p9.ini'

D1 = pydarm.darm.DARMModel(config)
D2 = pydarm.darm.DARMModel(config2)

#pydarm.plot.critique(D1,D2, label=['Original','10% gain changes'],filename='superactuator_differences.pdf',ifo='L1')

####Custom plotting
freq_min = 0.1
freq_max = 5000
freq_points = 10001

freq = np.logspace(np.log10(float(freq_min)),np.log10(float(freq_max)),int(freq_points))

R1 = D1.compute_response_function(freq)
R2 = D2.compute_response_function(freq)

fig = plt.figure(1)
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)


s1.plot(freq,abs(R1),label='Original')
s1.plot(freq,abs(R2),label='Changed')
s1.set_ylabel('Magnitude Response')
s1.set_xlabel('Freq [Hz]')
s1.set_xscale('log')
s1.set_yscale('log')
s1.legend()

s2.plot(freq,np.angle(R1,deg=True),label='Original')
s2.plot(freq,np.angle(R2,deg=True),label='Changed')
s2.set_ylabel('Phase Response [Deg]')
s2.set_xlabel('Freq [Hz]')
s2.set_xscale('log')
s2.set_yscale('linear')
s2.legend()

s3.plot(freq,abs(R2/R1),label='Changed/Original')
s3.set_ylabel('Magnitude Response Ratio')
s3.set_xlabel('Freq [Hz]')
s3.set_xscale('log')
s3.set_yscale('linear')
s3.legend()

s4.plot(freq,np.angle(R2/R1,deg=True),label='Changed/Original')
s4.set_ylabel('Phase Response Difference [Deg]')
s4.set_xlabel('Freq [Hz]')
s4.set_xscale('log')
s4.set_yscale('linear')
s4.legend()

fig.suptitle('Comparing R assuming a +/-10% shift in ETMX and ETMY PUM actuator DC gain')


uimx1 = D1.actuation.xarm.compute_actuation_single_stage(freq,stage='UIM')
uimx2 = D2.actuation.xarm.compute_actuation_single_stage(freq,stage='UIM')

pumx1 = D1.actuation.xarm.compute_actuation_single_stage(freq,stage='PUM')
pumx2 = D2.actuation.xarm.compute_actuation_single_stage(freq,stage='PUM')

tstx1 = D1.actuation.xarm.compute_actuation_single_stage(freq,stage='TST')
tstx2 = D2.actuation.xarm.compute_actuation_single_stage(freq,stage='TST')

uimy1 = D1.actuation.yarm.compute_actuation_single_stage(freq,stage='UIM')
uimy2 = D2.actuation.yarm.compute_actuation_single_stage(freq,stage='UIM')

pumy1 = D1.actuation.yarm.compute_actuation_single_stage(freq,stage='PUM')
pumy2 = D2.actuation.yarm.compute_actuation_single_stage(freq,stage='PUM')

tsty1 = D1.actuation.yarm.compute_actuation_single_stage(freq,stage='TST')
tsty2 = D2.actuation.yarm.compute_actuation_single_stage(freq,stage='TST')


fig2 = plt.figure(2)
s21 = fig2.add_subplot(221)
s22 = fig2.add_subplot(223)
s23 = fig2.add_subplot(222)
s24 = fig2.add_subplot(224)


s21.plot(freq,abs(pumx1),label='PUM X')
s21.plot(freq,abs(pumy1),label='PUM Y')
s21.set_ylabel('Magnitude')
s21.set_xlabel('Freq [Hz]')
s21.set_xscale('log')
s21.set_yscale('log')
s21.legend()

s22.plot(freq,np.angle(pumx1,deg=True),label='PUM X')
s22.plot(freq,np.angle(-pumy1,deg=True),label='PUM Y')
s22.set_ylabel('Phase [Deg]')
s22.set_xlabel('Freq [Hz]')
s22.set_xscale('log')
s22.set_yscale('linear')
s22.legend()

s23.plot(freq,abs(pumx1/pumy1),label='PUM X/PUM Y')
s23.set_ylabel('Magnitude Ratio (log scale)')
s23.set_xlabel('Freq [Hz]')
s23.set_xscale('log')
s23.set_yscale('log')
s23.legend()

s24.plot(freq,np.angle(pumx1/(-pumy1),deg=True),label='PUM X/PUM Y')
s24.set_ylabel('Phase Difference [Deg]')
s24.set_xlabel('Freq [Hz]')
s24.set_xscale('log')
s24.set_yscale('linear')
s24.legend()

fig2.suptitle('Comparing X and Y PUM actuation paths (including arm sign)')

fig3 = plt.figure(3)
s31 = fig3.add_subplot(221)
s32 = fig3.add_subplot(223)
s33 = fig3.add_subplot(222)
s34 = fig3.add_subplot(224)


s31.plot(freq,abs(pumx1-pumy1),label='Original PUM X+Y')
s31.plot(freq,abs(pumx2-pumy2),label='Changed PUM X+Y')
s31.set_ylabel('Magnitude')
s31.set_xlabel('Freq [Hz]')
s31.set_xscale('log')
s31.set_yscale('log')
s31.legend()

s32.plot(freq,np.angle(pumx1-pumy1,deg=True),label='Original PUM X+Y')
s32.plot(freq,np.angle(pumx2-pumy2,deg=True),label='Changed PUM X+Y')
s32.set_ylabel('Phase [Deg]')
s32.set_xlabel('Freq [Hz]')
s32.set_xscale('log')
s32.set_yscale('linear')
s32.legend()

s33.plot(freq,abs((pumx2-pumy2)/(pumx1-pumy1)),label='Changed/Original')
s33.set_ylabel('Magnitude Ratio (log scale)')
s33.set_xlabel('Freq [Hz]')
s33.set_xscale('log')
s33.set_yscale('log')
s33.legend()

s34.plot(freq,np.angle((pumx2-pumy2)/(pumx1-pumy1),deg=True),label='PUM X/PUM Y')
s34.set_ylabel('Phase Difference [Deg]')
s34.set_xlabel('Freq [Hz]')
s34.set_xscale('log')
s34.set_yscale('linear')
s34.legend()

fig3.suptitle('Comparing ETMX + ETMY PUM superactuator assuming +/-10% DC gain shift')

plt.show()

