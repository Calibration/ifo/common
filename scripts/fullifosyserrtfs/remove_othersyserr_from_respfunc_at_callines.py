## ################# ##
## Environment Setup ##
## ################# ##

# Do this in a terminal before running this script:
# $ conda activate cds-pydarm-test
# $ python -m pydarm --version
# 0.1.2.dev85+gffcb6ba
# $ export NDSSERVER="nds.ligo-wa.caltech.edu,$NDSSERVER"
# $ svn up /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O4/H1/
# $ git pull /ligo/gitcommon/Calibration/ifo/pydarmparams/
# $ git pull /ligo/gitcommon/Calibration/ifo/scripts/fullifosyserrtfs/

from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.time import * # tconvert, to_gps, from_gps
from scipy import signal
from copy import deepcopy
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
from matplotlib import cm
import numpy as np
import pydarm

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 2,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
## ################# ##
## Hard-coded Params ##
## ################# ##
gitdir = '/ligo/gitcommon/Calibration/ifo/pydarmparams/'
svndir = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'

datadir = svndir + 'Runs/O4/H1/Measurements/SystematicErrorTFs/Thermalization/'

power_eras = [75,
              60]

pydarm_params_refmodels = [gitdir + 'pydarm_H1_20230510T062635Z.ini', # 75W
                           gitdir + 'pydarm_H1_20230621T211522Z.ini'] # 60W

pydarm_params_no3p2kmodels = [gitdir + 'pydarm_H1_20230510T062635Z_no3p2k.ini', # 75W
                              gitdir + 'pydarm_H1_20230621T211522Z_no3p2k.ini'] # 60W

FFT_STRIDE = 120    # sec

# Eventually, use the whole list of 4 hour stretches at each power.
# Ref IDs match that from their respective aLOGs LHO:72950 (75W) and LHO:72950 (60W) 
# Removed 75W Ref ID 7 'cause it has a glitch in the data, see Comment (3) in LHO:69898 
#                               start       stop           Power   Ref ID           
gpstime_startstop = [np.array([[1367752698, 1367767098],    #75W   10
                               [1368087318, 1368099918],    #75W    9
                               [1368161418, 1368175818],    #75W    8
                               [1368384498, 1368396018],    #75W    6
                               [1368442818, 1368457218],    #75W    5
                               [1368559818, 1368574218],    #75W    4    # CCS TDCF kC and fC are 0. (!!) for first 4 strides (8 minutes), but it doesn't matter since GDS TDCFs aren't frozen 
                               [1368603918, 1368618318],    #75W    3
                               [1368632418, 1368646818],    #75W    2
                               [1368655218, 1368669618]]),  #75W    1
                     np.array([[1371444918, 1371459318],    #60W    1
                               [1374536538, 1374550938],    #60W    2
                               [1374610218, 1374624618],    #60W    3
                               [1374720618, 1374735018],    #60W    4
                               [1374783798, 1374798198],    #60W    5
                               #[1374831318, 1374845718],    #60W    6   # CCS TDCF kC and fC are 0. (!!) for first 2 strides (4 minutes), and GDS TDCFs are frozen, so can't use this data 
                               [1374861918, 1374876318],    #60W    7
                               [1374997878, 1375012278],    #60W    8
                               [1375149798, 1375164198],    #60W    9
                               [1375275438, 1375289838],    #60W   10 
                               [1375342818, 1375357218],    #60W   11
                               [1375566738, 1375581138]])]  #60W   12

                  
'''
# Just two times for starters
gpstime_startstop = [np.array([[1368655218, 1368669618]]), # 75 W  1
                     np.array([[1374783798, 1374798198]])] # 60 W  7
'''  
          
'''
# Just 15 minutes of each of those two times for starter starters.
gpstime_startstop = [np.array([[1368655218, 1368656118]]), # 75 W  1
                     np.array([[1374783798, 1374784698]])] # 60 W  9
'''

syserr_tag = ['No 3.2 kHz ESD pole','Frozen GDS TDCFs', 'Uncertain 102.3 Hz line','OM2 hot']

# Defined using T2300297, LHO:72950 (75W) and LHO:72950 (60W) 
#                  Missing  | Frozen | SRCLFF   | OM2 
#                  3.2 kHz  | GDS    | Botches  | HOT
#                  ESD Pole | TDCFs  | 102.3 Hz |           Power   Ref. ID
syserr_flags = [ [[True ,   False ,   False ,     False],   #75W    10
                  [True ,   False ,   False ,     False],   #75W    9
                  [True ,   False ,   False ,     False],   #75W    8
                  [True ,   False ,   False ,     False],   #75W    6
                  [True ,   False ,   False ,     False],   #75W    5
                  [True ,   False ,   False ,     False],   #75W    4
                  [True ,   False ,   False ,     False],   #75W    3 
                  [True ,   False ,   False ,     False],   #75W    2
                  [True ,   False ,   False ,     False]],  #75W    1
                 [[True ,   False ,   False ,     False],   #60W    1
                  [True ,   True  ,   False ,     True ],   #60W    2
                  [True ,   True  ,   False ,     True ],   #60W    3
                  [True ,   True  ,   False ,     True ],   #60W    4
                  [True ,   True  ,   False ,     True ],   #60W    5
                  #[True ,   True  ,   False ,     True ],   #60W    6
                  [True ,   True  ,   False ,     True ],   #60W    7
                  [True ,   False ,   False ,     True ],   #60W    8
                  [True ,   False ,   False ,     True ],   #60W    9
                  [True ,   False ,   True ,      True ],   #60W    10
                  [True ,   False ,   True ,      True ],   #60W    11
                  [False,   False ,   True ,      True ]] ] #60W    12   

# Notes:
#    - There's not a systematic error, per se, with the botched 102.3 Hz 
#      line, I just create the flag to either drop the 102.3 Hz line's 
#      data point from the plot, or put a "don't look at this data point" 
#      marker over it or something.
#      
#    - I don't even know if there *is* a systematic error intorduced
#      with OM2 HOT, but it'll be interesting there's a pattern after
#      other error is removed, so created a flag for it.                       

## ############# ##                   
## LOAD THE DATA ##                     
## ############# ##

times = [[],[]]
times_startUTC = [[],[]]
freqs = [[],[]]
all_timetime = [[],[]]
all_freqfreq = [[],[]]
all_etaR_tf = [[],[]]
all_tdcfs_ccs = [[],[]]
all_tdcfs_gds = [[],[]]

for kPower, thisPower in enumerate(power_eras):
    for jLockStretch in range(len(gpstime_startstop[kPower][:,0])):
        stretch_startgps = gpstime_startstop[kPower][:,0][jLockStretch]
        stretch_stopgps = gpstime_startstop[kPower][:,1][jLockStretch]
        
        fileTag = '{}{}W/etaR_PCALdLmeas_over_GDSdLmodel_{}W_{}-{}'.format(datadir,power_eras[kPower],power_eras[kPower],stretch_startgps,stretch_stopgps)
        
        timetime = np.loadtxt('{}_timetime.txt'.format(fileTag))
        freqfreq = np.loadtxt('{}_freqfreq.txt'.format(fileTag))
        magmag = np.loadtxt('{}_mag.txt'.format(fileTag))
        phapha = np.loadtxt('{}_pha.txt'.format(fileTag)) # Phase was saved in [radians]
        
        tftf = magmag * np.exp(1j * phapha)
        
        tdcfs_ccs = np.loadtxt('{}_tdcfs_ccs.txt'.format(fileTag))
        tdcfs_gds = np.loadtxt('{}_tdcfs_gds.txt'.format(fileTag))
    
        startutc = tconvert(timetime[0,0]).strftime('%Y-%m-%d %H:%M:%S')+' UTC'
        endutc = tconvert(timetime[-1,-1]).strftime('%Y-%m-%d %H:%M:%S')+' UTC'
        
        # convert time in absolute seconds to minutes relative to stretch_startgps
        timetime = (timetime - timetime[0,0])/60+1 
        
        all_timetime[kPower].append(timetime)
        all_freqfreq[kPower].append(freqfreq)
        all_etaR_tf[kPower].append(tftf)
        all_tdcfs_ccs[kPower].append(tdcfs_ccs)
        all_tdcfs_gds[kPower].append(tdcfs_gds)
    
        times[kPower].append(timetime[0,:])
        times_startUTC[kPower].append(startutc)
        freqs[kPower].append(freqfreq[:,0])
                     
## ####################### ##
## Instantiate DARM Models ##
## ####################### ##
freq = freqs[0][0]

R_ref    = [[],[]]
R_no3p2k = [[],[]]

# Use fancy pythonic generators to instantiate empty list of list of lists 
# so one can fill in, e.g., the 
# R_wCCSTDCFs[kPower][jLockStretch][iStride]'s calculated response
# function
R_wCCSTDCFs, R_wGDSTDCFs_no3p2k = ([[[[] for iStride in range(len(times[kPower][jLockStretch]))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))] for i in range(2))

kC_ind, fC_ind, kU_ind, kP_ind, kT_ind = 0, 1, 2, 3, 4

for kPower, thisPower in enumerate(power_eras):
    #create two DARM model objects
    # :: one with 3.2 kHz pole in place (i.e. as is)
    darmModel_obj = pydarm.darm.DARMModel(pydarm_params_refmodels[kPower])
        
    # :: one with the 3.2 kHz poles removed
    darmModel_obj_no3p2k = pydarm.darm.DARMModel(pydarm_params_no3p2kmodels[kPower])
    
    # extra response functions from these two options that don't depend on TDCFs.
    R_ref[kPower] = darmModel_obj.compute_response_function(freq)
    R_no3p2k[kPower] = darmModel_obj_no3p2k.compute_response_function(freq)
    
    for jLockStretch in range(len(gpstime_startstop[kPower][:,0])):
        print('Building models for {}W time at {}'.format(power_eras[kPower],times_startUTC[kPower][jLockStretch]))
        # Copy the reference model in order to apply different CCS vs. GDS TDCF sets
        darmModel_obj_wCCSTDCFs = deepcopy(darmModel_obj)
        darmModel_obj_wGDSTDCFs = deepcopy(darmModel_obj)
    
        darmModel_obj_wCCSTDCFs_no3p2k = deepcopy(darmModel_obj_no3p2k)
        darmModel_obj_wGDSTDCFs_no3p2k = deepcopy(darmModel_obj_no3p2k)
    
        for iStride in range(len(times[kPower][jLockStretch])):
            # Apply TDCFs to DARM models
            darmModel_obj_wCCSTDCFs.sensing.coupled_cavity_optical_gain *= all_tdcfs_ccs[kPower][jLockStretch][iStride][kC_ind]
            darmModel_obj_wCCSTDCFs.sensing.coupled_cavity_pole_frequency = all_tdcfs_ccs[kPower][jLockStretch][iStride][fC_ind]
            darmModel_obj_wCCSTDCFs.actuation.xarm.uim_npa *= all_tdcfs_ccs[kPower][jLockStretch][iStride][kU_ind]
            darmModel_obj_wCCSTDCFs.actuation.xarm.pum_npa *= all_tdcfs_ccs[kPower][jLockStretch][iStride][kP_ind]
            darmModel_obj_wCCSTDCFs.actuation.xarm.tst_npv2 *= all_tdcfs_ccs[kPower][jLockStretch][iStride][kT_ind]

            darmModel_obj_wGDSTDCFs_no3p2k.sensing.coupled_cavity_optical_gain *= all_tdcfs_gds[kPower][jLockStretch][iStride][kC_ind]
            darmModel_obj_wGDSTDCFs_no3p2k.sensing.coupled_cavity_pole_frequency = all_tdcfs_gds[kPower][jLockStretch][iStride][fC_ind]
            darmModel_obj_wGDSTDCFs_no3p2k.actuation.xarm.uim_npa *= all_tdcfs_gds[kPower][jLockStretch][iStride][kU_ind]
            darmModel_obj_wGDSTDCFs_no3p2k.actuation.xarm.pum_npa *= all_tdcfs_gds[kPower][jLockStretch][iStride][kP_ind]
            darmModel_obj_wGDSTDCFs_no3p2k.actuation.xarm.tst_npv2 *= all_tdcfs_gds[kPower][jLockStretch][iStride][kT_ind]
        
            # extract updated response functions from all of these options
            R_wCCSTDCFs[kPower][jLockStretch][iStride] = darmModel_obj_wCCSTDCFs.compute_response_function(freq)
            R_wGDSTDCFs_no3p2k[kPower][jLockStretch][iStride] = darmModel_obj_wCCSTDCFs_no3p2k.compute_response_function(freq)
           
## ############################################ ##
## Model and Correct for Known Systematic Error ##
## ############################################ ##
            
# See discussions in
# LHO:72817 to convey how to generate a systematic error correction to GDSdL if *just* the GDS TDCFs are frozen
# LHO:72879 to convey how to generate a systematic error correction to GDSdL if *just* the 3.2 kHz ESD pole is missing, and
# LHO:72879 and LHO:74255 to generate a systematic error correction to GDSdL if *both* are happening
            
# Remember, we're trying to pull *out* these systematic errors from our measurement, such that "all we have left"
# (as far as we know) is the "unknown" error that "thermalization" causes. Here, we're talking "the low frequency
# part of the sensing function that's going wild (see LHO:69898 and LHO:70150). 
            
# (a) we need all these options, given the sordid history of systematic error, and
# (b) GDSdL is in the denominator of our etaR_PCALdLmeas_over_GDSdLmodel transfer functions, so we create the systematic error correction
# as a multiplicative factor to GDSdL, and *divide* it into our measured etaR_PCALdLmeas_over_GDSdLmodel,
            
# Case I: Only missing 3.2 kHz pole (all the 75W data, and Ref ID 1 of 60W data)
#                       R_ref
#     GDSdL_corr_I =  ---------
#                      R_no3p2k
#
#                                  1         dL PCAL [m]            1
#     etaR_thermonly = etaR * ------------ = ----------- * ------------------
#                             GDSdL_corr_I    dL GDS [m]   (R_ref / R_no3p2k)
            
# Case II: missing 3.2 kHz pole *and* frozen GDS TDCFs  (only Ref ID 2-6 of 60W data)
#                       R_ref (updated with CCS TDCFs)         R_wCCSTDCFs
#     GDSdL_corr_II =  --------------------------------- = ------------------
#                      R_no3p2k (updated with GDS TDCFs)   R_wGDSTDCFs_no3p2k
#
#                                  1          dL PCAL [m]            1
#     etaR_thermonly = etaR * ------------- = ----------- * ------------------
#                             GDSdL_corr_II    dL GDS [m]   (R_ref / R_no3p2k)
            
# CASE III: No known systemtic error (only Ref ID 10 in 60W data)
#                       R_ref
#     GDSdL_corr_III = ------- = 1
#                       R_ref
#
#                                   1
#     etaR_thermonly = etaR * -------------- = etaR
#                             GDSdL_corr_III
            
## syserror flags list at the kthPower in the jthLockStretch has the order
#                 [    0    ,    1   ,    2     ,   3  ] 
#                  Missing  | Frozen | SRCLFF   | OM2 
#                  3.2 kHz  | GDS    | Botches  | HOT
#                  ESD Pole | TDCFs  | 102.3 Hz |   

# Use fancy pythonic generators again to instantiate empty list of list of lists 
# so one can fill in, e.g., the 
# all_GDSdL_corr[kPower][jLockStretch][iStride]'s calculated GDSdL correction
# transfer function

all_GDSdL_corr = [[[[] for iStride in range(len(times[kPower][jLockStretch]))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))]

all_etaR_thermonly_tf = [[[[] for iStride in range(len(times[kPower][jLockStretch]))] for jLockStretch in range(len(gpstime_startstop[kPower][:,0]))] for kPower in range(len(power_eras))]
       
# Now do the work!
for kPower, thisPower in enumerate(power_eras):        
    for jLockStretch in range(len(gpstime_startstop[kPower][:,0])):
        syserr_msg = '{}W time at {}'.format(power_eras[kPower],times_startUTC[kPower][jLockStretch])
        
        # CASE I
        if (syserr_flags[kPower][jLockStretch][0] and (not syserr_flags[kPower][jLockStretch][1])): 
            syserr_msg += ' has error from {},'.format(syserr_tag[0])
            for iStride in range(len(times[kPower][jLockStretch])):
                all_GDSdL_corr[kPower][jLockStretch][iStride] = R_ref[kPower] / R_no3p2k[kPower]
        # CASE II
        elif (syserr_flags[kPower][jLockStretch][0] and syserr_flags[kPower][jLockStretch][1]):
            syserr_msg += ' has error from {} and {},'.format(syserr_tag[0],syserr_tag[1])
            for iStride in range(len(times[kPower][jLockStretch])):
                all_GDSdL_corr[kPower][jLockStretch][iStride] = R_wCCSTDCFs[kPower][jLockStretch][iStride] / R_wGDSTDCFs_no3p2k[kPower][jLockStretch][iStride]
        # CASE III
        else:
            syserr_msg += ' has no known systematic error but'
            for iStride in range(len(times[kPower][jLockStretch])):
                all_GDSdL_corr[kPower][jLockStretch][iStride] = np.real(R_ref[kPower] / R_ref[kPower]) # Take the real part to discard any tiny imaginry parts from num. prec. error
            
        if syserr_flags[kPower][jLockStretch][2]:
            syserr_msg += ' has {},'.format(syserr_tag[2])
            
        if syserr_flags[kPower][jLockStretch][3]:
            syserr_msg += ' and may have error from {}'.format(syserr_tag[3])
        
        # Print all the errors envoked to the command line as a cross-check    
        print(syserr_msg)
        
        # Actually apply the GDSdL correction to the etaR_PCALdLmeas_over_GDSdLmodel transfer function
        for iStride in range(len(times[kPower][jLockStretch])):
            all_etaR_thermonly_tf[kPower][jLockStretch][iStride] = all_etaR_tf[kPower][jLockStretch][iStride] / all_GDSdL_corr[kPower][jLockStretch][iStride]
           
        # Save the new results! 
        stretch_startgps = gpstime_startstop[kPower][:,0][jLockStretch]
        stretch_stopgps = gpstime_startstop[kPower][:,1][jLockStretch]
        fileTag_GDSdL_corr =  '{}{}W/GDSdL_corr_{}W_{}-{}'.format(datadir,power_eras[kPower],power_eras[kPower],stretch_startgps,stretch_stopgps)
        fileTag_etaR = '{}{}W/etaR_thermonly_PCALdLmeas_over_GDSdLmodel_{}W_{}-{}'.format(datadir,power_eras[kPower],power_eras[kPower],stretch_startgps,stretch_stopgps)
        
        out_GDSdL_mag = abs(np.array(all_GDSdL_corr[kPower][jLockStretch]))
        out_GDSdL_pha = np.angle(np.array(all_GDSdL_corr[kPower][jLockStretch]))
        
        np.savetxt('{}_mag.txt'.format(fileTag_GDSdL_corr), out_GDSdL_mag, fmt='%.7g')
        np.savetxt('{}_pha.txt'.format(fileTag_GDSdL_corr), out_GDSdL_pha, fmt='%.7g')
        
        out_etaR_mag = abs(np.array(all_etaR_thermonly_tf[kPower][jLockStretch]))
        out_etaR_pha = np.angle(np.array(all_etaR_thermonly_tf[kPower][jLockStretch]))
        
        np.savetxt('{}_mag.txt'.format(fileTag_etaR), out_etaR_mag, fmt='%.7g')
        np.savetxt('{}_pha.txt'.format(fileTag_etaR), out_etaR_pha, fmt='%.7g')
            

