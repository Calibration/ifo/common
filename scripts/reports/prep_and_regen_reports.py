#!/usr/bin/env python3
import sys
import os
import yaml
from pydarm.cmd import list_reports
from pydarm.cmd._const import (
    IFO,
    CAL_CONFIG_ROOT,
    CAL_MEASUREMENT_ROOT,
    # CAL_REPORT_ROOT,
)
import shutil
from subprocess import run, CalledProcessError
from datetime import datetime
from gpstime import gpstime
from glob import glob

HF_DIR = os.path.join(CAL_MEASUREMENT_ROOT, 'hf_roaming_lines')
DEFAULT_CMD_FILE = os.path.join(CAL_CONFIG_ROOT, f'pydarm_cmd_{IFO}.yaml')
DATE_FMT = "%Y-%m-%d %H:%M:%S %Z"

ENV = os.environ.copy()
ENV['LOG_LEVEL'] = 'DEBUG'


if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('start', type=str)
    p.add_argument('end', type=str)
    args = p.parse_args()

    START_DATE_STR = args.start
    END_DATE_STR = args.end

    # START_DATE_STR = "2023-10-01 00:00:00 UTC"
    # START_DATE_STR = "2023-12-23 00:00:00 UTC"
    # END_DATE_STR = "2024-01-16 16:00:00 UTC"
    START_DATE = datetime.strptime(START_DATE_STR, DATE_FMT)
    END_DATE = datetime.strptime(END_DATE_STR, DATE_FMT)
    START_GPS = gpstime.fromdatetime(START_DATE)
    END_GPS = gpstime.fromdatetime(END_DATE)

    reports = list_reports(
        chronological=True,
        after=START_GPS,
        before=END_GPS,
    )
    print(reports)

    for report in reports:
        print('#########################')
        print(f'Committing {report.id}')
        commit_args = [
            'pydarm',
            'commit',
            '--message',
            "\"before-regen\"",
        ]
        if 'valid' in report.tags:
            commit_args.append('--valid')
        commit_args.append(f'{report.id}')
        run(commit_args, env=ENV)

        print('#########################')
        print(f'Overwriting pydarm_cmd_{IFO}.yaml in {report.id}...')

        rcmd_fn = report.gen_path(f'pydarm_cmd_{IFO}.yaml')
        shutil.copy(DEFAULT_CMD_FILE, rcmd_fn)


        print('#########################')
        print('HFRL measurements:')
        mdir = 'measurements/hf_roaming_lines'
        for hf in glob(os.path.join(HF_DIR, '*')):
            print(f'Copying {os.path.basename(hf)}')
            if not os.path.exists(report.gen_path(mdir)):
                os.makedirs(report.gen_path(mdir))
            shutil.copy(
                hf,
                report.gen_path(mdir)
            )

        print('#########################')
        print(f'Regenerating {report.id}')
        regen_args = [
            'pydarm',
            'report',
            '--regen',
            '--skip-mcmc',
            '--skip-gds',
            '--no-display',
        ]

        regen_args.append(str(report.id))
        run(regen_args, env=ENV, check=True)

        print('#########################')
        print(f'Committing {report.id}')
        commit_args = [
            'pydarm',
            'commit',
            '--message',
            "\"regen\"",
        ]
        if 'valid' in report.tags:
            commit_args.append('--valid')
        commit_args.append(f'{report.id}')
        run(commit_args, env=ENV)

        print('#########################')
        print(f'Uploading {report.id}')
        upload_args = [
            'pydarm',
            'upload',
            f'{report.id}'
        ]
        run(upload_args, env=ENV)
