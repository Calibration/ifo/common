from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner
import sys
import os

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/calibration/ifo/'

printFigs = True

run = 'O3'
IFO = 'H1'
todayDate =  dt.today().strftime('%Y%m%d')

sys.path.insert(0, cal_data_root+'/Common/pyDARM')
sys.path.insert(1, cal_data_root+'/Runs/O3/H1/params')

from src.computeDARM import computeDARM
from src.sensing import unc_from_coh
from modelparams_H1_20210423 import modelPars as modelPars

pars = [modelPars()]
oldModelDate = '20210423'

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20220527.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

resultsDir = \
    cal_data_root+'/Runs/O3/'+IFO+'/Results/CALCS_FE/'
    
figTag ='relativedelayAvsC_{}_proc{}_models{}vs{}_{}'\
    .format(\
        IFO,\
        todayDate,\
        modelDate,\
        oldModelDate,\
        'newvsoldpydarm')

freq = np.logspace(1,3,1000)

# Build the new pyDARM model:
calcsmodel_obj = pydarm.calcs.CALCSModel(model_parameters_file)
darmmodel_obj = pydarm.darm.DARMModel(model_parameters_file)

darm_olg_tf = darmmodel_obj.compute_darm_olg(freq)

darm_olg_ugf_ind = np.argmax(abs(darm_olg_tf) < 1.0)
darm_olg_ugf = freq[darm_olg_ugf_ind]
darm_olg_mag_at_ugf = abs(darm_olg_tf[darm_olg_ugf_ind])
darm_olg_pha_at_ugf = np.angle(darm_olg_tf[darm_olg_ugf_ind],deg=True)

delay_A_clockcycles, delay_residual_A_tf = calcsmodel_obj.residual_actuation_sim_delay(freq,clock=True)
delay_C_clockcycles, delay_residual_C_tf = calcsmodel_obj.residual_sensing_sim_delay(freq,clock=True)
delay_A_sec, __ = calcsmodel_obj.residual_actuation_sim_delay(freq,clock=False)
delay_C_sec, __ = calcsmodel_obj.residual_sensing_sim_delay(freq,clock=False)

delay_invCplusA_sec = (delay_C_sec + delay_A_sec)
delay_invCplusA_clockcycles = (delay_A_clockcycles + delay_C_clockcycles)

Cres_tf = darmmodel_obj.sensing.sensing_residual(freq)
invCres_tf = 1.0/Cres_tf
Ares_tf = darmmodel_obj.actuation.compute_actuation(freq)/calcsmodel_obj.calcs_darm_actuation(freq)
invAres_tf = 1.0/Ares_tf

Cres_tf_at_darmugf_deg = np.angle(Cres_tf[darm_olg_ugf_ind],deg=True)
invCres_tf_at_darmugf_deg = np.angle(invCres_tf[darm_olg_ugf_ind],deg=True)
Ares_tf_at_darmugf_deg = np.angle(Ares_tf[darm_olg_ugf_ind],deg=True)
invAres_tf_at_darmugf_deg = np.angle(invAres_tf[darm_olg_ugf_ind],deg=True)

Cres_tf_at_darmugf_sec = (np.pi/180.0)*Cres_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
invCres_tf_at_darmugf_sec = (np.pi/180.0)*invCres_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
Ares_tf_at_darmugf_sec = (np.pi/180.0)*Ares_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
invAres_tf_at_darmugf_sec = (np.pi/180.0)*invAres_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)

Cres_tf_at_darmugf_cc = Cres_tf_at_darmugf_sec * 16384.0
invCres_tf_at_darmugf_cc = invCres_tf_at_darmugf_sec * 16384.0
Ares_tf_at_darmugf_cc = Ares_tf_at_darmugf_sec * 16384.0
invAres_tf_at_darmugf_cc = invAres_tf_at_darmugf_sec * 16384.0

reldel_sub_tf_deg = np.angle(invCres_tf,deg=True) - np.angle(invAres_tf,deg=True)
reldel_add_tf_deg = np.angle(invCres_tf,deg=True) + np.angle(Ares_tf,deg=True)
reldel_sub_at_darm_ugf_deg = reldel_sub_tf_deg[darm_olg_ugf_ind]
reldel_add_at_darm_ugf_deg = reldel_add_tf_deg[darm_olg_ugf_ind]

reldel_sub_at_darm_ugf_sec = (np.pi/180.0)*reldel_sub_at_darm_ugf_deg / (2.0*np.pi*darm_olg_ugf)
reldel_sub_at_darm_ugf_cc = reldel_sub_at_darm_ugf_sec * 16384.0

reldel_add_at_darm_ugf_sec = (np.pi/180.0)*reldel_add_at_darm_ugf_deg / (2.0*np.pi*darm_olg_ugf)
reldel_add_at_darm_ugf_cc = reldel_add_at_darm_ugf_sec * 16384.0

# Build the old pyDARM model:
[D, C, A, G, CLG, R, sensProd, actProd] = computeDARM(pars[0], freq)

G_ugf_ind = np.argmax(abs(G) < 1.0)
G_ugf = freq[G_ugf_ind]
G_mag_at_ugf = abs(G[G_ugf_ind])
G_pha_at_ugf = np.angle(G[G_ugf_ind],deg=True)

# As appears extracted from
# cal_data_root+'Runs/O3/H1/Scripts/CALCS_FE/compute_relativedelay_AvsC_20210417.py'
# I get why invCres is used, but I don't understand why invAres is used.
old_invCres_tf = sensProd.calcsResid2gdsInvSensOut # Line 49
old_Cres_tf = 1.0/old_invCres_tf

old_invAres_tf = 1.0/actProd.calcs2gdsResidActFiltOut_NoDAQDownSample # Line 49
old_Ares_tf = 1.0/old_invAres_tf

old_Cres_tf_at_darmugf_deg = np.angle(old_Cres_tf[G_ugf_ind],deg=True)
old_invCres_tf_at_darmugf_deg = np.angle(old_invCres_tf[G_ugf_ind],deg=True)
old_Ares_tf_at_darmugf_deg = np.angle(old_Ares_tf[G_ugf_ind],deg=True)
old_invAres_tf_at_darmugf_deg = np.angle(old_invAres_tf[G_ugf_ind],deg=True)

old_Cres_tf_at_darmugf_sec = (np.pi/180.0)*old_Cres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_invCres_tf_at_darmugf_sec = (np.pi/180.0)*old_invCres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_Ares_tf_at_darmugf_sec = (np.pi/180.0)*old_Ares_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_invAres_tf_at_darmugf_sec = (np.pi/180.0)*old_invAres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)

old_Cres_tf_at_darmugf_cc = old_Cres_tf_at_darmugf_sec * 16384.0
old_invCres_tf_at_darmugf_cc = old_invCres_tf_at_darmugf_sec * 16384.0
old_Ares_tf_at_darmugf_cc = old_Ares_tf_at_darmugf_sec * 16384.0
old_invAres_tf_at_darmugf_cc = old_invAres_tf_at_darmugf_sec * 16384.0

old_reldel_sub_tf_deg = np.angle(old_invCres_tf,deg=True) - np.angle(old_invAres_tf,deg=True)
old_reldel_add_tf_deg = np.angle(old_invCres_tf,deg=True) + np.angle(old_Ares_tf,deg=True)
old_reldel_sub_at_darm_ugf_deg = old_reldel_sub_tf_deg[G_ugf_ind]
old_reldel_add_at_darm_ugf_deg = old_reldel_add_tf_deg[G_ugf_ind]

old_reldel_sub_at_darm_ugf_sec = (np.pi/180.0)*old_reldel_sub_at_darm_ugf_deg / (2.0*np.pi*G_ugf)
old_reldel_sub_at_darm_ugf_cc = old_reldel_sub_at_darm_ugf_sec * 16384.0

old_reldel_add_at_darm_ugf_sec = (np.pi/180.0)*old_reldel_add_at_darm_ugf_deg / (2.0*np.pi*G_ugf)
old_reldel_add_at_darm_ugf_cc = old_reldel_add_at_darm_ugf_sec * 16384.0

print('The estimated delay in A is:')
print('   {:.4g} [usec]'.format(delay_A_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(delay_A_sec,delay_A_sec*16384.0))
print('   Existing polyfit to integer clock cycles: {:.3f}'.format(delay_A_clockcycles))
print(' ')
print('The estimated delay in C is:')
print('   {:.4g} [usec]'.format(delay_C_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(delay_C_sec,delay_C_sec*16384.0))
print('   Existing polyfit to integer clock cycles: {:.3f}'.format(delay_C_clockcycles))
print(' ')
print('The relative delay between A and 1/C is:')
print('   {:.4g} [usec]'.format(delay_invCplusA_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(delay_invCplusA_sec,delay_invCplusA_sec*16384.0))
print('   Sum of existing polyfit to integer clock cycles: {:.3f}'.format(delay_invCplusA_clockcycles))
print(' ')
print('The DARM UGF is at index {}, which is {:.4f} [Hz], with magnitude {:.4f} [ ] and phase {:.4f} [deg]'.format(darm_olg_ugf_ind,darm_olg_ugf,darm_olg_mag_at_ugf, darm_olg_pha_at_ugf))

#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

plotFreqRange = [1e1, 1e3]
ugfmarkerstyle = '.'

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.semilogx(freq,abs(Cres_tf),label='C_res ({}): New pyDARM, HF OMC DCPD Chain poles, and Unity Gain AA'.format(modelDate))
s1.semilogx(freq,abs(old_Cres_tf),label='C_res ({}): Old pyDARM, old HF poles'.format(oldModelDate))
s1.semilogx(darm_olg_ugf,abs(Cres_tf[darm_olg_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} C_res @ DARM UGF: {:.4g}'.format(modelDate,abs(Cres_tf[darm_olg_ugf_ind])))
s1.semilogx(G_ugf,abs(old_Cres_tf[G_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} C_res @ DARM UGF: {:.4g}'.format(oldModelDate,abs(old_Cres_tf[G_ugf_ind])))
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.92,1.08])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.legend()
s1.set_ylabel('Magnitude [dimless?]')
s1.set_title('Sensing Residual, C_res')

s2.semilogx(freq,np.angle(Cres_tf,deg=True),label='C_res ({})'.format(modelDate))
s2.semilogx(freq,np.angle(old_Cres_tf,deg=True),label='C_res ({})'.format(oldModelDate))
s2.semilogx(darm_olg_ugf,np.angle(Cres_tf[darm_olg_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} C_res @ DARM UGF: {:.4g} [deg]'.format(modelDate,np.angle(Cres_tf[darm_olg_ugf_ind],deg=True)))
s2.semilogx(G_ugf,np.angle(old_Cres_tf[G_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} C_res @ DARM UGF: {:.4g} [deg]'.format(oldModelDate,np.angle(old_Cres_tf[G_ugf_ind],deg=True)))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-120,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.legend()
s2.set_title('{} DARM UGF: {:.4f} [Hz]'.format(modelDate,darm_olg_ugf))

s3.semilogx(freq,abs(Ares_tf),label='A_res ({}): New pyDARM, (no change in HF response)'.format(modelDate))
s3.semilogx(freq,abs(old_Ares_tf),label=r'A_res ({}): Old pyDARM, (same HF response as {})'.format(oldModelDate,modelDate))
s3.semilogx(darm_olg_ugf,abs(Ares_tf[darm_olg_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} A_res @ DARM UGF: {:.4g}'.format(modelDate,abs(Ares_tf[darm_olg_ugf_ind])))
s3.semilogx(G_ugf,abs(old_Ares_tf[G_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} A_res @ DARM UGF: {:.4g}'.format(oldModelDate,abs(old_Ares_tf[G_ugf_ind])))
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless?]')
s3.set_title('Actuation Residual, A_res')

s4.semilogx(freq,np.angle(Ares_tf,deg=True),label='A_res ({})'.format(modelDate))
s4.semilogx(freq,np.angle(old_Ares_tf,deg=True),label='A_res ({})'.format(oldModelDate))
s4.semilogx(darm_olg_ugf,np.angle(Ares_tf[darm_olg_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} A_res @ DARM UGF: {:.4g} [deg]'.format(modelDate,np.angle(Ares_tf[darm_olg_ugf_ind],deg=True)))
s4.semilogx(G_ugf,np.angle(old_Ares_tf[G_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} A_res @ DARM UGF: {:.4g} [deg]'.format(oldModelDate,np.angle(old_Ares_tf[G_ugf_ind],deg=True)))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-120,45)
s4.yaxis.set_major_locator(tck.MultipleLocator(15))
s4.yaxis.set_minor_locator(tck.MultipleLocator(5))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')
s4.legend()
s4.set_title('{} DARM UGF: {:.4f} [Hz]'.format(oldModelDate,G_ugf))

thisfig = fignames(resultsDir+figTag+'_CresAres_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.semilogx(freq,np.angle(invCres_tf,deg=True),label=r'{} $\measuredangle$ (1/C_res)'.format(modelDate))
s1.semilogx(freq,np.angle(old_invCres_tf,deg=True),label=r'{} $\measuredangle$ (1/C_res)'.format(oldModelDate))
s1.semilogx(freq,np.angle(invAres_tf,deg=True),label=r'{} $\measuredangle$ (1/A_res)'.format(modelDate))
s1.semilogx(freq,np.angle(old_invAres_tf,deg=True),label=r'{} $\measuredangle$ (1/A_res)'.format(oldModelDate))
s1.semilogx(darm_olg_ugf,invCres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/C_res) @ DARM UGF: {:.4f} [deg]'.format(modelDate,invCres_tf_at_darmugf_deg))
s1.semilogx(G_ugf,old_invCres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/C_res) @ DARM UGF: {:.4f} [deg]'.format(oldModelDate,old_invCres_tf_at_darmugf_deg))
s1.semilogx(darm_olg_ugf,invAres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/A_res) @ DARM UGF: {:.4f} [deg]'.format(modelDate,invAres_tf_at_darmugf_deg))
s1.semilogx(G_ugf,old_invAres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/A_res) @ DARM UGF: {:.4f} [deg]'.format(oldModelDate,old_invAres_tf_at_darmugf_deg))
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(-105,105)
s1.yaxis.set_major_locator(tck.MultipleLocator(15))
s1.yaxis.set_minor_locator(tck.MultipleLocator(5))
s1.legend(fontsize=6.0,loc='lower left')
s1.set_ylabel('Phase (deg)')
s1.set_title('The way "relative" delay --')

s2.semilogx(freq,np.angle(invCres_tf,deg=True),label=r'{} $\measuredangle$ (1/C_res)'.format(modelDate))
s2.semilogx(freq,np.angle(old_invCres_tf,deg=True),label=r'{} $\measuredangle$ (1/C_res)'.format(oldModelDate))
s2.semilogx(freq,np.angle(Ares_tf,deg=True),label=r'{} $\measuredangle$ A_res'.format(modelDate))
s2.semilogx(freq,np.angle(old_Ares_tf,deg=True),label=r'{} $\measuredangle$ A_res'.format(oldModelDate))
s2.semilogx(darm_olg_ugf,invCres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/C_res) @ DARM UGF: {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(modelDate,invCres_tf_at_darmugf_deg,1.0e6*invCres_tf_at_darmugf_sec,invCres_tf_at_darmugf_cc))
s2.semilogx(G_ugf,old_invCres_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ (1/C_res) @ DARM UGF: {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(oldModelDate,old_invCres_tf_at_darmugf_deg,1.0e6*old_invCres_tf_at_darmugf_sec,old_invCres_tf_at_darmugf_cc))
s2.semilogx(darm_olg_ugf,Ares_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ A_res @ DARM UGF: {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(modelDate,Ares_tf_at_darmugf_deg,1.0e6*Ares_tf_at_darmugf_sec,Ares_tf_at_darmugf_cc))
s2.semilogx(G_ugf,old_Ares_tf_at_darmugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$ A_res @ DARM UGF: {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(oldModelDate,old_Ares_tf_at_darmugf_deg,1.0e6*old_Ares_tf_at_darmugf_sec,old_Ares_tf_at_darmugf_cc))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-105,105)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.legend(fontsize=6.0,loc='lower left')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_title('Much less confusing...')

s3.semilogx(freq,np.angle(invCres_tf,deg=True) - np.angle(invAres_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res)'.format(modelDate))
s3.semilogx(freq,np.angle(old_invCres_tf,deg=True) - np.angle(old_invAres_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res)'.format(oldModelDate))
s3.semilogx(darm_olg_ugf,reldel_sub_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res) @ DARM UGF {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(modelDate,reldel_sub_at_darm_ugf_deg,1.0e6*reldel_sub_at_darm_ugf_sec,reldel_sub_at_darm_ugf_cc))
s3.semilogx(G_ugf,old_reldel_sub_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res) @ DARM UGF {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(oldModelDate,old_reldel_sub_at_darm_ugf_deg,1.0e6*old_reldel_sub_at_darm_ugf_sec,old_reldel_sub_at_darm_ugf_cc))
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim(-20,0.2)
s3.yaxis.set_major_locator(tck.MultipleLocator(5))
s3.yaxis.set_minor_locator(tck.MultipleLocator(1))
s3.legend(fontsize=6,loc='lower left')
s3.set_ylabel('Phase (deg)')
s3.set_title('-- was calculcated in old pydarm')

s4.semilogx(freq,np.angle(invCres_tf,deg=True) + np.angle(Ares_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res'.format(modelDate))
s4.semilogx(freq,np.angle(old_invCres_tf,deg=True) + np.angle(old_Ares_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res'.format(oldModelDate))
s4.semilogx(darm_olg_ugf,reldel_add_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res @ DARM UGF {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(modelDate,reldel_add_at_darm_ugf_deg,1.0e6*reldel_add_at_darm_ugf_sec,reldel_add_at_darm_ugf_cc))
s4.semilogx(G_ugf,old_reldel_add_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res @ DARM UGF {:.4f} [deg] = {:.1f} [usec] = {:.3f} [16k ccs]'.format(oldModelDate,old_reldel_add_at_darm_ugf_deg,1.0e6*old_reldel_add_at_darm_ugf_sec,old_reldel_add_at_darm_ugf_cc))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-20,0.2)
s4.yaxis.set_major_locator(tck.MultipleLocator(5))
s4.yaxis.set_minor_locator(tck.MultipleLocator(1))
s4.legend(fontsize=6,loc='lower left')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')
s4.set_title('... way like LHO:22117')

thisfig = fignames(resultsDir+figTag+'_invCresinvAres_plusminuscheck_phaseonly.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.semilogx(freq,np.angle(invCres_tf,deg=True) - np.angle(invAres_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res)'.format(modelDate))
s1.semilogx(freq,np.angle(old_invCres_tf,deg=True) - np.angle(old_invAres_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res)'.format(oldModelDate))
s1.semilogx(darm_olg_ugf,reldel_sub_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res) @ DARM UGF {:.4f} [deg]'.format(modelDate,reldel_sub_at_darm_ugf_deg))
s1.semilogx(G_ugf,old_reldel_sub_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) - $\measuredangle$(1/A_res) @ DARM UGF {:.4f} [deg]'.format(oldModelDate,old_reldel_sub_at_darm_ugf_deg))
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(-20,0.2)
s1.yaxis.set_major_locator(tck.MultipleLocator(5))
s1.yaxis.set_minor_locator(tck.MultipleLocator(1))
s1.legend()
s1.set_ylabel('Phase (deg)')
s1.set_title('In reality, its a phase')

s2.semilogx(freq,np.angle(invCres_tf,deg=True) + np.angle(Ares_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res'.format(modelDate))
s2.semilogx(freq,np.angle(old_invCres_tf,deg=True) + np.angle(old_Ares_tf,deg=True),label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res'.format(oldModelDate))
s2.semilogx(darm_olg_ugf,reldel_add_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res @ DARM UGF {:.4f} [deg]'.format(modelDate,reldel_add_at_darm_ugf_deg,1.0e6*reldel_add_at_darm_ugf_sec,reldel_add_at_darm_ugf_cc))
s2.semilogx(G_ugf,old_reldel_add_at_darm_ugf_deg,marker=ugfmarkerstyle,label=r'{} $\measuredangle$(1/C_res) + $\measuredangle$ A_res @ DARM UGF {:.4f} [deg]'.format(oldModelDate,old_reldel_add_at_darm_ugf_deg))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-20,0.2)
s2.yaxis.set_major_locator(tck.MultipleLocator(5))
s2.yaxis.set_minor_locator(tck.MultipleLocator(1))
s2.legend()
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_title('{} Relative "Delay": {:.1f} [usec] or {:.3f} [16k ccs]'.format(modelDate,1.0e6*reldel_add_at_darm_ugf_sec,reldel_add_at_darm_ugf_cc))


s3.semilogx(freq,1.0e6*(np.angle(invCres_tf) - np.angle(invAres_tf))/(2.0*np.pi*freq),label=r'{} ($\measuredangle$(1/C_res) - $\measuredangle$(1/A_res))/(2*pi*f)'.format(modelDate))
s3.semilogx(freq,1.0e6*(np.angle(old_invCres_tf) - np.angle(old_invAres_tf))/(2.0*np.pi*freq),label=r'{} ($\measuredangle$(1/C_res) - $\measuredangle$(1/A_res))/(2*pi*f)'.format(oldModelDate))
s3.semilogx(darm_olg_ugf,1.0e6*reldel_sub_at_darm_ugf_sec,marker=ugfmarkerstyle,label=r'{} ($\measuredangle$(1/C_res) - $\measuredangle$(1/A_res))/(2*pi*f) @ DARM UGF {:.1f} [usec]'.format(modelDate,1.0e6*reldel_sub_at_darm_ugf_sec))
s3.semilogx(G_ugf,1.0e6*old_reldel_sub_at_darm_ugf_sec,marker=ugfmarkerstyle,label=r'{} ($\measuredangle$(1/C_res) - $\measuredangle$(1/A_res))/(2*pi*f) @ DARM UGF {:.1f} [usec]'.format(oldModelDate,1.0e6*old_reldel_sub_at_darm_ugf_sec))
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim(-500,300)
s3.yaxis.set_major_locator(tck.MultipleLocator(100))
s3.yaxis.set_minor_locator(tck.MultipleLocator(20))
s3.legend()
s3.set_ylabel('Phase (usec delay)')
s3.set_title('But we can treat it like a delay')

s4.semilogx(freq,1.0e6*(np.angle(invCres_tf) + np.angle(Ares_tf))/(2.0*np.pi*freq),label=r'{} ($\measuredangle$(1/C_res) + $\measuredangle$ A_res)/(2*pi*f)'.format(modelDate))
s4.semilogx(freq,1.0e6*(np.angle(old_invCres_tf) + np.angle(old_Ares_tf))/(2.0*np.pi*freq),label=r'{} ($\measuredangle$(1/C_res) + $\measuredangle$ A_res)/(2*pi*f)'.format(oldModelDate))
s4.semilogx(darm_olg_ugf,1.0e6*reldel_add_at_darm_ugf_sec,marker=ugfmarkerstyle,label=r'{} ($\measuredangle$(1/C_res) + $\measuredangle$ A_res)/(2*pi*f) @ DARM UGF {:.1f} [usec]'.format(modelDate,1.0e6*reldel_add_at_darm_ugf_sec))
s4.semilogx(G_ugf,1.0e6*old_reldel_add_at_darm_ugf_sec,marker=ugfmarkerstyle,label=r'{} ($\measuredangle$(1/C_res) + $\measuredangle$ A_res)/(2*pi*f) @ DARM UGF {:.1f} [usec]'.format(oldModelDate,1.0e6*old_reldel_add_at_darm_ugf_sec))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-500,300)
s4.yaxis.set_major_locator(tck.MultipleLocator(100))
s4.yaxis.set_minor_locator(tck.MultipleLocator(20))
s4.legend()
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (usec delay)')
s4.set_title('{} Relative "Delay": {:.1f} [usec] or {:.3f} [16k ccs]'.format(oldModelDate,1.0e6*old_reldel_add_at_darm_ugf_sec,old_reldel_add_at_darm_ugf_cc))

thisfig = fignames(resultsDir+figTag+'_invCresplusAres_phaseasdelay.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')
