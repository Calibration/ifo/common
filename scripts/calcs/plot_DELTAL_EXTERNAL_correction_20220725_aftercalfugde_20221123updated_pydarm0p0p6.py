import pydarm
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import ticker as tck
from datetime import datetime as dt
from collections import namedtuple
import os
import pdb

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })

printFigs = True

cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/Calibration/ifo/'

IFO = 'H1'
todayDate =  dt.today().strftime('%Y%m%d')
modelDate = '20221118'
measDate = '20220722'

measurement_file = cal_data_root+'/Runs/O3/H1/Measurements/FullIFOSensingTFs/2022-07-22_H1_PCALY2DARMTF_BB_A_PCALYRXPD_B_DELTAL_EXTERNAL_TF_optgain_scaled.txt'
model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini'

resultsDir = \
    cal_data_root+'/Runs/O3/H1/Results/CALCS_FE/'
    
anythingelse = '_20221123update_pydarm0p0p6'    
 
figTag ='demonstratedeltalcorrection_{}_proc{}_model{}_meas{}_aftercalfudge{}'\
    .format(\
        IFO,\
        todayDate,\
        modelDate,\
        measDate,
        anythingelse)

# Create the model
calcs = pydarm.calcs.CALCSModel(model_parameters_file)

# Load in the measurement
dtt_tf = np.loadtxt(measurement_file)

freq = dtt_tf[1::, 0]
tf = dtt_tf[1::, 1] + 1j*dtt_tf[1::, 2]

corr_no_whitening = calcs.calcs_dtt_calibration(freq, False)


# whitening = calcs.deltal_ext_whitening(freq)
# Temporary how to get the answer before the above method
# comes in a future pyDARM release.
corr_wi_whitening = calcs.calcs_dtt_calibration(freq, True)
whitening = corr_no_whitening / corr_wi_whitening

pcal_corr = calcs.pcal.compute_pcal_correction(freq)
tf *= (1/pcal_corr)*(1/whitening)

#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

plotFreqRange = [20,500]

fig = plt.figure()
s1 = fig.add_subplot(211)
s2 = fig.add_subplot(212)

s1.semilogx(freq, abs(tf), '.', label='Uncorrected')
s1.semilogx(freq, abs(corr_no_whitening), label='Correction')
s1.semilogx(freq, abs(tf*corr_no_whitening), '.', label='Corrected')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.90,1.10])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.set_xlabel('Frequency (Hz)')
s1.set_ylabel('Magnitude [m/m]')
s1.legend()
s1.set_title('(CAL-DELTAL_EXTERNAL_DQ / CAL-PCALY_RX_PD_OUT_DQ) Transfer Function, and Corrections')

s2.semilogx(freq, np.angle(tf, deg=True), '.')
s2.semilogx(freq, np.angle(corr_no_whitening, deg=True))
s2.plot(freq, np.angle(tf*corr_no_whitening, deg=True), '.')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-10,10)
s2.yaxis.set_major_locator(tck.MultipleLocator(2))
s2.yaxis.set_minor_locator(tck.MultipleLocator(0.5))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')

thisfig = fignames(resultsDir+figTag+'.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')
