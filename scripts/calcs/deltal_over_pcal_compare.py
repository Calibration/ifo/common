from pydarm import darm, pcal, calcs
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import ticker as tck
from scipy import signal
#plt.ion()
# matplotlib settings
mpl.rcParams.update({'figure.figsize': (16, 9),
                     #'font.family': 'serif',
                     'lines.linewidth': 1,
                     'lines.markersize': 3,
                     'font.size': 14,
                     'xtick.labelsize': 14,
                     'ytick.labelsize': 12,
                     'legend.fancybox': True,
                     'legend.fontsize': 14,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     })

default_out_dir = "/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Results/CALCS_FE"
default_modelfile = "/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini"



def template_vs_pydarm(dtt_deltal_freqs, deltal_over_pcal_correction,
                       dtt_deltal_over_pcal_correction, pdfpages=None):
    fig, axs = plt.subplots(2, 2)
    fig.suptitle(f"DeltaL/Pcal, DTT Template vs pyDARM Comparison")
    fig.supxlabel("Frequency (Hz)")

    # overlay mag
    axs[0, 0].loglog(dtt_deltal_freqs, np.abs(deltal_over_pcal_correction),
                     label='pyDARM')
    axs[0, 0].loglog(dtt_deltal_freqs, np.abs(dtt_deltal_over_pcal_correction),
                     label='DTT Template')
    axs[0, 0].set_ylim(
        pow(10, np.floor(np.log10(min(
            min(np.abs(deltal_over_pcal_correction)),
            min(np.abs(dtt_deltal_over_pcal_correction)))))),
        pow(10, np.ceil(np.log10(max(
            max(np.abs(deltal_over_pcal_correction)),
            max(np.abs(dtt_deltal_over_pcal_correction)))))))
    axs[0, 0].set_ylabel("Magnitude")
    axs[0, 0].set_title("DeltaL/Pcal DTT Template and pyDARM TFs")

    # overlay phase
    axs[1, 0].semilogx(dtt_deltal_freqs, np.angle(deltal_over_pcal_correction,
                                                  deg=True))
    axs[1, 0].semilogx(dtt_deltal_freqs, np.angle(
        dtt_deltal_over_pcal_correction, deg=True))

    axs[1, 0].set_ylabel("Phase (deg.)")
    # residual mag
    residual_mag = np.abs(dtt_deltal_over_pcal_correction / deltal_over_pcal_correction)
    axs[0, 1].loglog(np.abs(residual_mag))
    axs[0, 1].set_ylim(
        pow(10, np.floor(np.log10(min(np.abs(residual_mag))))),
        pow(10, np.ceil(np.log10(max(np.abs(residual_mag))))))
    axs[0, 1].set_title("|DTT Template TF / pyDARM TF|")
    #print(f"Res mag maxylim: {pow(10, np.ceil(np.log10(max(np.abs(residual_mag)))))}")
    # residual phase
    axs[1, 1].semilogx(dtt_deltal_freqs, np.angle(
         dtt_deltal_over_pcal_correction / deltal_over_pcal_correction,
        deg=True))

    for i, ax in enumerate(axs.flat):
        ax.grid(True, which='major', color='black')
        ax.grid(True, which='minor', ls='--')
        #ax.grid(visible=True, which='minor', axis='y')


        #ax.xaxis.set_major_locator(tck.LogLocator(base=10))
        if i == 0 or i == 1:
            # mag y-axis
            locmin = mpl.ticker.LogLocator(base=10.0, numticks=60,
                                           subs=(.2,.3,.4,.5,.6,.7,.8,.9))
            ax.yaxis.set_minor_locator(locmin)
            ax.yaxis.set_major_locator(tck.LogLocator(base=10, numticks=30))
        elif i == 2 or i == 3:
            # phase y-axis
            ax.set_ylim(-185, 185)
            ax.yaxis.set_major_locator(tck.MultipleLocator(45))
            ax.yaxis.set_minor_locator(tck.MultipleLocator(15))
    axs[0, 0].legend()
    if pdfpages: pdfpages.savefig()


def calcs_calib_vs_dtt_deltal_ext(freqs, calcs_calib, dtt_deltal_ext,
                                  pdfpages=None):
    fig, axs = plt.subplots(2, 2)
    fig.suptitle(f"pyDARM CAL-CS Calibration vs DTT Template Calibration")
    fig.supxlabel("Frequency (Hz)")

    # overlay mag
    axs[0, 0].loglog(freqs, np.abs(calcs_calib),
                     label='pyDARM CALCS DTT Calib.')
    axs[0, 0].loglog(freqs, np.abs(dtt_deltal_ext),
                     label='DTT deltaL ext correction')
    axs[0, 0].set_ylim(
        pow(10, np.floor(np.log10(min(
            min(np.abs(calcs_calib)),
            min(np.abs(dtt_deltal_ext)))))),
        pow(10, np.ceil(np.log10(max(
            max(np.abs(calcs_calib)),
            max(np.abs(dtt_deltal_ext)))))))
    axs[0, 0].set_ylabel("Magnitude")
    axs[0, 0].set_title("pyDARM CALCS DTT Calib and DTT deltaL ext correction TFs")

    # overlay phase
    axs[1, 0].semilogx(freqs, np.angle(calcs_calib, deg=True))
    axs[1, 0].semilogx(freqs, np.angle(dtt_deltal_ext, deg=True))
    axs[1, 0].set_ylabel("Phase (deg.)")
    # residual mag
    residual_mag = np.abs(dtt_deltal_ext/calcs_calib)
    axs[0, 1].loglog(freqs, residual_mag)
    axs[0, 1].set_ylim(
        pow(10, np.floor(np.log10(min(residual_mag)))),
        pow(10, np.ceil(np.log10(max(residual_mag)))))
    axs[0, 1].set_title(f"|DTT Template/pyDARM CALCS |")
    # residual phase
    axs[1, 1].semilogx(freqs, np.angle(
         dtt_deltal_ext / calcs_calib,
        deg=True))

    for i, ax in enumerate(axs.flat):
        ax.grid(which='major', color='black')
        ax.grid(which='minor', ls='--')
        ax.grid(visible=True, which='minor', axis='y')

        ax.xaxis.set_major_locator(tck.LogLocator(base=10))
        if i == 0 or i == 1:
            # mag y-axis
            # ax.set_ylim(10**-10, 10**6)
            locmin = mpl.ticker.LogLocator(base=10.0, numticks=60,
                                           subs=(.2,.3,.4,.5,.6,.7,.8,.9))
            ax.yaxis.set_minor_locator(locmin)
            ax.yaxis.set_major_locator(tck.LogLocator(base=10, numticks=30))
        elif i == 2 or i == 3:
            # phase y-axis
            ax.set_ylim(-185, 185)
            ax.yaxis.set_major_locator(tck.MultipleLocator(45))
            ax.yaxis.set_minor_locator(tck.MultipleLocator(15))
    axs[0, 0].legend()
    if pdfpages: pdfpages.savefig()


def pcal_corr_vs_zpk(freqs, pcal_corr, zpk_resp,
                                  pdfpages=None):
    fig, axs = plt.subplots(2, 2)
    fig.suptitle(f"pyDARM vs DTT Template Pcal Correction")
    fig.supxlabel("Frequency (Hz)")

    # overlay mag
    axs[0, 0].loglog(freqs, np.abs(pcal_corr),
                     label='pyDARM Pcal Correction')
    axs[0, 0].loglog(freqs, np.abs(zpk_resp),
                     label='2 zpk poles at 1Hz')
    axs[0, 0].set_ylim(
        pow(10, np.floor(np.log10(min(
            min(np.abs(pcal_corr)),
            min(np.abs(zpk_resp)))))),
        pow(10, np.ceil(np.log10(max(
            max(np.abs(pcal_corr)),
            max(np.abs(zpk_resp)))))))
    axs[0, 0].set_ylabel("Magnitude")
    axs[0, 0].set_title("pyDARM and DTT Template Pcal Corrections")

    # overlay phase
    axs[1, 0].semilogx(freqs, np.angle(pcal_corr, deg=True))
    axs[1, 0].semilogx(freqs, np.angle(zpk_resp, deg=True))
    axs[1, 0].set_ylabel("Phase (deg.)")
    # residual mag
    residual_mag = np.abs(zpk_resp/pcal_corr)
    axs[0, 1].loglog(freqs, residual_mag)
    axs[0, 1].set_ylim(
        pow(10, np.floor(np.log10(min(residual_mag)))),
        pow(10, np.ceil(np.log10(max(residual_mag)))))
    axs[0, 1].set_title(f"|zpk([],[1,1],1) / pyDARM Pcal Correction|")
    # residual phase
    axs[1, 1].semilogx(freqs, np.angle(zpk_resp / pcal_corr , deg=True))

    for i, ax in enumerate(axs.flat):
        ax.grid(which='major', color='black')
        ax.grid(which='minor', ls='--')
        ax.grid(visible=True, which='minor', axis='y')

        ax.xaxis.set_major_locator(tck.LogLocator(base=10))
        if i == 0 or i == 1:
            # mag y-axis
            # ax.set_ylim(10**-10, 10**6)
            locmin = mpl.ticker.LogLocator(base=10.0, numticks=60,
                                           subs=(.2,.3,.4,.5,.6,.7,.8,.9))
            ax.yaxis.set_minor_locator(locmin)
            ax.yaxis.set_major_locator(tck.LogLocator(base=10, numticks=30))
        elif i == 2 or i == 3:
            # phase y-axis
            ax.set_ylim(-185, 185)
            ax.yaxis.set_major_locator(tck.MultipleLocator(45))
            ax.yaxis.set_minor_locator(tck.MultipleLocator(15))
    axs[0, 0].legend()
    if pdfpages: pdfpages.savefig()


if __name__ == "__main__":
    from matplotlib.backends.backend_pdf import PdfPages
    import argparse
    from datetime import datetime
    today = datetime.now().strftime("%Y%m%d")
    default_output_file = f"{default_out_dir}/{today}_deltal_over_pcal_comparison.pdf"
    p = argparse.ArgumentParser()
    p.add_argument('--outfile', '-o',
                   default=default_output_file,
                   help=f"output file. default is {default_output_file}")
    p.add_argument('--modelfile', '-m', type=str,
                   help=f'pyDARM parameter model file. Default is {default_modelfile}.',
                   default=default_modelfile)
    p.add_argument('--interactive', '-i', action='store_true', default=False,
                   help='plot in interactive matplotlib frames instead of PDF.')
    args = p.parse_args()
    print("Running...")
    model = darm.DARMModel(args.modelfile)
    pcal_model = pcal.PcalModel(args.modelfile)

    # define frequency spacing
    freqs = np.logspace(-1,np.log10(7444), num=1000)

    # load "old" dtt correction tf from template
    dtt_deltal_correction = np.loadtxt('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Measurements/CALCS_FE/2022-11-18_H1PCALY2DARMTF_BB_deltaL_only_calibrationtf_2018version.txt')
    dtt_deltal_freqs = dtt_deltal_correction[:, 0]
    dtt_deltal_correction_tf = np.array([10**(m/20)*np.exp(
        complex(0,p*np.pi/180.)) for m,p in dtt_deltal_correction[:, 1:]])

    # deltaL ext pcal corRrection
    calcs_model = calcs.CALCSModel(args.modelfile)
    deltal_over_pcal_correction = calcs_model.deltal_ext_pcal_corrrection(
        dtt_deltal_freqs)

    pcal_dewhiten_filt = signal.freqresp(
        signal.ZerosPolesGain([], -2.0*np.pi*np.asarray(
            pcal_model.pcal_dewhiten), 4*np.pi**2),
        2.*np.pi*dtt_deltal_freqs)[1]

    dtt_deltal_over_pcal_correction = dtt_deltal_correction_tf / \
                                      pcal_dewhiten_filt

    pdfpages = PdfPages(args.outfile) if not args.interactive else None
    template_vs_pydarm(dtt_deltal_freqs, deltal_over_pcal_correction,
                       dtt_deltal_over_pcal_correction, pdfpages)

    calcs_dtt_calibration = calcs_model.calcs_dtt_calibration(dtt_deltal_freqs)
    calcs_calib_vs_dtt_deltal_ext(dtt_deltal_freqs, calcs_dtt_calibration,
                                  dtt_deltal_correction_tf, pdfpages)
    pcal_corr = pcal_model.compute_pcal_correction(dtt_deltal_freqs,
                                                   endstation=True)
    pcal_corr_vs_zpk(dtt_deltal_freqs, pcal_corr, pcal_dewhiten_filt, pdfpages)
    print('done')
    if not args.interactive: 
        pdfpages.close()
        print(f"Results stored at {args.outfile}.")
    else: plt.show()
   





