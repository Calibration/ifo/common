#!/usr/bin/env python

from scipy.io import loadmat
import foton
sos1 = loadmat('sos1.mat')['sos1']
sos2 = loadmat('sos2.mat')['sos2']

ff = foton.FilterFile('./L1CALCS.txt')
ff['CS_DARM_SUPER_NYQUIST_COMP'][0].set_sos(sos1,format='py')
ff['CS_DARM_SUPER_NYQUIST_COMP'][1].set_sos(sos2,format='py')
ff.write()
