import pydarm


if __name__ == "__main__":
    from datetime import datetime
    import os
    modelparamfile = "/ligo/gitcommon/Calibration/ifo/pydarmparams/pydarm_modelparams_PostO3_H1_20221118.ini"
    output_dir = "/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/H1/Results/CALCS_FE"
    modeldate = os.path.splitext(modelparamfile)[0].split('_')[-1]
    today = datetime.now().strftime("%Y%m%d")

    darm = pydarm.darm.DARMModel(modelparamfile)

    f_pcal1 = 17.1 #   
    f_uim = 15.6
    f_pum = 16.4
    f_tst = 17.6
    #f_pcal2 = 410.3
    f_pcal2 = 450.03 #450.13 for pcalx
    f_pcal3 = 1083.7
    arm = 'x' # darm actuator

     # since the PCAL CMP EPICS channel have not yet been implemented, use 
     # dummy values for f_pcalx_cmp and f_pcaly_cmp
    epics_records = darm.compute_epics_records(f_pcal1, f_uim, f_pum, f_tst,
                                               f_pcal2, f_pcal3, 1.0, 1.0,
                                               endstation=True)
    print(epics_records)

    fout = f'{output_dir}/epicsrecords_model_H1_{modeldate}_created-{today}.txt'
    darm.write_epics_records('H1', epics_records,
         fout,
         push_to_epics=True)
    print(f"Results stored at {fout}.")
