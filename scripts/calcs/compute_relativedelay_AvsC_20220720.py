from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner
import sys
import os

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })
                    
                    
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/gitcommon/calibration/ifo/'

printFigs = True

run = 'O3'
IFO = 'H1'
todayDate =  dt.today().strftime('%Y%m%d')
pydarmVersion = pydarm.__version__

sys.path.insert(0, cal_data_root+'/Common/pyDARM')
sys.path.insert(1, cal_data_root+'/Runs/O3/H1/params')

from src.computeDARM import computeDARM
from src.sensing import unc_from_coh
from modelparams_H1_20210423 import modelPars as modelPars

pars = [modelPars()]
oldModelDate = '20210423'

model_parameters_file = cal_ifo_root+'pydarmparams/pydarm_modelparams_PostO3_H1_20220614.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

resultsDir = \
    cal_data_root+'/Runs/O3/'+IFO+'/Results/CALCS_FE/'
    
figTag ='relativedelayAvsC_{}_proc{}_models{}vs{}_{}'\
    .format(\
        IFO,\
        todayDate,\
        modelDate,\
        oldModelDate,\
        'newvsoldpydarm')

freq = np.logspace(1,4,1000)

# Build the new pyDARM model:
calcsmodel_obj = pydarm.calcs.CALCSModel(model_parameters_file)
darmmodel_obj = pydarm.darm.DARMModel(model_parameters_file)

#DEBUGGING
# Found that we needed to erase information about y_arm in actuation_y_arm. See Issue 120. Ended up temporarily erasing 20220614.ini parameter file's information about the actuation_y_arm.
# Found that hasattr(calcsmodel_obj.actuation,'yarm') is errantly true
# Found that getattr(getattr(calcsmodel_obj.actuation,'xarm')) is actually None
# Found that print(calcsmodel_obj.compute_actuation_single_stage(freq,arm='x',stage='UIM')) are all really really small, and indentified that my foton exports, Runs/O3/H1/Measurements/Foton/2022-06-08_H1CALCS_ActuationFunction_Foton_L1_tf.txt, probably aren't exporting everything that we agreed upon. To fix tomorrow.
# Done Created Runs/O3/H1/Measurements/Foton/2022-07-20_H1CALCS_ActuationFunction_Foton_??_tf.txt....
# Seems to work, but still get the divide by zero problem
## END DEBUGGING

# Find the DARM OLG's unity gain frequency
darm_olg_tf = darmmodel_obj.compute_darm_olg(freq)

darm_olg_ugf_ind = np.argmax(abs(darm_olg_tf) < 1.0)
darm_olg_ugf = freq[darm_olg_ugf_ind]
darm_olg_mag_at_ugf = abs(darm_olg_tf[darm_olg_ugf_ind])
darm_olg_pha_at_ugf = np.angle(darm_olg_tf[darm_olg_ugf_ind],deg=True)

# Grab the full "correction to C_CALCS" transfer function, delta C,
# and its inverse
deltaC_tf = calcsmodel_obj.C_corr(freq)
invdeltaC_tf = 1.0/deltaC_tf

# Approximate \delta C as a delay at the DARM UGF in terms of phase
deltaC_tf_at_darmugf_deg = np.angle(deltaC_tf[darm_olg_ugf_ind],deg=True)
invdeltaC_tf_at_darmugf_deg = np.angle(invdeltaC_tf[darm_olg_ugf_ind],deg=True)

# Create A_pydarm from actuation.py
A_U = darmmodel_obj.actuation.xarm.compute_actuation_single_stage(freq,stage='UIM')
A_P = darmmodel_obj.actuation.xarm.compute_actuation_single_stage(freq,stage='PUM')
A_T = darmmodel_obj.actuation.xarm.compute_actuation_single_stage(freq,stage='TST')
A = darmmodel_obj.actuation.compute_actuation(freq)

# Create A_CALCS from calcs.py
A_CALCS_U = calcsmodel_obj.compute_actuation_single_stage(freq,arm='x',stage='UIM')
A_CALCS_P = calcsmodel_obj.compute_actuation_single_stage(freq,arm='x',stage='PUM')
A_CALCS_T = calcsmodel_obj.compute_actuation_single_stage(freq,arm='x',stage='TST')
A_CALCS = calcsmodel_obj.calcs_darm_actuation(freq)

# Grab "correction to A_CALCS" transfer functions
# Grab each individual stage's correction transfer function for comparison
deltaA_U_tf = calcsmodel_obj.gds_actuation_correction(freq,'UIM')
deltaA_P_tf = calcsmodel_obj.gds_actuation_correction(freq,'PUM')
deltaA_T_tf = calcsmodel_obj.gds_actuation_correction(freq,'TST')
# Grab the total actuator's correction transfer function
deltaA_tf = calcsmodel_obj.A_corr(freq)
invdeltaA_tf = 1.0/deltaA_tf

# Approximate \delta A as a delay at the DARM UGF in terms of phase
deltaA_tf_at_darmugf_deg = np.angle(deltaA_tf[darm_olg_ugf_ind],deg=True)
invdeltaA_tf_at_darmugf_deg = np.angle(invdeltaA_tf[darm_olg_ugf_ind],deg=True)

# Convert all phase delays to time delays
deltaC_tf_at_darmugf_sec = (np.pi/180.0)*deltaC_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
invdeltaC_tf_at_darmugf_sec = (np.pi/180.0)*invdeltaC_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
deltaA_tf_at_darmugf_sec = (np.pi/180.0)*deltaA_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)
invdeltaA_tf_at_darmugf_sec = (np.pi/180.0)*invdeltaA_tf_at_darmugf_deg / (2.0*np.pi*darm_olg_ugf)

# Convert all time delays to 16 kHz clock cycle delays
deltaC_tf_at_darmugf_cc = deltaC_tf_at_darmugf_sec * 16384.0
invdeltaC_tf_tf_at_darmugf_cc = invdeltaC_tf_at_darmugf_sec * 16384.0
deltaA_tf_at_darmugf_cc = deltaA_tf_at_darmugf_sec * 16384.0
invdeltaA_tf_at_darmugf_cc = invdeltaA_tf_at_darmugf_sec * 16384.0

reldel_sub_tf_deg = np.angle(deltaC_tf,deg=True) - np.angle(deltaA_tf,deg=True)
reldel_add_tf_deg = np.angle(deltaC_tf,deg=True) + np.angle(deltaC_tf,deg=True)
reldel_sub_at_darm_ugf_deg = reldel_sub_tf_deg[darm_olg_ugf_ind]
reldel_add_at_darm_ugf_deg = reldel_add_tf_deg[darm_olg_ugf_ind]

reldel_sub_at_darm_ugf_sec = (np.pi/180.0)*reldel_sub_at_darm_ugf_deg / (2.0*np.pi*darm_olg_ugf)
reldel_sub_at_darm_ugf_cc = reldel_sub_at_darm_ugf_sec * 16384.0

reldel_add_at_darm_ugf_sec = (np.pi/180.0)*reldel_add_at_darm_ugf_deg / (2.0*np.pi*darm_olg_ugf)
reldel_add_at_darm_ugf_cc = reldel_add_at_darm_ugf_sec * 16384.0

# Build the old pyDARM model:
[D, C, A, G, CLG, R, sensProd, actProd] = computeDARM(pars[0], freq)

G_ugf_ind = np.argmax(abs(G) < 1.0)
G_ugf = freq[G_ugf_ind]
G_mag_at_ugf = abs(G[G_ugf_ind])
G_pha_at_ugf = np.angle(G[G_ugf_ind],deg=True)

# As appears extracted from
# cal_data_root+'Runs/O3/H1/Scripts/CALCS_FE/compute_relativedelay_AvsC_20210417.py'
# I get why invCres is used, but I don't understand why invAres is used.
# BLORP. See FIXME (12) starting on line 236. sensProd.calcsResid2gdsInvSensOut
# is actually C. Old pyDARM code compute_relativedelay_AvsC_20210417.py actually computes
#     C - 1/A not 1/C - 1/A. GAAAH. FIXED HERE.
old_Cres_tf = sensProd.calcsResid2gdsInvSensOut # Line 49
old_invCres_tf= 1.0/old_Cres_tf

old_invAres_tf = 1.0/actProd.calcs2gdsResidActFiltOut_NoDAQDownSample # Line 49
old_Ares_tf = 1.0/old_invAres_tf

old_Cres_tf_at_darmugf_deg = np.angle(old_Cres_tf[G_ugf_ind],deg=True)
old_invCres_tf_at_darmugf_deg = np.angle(old_invCres_tf[G_ugf_ind],deg=True)
old_Ares_tf_at_darmugf_deg = np.angle(old_Ares_tf[G_ugf_ind],deg=True)
old_invAres_tf_at_darmugf_deg = np.angle(old_invAres_tf[G_ugf_ind],deg=True)

old_Cres_tf_at_darmugf_sec = (np.pi/180.0)*old_Cres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_invCres_tf_at_darmugf_sec = (np.pi/180.0)*old_invCres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_Ares_tf_at_darmugf_sec = (np.pi/180.0)*old_Ares_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)
old_invAres_tf_at_darmugf_sec = (np.pi/180.0)*old_invAres_tf_at_darmugf_deg / (2.0*np.pi*G_ugf)

old_Cres_tf_at_darmugf_cc = old_Cres_tf_at_darmugf_sec * 16384.0
old_invCres_tf_at_darmugf_cc = old_invCres_tf_at_darmugf_sec * 16384.0
old_Ares_tf_at_darmugf_cc = old_Ares_tf_at_darmugf_sec * 16384.0
old_invAres_tf_at_darmugf_cc = old_invAres_tf_at_darmugf_sec * 16384.0

old_reldel_sub_tf_deg = np.angle(old_Cres_tf,deg=True) - np.angle(old_invAres_tf,deg=True)
old_reldel_add_tf_deg = np.angle(old_invCres_tf,deg=True) + np.angle(old_Ares_tf,deg=True)
old_reldel_sub_at_darm_ugf_deg = old_reldel_sub_tf_deg[G_ugf_ind]
old_reldel_add_at_darm_ugf_deg = old_reldel_add_tf_deg[G_ugf_ind]

old_reldel_sub_at_darm_ugf_sec = (np.pi/180.0)*old_reldel_sub_at_darm_ugf_deg / (2.0*np.pi*G_ugf)
old_reldel_sub_at_darm_ugf_cc = old_reldel_sub_at_darm_ugf_sec * 16384.0

old_reldel_add_at_darm_ugf_sec = (np.pi/180.0)*old_reldel_add_at_darm_ugf_deg / (2.0*np.pi*G_ugf)
old_reldel_add_at_darm_ugf_cc = old_reldel_add_at_darm_ugf_sec * 16384.0

print('The estimated delay in A is:')
print('   {:.4g} [usec]'.format(deltaA_tf_at_darmugf_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(deltaA_tf_at_darmugf_sec,deltaA_tf_at_darmugf_cc))
print(' ')
print('The estimated delay in C is:')
print('   {:.4g} [usec]'.format(deltaC_tf_at_darmugf_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(deltaC_tf_at_darmugf_sec,deltaC_tf_at_darmugf_cc*16384.0))
print(' ')
print('The relative delay between A and C is:')
print('   {:.4g} [usec]'.format(reldel_add_at_darm_ugf_sec*1.0e6))
print('   {:.4g} [sec] * 16384 [Hz] = {:.4f}'.format(reldel_add_at_darm_ugf_sec,reldel_add_at_darm_ugf_cc*16384.0))
print(' ')
print('The DARM UGF is at index {}, which is {:.4f} [Hz], with magnitude {:.4f} [ ] and phase {:.4f} [deg]'.format(darm_olg_ugf_ind,darm_olg_ugf,darm_olg_mag_at_ugf, darm_olg_pha_at_ugf))

#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []

# First just look at C_corr and the full A_corr
plotFreqRange = [1e1, 1e3]
ugfmarkerstyle = '.'

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.loglog(freq,abs(deltaC_tf),label='CALCSModel.C_corr ({}): New pyDARM ver{}'.format(modelDate,pydarmVersion))
s1.loglog(freq,abs(old_Cres_tf),label='C_res ({}): Old pyDARM, old HF poles'.format(oldModelDate))
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim([1e1,1e4])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend()
s1.set_ylabel('Magnitude [dimless]')
s1.set_title('Sensing Residual, C_res')

s2.semilogx(freq,np.angle(deltaC_tf,deg=True),label='CALCSModel.C_corr ({})'.format(modelDate))
s2.semilogx(freq,np.angle(old_Cres_tf,deg=True),label='C_res ({})'.format(oldModelDate))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim([1e1,1e4])
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-180,180)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.legend()
s2.set_title('{} DARM UGF: {:.4f} [Hz]'.format(modelDate,darm_olg_ugf))

s3.loglog(freq,abs(deltaA_tf),label='CALCSModel.A_corr ({}): New pyDARM ver{}'.format(modelDate,pydarmVersion))
s3.loglog(freq,abs(old_Ares_tf),label=r'A_res ({}): Old pyDARM, (same HF response as {})'.format(oldModelDate,modelDate))
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim([1e1,1e4])
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.yaxis.set_major_locator(tck.LogLocator(base=10))
s3.legend()
s3.set_ylabel('Magnitude [dimless?]')
s3.set_title('Actuation Residual, A_res')

s4.semilogx(freq,np.angle(deltaA_tf,deg=True),label='CALCSModel.A_corr ({})'.format(modelDate))
s4.semilogx(freq,np.angle(old_Ares_tf,deg=True),label='A_res ({})'.format(oldModelDate))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-180,180)
s4.yaxis.set_major_locator(tck.MultipleLocator(15))
s4.yaxis.set_minor_locator(tck.MultipleLocator(5))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')
s4.legend()
s4.set_title('{} DARM UGF: {:.4f} [Hz]'.format(oldModelDate,G_ugf))

thisfig = fignames(resultsDir+figTag+'_CresAres_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')

# First just look at C_corr and the full A_corr
# Same figure, just zoomed around the UGF.
fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.semilogx(freq,abs(deltaC_tf),label='CALCSModel.C_corr ({}): New pyDARM ver{}'.format(modelDate,pydarmVersion))
s1.semilogx(freq,abs(old_Cres_tf),label='C_res ({}): Old pyDARM, old HF poles'.format(oldModelDate))
s1.semilogx(darm_olg_ugf,abs(deltaC_tf[darm_olg_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} C_corr @ DARM UGF: {:.4g}'.format(modelDate,abs(deltaC_tf[darm_olg_ugf_ind])))
s1.semilogx(G_ugf,abs(old_Cres_tf[G_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} C_res @ DARM UGF: {:.4g}'.format(oldModelDate,abs(old_Cres_tf[G_ugf_ind])))
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim([0.92,1.08])
s1.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s1.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s1.legend()
s1.set_ylabel('Magnitude [dimless]')
s1.set_title('Sensing Residual, C_res')

s2.semilogx(freq,np.angle(deltaC_tf,deg=True),label='CALCSModel.C_corr ({})'.format(modelDate))
s2.semilogx(freq,np.angle(old_Cres_tf,deg=True),label='C_res ({})'.format(oldModelDate))
s2.semilogx(darm_olg_ugf,np.angle(deltaC_tf[darm_olg_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} C_corr @ DARM UGF: {:.4g} [deg]'.format(modelDate,np.angle(deltaC_tf[darm_olg_ugf_ind],deg=True)))
s2.semilogx(G_ugf,np.angle(old_Cres_tf[G_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} C_res @ DARM UGF: {:.4g} [deg]'.format(oldModelDate,np.angle(old_Cres_tf[G_ugf_ind],deg=True)))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-120,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.legend()
s2.set_title('{} DARM UGF: {:.4f} [Hz]'.format(modelDate,darm_olg_ugf))

s3.semilogx(freq,abs(deltaA_tf),label='CALCSModel.A_corr ({}): New pyDARM ver{}'.format(modelDate,pydarmVersion))
s3.semilogx(freq,abs(old_Ares_tf),label=r'A_res ({}): Old pyDARM, (same HF response as {})'.format(oldModelDate,modelDate))
s3.semilogx(darm_olg_ugf,abs(deltaA_tf[darm_olg_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} A_corr @ DARM UGF: {:.4g}'.format(modelDate,abs(deltaA_tf[darm_olg_ugf_ind])))
s3.semilogx(G_ugf,abs(old_Ares_tf[G_ugf_ind]),\
    marker=ugfmarkerstyle,\
    label='{} A_res @ DARM UGF: {:.4g}'.format(oldModelDate,abs(old_Ares_tf[G_ugf_ind])))
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless]')
s3.set_title('Actuation Residual, A_res')

s4.semilogx(freq,np.angle(deltaA_tf,deg=True),label='CALCSModel.A_corr ({})'.format(modelDate))
s4.semilogx(freq,np.angle(old_Ares_tf,deg=True),label='A_res ({})'.format(oldModelDate))
s4.semilogx(darm_olg_ugf,np.angle(deltaA_tf[darm_olg_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} A_res @ DARM UGF: {:.4g} [deg]'.format(modelDate,np.angle(deltaA_tf[darm_olg_ugf_ind],deg=True)))
s4.semilogx(G_ugf,np.angle(old_Ares_tf[G_ugf_ind],deg=True),\
    marker=ugfmarkerstyle,label='{} A_res @ DARM UGF: {:.4g} [deg]'.format(oldModelDate,np.angle(old_Ares_tf[G_ugf_ind],deg=True)))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-120,45)
s4.yaxis.set_major_locator(tck.MultipleLocator(15))
s4.yaxis.set_minor_locator(tck.MultipleLocator(5))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')
s4.legend()
s4.set_title('{} DARM UGF: {:.4f} [Hz]'.format(oldModelDate,G_ugf))

thisfig = fignames(resultsDir+figTag+'_CresAres_comp_zoomaroundDARMUGF.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')

### A_corr looks fishy.
# Look at the individal components.
# TST first.

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.loglog(freq,abs(A_T),label='actuation.py A_T')
s1.loglog(freq,abs(A_CALCS_T),label='calcs.py A_CALCS_T')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_xlim([1e1,1e4])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend()
s1.set_ylabel('Magnitude [m/ct]')
s1.set_title('Actuation vs. CALCS Actuation, TST')

s2.semilogx(freq,np.angle(A_T,deg=True))
s2.semilogx(freq,np.angle(A_CALCS_T,deg=True))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-185,185)
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')

s3.semilogx(freq,abs(A_T/A_CALCS_T),label='A_T / A_CALCS_T')
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless]')
s3.set_title('Actuation Residual')

s4.semilogx(freq,np.angle(A_T/A_CALCS_T,deg=True))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-185,185)
s4.yaxis.set_major_locator(tck.MultipleLocator(45))
s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')

thisfig = fignames(resultsDir+figTag+'_AvsA_CALCS_TST_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')

# PUM

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.loglog(freq,abs(A_P),label='actuation.py A_P')
s1.loglog(freq,abs(A_CALCS_P),label='calcs.py A_CALCS_P')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_xlim([1e1,1e4])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend()
s1.set_ylabel('Magnitude [m/ct]')
s1.set_title('Actuation vs. CALCS Actuation, PUM')

s2.semilogx(freq,np.angle(A_P,deg=True))
s2.semilogx(freq,np.angle(A_CALCS_P,deg=True))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-185,185)
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')

s3.semilogx(freq,abs(A_P/A_CALCS_P),label='A_P / A_CALCS_P')
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless]')
s3.set_title('Actuation Residual')

s4.semilogx(freq,np.angle(A_P/A_CALCS_P,deg=True))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-185,185)
s4.yaxis.set_major_locator(tck.MultipleLocator(45))
s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')

thisfig = fignames(resultsDir+figTag+'_AvsA_CALCS_PUM_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')
    
# UIM

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.loglog(freq,abs(A_P),label='actuation.py A_U')
s1.loglog(freq,abs(A_CALCS_P),label='calcs.py A_CALCS_U')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_xlim([1e1,1e4])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend()
s1.set_ylabel('Magnitude [m/ct]')
s1.set_title('Actuation vs. CALCS Actuation, UIM')

s2.semilogx(freq,np.angle(A_U,deg=True))
s2.semilogx(freq,np.angle(A_CALCS_U,deg=True))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-185,185)
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')

s3.semilogx(freq,abs(A_U/A_CALCS_U),label='A_U / A_CALCS_U')
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless]')
s3.set_title('Actuation Residual')

s4.semilogx(freq,np.angle(A_U/A_CALCS_U,deg=True))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-185,185)
s4.yaxis.set_major_locator(tck.MultipleLocator(45))
s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')

thisfig = fignames(resultsDir+figTag+'_AvsA_CALCS_UIM_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')


# Total A

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.loglog(freq,abs(A),label='actuation.py A')
s1.loglog(freq,abs(A_CALCS),label='calcs.py A_CALCS')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_xlim([1e1,1e4])
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend()
s1.set_ylabel('Magnitude [m/ct]')
s1.set_title('Actuation vs. CALCS Actuation, Total')

s2.semilogx(freq,np.angle(A,deg=True))
s2.semilogx(freq,np.angle(A_CALCS,deg=True))
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-185,185)
s2.yaxis.set_major_locator(tck.MultipleLocator(45))
s2.yaxis.set_minor_locator(tck.MultipleLocator(15))
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')

s3.semilogx(freq,abs(A/A_CALCS),label='A / A_CALCS')
s3.grid(which='major',color='black')
s3.grid(which='minor', ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.92,1.08])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.legend()
s3.set_ylabel('Magnitude [dimless]')
s3.set_title('Actuation Residual')

s4.semilogx(freq,np.angle(A/A_CALCS,deg=True))
s4.grid(which='major',color='black')
s4.grid(which='minor', ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim(-185,185)
s4.yaxis.set_major_locator(tck.MultipleLocator(45))
s4.yaxis.set_minor_locator(tck.MultipleLocator(15))
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase (deg)')

thisfig = fignames(resultsDir+figTag+'_AvsA_CALCS_Total_comp.pdf')
allfigures.append(thisfig)
if printFigs:
    plt.savefig(thisfig.filename,bbox_inches='tight')
