%%Stolen from Chris Wipf's example code
close all
clear all

%% example block diagram
%
%     +-----+    +-----+
% --->+dsys1+--->+dsys2+-------+
% in1 +-----+    +-----+       |
%     mag fit    gp delay fit  |
%                              v
%           +-----+          +-+-+
% --------->+dsys3+--------->+sum+--->
% in2       +-----+          +---+ out
%           thiran
%
% approximates (modulo thiran delay):
%
%     +---+
% --->+sys+------+
% in1 +---+      |
%     super-Nyq  |
%                v
%              +-+-+
% ------------>+sum+--->
% in2          +---+ out

%% define a super-Nyquist feature to fit
%Since we have 5 high frequency pole in sensing
%So swap them over to 5 zeros for inverse
%Still need to work out proper gain, but this is proof of principle

fs = 16384;  % sample rate
fmax = 5500;  % max freq in calibration band
ff = linspace(0, fmax, fmax + 1);  % freq vector
nff = ff/(fs/2);  % freq vector normalized to Nyquist
p = [];
z = -2*pi*[10340,10940,10790,10790,10790];
k = -1./prod(z);
sys = zpk(z,p,k);
Hsys = squeeze(freqresp(sys, ff, 'Hz'));

%% magnitude fit
% adjust order by eye until it looks nice
numorder = 8;
denorder = 8;
[n, d] = iirlpnorm(numorder, denorder, nff, [0 fmax/(fs/2)], abs(Hsys));
dsys1 = tf(n, d, 1/fs);
Hdsys1 = squeeze(freqresp(dsys1, ff, 'Hz'));

res1 = frd(Hdsys1./Hsys, ff, 'FrequencyUnit', 'Hz');
% flip signs if needed
if abs(angle(Hdsys1(1)/Hsys(1))) > pi/2
    dsys1 = -dsys1;
    Hdsys1 = -Hdsys1;
    res1 = -res1;
end

figure(1);
opt = bodeoptions('cstprefs');
opt.MagUnits = 'abs';
bodeplot(res1, 2*pi*ff, opt);
grid on;
title('Residual after magnitude fit');

%% group delay fit (all-pass filter)
% compute group delay in samples
gdelay1 = fs*diff(angle(squeeze(freqresp(res1, ff, 'Hz'))))'./diff(2*pi*ff);
gdelay1 = gdelay1 - min(gdelay1);  % ensure positive group delay

% adjust order by eye until it looks nice
ndorder = 10;
[n, d] = iirgrpdelay(ndorder, nff(2:end), [nff(2) fmax/(fs/2)], gdelay1);
dsys2 = tf(n, d, 1/fs);
Hdsys2 = squeeze(freqresp(dsys2, ff, 'Hz'));

res2 = frd(Hdsys1.*Hdsys2./Hsys, ff, 'FrequencyUnit', 'Hz');

figure(2);
bodeplot(res2, 2*pi*ff, opt);
grid on;
title('Residual after magnitude and group delay fits');


%% thiran compensation for delay (all-pass filter)
% compute group delay in sec at DC
gdelay2 = diff(angle(squeeze(freqresp(res2, ff, 'Hz'))))'./diff(2*pi*ff);
dcdelay = abs(gdelay2(1));

% design thiran filter to compensate delay
% (this filter would be used to apply a matching delay to
% other signals that need to be combined with this one)
dsys3 = thiran(dcdelay, 1/fs);
Hdsys3 = squeeze(freqresp(dsys3, ff, 'Hz'));

res3 = frd(Hdsys1.*Hdsys2./Hdsys3./Hsys, ff, 'FrequencyUnit', 'Hz');

figure(3);
bodeplot(res3, 2*pi*ff, opt);
grid on;
title('Residual after magnitude and group delay fits (thiran compensated)');


sos1 = tf2sos(dsys1.Numerator{1},dsys1.Denominator{1});

sos2 = tf2sos(dsys2.Numerator{1},dsys2.Denominator{1});

save sos1.mat sos1

save sos2.mat sos2

%%
data = load('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/L1/Measurements/CALCS_FE/CS_DARM_SUPER_NYQUIST_COMP_foton_export.txt');
fe = load('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/L1/Measurements/CALCS_FE/system_FM1_FM2_avg_100.txt');
fe_W = fe(:,1).*(2*pi);
W = data(:,1).*(2*pi);
data_mag = squeeze(abs(data(:,2) + 1j*data(:,3)));
data_pha = squeeze(phase(data(:,2) + 1j*data(:,3))*180/pi);
fe_mag = squeeze(abs(fe(:,2)+1j*fe(:,3)));
fe_pha = squeeze(phase(fe(:,2) + 1j*fe(:,3)))*180/pi;


[dmag,dpha] = bode(dsys1.*dsys2,W);
[mag,pha] = bode(sys,W);
[fe_dmag,fe_dpha] = bode(dsys1.*dsys2,fe_W);
[mat_mag,mat_pha] = bode(sys,fe_W);
dpha = dpha-1800;
fe_dpha = fe_dpha-1800;


set(0,'DefaultLineLineWidth',2)

figure(222)
subplot(2,2,1)

semilogx(data(:,1),data_mag)
hold on
semilogx(data(:,1),squeeze(dmag))
semilogx(data(:,1),squeeze(mag))
semilogx(fe(:,1),squeeze(fe_mag))
grid on
legend('foton','matlab discrete','continuous','fe','Location','NorthWest')
xlabel('Frequency [Hz]')
ylabel('Transfer Function [mag]')
title('Five Super-Nyquist Zeros for Inverse Sensing in FE')

subplot(2,2,3)
semilogx(data(:,1),data_pha)
hold on
semilogx(data(:,1),squeeze(dpha))
semilogx(data(:,1),squeeze(pha))
semilogx(fe(:,1),squeeze(fe_pha))
grid on
legend('foton','matlab discrete','continuous','fe','Location','SouthWest')
xlabel('Frequency [Hz]')
ylabel('Transfer Function phase [deg]')

subplot(2,2,2)
semilogx(fe(:,1),squeeze(fe_mag)./squeeze(fe_dmag))
hold on
semilogx(data(:,1),data_mag./squeeze(dmag))
grid on

legend('fe/discrete','foton/discrete')
xlabel('Frequency [Hz]')
ylabel('Ratio of TF (mag)')

subplot(2,2,4)
semilogx(fe(:,1),squeeze(fe_pha) - squeeze(fe_dpha))
hold on
semilogx(data(:,1),data_pha - squeeze(dpha))
grid on

legend('fe/discrete','foton/discrete','Location','SouthEast')
xlabel('Frequency [Hz]')
ylabel('Ratio of TF (phase - deg)')